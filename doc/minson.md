Формат MinSON
-------------

В формате MinSON любые данные рассматриваются как _объект_. Объект может
(опционально) обладать _именем_ (идентификатором) и содержит _значение_
определённого типа:

- массив (**array**),
- строка (**string**),
- целое число (**number**),
- вещественное число (**float-number**),
- булевое значение (**boolean**),
- пустота (**null**).

Массив представляет из себя _упорядоченный_ набор вложенных объектов, причём
для приложения возможен как последовательный доступ к ним, так и доступ по
ключу (идентификатору) к тем объектам, которые обладают идентификатором. Имена
объектов в пределах одного массива должны быть уникальными; если это не так,
допускается применение последнего представленного значения.

Ограничения формата:

* Максимальная вложенность массивов (**array**) определяется реализацией.
* Максимальная длина строк (**string**) и идентификаторов — 65534 байта.
* Число (**number**) должно быть представимо 64-битным знаковым целым.
* Вещественное число (**float-number**) дложно быть представимо 64-битным
  знаковым вещественным числом (двойной точности).
* Любое невалидное значение допускается преобразовывать в пустое (**null**).

Текстовое представление нечувствительно к пробельным символам, за исключением
строковых значений. Также в тексте допускаются коментарии, которые начинаются
с символа `#` и заканчиваются в конце строки.

Также в строковых значениях предусмотрено экранирование символов с помощью
символа `\` (обратный слеш). Если сразу за ним следует один из символов `t`,
`v`, `n`, `r` или `e`, то на этом месте строка должна содержать символ
табуляции, вертикальной табуляции, переноса строки, возврата каретки или
escape соответственно. Если за символом обратного слеша следует `x`, то
следующие за ним два символа должны быть шестнадцатеричным числом —
номером символа, который строка должна содержать в себе заместо этой
последовательности. Все остальные символы, следующие за обратным слешем,
должны содержаться в строке как есть.

### Синтаксис

Ниже представлен синтаксис MinSON в расширенной форме Бэкуса — Наура.

```
whitespaces = {space | tab | vertical-tab | line-feed | carriage-return | escape}.
comment = '#' {any-char-except-null} (line-feed | carriage-return | END-OF-FILE).

object = <whitespaces> [<identifier> <whitespaces>] <value> <whitespaces>.

identifier = <identifier-char> {<identifier-char>}.
identifier-char = digit | alpha | '.' | '_' | '-' | '~' | ':' | '/' | '@' | '$' | '!' | '?' | '+' | '*'.

value = <array> | <string> | <basic-value>.

array = '{' {<object>} '}'.

string = '"' {<string-char> | <escape-sequence>} '"'.
string-char is an any char except null, backslash('\') and double-quote('"').
escape-sequence = '\' any-char-except-null.

basic-value = '(' <whitespaces> (<number> | <float-number> | <boolean> | <null>) <whitespaces> ')'.

number = ['+' | '-'] (<decimal-number> | <binary-number> | <octal-number> | <hexadecimal-number>).
decimal-number = digit {digit}.
binary-number = 'b' ('0' | '1') {'0' | '1'}.
octal-number = 'o' oct-digit {oct-digit}.
hexadecimal-number = 'x' hex-digit {hex-digit}.

float-number = ['+' | '-'] <float-significand> [ <float-exponent> ].
float-significand = digit {digit} '.' | {digit} '.' digit {digit}.
float-exponent = ('e' | 'E') ['+' | '-'] digit {digit}.

boolean = ('y' | 'Y') | ('n' | 'N').
null = '*'.
```

### Пример

```minson
{
    # comment this!
    a1 {
        # named data...
        title       "\"Hello\", Bitch'es!"
        is_complete (y)
        
        # ordered data...
        (*)
        (123)
        (xFF)
        (o333)
        (b1101)
        (333.444)
        (0.5e-3)
    }
    
    a2{title"\"Hello\", Bitch'es!"is_complete(y)(*)(x7B)(xFF)(xDB)(xD)(333.444)(.5e-3)}
}
```

В данном примере представлен безымянный объект массива, в котором содержатся
два вложенных массива. Первый из них имеет имя "a1", снабжён комментариями и
содержит как именованные ("title", "is_complete") данные, так и неименованные
(**null**, число, число в шестнадцатеричной записи, в восьмеричной, в двоичной
и два вещественных числа). Второй вложенный массив имеет имя "a2" и
представляет из себя краткую запись таких же данных, что и в "a1".

/*
The zlib/libpng License

Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)

This software is provided 'as-is', without any express or implied warranty. In no event will
the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial
applications, and to alter it and redistribute it freely, subject to the following
restrictions:

    1. The origin of this software must not be misrepresented; you must not claim that
		you wrote the original software. If you use this software in a product,
		an acknowledgment in the product documentation would be appreciated but is
		not required.

    2. Altered source versions must be plainly marked as such, and must not be
		misrepresented as being the original software.

    3. This notice may not be removed or altered friosom any source distribution.
*/
#include "linux/LinuxMouse.h"
#include "linux/LinuxInputManager.h"
#include "OISException.h"
#include "OISEvents.h"

using namespace OIS;

//-------------------------------------------------------------------//
LinuxMouse::LinuxMouse(InputManager* creator, bool buffered) :
    Mouse(creator->inputSystemName(), buffered, 0, creator)
{
	display = 0;
	window  = 0;
	cursor  = 0;
    
	static_cast<LinuxInputManager*>(mCreator)->_setMouseUsed(true);
}

//-------------------------------------------------------------------//
void LinuxMouse::_initialize()
{
	// Clear old state
	mState.clear();
	mMoved = mNeedWarp = false;
    mouseFocusLost = false;
    mWarped = 0;
    
	// 5 is just some random value... hardly ever would anyone have a window smaller than 5
	oldXMouseX = oldXMouseY = 5;
	oldXMouseZ = 0;
    
	if(display != nullptr)
    {
	    XCloseDisplay(display); display = nullptr;
    }
	window = static_cast<LinuxInputManager*>(mCreator)->_getWindow();
    
	// Create our local X mListener connection
	if((display = XOpenDisplay(nullptr)) == nullptr)
		OIS_EXCEPT(E_General, "LinuxMouse::_initialize >> Error opening X!");
    
	// Set it to recieve Mouse Input events
	if(XSelectInput(display, window, ButtonPressMask | ButtonReleaseMask |
	                                 PointerMotionMask | FocusChangeMask) == BadWindow)
		OIS_EXCEPT(E_General, "LinuxMouse::_initialize >> X error!");
    
	//Create a blank cursor:
	Pixmap bm_no;
	XColor black, dummy;
	Colormap colormap;
	static char no_data[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    
	colormap = DefaultColormap(display, DefaultScreen(display));
	XAllocNamedColor(display, colormap, "black", &black, &dummy);
	bm_no = XCreateBitmapFromData(display, window, no_data, 8, 8);
	cursor = XCreatePixmapCursor(display, bm_no, bm_no, &black, &black, 0, 0);
	return;
}

//-------------------------------------------------------------------//
LinuxMouse::~LinuxMouse()
{
	if(display != nullptr)
	{
        XUngrabPointer(display, CurrentTime);
        XUndefineCursor(display, window);
		XFreeCursor(display, cursor);
		XCloseDisplay(display);
	}
    
	static_cast<LinuxInputManager*>(mCreator)->_setMouseUsed(false);
}

//-------------------------------------------------------------------//
void LinuxMouse::setBuffered(bool buffered)
{
	mBuffered = buffered;
}

//-------------------------------------------------------------------//
void LinuxMouse::setLockCursor(bool is_lock_)
{
    if(mLockCursorEnabled == is_lock_) return;
    mLockCursorEnabled = is_lock_;
    
    if(mouseFocusLost) return;
    mState.isLocked = false;
    
    if(mLockCursorEnabled)
    {
        oldXMouseX = mState.width / 2; // center x
        oldXMouseY = mState.height / 2; // center y
        XWarpPointer(display, None, window, 0, 0, 0, 0, oldXMouseX, oldXMouseY);
        XSync(display, False);
        
        ++mWarped; mNeedWarp = false;
        
        XGrabPointer(display, window, True, 0, GrabModeAsync, GrabModeAsync, window, None, CurrentTime);
        XDefineCursor(display, window, cursor);
        
        mState.isLocked = true;
    }
    else
    {
        XUngrabPointer(display, CurrentTime);
        XUndefineCursor(display, window);
    }
}

//-------------------------------------------------------------------//
void LinuxMouse::capture()
{
	// Clear out last frames values
	mState.X.rel = 0;
	mState.Y.rel = 0;
	mState.Z.rel = 0;
    
    while(XPending(display) > 0)
    {
        if(! _processXEvent()) break;
    }
    
    if(mNeedWarp)
    {
        oldXMouseX = mState.width / 2; // center x
        oldXMouseY = mState.height / 2; // center y
        XWarpPointer(display, None, window, 0, 0, 0, 0, oldXMouseX, oldXMouseY);
        XSync(display, False);
        
        ++mWarped; mNeedWarp = false;
    }
    
    if(mMoved)
	{
        if(mBuffered && mListener)
			mListener->mouseMoved(MouseEvent(this, mState));
		
		mMoved = false;
	}
}

//-------------------------------------------------------------------//
bool LinuxMouse::_processXEvent()
{
	//X11 Button Events: 1=left 2=middle 3=right; Our Bit Postion: 1=Left 2=Right 3=Middle
	//X11 Button Events: 8=backward 9=forward; Our Bit Position: 4=backward 5=forward
	static char const mask[10] = { 0, 1, 4, 2, 0, 0, 0, 0, 8, 10 };
    
    // Poll X11 for mouse event
	XEvent event;
    XNextEvent(display, &event);
    
    switch(event.type)
    {
    case MotionNotify:
    {
        // Ignore out of bounds mouse if we just warped
        if(mWarped > 0)
        {
            --mWarped; break;
        }
        
        // Compute this frames Relative X & Y motion
        int dx = event.xmotion.x - oldXMouseX;
        int dy = event.xmotion.y - oldXMouseY;
        
        // Store old values for next time to compute relative motion
        oldXMouseX = event.xmotion.x;
        oldXMouseY = event.xmotion.y;
        
        mState.X.rel += dx;
        mState.Y.rel += dy;
        
        if(mLockCursorEnabled && ! mouseFocusLost)
        {
            mState.X.abs += dx;
            mState.Y.abs += dy;
            
            if(mState.X.abs < 0)
                mState.X.abs = 0;
            else if(mState.X.abs > mState.width)
                mState.X.abs = mState.width;
            
            if(mState.Y.abs < 0)
                mState.Y.abs = 0;
            else if(mState.Y.abs > mState.height)
                mState.Y.abs = mState.height;
            
            mNeedWarp = true;
        }
        else
        {
            mState.X.abs = oldXMouseX;
            mState.Y.abs = oldXMouseY;
        }
        mMoved = true;
        break;
    }
    case ButtonPress:
    {
        if(event.xbutton.button < 10 && mask[event.xbutton.button])
        {
            mState.buttons |= mask[event.xbutton.button];
            if(mBuffered && mListener)
                if(! mListener->mousePressed(MouseEvent(this, mState),
                                             (MouseButtonID)(mask[event.xbutton.button] >> 1)))
                    return false;
        }
        break;
    }
    case ButtonRelease:
    {
        if(event.xbutton.button < 10 && mask[event.xbutton.button])
        {
            mState.buttons &= ~mask[event.xbutton.button];
            if(mBuffered && mListener)
                if(! mListener->mouseReleased(MouseEvent(this, mState),
                                              (MouseButtonID)(mask[event.xbutton.button] >> 1)))
                    return false;
        }
        else if(event.xbutton.button == 4) // The Z axis gets pushed/released pair message (this is up)
        {
            mState.Z.rel += 1;
            mState.Z.abs += 1;
            mMoved = true;
        }
        else if(event.xbutton.button == 5) // The Z axis gets pushed/released pair message (this is down)
        {
            mState.Z.rel -= 1;
            mState.Z.abs -= 1;
            mMoved = true;
        }
        break;
    }
    case FocusIn:
    {
        mouseFocusLost = false;
        if(mLockCursorEnabled)
        {
            mNeedWarp = true;
            
            XGrabPointer(display, window, True, 0, GrabModeAsync, GrabModeAsync, window, None, CurrentTime);
            XDefineCursor(display, window, cursor);
            
            mState.isLocked = true;
        }
        break;
    }
    case FocusOut:
    {
        mouseFocusLost = true;
        
        mState.X.abs = oldXMouseX;
        mState.Y.abs = oldXMouseY;
        
        XUngrabPointer(display, CurrentTime);
        XUndefineCursor(display, window);
        
        mState.isLocked = false;
        break;
    }
    default:;
    } //- switch(event.type)
    return true;
} //- _processXEvent()

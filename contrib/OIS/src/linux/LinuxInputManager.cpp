/*
The zlib/libpng License

Copyright (c) 2019 ТяФ/ка (tyafffka)

Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)

This software is provided 'as-is', without any express or implied warranty. In no event will
the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial
applications, and to alter it and redistribute it freely, subject to the following
restrictions:

    1. The origin of this software must not be misrepresented; you must not claim that
		you wrote the original software. If you use this software in a product,
		an acknowledgment in the product documentation would be appreciated but is
		not required.

    2. Altered source versions must be plainly marked as such, and must not be
		misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/
#include "linux/LinuxInputManager.h"
#include "linux/LinuxKeyboard.h"
#include "linux/LinuxJoyStickEvents.h"
#include "linux/LinuxMouse.h"
#include "OISException.h"
#include <cstdlib>
#include <stdio.h>

using namespace OIS;


//--------------------------------------------------------------------------------//
LinuxInputManager::LinuxInputManager() :
    InputManager("X11InputManager")
{
	window = 0;
    
	//Default settings
	keyboardUsed = mouseUsed = false;
    
	//Setup our internal factories
	mFactories.push_back(this);
}

//--------------------------------------------------------------------------------//
LinuxInputManager::~LinuxInputManager()
{
	//Close all joysticks
	LinuxJoyStick::_clearJoys(unusedJoyStickList);
}

//--------------------------------------------------------------------------------//
void LinuxInputManager::_initialize(std::size_t winHandle)
{
	window = winHandle;
    
	//Enumerate all devices attached
	_enumerateDevices();
}

//--------------------------------------------------------------------------------//
void LinuxInputManager::_enumerateDevices()
{
	//Enumerate all attached devices
	unusedJoyStickList = LinuxJoyStick::_scanJoys();
	joySticks = (int)unusedJoyStickList.size();
}

//----------------------------------------------------------------------------//
DeviceList LinuxInputManager::freeDeviceList()
{
	DeviceList ret;
    
	if(window)
	{
		if(! keyboardUsed)
			ret.insert(std::make_pair(OISKeyboard, mInputSystemName));
        
		if(! mouseUsed)
			ret.insert(std::make_pair(OISMouse, mInputSystemName));
	}
    
	for(JoyStickInfoList::iterator i = unusedJoyStickList.begin(); i != unusedJoyStickList.end(); ++i)
		ret.insert(std::make_pair(OISJoyStick, i->vendor));
	return ret;
}

//----------------------------------------------------------------------------//
int LinuxInputManager::totalDevices(Type iType)
{
	switch(iType)
	{
		case OISKeyboard: return window ? 1 : 0;
		case OISMouse: return window ? 1 : 0;
		case OISJoyStick: return joySticks;
		default: return 0;
	}
}

//----------------------------------------------------------------------------//
int LinuxInputManager::freeDevices(Type iType)
{
	switch(iType)
	{
		case OISKeyboard: return window ? (keyboardUsed ? 0 : 1) : 0;
		case OISMouse: return window ? (mouseUsed ? 0 : 1) : 0;
		case OISJoyStick: return (int)unusedJoyStickList.size();
		default: return 0;
	}
}

//----------------------------------------------------------------------------//
bool LinuxInputManager::vendorExist(Type iType, const std::string &vendor)
{
	if((iType == OISKeyboard || iType == OISMouse) && vendor == mInputSystemName)
	{
		return(window != 0);
	}
	else if(iType == OISJoyStick)
	{
		for(JoyStickInfoList::iterator i = unusedJoyStickList.begin(); i != unusedJoyStickList.end(); ++i)
			if(i->vendor == vendor)
				return true;
	}
	return false;
}

//----------------------------------------------------------------------------//
Object *LinuxInputManager::createObject(InputManager *creator, Type iType, bool bufferMode, const std::string &vendor)
{
	Object *obj = nullptr;
    
	switch(iType)
	{
		case OISKeyboard:
		{
			if(window != 0 && ! keyboardUsed)
				obj = new LinuxKeyboard(creator, bufferMode);
			break;
		}
		case OISMouse:
		{
			if(window != 0 && ! mouseUsed)
				obj = new LinuxMouse(creator, bufferMode);
			break;
		}
		case OISJoyStick:
		{
			for(JoyStickInfoList::iterator i = unusedJoyStickList.begin(); i != unusedJoyStickList.end(); ++i)
			{
				if(vendor == "" || i->vendor == vendor)
				{
					obj = new LinuxJoyStick(creator, bufferMode, *i);
					unusedJoyStickList.erase(i);
					break;
				}
			}
			break;
		}
		default: break;
	}
    
	if(obj == nullptr)
		OIS_EXCEPT(E_InputDeviceNonExistant, "No devices match requested type.");
	return obj;
}

//----------------------------------------------------------------------------//
void LinuxInputManager::destroyObject(Object* obj)
{
	if(obj)
	{
		if(obj->type() == OISJoyStick)
		{
			unusedJoyStickList.push_back(((LinuxJoyStick*)obj)->_getJoyInfo());
		}
		delete obj;
	}
}

/*
The zlib/libpng License

Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)

This software is provided 'as-is', without any express or implied warranty. In no event will
the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial
applications, and to alter it and redistribute it freely, subject to the following
restrictions:

    1. The origin of this software must not be misrepresented; you must not claim that
		you wrote the original software. If you use this software in a product,
		an acknowledgment in the product documentation would be appreciated but is
		not required.

    2. Altered source versions must be plainly marked as such, and must not be
		misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/
#include "linux/LinuxInputManager.h"
#include "linux/LinuxKeyboard.h"
#include "OISException.h"
#include "OISEvents.h"

#include <X11/keysym.h>
#include <X11/Xutil.h>
#include <cstring>
#include <iostream>

using namespace OIS;


static std::map<::KeySym, OIS::KeyCode> __sym_to_KC{
    {XK_1, KC_1},
    {XK_2, KC_2},
    {XK_3, KC_3},
    {XK_4, KC_4},
    {XK_5, KC_5},
    {XK_6, KC_6},
    {XK_7, KC_7},
    {XK_8, KC_8},
    {XK_9, KC_9},
    {XK_0, KC_0},
    
    {XK_BackSpace, KC_BACK},
    
    {XK_minus, KC_MINUS},
    {XK_equal, KC_EQUALS},
    {XK_space, KC_SPACE},
    {XK_comma, KC_COMMA},
    {XK_period, KC_PERIOD},
    
    {XK_backslash, KC_BACKSLASH},
    {XK_slash, KC_SLASH},
    {XK_bracketleft, KC_LBRACKET},
    {XK_bracketright, KC_RBRACKET},
    
    {XK_Escape,KC_ESCAPE},
    {XK_Caps_Lock, KC_CAPITAL},
    
    {XK_Tab, KC_TAB},
    {XK_Return, KC_RETURN},
    {XK_Control_L, KC_LCONTROL},
    {XK_Control_R, KC_RCONTROL},
    
    {XK_colon, KC_COLON},
    {XK_semicolon, KC_SEMICOLON},
    {XK_apostrophe, KC_APOSTROPHE},
    {XK_grave, KC_GRAVE},
    
    {XK_b, KC_B},
    {XK_a, KC_A},
    {XK_c, KC_C},
    {XK_d, KC_D},
    {XK_e, KC_E},
    {XK_f, KC_F},
    {XK_g, KC_G},
    {XK_h, KC_H},
    {XK_i, KC_I},
    {XK_j, KC_J},
    {XK_k, KC_K},
    {XK_l, KC_L},
    {XK_m, KC_M},
    {XK_n, KC_N},
    {XK_o, KC_O},
    {XK_p, KC_P},
    {XK_q, KC_Q},
    {XK_r, KC_R},
    {XK_s, KC_S},
    {XK_t, KC_T},
    {XK_u, KC_U},
    {XK_v, KC_V},
    {XK_w, KC_W},
    {XK_x, KC_X},
    {XK_y, KC_Y},
    {XK_z, KC_Z},
    
    {XK_F1, KC_F1},
    {XK_F2, KC_F2},
    {XK_F3, KC_F3},
    {XK_F4, KC_F4},
    {XK_F5, KC_F5},
    {XK_F6, KC_F6},
    {XK_F7, KC_F7},
    {XK_F8, KC_F8},
    {XK_F9, KC_F9},
    {XK_F10, KC_F10},
    {XK_F11, KC_F11},
    {XK_F12, KC_F12},
    {XK_F13, KC_F13},
    {XK_F14, KC_F14},
    {XK_F15, KC_F15},
    
    //Keypad
    {XK_KP_0, KC_NUMPAD0},
    {XK_KP_1, KC_NUMPAD1},
    {XK_KP_2, KC_NUMPAD2},
    {XK_KP_3, KC_NUMPAD3},
    {XK_KP_4, KC_NUMPAD4},
    {XK_KP_5, KC_NUMPAD5},
    {XK_KP_6, KC_NUMPAD6},
    {XK_KP_7, KC_NUMPAD7},
    {XK_KP_8, KC_NUMPAD8},
    {XK_KP_9, KC_NUMPAD9},
    {XK_KP_Add, KC_ADD},
    {XK_KP_Subtract, KC_SUBTRACT},
    {XK_KP_Decimal, KC_DECIMAL},
    {XK_KP_Equal, KC_NUMPADEQUALS},
    {XK_KP_Divide, KC_DIVIDE},
    {XK_KP_Multiply, KC_MULTIPLY},
    {XK_KP_Enter, KC_NUMPADENTER},
    
    //Keypad with numlock off
    {XK_KP_Home, KC_NUMPAD7},
    {XK_KP_Up, KC_NUMPAD8},
    {XK_KP_Page_Up, KC_NUMPAD9},
    {XK_KP_Left, KC_NUMPAD4},
    {XK_KP_Begin, KC_NUMPAD5},
    {XK_KP_Right, KC_NUMPAD6},
    {XK_KP_End, KC_NUMPAD1},
    {XK_KP_Down, KC_NUMPAD2},
    {XK_KP_Page_Down, KC_NUMPAD3},
    {XK_KP_Insert, KC_NUMPAD0},
    {XK_KP_Delete, KC_DECIMAL},
    
    {XK_Up, KC_UP},
    {XK_Down, KC_DOWN},
    {XK_Left, KC_LEFT},
    {XK_Right, KC_RIGHT},
    
    {XK_Page_Up, KC_PGUP},
    {XK_Page_Down, KC_PGDOWN},
    {XK_Home, KC_HOME},
    {XK_End, KC_END},
    
    {XK_Num_Lock, KC_NUMLOCK},
    {XK_Print, KC_SYSRQ},
    {XK_Scroll_Lock, KC_SCROLL},
    {XK_Pause, KC_PAUSE},
    
    {XK_Shift_R, KC_RSHIFT},
    {XK_Shift_L, KC_LSHIFT},
    {XK_Alt_R, KC_RMENU},
    {XK_Alt_L, KC_LMENU},
    
    {XK_Insert, KC_INSERT},
    {XK_Delete, KC_DELETE},
    
    {XK_Super_L, KC_LWIN},
    {XK_Super_R, KC_RWIN},
    {XK_Menu, KC_APPS},
};


//-------------------------------------------------------------------//
LinuxKeyboard::LinuxKeyboard(InputManager *creator, bool buffered) :
    Keyboard(creator->inputSystemName(), buffered, 0, creator),
    xim(nullptr), ximStyle(0), xic(nullptr)
{
	display = nullptr;
	window = 0;
    
	static_cast<LinuxInputManager *>(mCreator)->_setKeyboardUsed(true);
}

//-------------------------------------------------------------------//
void LinuxKeyboard::_initialize()
{
	// Set the locale to (hopefully) the users LANG UTF-8 Env var
	if(setlocale(LC_ALL, "") == nullptr)
		OIS_WARN(E_General, "LinuxKeyboard::_initialize: Failed to set default locale.");
    
	// Clear our keyboard state buffer
	memset(&KeyBuffer, 0, 256);
	mModifiers = 0;
    
	if(display != nullptr)
    {
	    XCloseDisplay(display); display = nullptr;
    }
	window = static_cast<LinuxInputManager *>(mCreator)->_getWindow();
    
	// Create our local X connection
	if((display = XOpenDisplay(nullptr)) == nullptr)
		OIS_EXCEPT(E_General, "LinuxKeyboard::_initialize >> Error opening X!");
    
	// Fill up keyConversion
	for(auto const &pair : __sym_to_KC)
    {
        keyConversion.insert(XtoOIS_KeyMap::value_type{
            XKeysymToKeycode(display, pair.first), pair.second
        });
    }
	
	// Configure locale modifiers
	if(XSetLocaleModifiers("@im=none") == nullptr)
		OIS_WARN(E_General, "LinuxKeyboard::_initialize: Failed to configure locale modifiers.");
    
	// Open input method
	xim = XOpenIM(display, nullptr, nullptr, nullptr);
	if(xim == nullptr)
	{
		OIS_WARN(E_General, "LinuxKeyboard::_initialize: Failed to open input method.");
	}
	else
	{
		XIMStyles *ximStyles;
		char const *ret = XGetIMValues(xim, XNQueryInputStyle, &ximStyles, nullptr);
		if(ret != nullptr || ximStyles == nullptr)
		{
			OIS_WARN(E_General, "LinuxKeyboard::_initialize: Input method does not support any styles.");
		}
		else
		{
			ximStyle = 0;
			for(int i = 0; i < ximStyles->count_styles; i++)
			{
				if(ximStyles->supported_styles[i] == (XIMPreeditNothing | XIMStatusNothing))
				{
					ximStyle = ximStyles->supported_styles[i];
					break;
				}
			}
            
			if(ximStyle == 0)
				OIS_WARN(E_General, "LinuxKeyboard::_initialize: Input method does not support the requested style.");
            
			XFree(ximStyles);
		}
	}
    
	//Set it to receive Input events
	if(XSelectInput(display, window, KeyPressMask | KeyReleaseMask) == BadWindow)
		OIS_EXCEPT(E_General, "LinuxKeyboard::_initialize: X error!");
    
	if(xim != nullptr && ximStyle)
	{
		xic = XCreateIC(xim, XNInputStyle, ximStyle,
		                XNClientWindow, window, XNFocusWindow, window, nullptr);
		if(xic == nullptr)
			OIS_WARN(E_General, "LinuxKeyboard::_initialize: Failed to create input context.");
	}
}

//-------------------------------------------------------------------//
LinuxKeyboard::~LinuxKeyboard()
{
	if(display)
	{
		if(xic) XDestroyIC(xic);
		if(xim) XCloseIM(xim);
		
		XCloseDisplay(display);
	}
    
	static_cast<LinuxInputManager*>(mCreator)->_setKeyboardUsed(false);
}

//-------------------------------------------------------------------//
unsigned int UTF8ToUTF32(unsigned char* buf)
{
	unsigned char& FirstChar = buf[0];
    
	if(FirstChar < 128)
		return FirstChar;
    
	unsigned int val = 0;
	unsigned int len = 0;
    
	if((FirstChar & 0xE0) == 0xC0) //2 Chars
	{
		len = 2;
		val = FirstChar & 0x1FU;
	}
	else if((FirstChar & 0xF0) == 0xE0) //3 Chars
	{
		len = 3;
		val = FirstChar & 0x0FU;
	}
	else if((FirstChar & 0xF8) == 0xF0) //4 Chars
	{
		len = 4;
		val = FirstChar & 0x07U;
	}
	else if((FirstChar & 0xFC) == 0xF8) //5 Chars
	{
		len = 5;
		val = FirstChar & 0x03U;
	}
	else // if((FirstChar & 0xFE) == 0xFC) //6 Chars
	{
		len = 6;
		val = FirstChar & 0x01U;
	}
    
	for(int i = 1; i < len; i++)
		val = (val << 6) | (buf[i] & 0x3F);
	return val;
}

//-------------------------------------------------------------------//
bool LinuxKeyboard::isKeyDown(KeyCode key) const
{
	return KeyBuffer[key];
}

//-------------------------------------------------------------------//
void LinuxKeyboard::capture()
{
	XEvent event;
	while(XPending(display) > 0)
	{
		XNextEvent(display, &event);
		if(KeyPress == event.type)
		{
			_handleKeyPress(event);
		}
		else if(KeyRelease == event.type)
		{
			_handleKeyRelease(event);
		}
	}
}

//-------------------------------------------------------------------//
void LinuxKeyboard::_handleKeyPress(XEvent &event)
{
	XKeyEvent &e = (XKeyEvent &)event;
	static std::vector<char> buf(32);
	
	unsigned int character = 0;
	int bytes = 0;
    bool haveChar =(! XFilterEvent(&event, None));
    
    if(xic)
	{
		Status status;
		for(;;)
		{
			bytes = Xutf8LookupString(xic, &e, &buf[0], (int)(buf.size() - 1), nullptr, &status);
			buf[bytes] = '\0';
            
			if(status != XBufferOverflow) break;
			buf.resize(buf.size() * 2);
		}
	}
	else
	{
		bytes = XLookupString(&e, &buf[0], (int)(buf.size() - 1), nullptr, nullptr);
		buf[bytes] = '\0';
	}
    
	if(haveChar && bytes > 0)
	{
		if(mTextMode == Unicode)
			character = UTF8ToUTF32(reinterpret_cast<unsigned char *>(&buf[0]));
		else if(mTextMode == Ascii)
			character = *reinterpret_cast<unsigned char *>(&buf[0]);
	}
    
    KeyCode kc = _XKeyToOISKeyCode((::KeyCode)e.keycode);
	_injectKeyDown(kc, character);
}

void LinuxKeyboard::_handleKeyRelease(XEvent& event)
{
	XKeyEvent& e = (XKeyEvent&)event;
	KeySym keySym;
    
	XFilterEvent(&event, None);
	if(!_isKeyRepeat(event))
	{
		XLookupString(&e, NULL, 0, &keySym, NULL);
        
		KeyCode kc = _XKeyToOISKeyCode((::KeyCode)e.keycode);
		_injectKeyUp(kc);
	}
}

//-------------------------------------------------------------------//
OIS::KeyCode LinuxKeyboard::_XKeyToOISKeyCode(::KeyCode key)
{
    XtoOIS_KeyMap::iterator found = keyConversion.find(key);
    if(found != keyConversion.end()) return found->second;
    return KC_UNASSIGNED;
}

::KeySym LinuxKeyboard::_OISKeyCodeToKeySym(KeyCode kc)
{
    if(kc != KC_UNASSIGNED)
    {
        for(auto const &pair : __sym_to_KC)
        {
            if(pair.second == kc) return pair.first;
        }
    }
    return NoSymbol;
}

//-------------------------------------------------------------------//
void LinuxKeyboard::setBuffered(bool buffered)
{
	mBuffered = buffered;
}

//-------------------------------------------------------------------//
bool LinuxKeyboard::_injectKeyDown(KeyCode kc, unsigned int text)
{
	if(kc > 255) kc = KC_UNASSIGNED;
	KeyBuffer[kc] = 1;
    
	//Turn on modifier flags
	if(kc == KC_LCONTROL || kc == KC_RCONTROL)
		mModifiers |= Ctrl;
	else if(kc == KC_LSHIFT || kc == KC_RSHIFT)
		mModifiers |= Shift;
	else if(kc == KC_LMENU || kc == KC_RMENU)
		mModifiers |= Alt;
    
	if(mBuffered && mListener)
		return mListener->keyPressed(KeyEvent(this, kc, text));
	return true;
}

//-------------------------------------------------------------------//
bool LinuxKeyboard::_injectKeyUp(KeyCode kc)
{
	if(kc > 255) kc = KC_UNASSIGNED;
	KeyBuffer[kc] = 0;
    
	// Turn off modifier flags
	if(kc == KC_LCONTROL || kc == KC_RCONTROL)
		mModifiers &= ~Ctrl;
	else if(kc == KC_LSHIFT || kc == KC_RSHIFT)
		mModifiers &= ~Shift;
	else if(kc == KC_LMENU || kc == KC_RMENU)
		mModifiers &= ~Alt;
    
	if(mBuffered && mListener)
		return mListener->keyReleased(KeyEvent(this, kc, 0));
    
	return true;
}

//-------------------------------------------------------------------//
const std::string& LinuxKeyboard::getAsString(OIS::KeyCode kc)
{
	mGetString = "Unknown";
	KeySym sym = _OISKeyCodeToKeySym(kc);
	
	if(sym != NoSymbol)
	{
		char const *temp = XKeysymToString(sym);
		if(temp) mGetString = temp;
	}
	return mGetString;
}

//-------------------------------------------------------------------//
OIS::KeyCode LinuxKeyboard::getAsKeyCode(std::string str)
{
    auto found = __sym_to_KC.find(XStringToKeysym(str.c_str()));
    if(found != __sym_to_KC.end()) return found->second;
    return KC_UNASSIGNED;
}

//-------------------------------------------------------------------//
void LinuxKeyboard::copyKeyStates(char keys[256]) const
{
	memcpy(keys, KeyBuffer, 256);
}

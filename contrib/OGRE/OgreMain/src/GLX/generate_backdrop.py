#!/usr/bin/env python3
# coding: utf-8

import os
import sys

if len(sys.argv) < 2:
    print("Usage: %s <png-file-in>" % (sys.argv[0],))
    sys.exit(3)


CHUNK_SIZE = 8 * 1024
drop_size = os.stat(sys.argv[1]).st_size

out_file = os.path.join(os.path.dirname(__file__), "GLX_backdrop.h")


with open(sys.argv[1], 'rb') as f, open(out_file, 'wt') as fw:
    fw.write("const unsigned char GLX_backdrop_data[%i]={\n" % (drop_size,))
    
    while True:
        chunk = f.read(CHUNK_SIZE)
        if not chunk: break
        
        fw.write("".join(str(x) + "," for x in chunk) + "\n")
        
        if len(chunk) < CHUNK_SIZE: break
    
    fw.write("\n};\n")
    fw.flush()

#include <stdio.h>

#include "GLX_backdrop.h"

#define CHUNK_SIZE 8 * 1024


int main(int argc, char **argv)
{
    if(argc < 2)
    {
        printf("Usage: %s <png-file-out>", argv[0]);
        return 3;
    }
    
    FILE *fw = fopen(argv[1], "wb");
    if(fw == NULL) return 4;
    
    size_t dropsize = sizeof(GLX_backdrop_data);
    for(size_t i = 0; i < dropsize; i += CHUNK_SIZE)
    {
        size_t to_write = dropsize - i;
        if(to_write > CHUNK_SIZE) to_write = CHUNK_SIZE;
        
        fwrite(GLX_backdrop_data + i, 1, to_write, fw);
    }
    
    fclose(fw);
    return 0;
}

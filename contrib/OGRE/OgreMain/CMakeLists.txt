#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

############################################################
# OgreMain core library
############################################################

PROJECT(OgreMain VERSION ${OGRE_VERSION})

include(PrecompiledHeader)

# define header and source files for the library
file(GLOB HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
list(APPEND HEADER_FILES
    ${OGRE_BINARY_DIR}/include/OgreBuildSettings.h
    ${CMAKE_BINARY_DIR}/include/OgreExports.h
    src/OgreImageResampler.h
    src/OgrePixelConversions.h
    src/OgreSIMDHelper.h
)

file(GLOB SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")

# silence deprecation warning for internal call
if(UNIX)
    set_source_files_properties(src/OgreResource.cpp PROPERTIES COMPILE_FLAGS "-Wno-deprecated-declarations")
endif()

# Remove optional header files
list(REMOVE_ITEM HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreFreeImageCodec.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreDDSCodec.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgrePVRTCCodec.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreETCCodec.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreZip.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreSTBICodec.h"
)

# Remove optional source files
list(REMOVE_ITEM SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreFreeImageCodec.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreConfigDialogNoOp.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreFileSystemLayerNoOp.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreDDSCodec.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePVRTCCodec.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreETCCodec.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreZip.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePOSIXTimer.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreSearchOps.cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreSTBICodec.cpp"
)

# Add threading (backport from 2.X)
list(APPEND HEADER_FILES ${CMAKE_CURRENT_SOURCE_DIR}/include/Threading/OgreThreads.h
    ${CMAKE_CURRENT_SOURCE_DIR}/include/Threading/OgreBarrier.h
	${CMAKE_CURRENT_SOURCE_DIR}/include/Threading/OgreLightweightMutex.h
)
	
if(WIN32)
	list(APPEND SOURCE_FILES ${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreBarrierWin.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreThreadsWin.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreLightweightMutexWin.cpp
    )
else()
	list(APPEND SOURCE_FILES ${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreBarrierPThreads.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreLightweightMutexPThreads.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/src/Threading/OgreThreadsPThreads.cpp
    )
endif()

# Add platform specific files
if(OGRE_BUILD_PLATFORM_NACL)
    include_directories("include/NaCl")
    file(GLOB PLATFORM_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/include/NaCl/*.h")
    file(GLOB PLATFORM_SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/NaCl/*.cpp"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePOSIXTimer.cpp"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreFileSystemLayerNoOp.cpp"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreConfigDialogNoOp.cpp"
    )
    
    set(PLATFORM_LIBS nosys)
    set(PLATFORM_HEADER_INSTALL "NaCl")
elseif(WIN32)
    list(REMOVE_ITEM HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/include/OgrePOSIXTimerImp.h")
    
    include_directories("include/WIN32")
    set(PLATFORM_HEADERS
        include/WIN32/OgreErrorDialogImp.h
        include/WIN32/OgreTimerImp.h
        include/WIN32/OgreMinGWSupport.h
        include/WIN32/OgreComPtr.h
    )
    
    list(REMOVE_ITEM SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreErrorDialogNoOp.cpp")
    set(PLATFORM_SOURCE_FILES
        src/WIN32/OgreOptimisedUtilDirectXMath.cpp
        src/WIN32/OgreConfigDialog.cpp
        src/WIN32/OgreErrorDialog.cpp
        src/WIN32/OgreFileSystemLayer.cpp
        src/WIN32/OgreTimer.cpp
        src/WIN32/resource.h
    )  
    set(RESOURCE_FILES
        src/WIN32/resource.h
        src/WIN32/OgreWin32Resources.rc
    )
    source_group(Resources FILES ${RESOURCE_FILES})
    set(PLATFORM_HEADER_INSTALL "WIN32")
    
    if(MINGW)
        # Older CMake versions do not know how to deal with resource files on MinGW.
        # Newer ones do, but pass along invalid command options to windres.
        # This is a manual fix for the moment.
        if(NOT CMAKE_RC_COMPILER)
            message(SEND_ERROR "Could not find MinGW resource compiler. Please specify path in CMAKE_RC_COMPILER.")
        endif()
        add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/OgreWin32Resources.rc.obj
            COMMAND ${CMAKE_RC_COMPILER}
            -I${CMAKE_CURRENT_SOURCE_DIR}/src/WIN32 
            -i${CMAKE_CURRENT_SOURCE_DIR}/src/WIN32/OgreWin32Resources.rc
            -o${CMAKE_CURRENT_BINARY_DIR}/OgreWin32Resources.rc.obj
            DEPENDS ${RESOURCE_FILES}
        )
        list(APPEND PLATFORM_SOURCE_FILES ${CMAKE_CURRENT_BINARY_DIR}/OgreWin32Resources.rc.obj)
    else()
        list(APPEND PLATFORM_SOURCE_FILES src/WIN32/OgreWin32Resources.rc)
    endif()
elseif(APPLE)
    include_directories("include/OSX")
    
    file(GLOB PLATFORM_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/include/OSX/*.h")
    
    list(REMOVE_ITEM SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreErrorDialogNoOp.cpp")
    file(GLOB PLATFORM_SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/OSX/*.cpp"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OSX/*.mm"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePOSIXTimer.cpp"
        include/OSX/ogrelogo.png
    )
    
    set(RESOURCE_FILES
	    include/OSX/ogrelogo.png
    )
    source_group(Resources FILES ${RESOURCE_FILES})
    
    set(PLATFORM_LIBS ${Carbon_LIBRARY_FWK} ${Cocoa_LIBRARY_FWK})
    set(PLATFORM_HEADER_INSTALL "OSX")
elseif(UNIX)
    include_directories(include/GLX ${X11_INCLUDE_DIR})
    
    file(GLOB PLATFORM_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/include/GLX/*.h")
    file(GLOB PLATFORM_SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/GLX/*.cpp"
        "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePOSIXTimer.cpp"
    )
    
    set(PLATFORM_LIBS ${X11_LIBRARIES} ${X11_Xt_LIB} ${XAW_LIBRARY} pthread)
    set(PLATFORM_HEADER_INSTALL "GLX")
    # some platforms require explicit linking to libdl, see if it's available
    find_library(DL_LIBRARY NAMES dl dlsym)
    mark_as_advanced(DL_LIBRARY)
    if(DL_LIBRARY)
        list(APPEND PLATFORM_LIBS dl)
    endif()
endif()

if(NOT WIN32)
    list(APPEND PLATFORM_SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/OgreSearchOps.cpp")
endif()

# Configure threading files
file(GLOB THREAD_HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/include/Threading/*.h")
include_directories("include/Threading")

if(OGRE_THREAD_PROVIDER EQUAL 0)
	list(APPEND THREAD_HEADER_FILES
		include/Threading/OgreThreadDefinesNone.h
		include/Threading/OgreDefaultWorkQueueStandard.h
	)
	set(THREAD_SOURCE_FILES
		src/Threading/OgreDefaultWorkQueueStandard.cpp
	)
elseif(OGRE_THREAD_PROVIDER EQUAL 1)
	list(APPEND THREAD_HEADER_FILES
		include/Threading/OgreThreadDefinesBoost.h
		include/Threading/OgreThreadHeadersBoost.h
		include/Threading/OgreDefaultWorkQueueStandard.h
	)
	set(THREAD_SOURCE_FILES
		src/Threading/OgreDefaultWorkQueueStandard.cpp
	)
elseif(OGRE_THREAD_PROVIDER EQUAL 2)
	list(APPEND THREAD_HEADER_FILES
		include/Threading/OgreThreadDefinesPoco.h
		include/Threading/OgreThreadHeadersPoco.h
		include/Threading/OgreDefaultWorkQueueStandard.h
	)
	set(THREAD_SOURCE_FILES
		src/Threading/OgreDefaultWorkQueueStandard.cpp
	)
elseif(OGRE_THREAD_PROVIDER EQUAL 3)
	list(APPEND THREAD_HEADER_FILES
		include/Threading/OgreThreadDefinesTBB.h
		include/Threading/OgreThreadHeadersTBB.h
		include/Threading/OgreDefaultWorkQueueTBB.h
	)
	set(THREAD_SOURCE_FILES
		src/Threading/OgreDefaultWorkQueueTBB.cpp
	)
elseif(OGRE_THREAD_PROVIDER EQUAL 4)
	list(APPEND THREAD_HEADER_FILES
		include/Threading/OgreThreadDefinesSTD.h
		include/Threading/OgreThreadHeadersSTD.h
		include/Threading/OgreDefaultWorkQueueStandard.h
	)
	list(APPEND THREAD_SOURCE_FILES
		src/Threading/OgreDefaultWorkQueueStandard.cpp
	)
endif()

list(APPEND HEADER_FILES ${THREAD_HEADER_FILES})

# Add needed definitions and nedmalloc include dir
if(NOT OGRE_CONFIG_ALLOCATOR EQUAL 1)
  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/nedmalloc)
endif()

set(LIBRARIES ${PLATFORM_LIBS})

if(OGRE_CONFIG_THREADS)
    list(APPEND LIBRARIES ${OGRE_THREAD_LIBRARIES})
endif()

if(OGRE_CONFIG_ENABLE_FREEIMAGE)
    add_definitions(-DFREEIMAGE_LIB)
    list(APPEND HEADER_FILES include/OgreFreeImageCodec.h)
    list(APPEND SOURCE_FILES src/OgreFreeImageCodec.cpp)
    list(APPEND LIBRARIES    "${FreeImage_LIBRARIES}")
endif()

if(OGRE_CONFIG_ENABLE_STBI)
    list(APPEND HEADER_FILES include/OgreSTBICodec.h)
    list(APPEND SOURCE_FILES src/OgreSTBICodec.cpp)
    
    if(UNIX)
        set_source_files_properties(src/OgreSTBICodec.cpp PROPERTIES COMPILE_FLAGS "-Wno-cast-qual -Wno-unused-function -Wno-shadow -Wno-missing-declarations")
    endif()
    
    if(CMAKE_COMPILER_IS_GNUCXX)
        # workaround gcc5 bug on Ubuntu 16.04 regarding __builtin_cpu_supports
        list(APPEND LIBRARIES gcc)
  endif()
endif ()

if(OGRE_CONFIG_ENABLE_DDS)
    list(APPEND HEADER_FILES include/OgreDDSCodec.h)
    list(APPEND SOURCE_FILES src/OgreDDSCodec.cpp)
endif()

if(OGRE_CONFIG_ENABLE_PVRTC)
    list(APPEND HEADER_FILES include/OgrePVRTCCodec.h)
    list(APPEND SOURCE_FILES src/OgrePVRTCCodec.cpp)
endif()

if(OGRE_CONFIG_ENABLE_ETC)
    list(APPEND HEADER_FILES include/OgreETCCodec.h)
    list(APPEND SOURCE_FILES src/OgreETCCodec.cpp)
endif()

if(OGRE_CONFIG_ENABLE_ASTC)
    list(APPEND HEADER_FILES include/OgreASTCCodec.h)
    list(APPEND SOURCE_FILES src/OgreASTCCodec.cpp)
endif()

if(OGRE_CONFIG_ENABLE_ZIP)
    list(APPEND HEADER_FILES include/OgreZip.h)
    list(APPEND SOURCE_FILES src/OgreZip.cpp)
    
    list(APPEND LIBRARIES "${ZZip_LIBRARIES}")
    list(APPEND LIBRARIES "${ZLIB_LIBRARIES}")
endif()

if(OGRE_CONFIG_ENABLE_GLES2_GLSL_OPTIMISER)
    list(APPEND LIBRARIES "${GLSL_Optimizer_LIBRARIES}")
endif()

if(OGRE_CONFIG_ENABLE_GLES2_CG_SUPPORT)
    list(APPEND LIBRARIES "${HLSL2GLSL_LIBRARIES}")
endif()

set(TARGET_LINK_FLAGS "")

# setup OgreMain target
# exclude OgreAlignedAllocator.cpp from unity builds; causes problems on Linux
ogre_add_library(OgreMain ${OGRE_LIB_TYPE}
    ${PREC_HEADER} ${HEADER_FILES} ${SOURCE_FILES}
    ${PLATFORM_HEADERS} ${PLATFORM_SOURCE_FILES}
    ${THREAD_HEADER_FILES} ${THREAD_SOURCE_FILES}
    SEPARATE "src/OgreAlignedAllocator.cpp"
)

generate_export_header(OgreMain 
    EXPORT_MACRO_NAME _OgreExport
    NO_EXPORT_MACRO_NAME _OgrePrivate
    DEPRECATED_MACRO_NAME OGRE_DEPRECATED
    EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/include/OgreExports.h"
)

# In visual studio 2010 - 64 bit we get this error: "LINK : fatal error LNK1210: exceeded internal ILK size limit; link with /INCREMENTAL:NO"
if(WIN32 AND MSVC10 AND CMAKE_CL_64)
    set_target_properties(OgreMain PROPERTIES 
        VERSION ${OGRE_SOVERSION}
        LINK_FLAGS "/INCREMENTAL:NO"
    )
else()
    set_target_properties(OgreMain PROPERTIES
        VERSION ${OGRE_SOVERSION} SOVERSION ${OGRE_SOVERSION}
    )
endif()

if(OGRE_GCC_VERSION VERSION_EQUAL 4.8 OR OGRE_GCC_VERSION VERSION_GREATER 4.8)
    list(APPEND LIBRARIES "-latomic")
endif()

if(APPLE)
    set_target_properties(OgreMain PROPERTIES
        LINK_FLAGS "-framework IOKit -framework Cocoa -framework Carbon -framework OpenGL -framework CoreVideo"
    )
    set(OGRE_OSX_BUILD_CONFIGURATION "$(PLATFORM_NAME)/$(CONFIGURATION)")
endif()

target_link_libraries(OgreMain ${LIBRARIES})
if(MINGW)
    # may need winsock htons functions for FreeImage
    target_link_libraries(OgreMain ws2_32)
endif()

# specify a precompiled header to use
use_precompiled_header(OgreMain 
    "${CMAKE_CURRENT_SOURCE_DIR}/include/OgreStableHeaders.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/OgrePrecompiledHeaders.cpp"
)

# install OgreMain
ogre_config_lib(OgreMain TRUE)

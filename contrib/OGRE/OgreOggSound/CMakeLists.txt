#--------------------------------------------------------------#
#                     OgreOggSound sources                     #
#--------------------------------------------------------------#

# find OpenAL, OGG, Vorbis

find_package(OpenAL QUIET REQUIRED)

if(APPLE)
    find_library(OGG Ogg REQUIRED)
    find_library(VORBIS Vorbis REQUIRED)
    
    set(OGG_LIBRARIES ${OGG})
    set(VORBISFILE_LIBRARIES ${VORBIS})
    set(VORBISFILE_INCLUDE_DIRS ${VORBIS_INCLUDE_DIRS})
elseif(UNIX)
    find_package(PkgConfig REQUIRED)
    
    pkg_check_modules(OPENAL REQUIRED openal)
    pkg_check_modules(OGG REQUIRED ogg)
    pkg_check_modules(VORBISFILE REQUIRED vorbisfile)
endif()

include_directories(
    ${OPENAL_INCLUDE_DIRS}
    ${OGG_INCLUDE_DIRS}
    ${VORBISFILE_INCLUDE_DIRS}
)

add_definitions(-DOGGSOUND_THREADED=0)
add_definitions(-DOGGSOUND_EXPORT=1)

#--------------------------------------------------------------#


add_library(OgreOggSound ${OGRE_LIB_TYPE}
    OgreOggISound.cpp
    OgreOggListener.cpp
    OgreOggSoundFactory.cpp
    OgreOggSoundManager.cpp
    OgreOggSoundPlugin.cpp
    OgreOggSoundPluginDllStart.cpp
    OgreOggSoundRecord.cpp
    OgreOggStaticSound.cpp
    OgreOggStaticWavSound.cpp
    OgreOggStreamSound.cpp
    OgreOggStreamWavSound.cpp
)

target_link_libraries(OgreOggSound
    OgreMain
    ${OPENAL_LIBRARIES}
    ${OGG_LIBRARIES}
    ${VORBISFILE_LIBRARIES}
)

set_target_properties(OgreOggSound PROPERTIES
    VERSION ${OGRE_VERSION} SOVERSION ${OGRE_VERSION}
)

ogre_config_component(OgreOggSound)

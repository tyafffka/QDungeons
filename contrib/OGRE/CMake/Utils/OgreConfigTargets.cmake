#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

# Configure settings and install targets
if(APPLE)
  macro(set_xcode_property targ xc_prop_name xc_prop_val)
    set_property( TARGET ${targ} PROPERTY XCODE_ATTRIBUTE_${xc_prop_name} ${xc_prop_val} )
  endmacro(set_xcode_property)

  if(NOT OGRE_BUILD_PLATFORM_ANDROID AND NOT APPLE_IOS)
    set(PLATFORM_NAME "macosx")
  elseif(APPLE_IOS)
    set(PLATFORM_NAME "$(PLATFORM_NAME)")
  endif()
endif()

# Default build output paths
if (NOT OGRE_ARCHIVE_OUTPUT)
  if(APPLE AND NOT OGRE_BUILD_PLATFORM_ANDROID)
    set(OGRE_ARCHIVE_OUTPUT ${OGRE_BINARY_DIR}/lib/${PLATFORM_NAME})
  else()
    set(OGRE_ARCHIVE_OUTPUT ${OGRE_BINARY_DIR}/lib)
  endif()
endif ()
if (NOT OGRE_LIBRARY_OUTPUT)
  if(APPLE AND NOT OGRE_BUILD_PLATFORM_ANDROID)
    set(OGRE_LIBRARY_OUTPUT ${OGRE_BINARY_DIR}/lib/${PLATFORM_NAME})
  else()
    set(OGRE_LIBRARY_OUTPUT ${OGRE_BINARY_DIR}/lib)
  endif()
endif ()
if (NOT OGRE_RUNTIME_OUTPUT)
  set(OGRE_RUNTIME_OUTPUT ${OGRE_BINARY_DIR}/bin)
endif ()

if(WIN32)
    set(OGRE_RELEASE_PATH "")
    set(OGRE_RELWDBG_PATH "")
    set(OGRE_MINSIZE_PATH "")
    set(OGRE_DEBUG_PATH "")
    set(OGRE_LIB_RELEASE_PATH "")
    set(OGRE_LIB_RELWDBG_PATH "")
    set(OGRE_LIB_MINSIZE_PATH "")
    set(OGRE_LIB_DEBUG_PATH "")
elseif(UNIX)
    set(OGRE_RELEASE_PATH "")
    set(OGRE_RELWDBG_PATH "")
    set(OGRE_MINSIZE_PATH "")
    set(OGRE_DEBUG_PATH "/debug")
    if(NOT APPLE)
        set(OGRE_DEBUG_PATH "")
    endif()
    set(OGRE_LIB_RELEASE_PATH "")
    set(OGRE_LIB_RELWDBG_PATH "")
    set(OGRE_LIB_MINSIZE_PATH "")
    set(OGRE_LIB_DEBUG_PATH "")
    if(APPLE)
        set(OGRE_RELEASE_PATH "/${PLATFORM_NAME}")
    endif()
endif()


# create vcproj.user file for Visual Studio to set debug working directory
function(ogre_create_vcproj_userfile TARGETNAME)
    if(MSVC)
        configure_file(
            "${OGRE_TEMPLATES_DIR}/VisualStudioUserFile.vcproj.user.in"
            "${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.vcproj.user"
            @ONLY
        )
        configure_file(
            "${OGRE_TEMPLATES_DIR}/VisualStudioUserFile.vcxproj.user.in"
            "${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.vcxproj.user"
            @ONLY
        )
    endif()
endfunction(ogre_create_vcproj_userfile)


# install targets according to current build type
function(ogre_install_target TARGETNAME SUFFIX EXPORT)
	# Skip all install targets in SDK
	if(OGRE_SDK_BUILD)
		return()
	endif()
    
	install(TARGETS ${TARGETNAME}
		BUNDLE DESTINATION "bin"
		RUNTIME DESTINATION "bin"
		LIBRARY DESTINATION "lib"
		FRAMEWORK DESTINATION "lib"
    )
endfunction(ogre_install_target)


# setup common target settings
function(ogre_config_common TARGETNAME)
    if(NOT OGRE_STATIC AND (CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang"))
        set_target_properties(${TARGETNAME} PROPERTIES XCODE_ATTRIBUTE_ONLY_ACTIVE_ARCH "NO")
        # add GCC visibility flags to shared library build
        set_target_properties(${TARGETNAME} PROPERTIES COMPILE_FLAGS "${OGRE_VISIBILITY_FLAGS}")
        set_target_properties(${TARGETNAME} PROPERTIES XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN "${XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN}")
        set_target_properties(${TARGETNAME} PROPERTIES XCODE_ATTRIBUTE_GCC_INLINES_ARE_PRIVATE_EXTERN "${XCODE_ATTRIBUTE_GCC_INLINES_ARE_PRIVATE_EXTERN}")
        #set_target_properties(${TARGETNAME} PROPERTIES XCODE_ATTRIBUTE_GCC_INLINES_ARE_PRIVATE_EXTERN[arch=x86_64] "YES")
    endif()
    
    ogre_create_vcproj_userfile(${TARGETNAME})
endfunction(ogre_config_common)


# checks whether the target LIBNAME produces a pdb file
function(ogre_produces_pdb VARNAME LIBNAME)
  get_target_property(TYPE ${LIBNAME} TYPE)
  if (TYPE STREQUAL "SHARED_LIBRARY" OR TYPE STREQUAL "MODULE_LIBRARY" OR TYPE STREQUAL "EXECUTABLE")
    set(${VARNAME} ON PARENT_SCOPE)
  else ()
    set(${VARNAME} OFF PARENT_SCOPE)
  endif ()
endfunction(ogre_produces_pdb)


# setup library build
function(ogre_config_lib LIBNAME EXPORT)
    ogre_config_common(${LIBNAME})
    
    if(NOT OGRE_STATIC)
	    if(MINGW)
	        # remove lib prefix from DLL outputs
	        set_target_properties(${LIBNAME} PROPERTIES PREFIX "")
	    endif()
    endif()
    
    ogre_install_target(${LIBNAME} "" ${EXPORT})
endfunction(ogre_config_lib)


function(ogre_config_component LIBNAME)
    ogre_config_lib(${LIBNAME} FALSE)
endfunction(ogre_config_component)


# setup plugin build
function(ogre_config_plugin PLUGINNAME)
    ogre_config_common(${PLUGINNAME})
    set_target_properties(${PLUGINNAME} PROPERTIES VERSION ${OGRE_SOVERSION})
    
    if(NOT OGRE_STATIC)
        if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
            # disable "lib" prefix on Unix
            set_target_properties(${PLUGINNAME} PROPERTIES PREFIX "")
        endif(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    endif()
    
    ogre_install_target(${PLUGINNAME} "" ${OGRE_STATIC})
endfunction(ogre_config_plugin)


# setup Ogre sample build
function(ogre_config_sample_common SAMPLENAME)
    ogre_config_common(${SAMPLENAME})
    
    if(APPLE)
        # On OS X, create .app bundle
        set_property(TARGET ${SAMPLENAME} PROPERTY MACOSX_BUNDLE TRUE)
        # Add the path where the Ogre framework was found
        if(${OGRE_FRAMEWORK_PATH})
            set_target_properties(${SAMPLENAME} PROPERTIES
                COMPILE_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
                LINK_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
            )
        endif()
    endif(APPLE)
    
    if(NOT OGRE_STATIC)
        if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
            # disable "lib" prefix on Unix
            set_target_properties(${SAMPLENAME} PROPERTIES PREFIX "")
        endif(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    endif()
    
    if(NOT WIN32)
        set_target_properties(${SAMPLENAME} PROPERTIES VERSION ${OGRE_SOVERSION} SOVERSION ${OGRE_SOVERSION})
    endif()
endfunction(ogre_config_sample_common)


function(ogre_config_sample_exe SAMPLENAME)
    ogre_config_sample_common(${SAMPLENAME})
    
    if(APPLE AND OGRE_SDK_BUILD)
        # Add the path where the Ogre framework was found
        if(NOT ${OGRE_FRAMEWORK_PATH} STREQUAL "")
            set_target_properties(${SAMPLENAME} PROPERTIES
                COMPILE_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
                LINK_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
            )
        endif()
    endif()
endfunction(ogre_config_sample_exe)


function(ogre_config_sample_lib SAMPLENAME)
    ogre_config_sample_common(${SAMPLENAME})
    
    if(APPLE AND OGRE_SDK_BUILD)
        # Add the path where the Ogre framework was found
        if(NOT ${OGRE_FRAMEWORK_PATH} STREQUAL "")
            set_target_properties(${SAMPLENAME} PROPERTIES
                COMPILE_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
                LINK_FLAGS "-F${OGRE_FRAMEWORK_PATH}"
            )
        endif()
    endif()
    
    # Add sample to the list of link targets
    # Global property so that we can build this up across entire sample tree
    # since vars are local to containing scope of directories / functions
    get_property(OGRE_SAMPLES_LIST GLOBAL PROPERTY "OGRE_SAMPLES_LIST")
    set_property(GLOBAL PROPERTY "OGRE_SAMPLES_LIST" ${OGRE_SAMPLES_LIST} ${SAMPLENAME})
endfunction(ogre_config_sample_lib)


# setup Ogre tool build
function(ogre_config_tool TOOLNAME)
    ogre_config_common(${TOOLNAME})
    ogre_install_target(${TOOLNAME} "" ${OGRE_STATIC})
endfunction(ogre_config_tool)


# Get component include dir (different when referencing SDK)
function(ogre_add_component_include_dir COMPONENTNAME)
	if(OGRE_SDK_BUILD)
	    include_directories("${OGRE_INCLUDE_DIR}/${COMPONENTNAME}")
	else()
	    include_directories("${OGRE_SOURCE_DIR}/Components/${COMPONENTNAME}/include")	
	endif()
endfunction(ogre_add_component_include_dir)

#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

#####################################################
# Install dependencies 
#####################################################

set(OGRE_DEP_DIR ${OGREDEPS_PATH})

macro(install_me INPUT)
    if(EXISTS "${OGREDEPS_PATH}/bin/${INPUT}")
        if(IS_DIRECTORY "${OGREDEPS_PATH}/bin/${INPUT}")
            install(DIRECTORY "${OGREDEPS_PATH}/bin/${INPUT}" DESTINATION "bin"
                    USE_SOURCE_PERMISSIONS)
        else()
            install(FILES "${OGREDEPS_PATH}/bin/${INPUT}" DESTINATION "bin")
        endif()
    endif()
    
    if(EXISTS "${OGREDEPS_PATH}/lib/${INPUT}")
        if(IS_DIRECTORY "${OGREDEPS_PATH}/lib/${INPUT}")
            install(DIRECTORY "${OGREDEPS_PATH}/lib/${INPUT}" DESTINATION "lib"
                    USE_SOURCE_PERMISSIONS)
        else()
            install(FILES "${OGREDEPS_PATH}/lib/${INPUT}" DESTINATION "lib")
        endif()
    endif()
endmacro()

if(NOT OGRE_STATIC)
    if(EXISTS "${OGREDEPS_PATH}/bin/")
        install(DIRECTORY "${OGREDEPS_PATH}/bin/" DESTINATION "bin")
    endif()
    if(EXISTS "${OGREDEPS_PATH}/lib/")
        install(DIRECTORY "${OGREDEPS_PATH}/lib/" DESTINATION "lib"
                USE_SOURCE_PERMISSIONS PATTERN "cmake" EXCLUDE)
    endif()
endif()

if(WIN32)
    # install GLES dlls
    if(OGRE_BUILD_RENDERSYSTEM_GLES)
        install_me(libgles_cm.dll)
    endif()
    
    # install GLES2 dlls
    if(OGRE_BUILD_RENDERSYSTEM_GLES2)
        install_me(libGLESv2.dll)
        install_me(libEGL.dll)
    endif()
endif() # WIN32

#include "m.h"
#include "_array.h"
#include <time.h>

#ifndef _MINSON_HASHTABLE_INITIAL_SIZE
# define _MINSON_HASHTABLE_INITIAL_SIZE 20
#endif
#ifndef _MINSON_HASHTABLE_MAX_LOAD_FACTOR
# define _MINSON_HASHTABLE_MAX_LOAD_FACTOR 0.7f
#endif
#ifndef _MINSON_HASHTABLE_RESIZE_FACTOR
# define _MINSON_HASHTABLE_RESIZE_FACTOR 1.7f
#endif



static uint32_t MurmurHash2AM(uint8_t const *key_, uint32_t key_lenght_,
                              uint32_t seed_)
{
static uint32_t const m = 0x5BD1E995;
static int const r = 24;
    
#define mmix(h, k) \
    do{ k *= m; k ^= k >> r; k *= m; h *= m; h ^= k; }while(0)
    
    uint32_t l = key_lenght_, hash = seed_, i = 0;
    
    while(key_lenght_ >= 4)
    {
        uint32_t k = *m_pcast(uint32_t *, key_) + (++i);
        mmix(hash, k);
        
        key_ += 4;
        key_lenght_ -= 4;
    }
    
    uint32_t t = 0;
    switch(key_lenght_)
    {
    case 3: t ^= key_[2] << 16;
    case 2: t ^= key_[1] << 8;
    case 1: t ^= key_[0];
    default:;
    }
    
    mmix(hash, t);
    mmix(hash, l);
    
    hash ^= hash >> 13;
    hash *= m;
    hash ^= hash >> 15;
    
#undef mmix
    return hash;
}


void _array_generate_seed(t_minson_array *this_)
{
    this_->_hashtable_seed = (uint32_t)((time(NULL) & 0xFFFF) << 16) |
                             (uint32_t)(clock() & 0xFFFF);
    return;
}


void _array_get_cells(t_minson_array const *this_, char const *name_,
                      uint32_t *out_cell1_, uint32_t *out_cell2_)
{
    uint32_t name_lenght = (uint32_t)strlen(name_);
    
    uint32_t hash = MurmurHash2AM(
        m_pcast(uint8_t *, name_), name_lenght, this_->_hashtable_seed
    );
    *out_cell1_ = hash % this_->_hashtable_size;
    
    do
    {
        hash = MurmurHash2AM(m_pcast(uint8_t *, name_), name_lenght, hash);
        *out_cell2_ = hash % this_->_hashtable_size;
    }
    while(*out_cell1_ == *out_cell2_);
    return;
}

// --- *** ---


void _array_alloc_table(t_minson_array *this_)
{
    if(this_->_hashtable_size == 0)
        this_->_hashtable_size = _MINSON_HASHTABLE_INITIAL_SIZE;
    
    this_->_hashtable = m_new(struct _t_minson_array_element *, this_->_hashtable_size);
    
    struct _t_minson_array_element **cell = this_->_hashtable;
    struct _t_minson_array_element **table_end = this_->_hashtable + this_->_hashtable_size;
    while(cell != table_end) *cell++ = NULL;
    return;
}


void _array_insert(t_minson_array *this_, struct _t_minson_array_element *element_)
{
    element_->_next = NULL; element_->_prev = this_->_last;
    if(element_->_prev != NULL) element_->_prev->_next = element_;
    this_->_last = element_;
    if(this_->_first == NULL) this_->_first = element_;
    
    ++(this_->_elements_count);
    
    if(element_->_object->name == NULL)
    {
        element_->_hashtable_cell_1 = element_->_hashtable_cell_2 = NULL;
        return;
    }
    
    uint32_t cell1, cell2;
    _array_get_cells(this_, element_->_object->name, &cell1, &cell2);
    element_->_hashtable_cell_1 = this_->_hashtable + cell1;
    element_->_hashtable_cell_2 = this_->_hashtable + cell2;
    
    struct _t_minson_array_element **start_cell = element_->_hashtable_cell_1;
    struct _t_minson_array_element **move_cell = start_cell;
    
    struct _t_minson_array_element *move_element = element_;
    
    for(;;)
    {
        if(*(move_element->_hashtable_cell_1) == NULL)
        {
            *(move_element->_hashtable_cell_1) = move_element; break;
        }
        
        if(*(move_element->_hashtable_cell_2) == NULL)
        {
            *(move_element->_hashtable_cell_2) = move_element; break;
        }
        
        move_cell = (move_cell == move_element->_hashtable_cell_1)?
                    move_element->_hashtable_cell_2 : move_element->_hashtable_cell_1;
        if(move_cell == start_cell) // cycle!
        {
            _array_reorder(this_); return;
        }
        
        struct _t_minson_array_element *tmp_element = *move_cell;
        *move_cell = move_element;
        move_element = tmp_element;
    }
    
    ++(this_->_hashtable_elements_count);
    
    if((float)(this_->_hashtable_elements_count) / (float)(this_->_hashtable_size) >
       _MINSON_HASHTABLE_MAX_LOAD_FACTOR)
    {
        _array_reorder(this_);
    }
    return;
}


void _array_reorder(t_minson_array *this_)
{
    struct _t_minson_array_element **old_table = this_->_hashtable;
    t_minson_iter iter = minsonFirst(this_);
    
    this_->_hashtable_size = (uint32_t)(
        (float)this_->_hashtable_size * _MINSON_HASHTABLE_RESIZE_FACTOR
    );
    _array_alloc_table(this_);
    _array_generate_seed(this_);
    
    this_->_first = this_->_last = NULL;
    this_->_hashtable_elements_count = this_->_elements_count = 0;
    
    while(minsonNotEnd(iter))
    {
        struct _t_minson_array_element *p_item = iter;
        minsonNext(iter);
        _array_insert(this_, p_item);
    }
    
    m_delete(old_table);
    return;
}

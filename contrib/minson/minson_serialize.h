#ifndef _OFFNET_MINSON_SERIALIZE_H
#define _OFFNET_MINSON_SERIALIZE_H

#include "minson_object.h"
#include "minson_string_builder.h"
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


/* Формирует текстовое представление объекта, записывая его в строковый буфер `out_`. */
uint8_t /*bool*/ minsonSerialize(t_minson_object *object_, t_minson_string_builder *out_);

/* Формирует текстовое представление объекта с отступами и переносами строк,
 * записывая его в строковый буфер `out_`. */
uint8_t /*bool*/ minsonPrettySerialize(t_minson_object *object_, t_minson_string_builder *out_);

/* Формирует текстовое представление, записывая его в файл `filename_`. */
uint8_t /*bool*/ minsonSerializeFile(t_minson_object *object_, char const *filename_);

/* Формирует текстовое представление с отступами и переносами строк,
 * записывая его в файл `filename_`. */
uint8_t /*bool*/ minsonPrettySerializeFile(t_minson_object *object_, char const *filename_);

/* Формирует текстовое представление объекта, записывая его в файловый поток. */
uint8_t /*bool*/ minsonSerializeStream(t_minson_object *object_, FILE *stream_);

/* Формирует текстовое представление объекта с отступами и переносами строк,
 * записывая его в файловый поток. */
uint8_t /*bool*/ minsonPrettySerializeStream(t_minson_object *object_, FILE *stream_);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_MINSON_SERIALIZE_H

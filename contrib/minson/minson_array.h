#ifndef _OFFNET_MINSON_ARRAY_H
#define _OFFNET_MINSON_ARRAY_H

#include "minson_object.h"

#ifdef __cplusplus
extern "C" {
#endif


struct _t_minson_array_element
{
    struct _t_minson_array_element **_hashtable_cell_1, **_hashtable_cell_2;
    struct _t_minson_array_element *_prev, *_next;
    
    t_minson_object *_object;
};


typedef struct t_minson_array
{
    t_minson_object h;
    
    struct _t_minson_array_element **_hashtable;
    struct _t_minson_array_element *_first, *_last;
    
    uint32_t _hashtable_size, _hashtable_elements_count, _elements_count;
    uint32_t _hashtable_seed;
}
t_minson_array;


/* Создаёт объект массива с указанным именем (или безымянный, если `name_` равно `NULL`).
 * Объект следует удалять с помощью функции `minsonDelete`. */
t_minson_array *minsonCreateArray(char const *name_);
/* Проверяет, что тип объекта - массив, и возвращает указатель на массив (тот же адрес)
 * в случае успеха и `NULL` в ином случае. */
t_minson_array *minsonCastArray(t_minson_object *object_);
/* Преобразует указатель на массив к указателю на объект. */
t_minson_object *minsonUpcast(t_minson_array *array_);
/* Создаёт рекурсивную копию массива.
 * Эквивалентно `minsonCastArray(minsonDuplicate(minsonUpcast(array_)))`. */
t_minson_array *minsonDuplicateArray(t_minson_array *array_);
/* Удаляет объект по указателю на массив. Эквивалентно `minsonDelete(minsonUpcast(array_))`. */
void minsonDeleteArray(t_minson_array *array_);

/* Инициализирует структуру, на которую указывает `this_`.
 * _НЕ_ требуется, если объект создан с помощью `minsonCreateArray`. */
void minsonArrayInit(t_minson_array *this_);
/* Деинициализирует структуру (волженные объекты при этом _НЕ_ удаляются).
 * _НЕ_ требуется, если объект удаляется с помощью `minsonDelete` или `minsonDeleteArray`. */
void minsonArrayClear(t_minson_array *this_);

/* Добавляет объект в массив `this_`. */
void minsonArrayAdd(t_minson_array *this_, t_minson_object *object_);
/* Ищет в массиве объект по имени и возвращает его в случае успеха, иначе возвращает `NULL`. */
t_minson_object *minsonArrayGet(t_minson_array const *this_, char const *name_);
/* Ищет объект в массиве по имени и извлекает его из массива. Возвращает извлечённый объект. */
t_minson_object *minsonArrayRemove(t_minson_array *this_, char const *name_);
/* Дополняет массив, копируя объекты из массива `from_` (использует `minsonDuplicate`).
 * Именованные объекты заменяются (старый объект удаляется с помощью `minsonDelete`).
 * Рекурсивно для вложенных массивов. */
void minsonArrayUpdate(t_minson_array *this_, t_minson_array *from_);

/* Возвращает общее число объектов, вложенных в массив. */
#define minsonArrayCount(this_) ((this_)->_elements_count)
/* Возвращает число объектов, имеющих имя. */
#define minsonArrayNamedCount(this_) ((this_)->_hashtable_elements_count)


/* Создают MinSON-объект с указанным значением соответствующего типа и
 * помещают его в конец указанного массива. */
#define minsonArrayAddArray(this_, name_) minsonArrayAdd((this_), minsonUpcast(minsonCreateArray((name_))))
#define minsonArrayAddString(this_, name_, value_) minsonArrayAdd((this_), minsonCreateString((name_), (value_)))
#define minsonArrayAddNumber(this_, name_, value_) minsonArrayAdd((this_), minsonCreateNumber((name_), (value_)))
#define minsonArrayAddFloatNumber(this_, name_, value_) minsonArrayAdd((this_), minsonCreateFloatNumber((name_), (value_)))
#define minsonArrayAddBoolean(this_, name_, value_) minsonArrayAdd((this_), minsonCreateBoolean((name_), (value_)))
#define minsonArrayAddNull(this_, name_) minsonArrayAdd((this_), minsonCreateNull((name_)))

/* Ищет объект в массиве по имени и преобразует указатель на него в указатель на массив.
 * В случае неудачи возвращает `NULL`. */
#define minsonArrayGetArray(this_, name_) minsonCastArray(minsonArrayGet((this_), (name_)))
/* Ищет объект в массиве по имени и возвращает строковое значение, если тип объекта - строка.
 * В случае неудачи возвращает `NULL`. */
#define minsonArrayGetString(this_, name_) minsonGetString(minsonArrayGet((this_), (name_)))
/* Ищут объект в массиве по имени и записывают значение соответствующего типа в `out_value_`,
 * если тип совпадает. Возвращают ненулевое значение в случае успеха и ноль в ином случае. */
#define minsonArrayGetNumber(this_, name_, out_value_) minsonGetNumber(minsonArrayGet((this_), (name_)), (out_value_))
#define minsonArrayGetFloatNumber(this_, name_, out_value_) minsonGetFloatNumber(minsonArrayGet((this_), (name_)), (out_value_))
#define minsonArrayGetBoolean(this_, name_, out_value_) minsonGetBoolean(minsonArrayGet((this_), (name_)), (out_value_))
/* Ищет объект в массиве по имени и проверяет, что тип объекта - пустой.
 * Возвращают ненулевое значение в случае успеха и ноль в ином случае. */
#define minsonArrayIsNull(this_, name_) minsonIsNull(minsonArrayGet((this_), (name_)))


typedef struct _t_minson_array_element *t_minson_iter;

/* Возвращает итератор, указывающий на первый элемент массива. */
#define minsonFirst(array_) ((array_)->_first)
/* Возвращает итератор, указывающий на последний элемент массива. */
#define minsonLast(array_) ((array_)->_last)
/* Проверяет, достиг ли итератор конца и возвращает `false`, если достиг и `true` если нет. */
#define minsonNotEnd(iter) ((iter) != NULL)
/* Переместить итератор к следующему элементу. _`v_iter` должен быть переменной_. */
#define minsonNext(v_iter) (v_iter = v_iter->_next)
/* Переместить итератор к предыдущему элементу. _`v_iter` должен быть переменной_. */
#define minsonPrev(v_iter) (v_iter = v_iter->_prev)

/* Возвращает объект, на который указывает итератор. */
#define minsonIterObject(iter_) ((iter_)->_object)
/* Возвращает имя объекта, на который указывает итератор. */
#define minsonIterName(iter_) ((iter_)->_object->name)

/* Ищет в массиве объект по имени и возвращает итератор,
 * который на него указывает, в случае успеха, иначе возвращает `NULL`. */
t_minson_iter minsonArrayGetIter(t_minson_array const *this_, char const *name_);

/* Извлекает объект, на который указывает итератор, из массива.
 * Возвращает извлечённый объект. */
t_minson_object *minsonIterRemove(t_minson_array *this_, t_minson_iter iter_);


/* Преобразует указатель на объект внутри итератора в указатель на массив.
 * В случае неудачи возвращает `NULL`. */
#define minsonIterGetArray(iter_) minsonCastArray(minsonIterObject(iter_))
/* Возвращает строковое значение, если тип объекта внутри итератора - строка,
 * или `NULL` в случае неудачи.*/
#define minsonIterGetString(iter_) minsonGetString(minsonIterObject(iter_))
/* Записывают значение соответствующего типа в `out_value_`,
 * если тип объекта внутри итератора совпадает.
 * Возвращают ненулевое значение в случае успеха и ноль в ином случае. */
#define minsonIterGetNumber(iter_, out_value_) minsonGetNumber(minsonIterObject(iter_), (out_value_))
#define minsonIterGetFloatNumber(iter_, out_value_) minsonGetFloatNumber(minsonIterObject(iter_), (out_value_))
#define minsonIterGetBoolean(iter_, out_value_) minsonGetBoolean(minsonIterObject(iter_), (out_value_))
/* Проверяет, что тип объекта внутри итератора - пустой.
 * Возвращает ненулевое значение в случае успеха и ноль в ином случае.*/
#define minsonIterIsNull(iter_) minsonIsNull(minsonIterObject(iter_))


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_MINSON_ARRAY_H

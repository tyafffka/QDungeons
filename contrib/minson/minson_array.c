#include "m.h"
#include "minson_array.h"
#include "_array.h"



t_minson_array *minsonCreateArray(char const *name_)
{
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_array *array = m_new(t_minson_array, 1);
    array->h.type = C_MINSON_ARRAY;
    array->h.name = m_strdup(name_);
    
    minsonArrayInit(array);
    return array;
}


t_minson_array *minsonCastArray(t_minson_object *object_)
{
    if(object_ == NULL) return NULL;
    if(object_->type != C_MINSON_ARRAY) return NULL;
    return m_pcast(t_minson_array *, object_);
}


t_minson_object *minsonUpcast(t_minson_array *array_)
{   return m_pcast(t_minson_object *, array_);   }


t_minson_array *minsonDuplicateArray(t_minson_array *array_)
{
    return m_pcast(t_minson_array *, minsonDuplicate(
        m_pcast(t_minson_object *, array_)
    ));
}


void minsonDeleteArray(t_minson_array *array_)
{
    minsonDelete(m_pcast(t_minson_object *, array_));
    return;
}

// --- *** ---


void minsonArrayInit(t_minson_array *this_)
{
    this_->_hashtable_size = 0;
    _array_alloc_table(this_);
    _array_generate_seed(this_);
    
    this_->_first = this_->_last = NULL;
    this_->_hashtable_elements_count = this_->_elements_count = 0;
    return;
}


void minsonArrayClear(t_minson_array *this_)
{
    for(t_minson_iter iter = minsonFirst(this_); minsonNotEnd(iter);)
    {
        struct _t_minson_array_element *p_item = iter;
        minsonNext(iter);
        m_delete(p_item);
    }
    
    m_delete(this_->_hashtable); this_->_hashtable = NULL;
    this_->_hashtable_size = 0;
    
    this_->_first = this_->_last = NULL;
    this_->_hashtable_elements_count = this_->_elements_count = 0;
    return;
}


void minsonArrayAdd(t_minson_array *this_, t_minson_object *object_)
{
    if(object_ == NULL) return;
    
    struct _t_minson_array_element *element = m_new(struct _t_minson_array_element, 1);
    element->_object = object_;
    
    _array_insert(this_, element);
    return;
}


t_minson_object *minsonArrayGet(t_minson_array const *this_, char const *name_)
{
    t_minson_iter iter = minsonArrayGetIter(this_, name_);
    return (iter != NULL)? minsonIterObject(iter) : NULL;
}


t_minson_object *minsonArrayRemove(t_minson_array *this_, char const *name_)
{
    if(name_ == NULL) return NULL;
    
    struct _t_minson_array_element *element;
    
    uint32_t cell1, cell2;
    _array_get_cells(this_, name_, &cell1, &cell2);
    
    struct _t_minson_array_element **p_cell = this_->_hashtable + cell1;
    if(*p_cell != NULL) if(strcmp((*p_cell)->_object->name, name_) == 0)
        goto __remove;
    
    p_cell = this_->_hashtable + cell2;
    if(*p_cell != NULL) if(strcmp((*p_cell)->_object->name, name_) == 0)
        goto __remove;
    
    return NULL;
__remove:
    element = *p_cell; *p_cell = NULL;
    
    --(this_->_elements_count), --(this_->_hashtable_elements_count);
    
    if(element->_next != NULL) element->_next->_prev = element->_prev;
    if(element->_prev != NULL) element->_prev->_next = element->_next;
    if(this_->_first == element) this_->_first = element->_next;
    if(this_->_last == element) this_->_last = element->_prev;
    
    t_minson_object *object = element->_object;
    m_delete(element);
    return object;
}


void minsonArrayUpdate(t_minson_array *this_, t_minson_array *from_)
{
    for(t_minson_iter it = minsonFirst(from_); minsonNotEnd(it); minsonNext(it))
    {
        if(it->_object->name == NULL)
        {
            minsonArrayAdd(this_, minsonDuplicate(it->_object));
            continue;
        }
        
        t_minson_iter found = minsonArrayGetIter(this_, it->_object->name);
        if(found == NULL)
        {
            minsonArrayAdd(this_, minsonDuplicate(it->_object));
            continue;
        }
        
        if(found->_object->type == C_MINSON_ARRAY &&
           it->_object->type == C_MINSON_ARRAY)
        {
            minsonArrayUpdate(m_pcast(t_minson_array *, found->_object),
                              m_pcast(t_minson_array *, it->_object));
        }
        else
        {
            minsonDelete(found->_object);
            found->_object = minsonDuplicate(it->_object);
        }
    }
    return;
}


t_minson_iter minsonArrayGetIter(t_minson_array const *this_, char const *name_)
{
    if(name_ == NULL) return NULL;
    
    uint32_t cell1, cell2;
    _array_get_cells(this_, name_, &cell1, &cell2);
    
    struct _t_minson_array_element **p_cell = this_->_hashtable + cell1;
    if(*p_cell != NULL) if(strcmp((*p_cell)->_object->name, name_) == 0)
        return *p_cell;
    
    p_cell = this_->_hashtable + cell2;
    if(*p_cell != NULL) if(strcmp((*p_cell)->_object->name, name_) == 0)
        return *p_cell;
    
    return NULL;
}


t_minson_object *minsonIterRemove(t_minson_array *this_, t_minson_iter iter_)
{
    if(iter_ == NULL) return NULL;
    
    if(iter_->_object->name != NULL)
    {
        struct _t_minson_array_element **p_cell = iter_->_hashtable_cell_1;
        if(*p_cell != iter_) p_cell = iter_->_hashtable_cell_2;
        if(*p_cell != iter_) return NULL;
        
        if((p_cell < this_->_hashtable) ||
           (this_->_hashtable + this_->_hashtable_size <= p_cell))
        {   return NULL;   }
        
        *p_cell = NULL;
        --(this_->_hashtable_elements_count);
    }
    
    --(this_->_elements_count);
    
    if(iter_->_next != NULL) iter_->_next->_prev = iter_->_prev;
    if(iter_->_prev != NULL) iter_->_prev->_next = iter_->_next;
    if(this_->_first == iter_) this_->_first = iter_->_next;
    if(this_->_last == iter_) this_->_last = iter_->_prev;
    
    t_minson_object *object = iter_->_object;
    m_delete(iter_);
    return object;
}

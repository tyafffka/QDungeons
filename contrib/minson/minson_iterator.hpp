#ifndef _OFFNET_MINSON_ITERATOR_HPP
#define _OFFNET_MINSON_ITERATOR_HPP

#include "minson_object.hpp"

namespace minson
{


/* Класс итератора по массиву. */
class iterator
{
    friend class array;
    
private:
    t_minson_iter __p;
    bool __is_ascending;
    
    explicit iterator(t_minson_iter p_, bool is_asc_) :
        __p(p_), __is_ascending(is_asc_)
    {}
    
public:
    iterator(iterator const &cp) :
        __p(cp.__p), __is_ascending(cp.__is_ascending)
    {}
    
    static iterator end() {   return iterator(nullptr, true);   }
    static iterator rend() {   return iterator(nullptr, false);   }
    
    iterator &operator ++()
    {
        if(__p)
        {
            if(__is_ascending) minsonNext(__p);
            else minsonPrev(__p);
        }
        return *this;
    }
    iterator &operator --()
    {
        if(__p)
        {
            if(__is_ascending) minsonPrev(__p);
            else minsonNext(__p);
        }
        return *this;
    }
    
    bool operator ==(iterator const &r_) const
    {   return(__p == r_.__p);   }
    bool operator !=(iterator const &r_) const
    {   return(__p != r_.__p);   }
    operator bool() const
    {   return (bool)__p;   }
    
    bool hasName() const /*throw(null_pointer_exception)*/
    {
        if(! __p) throw null_pointer_exception{};
        return (bool)minsonIterName(__p);
    }
    std::string name() const /*throw(null_pointer_exception)*/
    {
        if(! __p) throw null_pointer_exception();
        char const *tmp__ = minsonIterName(__p);
        return tmp__ ? std::string(tmp__) : std::string();
    }
    
    bool isNull() const /*throw(null_pointer_exception)*/
    {
        if(! __p) throw null_pointer_exception();
        return(minsonIterIsNull(__p) != 0);
    }
    
    object operator *() const /*throw(null_pointer_exception)*/
    {
        if(! __p) throw null_pointer_exception();
        return object(minsonIterObject(__p), false);
    }
}; // class iterator


} // namespace minson

#endif // _OFFNET_MINSON_ITERATOR_HPP

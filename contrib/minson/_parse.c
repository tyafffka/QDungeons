#include "m.h"
#include "_parse.h"
#include <stdbool.h>


static void __init_base_context(struct _base_context *this_,
                                int (*callback_)(struct _base_context *))
{
    this_->token_type = _C_END;
    this_->token_value = NULL;
    this_->at_line = 1;
    this_->at_column = 0;
    this_->_get_callback = callback_;
    this_->__buffered_char = '\0';
    return;
}

static void __clear_base_context(struct _base_context *this_)
{
    if(this_->token_value != NULL)
    {
        minsonDeleteStringBuilder(this_->token_value);
        this_->token_value = NULL;
    }
    return;
}

// ---

struct _t_string_context
{
    struct _base_context h;
    char const *stringstream, *stringstream_end;
};

static int __string_context_get(struct _base_context *this_)
{
    struct _t_string_context *this = m_pcast(struct _t_string_context *, this_);
    if(this->stringstream >= this->stringstream_end) return EOF;
    
    register int ch = (int)*(this->stringstream)++;
    
    if(ch == '\n')
    {   ++(this_->at_line); this_->at_column = 0;   }
    else
    {   ++(this_->at_column);   }
    return ch;
}

struct _t_string_context *_new_parse_str(char const *begin_, char const *end_)
{
    if(begin_ == NULL || end_ == NULL || end_ < begin_) return NULL;
    
    struct _t_string_context *this = m_new(struct _t_string_context, 1);
    
    __init_base_context(&(this->h), __string_context_get);
    this->stringstream = begin_; this->stringstream_end = end_;
    return this;
}

void _delete_parse_str(struct _t_string_context *this_)
{
    if(this_ == NULL) return;
    
    __clear_base_context(&(this_->h));
    m_delete(this_);
    return;
}

// ---

struct _t_stream_context
{
    struct _base_context h;
    FILE *stdstream;
};

static int __stream_context_get(struct _base_context *this_)
{
    struct _t_stream_context *this = m_pcast(struct _t_stream_context *, this_);
    if(feof(this->stdstream)) return EOF;
    
    register int ch = fgetc(this->stdstream);
    
    if(ch == '\n')
    {   ++(this_->at_line); this_->at_column = 0;   }
    else
    {   ++(this_->at_column);   }
    return ch;
}

struct _t_stream_context *_new_parse_stream(FILE *stream_)
{
    struct _t_stream_context *this = m_new(struct _t_stream_context, 1);
    
    __init_base_context(&(this->h), __stream_context_get);
    this->stdstream = stream_;
    return this;
}

void _delete_parse_stream(struct _t_stream_context *this_)
{
    if(this_ == NULL) return;
    
    __clear_base_context(&(this_->h));
    m_delete(this_);
    return;
}


// --- *** ---

static bool __is_identifier_char[128] = {
/*  0                             15 */
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
/* 16                             31 */
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
/* sp ! " # $ % & ' ( ) * + , - . / */
    0,1,0,0,1,0,0,0,0,0,1,1,0,1,1,1,
/*  0 1 2 3 4 5 6 7 8 9 : ; < = > ? */
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,
/*  @ A B C D E F G H I J K L M N O */
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
/*  P Q R S T U V W X Y Z [ \ ] ^ _ */
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,
/*  ` a b c d e f g h i j k l m n o */
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
/*  p q r s t u v w x y z { | } ~ 127*/
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0
};


// --- *** ---

int /*nix*/ _parse_next(struct _base_context *this_)
{
    this_->token_type = _C_END;
    if(this_->token_value != NULL)
    {
        minsonDeleteStringBuilder(this_->token_value);
        this_->token_value = NULL;
    }
    
    bool is_escape = false, is_comment = false;
    int escape_hex = -1;
    
    for(;;)
    {
        register int ch;
        if(this_->__buffered_char != '\0')
        {
            ch = this_->__buffered_char; this_->__buffered_char = '\0';
        }
        else
        {   ch = (*(this_->_get_callback))(this_);   }
        
        if(is_comment)
        {
            if(ch == EOF)
            {
                this_->__buffered_char = ch;
                return (this_->token_type == _C_BASIC)? __PARSE_WRONG_VALUE : __PARSE_OK;
            }
            
            if(ch == '\n' || ch == '\r') is_comment = false;
            continue;
        }
        
        bool is_whitespace =(ch == ' ' || ch == '\t' || ch == '\v' ||
                             ch == '\n' || ch == '\r' || ch == '\x1b' /*escape*/);
        bool is_identifier = (0 <= ch && ch < 128)? __is_identifier_char[ch] : false;
        
        switch(this_->token_type)
        {
        case _C_END:
            if(is_whitespace) {}
            else if(ch == '#')
            {   is_comment = true;   }
            else if(is_identifier)
            {
                this_->token_type = _C_IDENTIFIER;
                this_->token_value = minsonCreateStringBuilder();
                minsonStringBuilderPutChar(this_->token_value, (char)ch);
            }
            else if(ch == '"')
            {
                this_->token_type = _C_STRING;
                this_->token_value = minsonCreateStringBuilder();
            }
            else if(ch == '(')
            {
                this_->token_type = _C_BASIC;
                this_->token_value = minsonCreateStringBuilder();
            }
            else if(ch == '{')
            {
                this_->token_type = _C_ARRAY_BEGIN;
                return __PARSE_OK;
            }
            else if(ch == '}')
            {
                this_->token_type = _C_ARRAY_END;
                return __PARSE_OK;
            }
            else
            {   return (ch == EOF)? __PARSE_OK : __PARSE_UNKNOWN_TOKEN;   }
            continue;
            
        case _C_IDENTIFIER:
            if(is_identifier)
            {   minsonStringBuilderPutChar(this_->token_value, (char)ch);   }
            else
            {
                this_->__buffered_char = ch;
                return __PARSE_OK;
            }
            continue;
            
        case _C_STRING:
            if(escape_hex != -1)
            {
                int a;
                if('0' <= ch && ch <= '9') a = ch - '0';
                else if('a' <= ch && ch <= 'f') a = ch - 'a' + 10;
                else if('A' <= ch && ch <= 'F') a = ch - 'A' + 10;
                else
                {
                    minsonStringBuilderPutChar(this_->token_value, 'x');
                    minsonStringBuilderPutChar(this_->token_value, (char)ch);
                    escape_hex = -1;
                    continue;
                }
                
                if(escape_hex == -2)
                {   escape_hex = a << 4;   }
                else
                {
                    minsonStringBuilderPutChar(this_->token_value, (char)(escape_hex | a));
                    escape_hex = -1;
                }
            }
            else if(is_escape)
            {
                switch(ch)
                {
                case 't':
                    minsonStringBuilderPutChar(this_->token_value, '\t');
                    break;
                case 'v':
                    minsonStringBuilderPutChar(this_->token_value, '\v');
                    break;
                case 'n':
                    minsonStringBuilderPutChar(this_->token_value, '\n');
                    break;
                case 'r':
                    minsonStringBuilderPutChar(this_->token_value, '\r');
                    break;
                case 'e':
                    minsonStringBuilderPutChar(this_->token_value, '\x1b');
                    break;
                case 'x':
                    escape_hex = -2;
                    break;
                default:
                    minsonStringBuilderPutChar(this_->token_value, (char)ch);
                }
                is_escape = false;
            }
            else if(ch == '\\')
            {   is_escape = true;   }
            else if(ch == '"')
            {   return __PARSE_OK;   }
            else if(ch == EOF)
            {   return __PARSE_WRONG_VALUE;   }
            else
            {   minsonStringBuilderPutChar(this_->token_value, (char)ch);   }
            continue;
            
        case _C_BASIC:
            if(ch == ')')
            {
                return __PARSE_OK;
            }
            else if(ch == '#')
            {   is_comment = true;   }
            else if(! is_whitespace)
            {   minsonStringBuilderPutChar(this_->token_value, (char)ch);   }
            continue;
        }
    } //- forever
    return __PARSE_OK;
}

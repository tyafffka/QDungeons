#ifndef __MINSON_ARRAY_H
#define __MINSON_ARRAY_H

#include "minson_array.h"


void _array_generate_seed(t_minson_array *this_);
void _array_get_cells(t_minson_array const *this_, char const *name_,
                      uint32_t *out_cell1_, uint32_t *out_cell2_);

void _array_alloc_table(t_minson_array *this_);
void _array_insert(t_minson_array *this_, struct _t_minson_array_element *element_);
void _array_reorder(t_minson_array *this_);


#endif // __MINSON_ARRAY_H

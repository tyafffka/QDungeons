#include "m.h"
#include "minson_serialize.h"
#include "minson_array.h"

#ifndef thread_local
# define thread_local _Thread_local
#endif

static thread_local int __pretty_level = -1;


static uint8_t /*bool*/ __write_string(char const *value_, t_minson_string_builder *out_)
{
    if(value_ == NULL) return 0;
    
    minsonStringBuilderPutChar(out_, '"');
    for(; *value_; ++value_)
    {
        switch(*value_)
        {
        case '\t':
            minsonStringBuilderPutChar(out_, '\\'); minsonStringBuilderPutChar(out_, 't');
            continue;
        case '\v':
            minsonStringBuilderPutChar(out_, '\\'); minsonStringBuilderPutChar(out_, 'v');
            continue;
        case '\n':
            minsonStringBuilderPutChar(out_, '\\'); minsonStringBuilderPutChar(out_, 'n');
            continue;
        case '\r':
            minsonStringBuilderPutChar(out_, '\\'); minsonStringBuilderPutChar(out_, 'r');
            continue;
        case '\x1b':
            minsonStringBuilderPutChar(out_, '\\'); minsonStringBuilderPutChar(out_, 'e');
            continue;
        case '"': case '\\':
            minsonStringBuilderPutChar(out_, '\\');
        default:
            minsonStringBuilderPutChar(out_, *value_);
        }
    }
    
    minsonStringBuilderPutChar(out_, '"');
    return 1;
}


uint8_t /*bool*/ minsonSerialize(t_minson_object *object_, t_minson_string_builder *out_)
{
    if(object_ == NULL || out_ == NULL) return 0;
    
    if(__pretty_level > 0)
        for(int tab = 0; tab < __pretty_level; ++tab)
        {
            minsonStringBuilderPutChar(out_, ' '); minsonStringBuilderPutChar(out_, ' ');
        }
    
    if(object_->name != NULL)
    {
        size_t name_lenght = strlen(object_->name);
        minsonStringBuilderMove(out_, strdup(object_->name), name_lenght);
        if(__pretty_level >= 0) minsonStringBuilderPutChar(out_, ' ');
    }
    
    switch(object_->type)
    {
    case C_MINSON_ARRAY:
        minsonStringBuilderPutChar(out_, '{');
        if(__pretty_level >= 0)
        {
            minsonStringBuilderPutChar(out_, '\n'); ++__pretty_level;
        }
        
        for(t_minson_iter it = minsonFirst(m_pcast(t_minson_array *, object_));
            minsonNotEnd(it); minsonNext(it))
        {
            if(! minsonSerialize(minsonIterObject(it), out_)) return 0;
        }
        
        if(__pretty_level > 0)
        {
            --__pretty_level;
            for(int tab = 0; tab < __pretty_level; ++tab)
            {
                minsonStringBuilderPutChar(out_, ' '); minsonStringBuilderPutChar(out_, ' ');
            }
        }
        minsonStringBuilderPutChar(out_, '}');
        break;
        
    case C_MINSON_STRING:
        if(! __write_string(m_pcast(t_minson_string *, object_)->value, out_))
            return 0;
        break;
        
    case C_MINSON_NUMBER:
    {
        int64_t value = m_pcast(t_minson_number *, object_)->value;
        minsonStringBuilderPutChar(out_, '(');
        
        if(value < 0)
        {
            minsonStringBuilderPutChar(out_, '-');
            value = -value;
        }
        
        char *buffer = (char *)malloc(22);
        int subwrited = snprintf(buffer, 22, (__pretty_level >= 0)? "%ld)" : "x%lX)", value);
        if(subwrited <= 0) return 0;
        
        minsonStringBuilderMove(out_, buffer, (size_t)subwrited);
        break;
    }
    case C_MINSON_FLOAT_NUMBER:
    {
        char *buffer = (char *)malloc(20);
        int subwrited = snprintf(buffer, 20, "(%#.12g)",
                                 m_pcast(t_minson_float_number *, object_)->value);
        if(subwrited <= 0) return 0;
        
        minsonStringBuilderMove(out_, buffer, (size_t)subwrited);
        break;
    }
    case C_MINSON_BOOLEAN:
        minsonStringBuilderPutChar(out_, '(');
        minsonStringBuilderPutChar(
            out_, (m_pcast(t_minson_boolean *, object_)->value)? (char)'y' : (char)'n'
        );
        minsonStringBuilderPutChar(out_, ')');
        break;
        
    case C_MINSON_NULL:
        minsonStringBuilderPutChar(out_, '(');
        minsonStringBuilderPutChar(out_, '*');
        minsonStringBuilderPutChar(out_, ')');
        break;
    }
    
    if(__pretty_level >= 0) minsonStringBuilderPutChar(out_, '\n');
    return 1;
}


uint8_t /*bool*/ minsonPrettySerialize(t_minson_object *object_, t_minson_string_builder *out_)
{
    __pretty_level = 0;
    uint8_t r = minsonSerialize(object_, out_);
    __pretty_level = -1;
    return r;
}

// --- *** ---


uint8_t /*bool*/ minsonSerializeFile(t_minson_object *object_, char const *filename_)
{
    if(filename_ == NULL) return 0;
    FILE *stream = fopen(filename_, "wt");
    if(stream == NULL) return 0;
    
    uint8_t r = minsonSerializeStream(object_, stream);
    
    fclose(stream);
    return r;
}


uint8_t /*bool*/ minsonPrettySerializeFile(t_minson_object *object_, char const *filename_)
{
    __pretty_level = 0;
    uint8_t r = minsonSerializeFile(object_, filename_);
    __pretty_level = -1;
    return r;
}


static uint8_t /*bool*/ __stream_write_string(char const *value_, FILE *stream_)
{
    if(value_ == NULL) return 0;
    
    if(fputc('"', stream_) == EOF) return 0;
    for(; *value_; ++value_)
    {
        switch(*value_)
        {
        case '\t':
            if(fputc('\\', stream_) == EOF) return 0;
            if(fputc('t', stream_) == EOF) return 0;
            continue;
        case '\v':
            if(fputc('\\', stream_) == EOF) return 0;
            if(fputc('v', stream_) == EOF) return 0;
            continue;
        case '\n':
            if(fputc('\\', stream_) == EOF) return 0;
            if(fputc('n', stream_) == EOF) return 0;
            continue;
        case '\r':
            if(fputc('\\', stream_) == EOF) return 0;
            if(fputc('r', stream_) == EOF) return 0;
            continue;
        case '\x1b':
            if(fputc('\\', stream_) == EOF) return 0;
            if(fputc('e', stream_) == EOF) return 0;
            continue;
        case '"': case '\\':
            if(fputc('\\', stream_) == EOF) return 0;
        default:
            if(fputc(*value_, stream_) == EOF) return 0;
        }
    }
    
    if(fputc('"', stream_) == EOF) return 0;
    return 1;
}


uint8_t /*bool*/ minsonSerializeStream(t_minson_object *object_, FILE *stream_)
{
    if(object_ == NULL || stream_ == NULL) return 0;
    
    if(__pretty_level > 0)
        for(int tab = 0; tab < __pretty_level; ++tab)
        {
            if(fwrite((void const *)"  ", 1, 2, stream_) < 2) return 0;
        }
    
    if(object_->name != NULL)
    {
        size_t name_lenght = strlen(object_->name);
        if(fwrite((void const *)object_->name, 1, name_lenght, stream_) < name_lenght)
            return 0;
        if(__pretty_level >= 0) if(fputc(' ', stream_) == EOF) return 0;
    }
    
    switch(object_->type)
    {
    case C_MINSON_ARRAY:
        if(fputc('{', stream_) == EOF) return 0;
        if(__pretty_level >= 0)
        {
            if(fputc('\n', stream_) == EOF) return 0;
            ++__pretty_level;
        }
        
        for(t_minson_iter it = minsonFirst(m_pcast(t_minson_array *, object_));
            minsonNotEnd(it); minsonNext(it))
        {
            if(! minsonSerializeStream(minsonIterObject(it), stream_))
            {
                if(__pretty_level > 0) --__pretty_level;
                return 0;
            }
        }
        
        if(__pretty_level > 0)
        {
            --__pretty_level;
            for(int tab = 0; tab < __pretty_level; ++tab)
            {
                if(fwrite((void const *)"  ", 1, 2, stream_) < 2) return 0;
            }
        }
        if(fputc('}', stream_) == EOF) return 0;
        break;
        
    case C_MINSON_STRING:
        if(! __stream_write_string(m_pcast(t_minson_string *, object_)->value, stream_))
            return 0;
        break;
        
    case C_MINSON_NUMBER:
    {
        int64_t value = m_pcast(t_minson_number *, object_)->value;
        if(fputc('(', stream_) == EOF) return 0;
        
        if(value < 0)
        {
            if(fputc('-', stream_) == EOF) return 0;
            value = -value;
        }
        
        if(fprintf(stream_, (__pretty_level >= 0)? "%ld)" : "x%lX)", value) <= 0) return 0;
        break;
    }
    case C_MINSON_FLOAT_NUMBER:
        if(fprintf(stream_, "(%#.12g)",
                   m_pcast(t_minson_float_number *, object_)->value) <= 0)
        {   return 0;   }
        break;
        
    case C_MINSON_BOOLEAN:
        if(fputc('(', stream_) == EOF) return 0;
        if(fputc((m_pcast(t_minson_boolean *, object_)->value)? 'y' : 'n',
                 stream_) == EOF) return 0;
        if(fputc(')', stream_) == EOF) return 0;
        break;
        
    case C_MINSON_NULL:
        if(fwrite((void const *)"(*)", 1, 3, stream_) <= 0) return 0;
        break;
    }
    
    if(__pretty_level >= 0) if(fputc('\n', stream_) == EOF) return 0;
    fflush(stream_);
    return 1;
}


uint8_t /*bool*/ minsonPrettySerializeStream(t_minson_object *object_, FILE *stream_)
{
    __pretty_level = 0;
    uint8_t r = minsonSerializeStream(object_, stream_);
    __pretty_level = -1;
    return r;
}

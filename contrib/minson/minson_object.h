#ifndef _OFFNET_MINSON_OBJECT_H
#define _OFFNET_MINSON_OBJECT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef enum c_minson_type
{
    C_MINSON_ARRAY, C_MINSON_STRING,
    C_MINSON_NUMBER, C_MINSON_FLOAT_NUMBER, C_MINSON_BOOLEAN,
    C_MINSON_NULL,
}
c_minson_type;

typedef struct t_minson_object
{
    char const *name;
    c_minson_type type;
}
t_minson_object;


typedef struct t_minson_string
{
    struct t_minson_object h;
    char const *value;
}
t_minson_string;

typedef struct t_minson_number
{
    struct t_minson_object h;
    int64_t value;
}
t_minson_number;

typedef struct t_minson_float_number
{
    struct t_minson_object h;
    double value;
}
t_minson_float_number;

typedef struct t_minson_boolean
{
    struct t_minson_object h;
    uint8_t /*bool*/ value;
}
t_minson_boolean;


/* Создают MinSON-объект с указанным значением соответствующего типа.
 * Параметр `name_` может быть `NULL`, в этом случае объект будет безымянным.
 * Созданные таким образом объекты следует удалять с помощью функции `minsonDelete`. */
t_minson_object *minsonCreateString(char const *name_, char const *value_);
t_minson_object *minsonCreateNumber(char const *name_, int64_t value_);
t_minson_object *minsonCreateFloatNumber(char const *name_, double value_);
t_minson_object *minsonCreateBoolean(char const *name_, uint8_t /*bool*/ value_);
t_minson_object *minsonCreateNull(char const *name_);

/* Проверяет, что тип объекта - строка, и возвращает значение (указатель на _внутренний_ буфер).
 * В случае ошибок возвращает `NULL`. */
char const *minsonGetString(t_minson_object *object_);
/* Проверяют тип объекта и записывают значение соответствующего типа по адресу,
 * указанному в `out_value_`. Возвращают ненулевое значение в случае успеха,
 * и ноль в ином случае. */
uint8_t /*bool*/ minsonGetNumber(t_minson_object *object_, int64_t *out_value_);
uint8_t /*bool*/ minsonGetFloatNumber(t_minson_object *object_, double *out_value_);
uint8_t /*bool*/ minsonGetBoolean(t_minson_object *object_, uint8_t /*bool*/ *out_value_);
/* Проверяет, что тип объекта - пустой, и возвращает ненулевое значение в случае
 * успеха и ноль в ином случае. */
uint8_t /*bool*/ minsonIsNull(t_minson_object *object_);

/* Создаёт копию объекта таким же образом, как это делают функции `minsonCreate...`.
 * Массивы копируются рекурсивно. */
t_minson_object *minsonDuplicate(t_minson_object *object_);

/* Удаляет объект (очищает память), созданный с помощью функций `minsonCreate...`,
 * `minsonParse...` и `minsonDuplicate`. Массивы удаляются рекурсивно. */
void minsonDelete(t_minson_object *object_);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_MINSON_OBJECT_H

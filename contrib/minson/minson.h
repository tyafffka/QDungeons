#ifndef _OFFNET_MINSON_H
#define _OFFNET_MINSON_H

#include "minson_object.h"
#include "minson_array.h"
#include "minson_serialize.h"

#ifdef __cplusplus
extern "C" {
#else
# ifndef thread_local
#  define thread_local _Thread_local
# endif
#endif


#define MINSON_ERROR_OK            0 // разбор выполнен без ошибок
#define MINSON_ERROR_UNKNOWN_TOKEN 2 // во время разбора встретилась неизвестная лексема
#define MINSON_ERROR_WRONG_VALUE   3 // во время разбора не удалось разобрать значение объекта
#define MINSON_ERROR_WRONG_SYNTAX  5 // во время разбора встретилась неверная синтаксическая конструкция


/* тип ошибки, возникшей при разборе */
extern thread_local int /*nix*/ minson_error_code;
/* номер строки во входных данных разбора, где была обнаружена ошибка */
extern thread_local int minson_error_line;
/* номер символа в строке, где была обнаружена ошибка */
extern thread_local int minson_error_column;


/* Разбирает MinSON-объект из текстового представления, содержащегося в
 * нуль-терминированной строке.
 * Возвращает разобранный объект, который следует удалять с помощью функции `minsonDelete`.
 * В случае ошибки возвращает `NULL`. */
t_minson_object *minsonParse(char const *str_);
/* Разбирает MinSON-объект из текстового представления, содержащегося в памяти от
 * `str_begin_` (включительно) до `str_end_` (не включая).
 * Возвращает разобранный объект, который следует удалять с помощью функции `minsonDelete`.
 * В случае ошибки возвращает `NULL`. */
t_minson_object *minsonParse2(char const *str_begin_, char const *str_end_);
/* Разбирает MinSON-объект, текстовое представление которого записано в файле `filename_`.
 * Возвращает разобранный объект, который следует удалять с помощью функции `minsonDelete`.
 * В случае ошибки возвращает `NULL`. */
t_minson_object *minsonParseFile(char const *filename_);
/* Разбирает MinSON-объект из текстового представления, содержащегося в файловом потоке.
 * Разбор продолжается до конца объекта. Возвращает разобранный объект, который следует
 * удалять с помощью функции `minsonDelete`, или `NULL` в случае ошибки. */
t_minson_object *minsonParseStream(FILE *stream_);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_MINSON_H

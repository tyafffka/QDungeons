#include "m.h"
#include "minson.h"
#include "_parse.h"
#include "_parse_basic.h"
#include <stdbool.h>


thread_local int /*nix*/ minson_error_code;
thread_local int minson_error_line;
thread_local int minson_error_column;



static bool __next_token(struct _base_context *context_)
{
    if((minson_error_code = _parse_next(context_)) != __PARSE_OK)
    {
        if(context_->token_value != NULL)
        {
            minsonDeleteStringBuilder(context_->token_value);
            context_->token_value = NULL;
        }
        
        minson_error_line = context_->at_line;
        minson_error_column = context_->at_column;
        return false;
    }
    return true;
}


static t_minson_object *__build_basic(struct _base_context *context_)
{
    size_t str_value_size = minsonStringBuilderSize(context_->token_value);
    char str_value[str_value_size + 1];
    minsonBuildString(context_->token_value, str_value);
    str_value[str_value_size] = '\0';
    
    t_minson_object *object = NULL;
    
    enum _c_basic_type type = _detect_basic_type(str_value);
    switch(type)
    {
    case _C_BASIC_NUMBER:
    {
        int64_t value__;
        minson_error_code = _parse_number(str_value, &value__);
        if(minson_error_code != __PARSE_OK)
        {
            minson_error_line = context_->at_line;
            minson_error_column = context_->at_column;
            break;
        }
        
        t_minson_number *number = m_new(t_minson_number, 1);
        object = m_pcast(t_minson_object *, number);
        
        object->type = C_MINSON_NUMBER;
        number->value = value__;
        break;
    }
    case _C_BASIC_FLOAT:
    {
        double value__;
        minson_error_code = _parse_float(str_value, &value__);
        if(minson_error_code != __PARSE_OK)
        {
            minson_error_line = context_->at_line;
            minson_error_column = context_->at_column;
            break;
        }
        
        t_minson_float_number *float_number = m_new(t_minson_float_number, 1);
        object = m_pcast(t_minson_object *, float_number);
        
        object->type = C_MINSON_FLOAT_NUMBER;
        float_number->value = value__;
        break;
    }
    case _C_BASIC_TRUE: case _C_BASIC_FALSE:
    {
        t_minson_boolean *boolean = m_new(t_minson_boolean, 1);
        object = m_pcast(t_minson_object *, boolean);
        
        object->type = C_MINSON_BOOLEAN;
        boolean->value = (uint8_t)((type == _C_BASIC_TRUE)? 1 : 0);
        break;
    }
    case _C_BASIC_NULL:
    {
        object = m_new(t_minson_object, 1);
        object->type = C_MINSON_NULL;
        break;
    }
    default:
        minson_error_code = MINSON_ERROR_WRONG_VALUE;
        minson_error_line = context_->at_line;
        minson_error_column = context_->at_column;
    } //- switch(basic type)
    
    minsonDeleteStringBuilder(context_->token_value);
    context_->token_value = NULL;
    return object;
}


static t_minson_object *__build_object(struct _base_context *context_)
{
    minson_error_code = MINSON_ERROR_OK;
    minson_error_line = 0;
    minson_error_column = 0;
    
    if(context_->token_type == _C_ARRAY_END) return NULL;
    
    char *name__ = NULL;
    if(context_->token_type == _C_IDENTIFIER)
    {
        size_t name_size = minsonStringBuilderSize(context_->token_value);
        name__ = m_new(char, name_size + 1);
        minsonBuildString(context_->token_value, name__);
        name__[name_size] = '\0';
        
        if(! __next_token(context_)) goto __fail;
    }
    
    t_minson_object *object = NULL;
    
    switch(context_->token_type)
    {
    case _C_STRING:
    {
        t_minson_string *string = m_new(t_minson_string, 1);
        object = m_pcast(t_minson_object *, string);
        
        size_t value_size = minsonStringBuilderSize(context_->token_value);
        char *value = m_new(char, value_size + 1);
        minsonBuildString(context_->token_value, value);
        value[value_size] = '\0';
        
        object->type = C_MINSON_STRING;
        string->value = value;
        break;
    }
    case _C_BASIC:
        object = __build_basic(context_);
        if(object == NULL) goto __fail;
        break;
    
    case _C_ARRAY_BEGIN:
    {
        t_minson_array *array = minsonCreateArray(NULL);
        object = m_pcast(t_minson_object *, array);
        
        for(;;)
        {
            if(! __next_token(context_))
            {
                minsonDeleteArray(array); goto __fail;
            }
            
            t_minson_object *subobject = __build_object(context_);
            if(subobject == NULL) break;
            
            minsonDelete(minsonArrayRemove(array, subobject->name));
            minsonArrayAdd(array, subobject);
        }
        
        if(context_->token_type != _C_ARRAY_END ||
           minson_error_code != MINSON_ERROR_OK)
        {
            minsonDeleteArray(array);
            
            if(minson_error_code == MINSON_ERROR_OK)
            {
                minson_error_code = MINSON_ERROR_WRONG_SYNTAX;
                minson_error_line = context_->at_line;
                minson_error_column = context_->at_column;
            }
            goto __fail;
        }
        break;
    }
    default:
        minson_error_code = MINSON_ERROR_WRONG_SYNTAX;
        minson_error_line = context_->at_line;
        minson_error_column = context_->at_column;
        goto __fail;
    } //- switch(token type)
    
    object->name = name__;
    return object;
    
__fail:
    if(name__ != NULL) m_delete(name__);
    return NULL;
}


static t_minson_object *__get_object(struct _base_context *context_, bool hardway_)
{
    if(context_ == NULL) return NULL;
    if(! __next_token(context_)) return NULL;
    if(context_->token_type == _C_END) return NULL;
    
    t_minson_object *object = __build_object(context_);
    if(object == NULL)
    {
        if(minson_error_code == MINSON_ERROR_OK)
        {
            minson_error_code = MINSON_ERROR_WRONG_SYNTAX;
            minson_error_line = context_->at_line;
            minson_error_column = context_->at_column;
        }
        return NULL;
    }
    
    if(! hardway_) return object;
    
    if(__next_token(context_))
    {
        if(context_->token_type == _C_END)
            return object;
        else
        {
            minson_error_code = MINSON_ERROR_WRONG_SYNTAX;
            minson_error_line = context_->at_line;
            minson_error_column = context_->at_column;
        }
    }
    
    minsonDelete(object);
    return NULL;
}


t_minson_object *minsonParse(char const *str_)
{
    return minsonParse2(str_, str_ + strlen(str_));
}


t_minson_object *minsonParse2(char const *str_begin_, char const *str_end_)
{
    struct _t_string_context *context = _new_parse_str(str_begin_, str_end_);
    
    t_minson_object *object = __get_object(
        m_pcast(struct _base_context *, context), true
    );
    
    _delete_parse_str(context);
    return object;
}


t_minson_object *minsonParseFile(char const *filename_)
{
    if(filename_ == NULL) return NULL;
    FILE *stream = fopen(filename_, "rt");
    if(stream == NULL) return NULL;
    
    t_minson_object *object = minsonParseStream(stream);
    
    fclose(stream);
    return object;
}


t_minson_object *minsonParseStream(FILE *stream_)
{
    struct _t_stream_context *context = _new_parse_stream(stream_);
    
    t_minson_object *object = __get_object(
        m_pcast(struct _base_context *, context), false
    );
    
    _delete_parse_stream(context);
    return object;
}

#ifndef _OFFNET_MINSON_OBJECT_HPP
#define _OFFNET_MINSON_OBJECT_HPP

#if __cplusplus < 201103L
# error "MinSON C++ library required at least C++11!"
#endif

#include <minson.h>
#include <string>

namespace minson
{


struct exception {};

/* _Исключение_. Генерируется, когда в конструктор передан нулевой указатель,
 * или внутри экземпляра класса была попытка взаимодействия с таковым указателем. */
struct null_pointer_exception : exception {};

/* _Исключение_. Генерируется при попытке привести MinSON-объект к типу данных,
 * которому он не соответствует. */
struct cast_error : exception {};

/* _Исключение_. Генерируется при попытке доступа к несуществующему элементу массива. */
struct not_found_exception : exception {};

/* _Исключение_. Генерируется при ошибке разбора.
 * - `code` - тип ошибки; одна из констант `MINSON_ERROR_...`.
 * - `line`, `column` - позиция символа во входных данных, где была обнаружена ошибка. */
struct parse_error : exception
{
    int code, line, column;
    parse_error(int code_, int line_, int column_) : code(code_), line(line_), column(column_) {}
};

/* _Исключение_. Генерируется, если обнаружена ошибка при сериализации.
 * - `stop_size` - размер результата, на котором была обнаружена ошибка. */
struct serialize_error : exception
{
    size_t stop_size;
    explicit serialize_error(size_t stop_size_) : stop_size(stop_size_) {}
};


/* Класс MinSON-объекта. */
class object
{
    friend class iterator;
    friend class array;
    friend class string_builder;
    
private:
    t_minson_object *__obj;
    bool __is_own;
    
    explicit object(t_minson_object *obj_, bool is_own_) /*throw(null_pointer_exception)*/ :
        __obj(obj_), __is_own(is_own_)
    {
        if(! __obj) throw null_pointer_exception();
    }
    
public:
    t_minson_object *__get_obj() const {   return __obj;   }
    
    object(t_minson_object *cp) :
        object(minsonDuplicate(cp), true)
    {}
    object(object const &cp) :
        __obj(cp.__obj), __is_own(cp.__is_own)
    {   if(__is_own) __obj = minsonDuplicate(__obj);   }
    object(object &&mv) :
        __obj(mv.__obj), __is_own(mv.__is_own)
    {   mv.__is_own = false;   }
    ~object()
    {   if(__is_own) minsonDelete(__obj);   }
    
    object &operator =(object const &cp)
    {
        if(__is_own) minsonDelete(__obj);
        __obj = cp.__is_own ? minsonDuplicate(cp.__obj) : cp.__obj;
        __is_own = cp.__is_own;
        return *this;
    }
    object &operator =(object &&mv)
    {
        if(__is_own) minsonDelete(__obj);
        __obj = mv.__obj; __is_own = mv.__is_own;
        mv.__is_own = false;
        return *this;
    }
    
    static object wrap(t_minson_object *wrp)
    {   return object(wrp, true);   }
    
    static object string(std::string const &value_, std::string const &name_="")
    {   return wrap(minsonCreateString( name_.c_str(), value_.c_str() ));   }
    
    static object number(int64_t value_, std::string const &name_="")
    {   return wrap(minsonCreateNumber( name_.c_str(), value_ ));   }
    
    static object floatNumber(double value_, std::string const &name_="")
    {   return wrap(minsonCreateFloatNumber( name_.c_str(), value_ ));   }
    
    static object boolean(bool value_, std::string const &name_="")
    {   return wrap(minsonCreateBoolean( name_.c_str(), (uint8_t)(value_ ? 1: 0) ));   }
    
    static object null(std::string const &name_="")
    {   return wrap(minsonCreateNull( name_.c_str() ));   }
    
    bool hasName() const
    {   return (bool)(__obj->name);   }
    
    std::string name() const
    {
        char const *tmp__ = __obj->name;
        return tmp__ ? std::string(tmp__) : std::string();
    }
    
    operator std::string() const /*throw(cast_error)*/
    {
        char const *tmp__ = minsonGetString(__obj);
        if(! tmp__) throw cast_error();
        return std::string(tmp__);
    }
    operator int64_t() const /*throw(cast_error)*/
    {
        int64_t tmp__;
        if(! minsonGetNumber(__obj, &tmp__)) throw cast_error();
        return tmp__;
    }
    operator double() const /*throw(cast_error)*/
    {
        double tmp__;
        if(! minsonGetFloatNumber(__obj, &tmp__)) throw cast_error();
        return tmp__;
    }
    operator bool() const /*throw(cast_error)*/
    {
        uint8_t tmp__;
        if(! minsonGetBoolean(__obj, &tmp__)) throw cast_error();
        return(tmp__ != 0);
    }
    
    bool isNull() const
    {   return(minsonIsNull(__obj) != 0);   }
};


} // namespace minson

#endif // _OFFNET_MINSON_OBJECT_HPP

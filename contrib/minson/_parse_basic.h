#ifndef __MINSON_PARSE_BASIC_H
#define __MINSON_PARSE_BASIC_H

#include "_parse.h"
#include <stdint.h>


enum _c_basic_type
{
    _C_BASIC_NUMBER, _C_BASIC_FLOAT,
    _C_BASIC_TRUE, _C_BASIC_FALSE, _C_BASIC_NULL,
    _C_BASIC_WRONG_TYPE
};


enum _c_basic_type _detect_basic_type(char const *value_);

int /*nix*/ _parse_number(char const *value_, int64_t *out_);
int /*nix*/ _parse_float(char const *value_, double *out_);


#endif // __MINSON_PARSE_BASIC_H

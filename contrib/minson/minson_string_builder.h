#ifndef _OFFNET_MINSON_STRING_BUILDER_H
#define _OFFNET_MINSON_STRING_BUILDER_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


/* Объект строкового буфера. Накапливает отрезки для дальнейшего формирования строки. */
typedef struct t_minson_string_builder t_minson_string_builder;


/* Создаёт объект строкового буфера,
 * который следует удалять с помощью функции `minsonDeleteStringBuilder`. */
t_minson_string_builder *minsonCreateStringBuilder();
/* Деинициализирует и удаляет объект строкового буфера. */
void minsonDeleteStringBuilder(t_minson_string_builder *this_);

/* Поместить отрезок `str_` размера `str_size_` в строковый буфер.
 * Память, используемая отрезком, должна быть выделена с помощью функции `malloc`.
 * Владение этой памятью переходит к буферу. */
void minsonStringBuilderMove(t_minson_string_builder *this_,
                             char const *str_, size_t str_size_);
/* Поместить символ `ch_` в строковый буфер. */
void minsonStringBuilderPutChar(t_minson_string_builder *this_, char ch_);

/* Возвращает размер строки, формируемой из накопленных отрезков. */
size_t minsonStringBuilderSize(t_minson_string_builder *this_);

/* Записывает полученную строку в `out_string_`, размер которого должен быть не менее,
 * чем значение, возвращаемое функцией `minsonStringBuilderSize`.
 * Завершающий ноль _НЕ_ ставится. */
void minsonBuildString(t_minson_string_builder *this_, char *out_string_);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_MINSON_STRING_BUILDER_H

#ifndef __M_H
#define __M_H

#ifdef _GNU_SOURCE
# include <malloc.h>
# include <string.h>
#else
# define _GNU_SOURCE
# include <malloc.h>
# include <string.h>
# undef _GNU_SOURCE
#endif


#define m_new(type, count) (type *)malloc(sizeof(type) * (count))
#define m_delete(p) free((void *)(p))

#define m_cast(type, m) (*((type *)(void *)&(m)))
#define m_pcast(ptype, p) ((ptype)(void *)(p))

#define m_strdup(str) (((str) == NULL)? NULL : strdup(str))


#endif // __M_H

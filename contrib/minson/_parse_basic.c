#include "_parse_basic.h"


enum _c_basic_type _detect_basic_type(char const *value_)
{
    if(value_[0] == 'y' || value_[0] == 'Y')
    {
        if(value_[1] == '\0') return _C_BASIC_TRUE;
    }
    else if(value_[0] == 'n' || value_[0] == 'N')
    {
        if(value_[1] == '\0') return _C_BASIC_FALSE;
    }
    else if(value_[0] == '*')
    {
        if(value_[1] == '\0') return _C_BASIC_NULL;
    }
    else
    {
        if(value_[0] == '+' || value_[0] == '-') ++value_;
        
        if(value_[0] == 'b' || value_[0] == 'o' || value_[0] == 'x')
            return _C_BASIC_NUMBER;
        
        for(; *value_ != '\0'; ++value_)
        {
            if(*value_ == '.') return _C_BASIC_FLOAT;
            if(*value_ < '0' || '9' < *value_) return _C_BASIC_WRONG_TYPE;
        }
        
        return _C_BASIC_NUMBER;
    }
    return _C_BASIC_WRONG_TYPE;
}


int /*nix*/ _parse_number(char const *value_, int64_t *out_)
{
    int8_t sign = 1;
    
    if(value_[0] == '+') {   ++value_;   }
    else if(value_[0] == '-') {   sign = -1; ++value_;   }
    
    int8_t base = 10;
    
    if(value_[0] == 'b') {   base = 2; ++value_;   }
    else if(value_[0] == 'o') {   base = 8; ++value_;   }
    else if(value_[0] == 'x') {   base = 16; ++value_;   }
    
    uint64_t unsigned_value = 0;
    
    for(; *value_ != '\0'; ++value_)
    {
        int8_t digit = *value_ - '0';
        
        switch(base)
        {
        case 2:
            if(*value_ != '0' && *value_ != '1') return __PARSE_WRONG_VALUE;
            break;
            
        case 8:
            if(*value_ < '0' || '7' < *value_) return __PARSE_WRONG_VALUE;
            break;
            
        case 10:
            if(*value_ < '0' || '9' < *value_) return __PARSE_WRONG_VALUE;
            break;
            
        case 16:
            if('0' <= *value_ && *value_ <= '9')
            {   break;   }
            else if('a' <= *value_ && *value_ <= 'f')
            {
                digit = *value_ - 'a' + 10; break;
            }
            else if('A' <= *value_ && *value_ <= 'F')
            {
                digit = *value_ - 'A' + 10; break;
            }
            else
            {   return __PARSE_WRONG_VALUE;   }
        }
        
        unsigned_value = unsigned_value * base + digit;
    }
    
    *out_ = sign * (int64_t)unsigned_value;
    return __PARSE_OK;
}


static int /*nix*/ __parse_float_exponent(char const *value_,
                                          int8_t *out_sign_, uint16_t *out_value_)
{
    *out_sign_ = 1;
    
    if(value_[0] == '+') {   ++value_;   }
    else if(value_[0] == '-') {   *out_sign_ = -1; ++value_;   }
    
    *out_value_ = 0;
    
    for(; *value_ != '\0'; ++value_)
    {
        if(*value_ < '0' || '9' < *value_) return __PARSE_WRONG_VALUE;
        
        *out_value_ = *out_value_ * 10 + *value_ - '0';
    }
    return __PARSE_OK;
}


static void __exponentation(double *value_,
                            int8_t exponent_sign_, uint16_t exponent_)
{
    if(exponent_ == 0) return;
    
    register double mult = 1.0, mult_accum = (exponent_sign_ > 0)? 10.0 : 0.1;
    
    while(exponent_ > 1)
    {
        if((exponent_ & 0x1) != 0)
        {
            mult *= mult_accum;
        }
        mult_accum *= mult_accum;
        
        exponent_ >>= 1;
    }
    mult *= mult_accum;
    
    *value_ *= mult;
    return;
}


int /*nix*/ _parse_float(char const *value_, double *out_)
{
    int8_t sign = 1;
    
    if(value_[0] == '+') {   ++value_;   }
    else if(value_[0] == '-') {   sign = -1; ++value_;   }
    
    double unsigned_value = 0.0, denum_base = 1.0;
    
    for(; *value_ != '\0'; ++value_)
    {
        if(*value_ == '.')
        {
            if(denum_base < 1.0) return __PARSE_WRONG_VALUE;
            
            denum_base = 0.1;
            continue;
        }
        else if(*value_ == 'e' || *value_ == 'E')
        {
            int8_t exponent_sign = 1;
            uint16_t exponent_value = 0;
            
            int r = __parse_float_exponent(++value_, &exponent_sign, &exponent_value);
            if(r == __PARSE_WRONG_VALUE) return __PARSE_WRONG_VALUE;
            
            __exponentation(&unsigned_value, exponent_sign, exponent_value);
            break;
        }
        else if(*value_ < '0' || '9' < *value_)
        {   return __PARSE_WRONG_VALUE;   }
        
        int8_t digit = *value_ - '0';
        
        if(denum_base >= 1.0)
            unsigned_value = unsigned_value * 10.0 + digit;
        else
        {
            unsigned_value += digit * denum_base;
            denum_base *= 0.1;
        }
    }
    
    *out_ = sign * unsigned_value;
    return __PARSE_OK;
}

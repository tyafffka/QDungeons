#ifndef _QDUNGEONS_PLAYER_ENTITY_HPP
#define _QDUNGEONS_PLAYER_ENTITY_HPP

#include "game_model/base_entity.hpp"


/* Персонаж игрока -- сущность, которой управляет сам игрок. */
class PlayerEntity : public BaseEntity
{
protected:
    Ogre::Light *_player_light = nullptr;
    b2Vec2 _velocity;
    b2Rot _direction;
    bool _is_move_animated = false;
    
    void createFrontend() SYNC_ENDS override;
    
public:
    enum c_directions : uint8_t
    {
        NO_DIRECTION=0, FORWARD=0x01, BACK=0x02, LEFT=0x04, RIGHT=0x08
    };
    
    PlayerEntity();
    ~PlayerEntity() SYNC_ENDS;
    
    // ---
    
    void onFrame(float frame_time_ /*sec*/) SYNC_ENDS override;
    void onPhysics() override;
    
    void attach(b2World &world_, float x_, float y_) override;
    
    // ---
    
    /* Персонаж игрока направляется в сторону `direction_`
     * (комбинация флагов `c_directions`). */
    void movePlayer(uint8_t direction_);
    /* Установка угла поворота персонажа игрока. */
    void rotatePlayer(b2Rot r_);
    
    bool isMove() const
    {   return(_velocity.x != .0f || _velocity.y != .0f);   }
};


#endif // _QDUNGEONS_PLAYER_ENTITY_HPP

#include "player.hpp"
#include "game_model/game_model.hpp"
#include "libutils/application.hpp"
#include "libutils/values.hpp"
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreLight.h>
#include <Box2D/Dynamics/b2World.h>
#include <Box2D/Collision/Shapes/b2CircleShape.h>

METATYPE_CLASS(PlayerEntity);


static LibUtils::GlobalMetadata<float> __player_speed{"player_speed"};
static LibUtils::GlobalMetadata<float> __player_radius{"player_radius"};
static LibUtils::GlobalMetadata<float> __light_height{"player_light_height"};
static LibUtils::GlobalMetadata<t_attenuation> __light_attenuation{"player_light_attenuation"};
static LibUtils::GlobalMetadata<t_color> __yellow{"yellow"};


void PlayerEntity::createFrontend() SYNC_ENDS
{
    BaseEntity::createFrontend();
    
    _player_light = LibUtils::sceneManager->createLight();
    _player_light->setType(Ogre::Light::LT_POINT);
    _player_light->setDiffuseColour(*__yellow);
    _player_light->setSpecularColour(*__yellow);
    __light_attenuation->applyTo(_player_light);
    _player_light->setPosition(Ogre::Vector3{.0f, .0f, *__light_height});
    _player_light->setVisible(isVisible);
    
    node->attachObject(_player_light);
    
    onPlayerAppeared(this);
    return;
}


PlayerEntity::PlayerEntity()
{
    _velocity.Set(.0f, .0f); _direction.Set(.0f);
    return;
}


PlayerEntity::~PlayerEntity() SYNC_ENDS
{
    if(_player_light) LibUtils::sceneManager->destroyLight(_player_light);
    return;
}

// --- *** ---


void PlayerEntity::onFrame(float frame_time_) SYNC_ENDS
{
    BaseEntity::onFrame(frame_time_);
    
    if(isMove() && ! _is_move_animated)
    {
        Ogre::AnimationState *animation = _entity->getAnimationState("walk");
        if(animation != nullptr)
        {
            animation->setEnabled(true);
            animation->setTimePosition(.0f);
            animation->setLoop(true);
        }
        
        _is_move_animated = true;
    }
    else if(! isMove() && _is_move_animated)
    {
        Ogre::AnimationState *animation = _entity->getAnimationState("walk");
        if(animation != nullptr) animation->setEnabled(false);
        
        _is_move_animated = false;
    }
    return;
}


void PlayerEntity::onPhysics()
{
    if(isMove()) entityBody->SetLinearVelocity(_velocity);
    return;
}


void PlayerEntity::attach(b2World &world_, float x_, float y_)
{
    BaseEntity::attach(world_, x_, y_);
    
    b2BodyDef body_def;
    body_def.type = b2_dynamicBody;
    body_def.position.Set(x_, y_);
    body_def.fixedRotation = true;
    
    b2CircleShape body_shape;
    body_shape.m_radius = *__player_radius;
    
    entityBody = world_.CreateBody(&body_def);
    entityBody->CreateFixture(&body_shape, .0f);
    return;
}

// --- *** ---


void PlayerEntity::movePlayer(uint8_t direction_)
{
    if(direction_ == NO_DIRECTION)
    {   _velocity.Set(.0f, .0f);   }
    else
    {
        switch(direction_)
        {
        case RIGHT:
            _direction.Set(.0f); break;
        case (FORWARD | RIGHT):
            _direction.Set((float)M_PI_4); break;
        case FORWARD:
            _direction.Set((float)M_PI_2); break;
        case (FORWARD | LEFT):
            _direction.Set((float)(3.0 * M_PI_4)); break;
        case LEFT:
            _direction.Set((float)M_PI); break;
        case (BACK | LEFT):
            _direction.Set((float)(5.0 * M_PI_4)); break;
        case BACK:
            _direction.Set((float)(3.0 * M_PI_2)); break;
        case (BACK | RIGHT):
            _direction.Set((float)(7.0 * M_PI_4)); break;
        default: return;
        }
        
        b2Rot r = b2Mul(_direction, entityBody->GetTransform().q);
        _velocity.x = (*__player_speed) * r.c;
        _velocity.y = (*__player_speed) * r.s;
    }
    
    entityBody->SetLinearVelocity(_velocity);
    return;
}


void PlayerEntity::rotatePlayer(b2Rot r_)
{
    entityBody->SetTransform(entityBody->GetPosition(), r_.GetAngle());
    
    if(isMove())
    {
        b2Rot r = b2Mul(_direction, r_);
        _velocity.x = (*__player_speed) * r.c;
        _velocity.y = (*__player_speed) * r.s;
        entityBody->SetLinearVelocity(_velocity);
    }
    return;
}

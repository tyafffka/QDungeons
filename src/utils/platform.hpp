#ifndef __UTILS_PLATFORM_HPP
#define __UTILS_PLATFORM_HPP

/* Модуль общих функций, реализация которых зависит от операционной системы
 * (платформы). Поддерживаются следующие системы: MS Windows, Linux-based,
 * Apple MacOS. Все строки принимаются и возвращаются в кодировке `UTF-8`. */

#include <string>
#include <vector>


enum file_type
{
    FILE_NOT_EXIST, FILE_TYPE_DIRECTORY, FILE_TYPE_REGULAR, FILE_TYPE_SPECIAL
};


/* Преобразовывает строку `path_` следующим образом: заменяет разделители
 * на используемые системой, устраняет дубликаты разделителей, отбрасывает
 * разделитель в конце. Возвращает результат преобразования. */
std::string normalizePath(std::string const &path_) noexcept;

/* Проверяет объект файловой системы на существование и возвращает,
 * является ли он обычным файлом, папкой или особым объектом. */
file_type getFileType(std::string const &path_) noexcept;

/* Возвращает список имён файлов из папки `root_`, выбранных по маске `mask_`.
 * - `root_` -- полный путь к папке (без разделителя в конце);
 * - `mask_` -- маска имени файла в виде шаблона оболочки (без разделителей);
 * - `for_subdirs_` -- выбирать ли подпапки вместо файлов;
 * - `recursive_` -- производить ли рекурсивный поиск во всех подпапках;
 *   в этом случае список будет содержать относительные пути к файлам. */
std::vector<std::string> findInDir(std::string const &root_, std::string const &mask_,
                                   bool for_subdirs_, bool recursive_=false) noexcept;

/* Создаёт папку, если она ещё не создана.
 * - `dir_` -- полный путь к создаваемой папке (без разделителя в конце);
 * - `recursive_` -- создавать ли все вышестоящие папки.
 * Возвращает `true`, если папка была успешно создана или уже существовала. */
bool makeDir(std::string const &dir_, bool recursive_=false) noexcept;

/* Удаляет папку, если она существует, и возвращает успешность операции
 * или `true`, если папки не существовало до этого.
 * - `dir_` -- полный путь к удаляемой папке (без разделителя в конце);
 * - `force_` -- удалять ли папку, если она не пустая, иначе удаления
 *   не произойдёт. */
bool removeDir(std::string const &dir_, bool force_=false) noexcept;

/* Удаляет файл, если он существует, и возвращает успешность операции
 * или `true`, если файла не существовало до этого.
 * - `file_` -- полный путь к файлу. */
bool removeFile(std::string const &file_) noexcept;

/* Возвращает путь к папке, где расположен исполняемый файл,
 * из которого запущен текущий процесс. */
std::string const &executableOrigin() noexcept;


#endif // __UTILS_PLATFORM_HPP

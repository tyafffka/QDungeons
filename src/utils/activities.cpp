#include "activities.hpp"
#include "memory/transient_ref.hpp"


memory::allocator *activityAllocator = &memory::std_allocator;

thread_local BaseActivity::__t_context *BaseActivity::__current_context = nullptr;

// --- *** ---


BaseActivity::__t_context::__t_context() noexcept :
    time(), is_cooldown(false), is_finish_all(false)
{   BaseActivity::__current_context = this;   }


BaseActivity::__t_context::~__t_context() noexcept
{   BaseActivity::__current_context = nullptr;   }


void BaseActivity::__add_activity(BaseActivity *activity_) noexcept
{
    activityAllocator->destroy(__next); __next = activity_;
}


void BaseActivity::__run()
{
    memory::transient_ref<BaseActivity> next = nullptr;
    for(;;)
    {
        if(__context->is_cooldown.exchange(true)) nanodelay();
        __context->time.update();
        applyEvents();
        
        if(__context->is_finish_all)
        {
            if(__is_active) onStop();
            return;
        }
        else if(__next != nullptr)
        {
            next = memory::trans(__next, activityAllocator);
            __next = nullptr;
            
            if(__is_active)
            {   onStop(); __is_active = false;   }
            
            next->__run();
            if(isFinish()) return;
        }
        else if(__is_finish)
        {
            if(__is_active) onStop();
            return;
        }
        else if(! __is_active)
        {
            __is_active = true; onStart(next.base());
            next = nullptr;
        }
        else
        {   onLoop();   }
    }
}

// --- *** ---


BaseActivity::BaseActivity() :
    __context(__current_context), __next(nullptr),
    __is_active(false), __is_finish(false)
{
    if(__context == nullptr) throw std::runtime_error(
        "using activities out of runActivity()"
    );
}


BaseActivity::~BaseActivity() noexcept
{   activityAllocator->destroy(__next);   }


void BaseActivity::finish() noexcept
{   __is_finish = true;   }


void BaseActivity::finishAll() noexcept
{   __context->is_finish_all = true;   }


bool BaseActivity::isActive() noexcept
{   return (bool)__is_active;   }


bool BaseActivity::isFinish() noexcept
{   return((bool)__is_finish || (bool)__context->is_finish_all);   }


void BaseActivity::cancelCooldown() noexcept
{   __context->is_cooldown = false;   }


TTimeCounter const &BaseActivity::timeCounter() noexcept
{   return __context->time;   }

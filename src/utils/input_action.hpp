#ifndef __UTILS_INPUT_ACTION_HPP
#define __UTILS_INPUT_ACTION_HPP

#include "memory/map.hpp"
#include "memory/vector.hpp"
#include <initializer_list>
#include <stdint.h>


/* Базовый класс для абстракции ввода в виде так называемого "действия".
 * Позволяет назначить несколько клавиш и осей действию через слоты.
 * От наследного класса требуется определить методы проверки
 * нажатия клавиш (`getKeyPushed`) и значений осей (`getAxisValue`),
 * а также количество клавиш (`getKeysCount`) и осей (`getAxesCount`). */
class BaseInputAction
{
    friend class InputConflictsContext;
    
public:
    enum c_key_type : uint8_t // Тип клавиши или оси, назначаемой слоту:
    {
        C_NO_KEY, // не назначено -- пустой слот;
        C_KEY, // клавиша;
        C_POSITIVE_SEMIAXIS, // положительное направление оси;
        C_NEGATIVE_SEMIAXIS, // отрицательное направление оси.
    };
    
    typedef uint16_t t_key_num;
    typedef uintmax_t t_slot_id;
    
private:
    struct __t_slot
    {
        t_slot_id id;
        c_key_type type;
        t_key_num key;
        
        bool operator <(__t_slot const &r_) const noexcept {   return(id < r_.id);   }
        bool operator <(t_slot_id r_) const noexcept {   return(id < r_);   }
    };
    
    memory::vector<__t_slot> __slots;
    
    __t_slot const *__find_slot(t_slot_id slot_id_) const noexcept;
    
protected:
    float _value;
    bool _is_value_changed;
    
public:
    explicit BaseInputAction(memory::allocator *alloc_=&memory::std_allocator) noexcept :
        __slots(alloc_), _value(.0f), _is_value_changed(false)
    {}
    virtual ~BaseInputAction() noexcept = default;
    
    /* Должен возвращать количество клавиш, доступных для указанного слота. */
    virtual t_key_num getKeysCount(t_slot_id slot_id_) const = 0;
    /* Должен возвращать количество осей, доступных для указанного слота. */
    virtual t_key_num getAxesCount(t_slot_id slot_id_) const = 0;
    /* Должен проверять, нажата ли клавиша. */
    virtual bool getKeyPushed(t_slot_id slot_id_, t_key_num key_) const = 0;
    /* Должен возвращать текущее значение оси от `-1.0` до `1.0`. */
    virtual float getAxisValue(t_slot_id slot_id_, t_key_num axis_) const = 0;
    
    /* Создаёт новый слот с указанным ID.
     * ID должен быть уникальным в рамках действия (не проверяется). */
    void addSlot(t_slot_id slot_id_);
    /* Удаляет слот с указанным ID, если есть. */
    void removeSlot(t_slot_id slot_id_) noexcept;
    /* Назначает клавишу или ось указанному слоту. */
    void setKey(t_slot_id slot_id_, c_key_type type_, t_key_num key_) noexcept;
    /* Убирает назначение клавиши / оси из указанного слота. */
    void resetKey(t_slot_id slot_id_) noexcept;
    /* Возвращает тип клавиши или оси, назначенной указанному слоту. */
    c_key_type getKeyType(t_slot_id slot_id_) const noexcept;
    /* Возвращает номер клавиши или оси, назначенной указанному слоту. */
    t_key_num getKey(t_slot_id slot_id_) const noexcept;
    
    /* Проверяет, что все кнопки и оси, доступные для слота,
     * отжаты и находятся в исходном положении. */
    bool isInputSilenced(t_slot_id slot_id_) const;
    /* Производит "захват" первой клавиши или оси, которая нажата или отклонена.
     * Присваивает её указанному слоту и возвращает `true`.
     * Если захват не произведён, возвращает `false`. */
    bool catchKey(t_slot_id slot_id_);
    /* Агрегирует текущее состояние клавиш и осей, назначенных слотам,
     * и обновляет в соответствии с ним значение действия. */
    void update();
    
    /* Возвращает текущее значение в сыром виде от `0` до `1.0`. */
    float getValue() const noexcept
    {   return _value;   }
    
    /* Возвращает результирующее значение в паре с `neg_`, имеющим
     * противоположный смысл по сравнению с данным действием. */
    float fullValue(BaseInputAction const &neg_) const noexcept
    {   return _value - neg_._value;   }
    
    /* Возвращает, нажата ли любая назначенная клавиша или ось. */
    bool isActivated() const noexcept
    {   return(_value != .0f);   }
    
    /* Возвращает, была ли только что нажата клавиша или ось при последнем обновлении. */
    bool isJustActivated() const noexcept
    {   return(_value != .0f && _is_value_changed);   }
    
    /* Возвращает, была ли только что отпущена клавиша или ось при последнем обновлении. */
    bool isJustDeactivated() const noexcept
    {   return(_value == .0f && _is_value_changed);   }
};


/* Вспомогательный класс для поиска конфликтов в назначенных клавишах и осях
 * между действиями. Поиск осуществляется в два шага: сначала добавляются
 * необходимые слоты всех рассмативаемых действий, затем проверяется каждый
 * слот каждого действия на наличие совпадения с другими действиями. */
class InputConflictsContext
{
private:
    typedef BaseInputAction::c_key_type c_key_type;
    typedef BaseInputAction::t_key_num t_key_num;
    typedef BaseInputAction::t_slot_id t_slot_id;
    
    struct __t_ident
    {
        c_key_type type;
        t_key_num key;
        
        bool operator <(__t_ident const &r_) const noexcept
        {
            if((uint8_t)type < (uint8_t)r_.type) return true;
            if((uint8_t)type > (uint8_t)r_.type) return false;
            return(key < r_.key);
        }
    };
    
    memory::map<__t_ident, bool> __has_repeat;
    
public:
    explicit InputConflictsContext(memory::allocator *alloc_=&memory::std_allocator) noexcept :
        __has_repeat(alloc_)
    {}
    
    /* Добавляет к рассмотрению список слотов `slots_` действия `action_`. */
    void addAction(BaseInputAction const *action_,
                   std::initializer_list<t_slot_id> slots_);
    /* Возвращает, есть ли совпадения у указанного слота указанного действия. */
    bool isConflict(BaseInputAction const *action_, t_slot_id slot_id_) const noexcept;
};


#endif //- __UTILS_INPUT_ACTION_HPP

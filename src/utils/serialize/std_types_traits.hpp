#ifndef _SERIALIZE__STD_TYPES_TRAITS_HPP
#define _SERIALIZE__STD_TYPES_TRAITS_HPP

#include "serialize.hpp"
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <array>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>

namespace serialize
{


// --- Integral ---

template<>
struct type_traits<bool> : value_traits
{
    using type = bool;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_bool(value_);   }
    
    static type get(value_decoder &decoder_)
    {   return decoder_.get_bool();   }
};

template<>
struct type_traits<char> : value_traits
{
    using type = char;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_char(value_);   }
    
    static type get(value_decoder &decoder_)
    {   return decoder_.get_char();   }
};


template<typename T>
struct __integral_traits : value_traits
{
    using type = T;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_int((long long int)value_);   }
    
    static type get(value_decoder &decoder_)
    {   return (type)decoder_.get_int();   }
};

template<>
struct type_traits<signed char> : __integral_traits<signed char> {};

template<>
struct type_traits<unsigned char> : __integral_traits<unsigned char> {};

template<>
struct type_traits<short> : __integral_traits<short> {};

template<>
struct type_traits<unsigned short> : __integral_traits<unsigned short> {};

template<>
struct type_traits<int> : __integral_traits<int> {};

template<>
struct type_traits<unsigned int> : __integral_traits<unsigned int> {};

template<>
struct type_traits<long> : __integral_traits<long> {};

template<>
struct type_traits<unsigned long> : __integral_traits<unsigned long> {};

template<>
struct type_traits<long long> : __integral_traits<long long> {};

template<>
struct type_traits<unsigned long long> : __integral_traits<unsigned long long> {};


// --- Floating point ---

template<typename T>
struct __floating_point_traits : value_traits
{
    using type = T;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_float(value_);   }
    
    static type get(value_decoder &decoder_)
    {   return (type)decoder_.get_float();   }
};

template<>
struct type_traits<float> : __floating_point_traits<float> {};

template<>
struct type_traits<double> : __floating_point_traits<double> {};

template<>
struct type_traits<long double> : __floating_point_traits<long double> {};


// --- Strings ---

template<typename _Alloc>
struct type_traits<std::basic_string<
    char, std::char_traits<char>, _Alloc
>> : value_traits
{
    using type = std::basic_string<char, std::char_traits<char>, _Alloc>;
    
    static void put(value_encoder &encoder_, type const &value_)
    {   encoder_.put_string(value_);   }
    
    static type get(value_decoder &decoder_)
    {
        string_view v = decoder_.get_string(); return type(v.data(), v.size());
    }
};

template<typename _Alloc>
struct type_traits<std::basic_string<
    char16_t, std::char_traits<char16_t>, _Alloc
>> : value_traits
{
    using type = std::basic_string<char16_t, std::char_traits<char16_t>, _Alloc>;
    
    static void put(value_encoder &encoder_, type const &value_)
    {   encoder_.put_string16(value_);   }
    
    static type get(value_decoder &decoder_)
    {
        u16string_view v = decoder_.get_string16(); return type(v.data(), v.size());
    }
};

template<typename _Alloc>
struct type_traits<std::basic_string<
    char32_t, std::char_traits<char32_t>, _Alloc
>> : value_traits
{
    using type = std::basic_string<char32_t, std::char_traits<char32_t>, _Alloc>;
    
    static void put(value_encoder &encoder_, type const &value_)
    {   encoder_.put_string32(value_);   }
    
    static type get(value_decoder &decoder_)
    {
        u32string_view v = decoder_.get_string32(); return type(v.data(), v.size());
    }
};

template<>
struct type_traits<string_view> : value_traits
{
    using type = string_view;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_string(value_);   }
    
    //static type get(value_decoder &decoder_);
};

template<>
struct type_traits<u16string_view> : value_traits
{
    using type = u16string_view;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_string16(value_);   }
    
    //static type get(value_decoder &decoder_);
};

template<>
struct type_traits<u32string_view> : value_traits
{
    using type = u32string_view;
    
    static void put(value_encoder &encoder_, type value_)
    {   encoder_.put_string32(value_);   }
    
    //static type get(value_decoder &decoder_);
};


// --- Sequences ---

template<typename _Item, std::size_t _N>
struct type_traits<_Item[_N]> : sequence_traits
{
    using type = _Item[_N];
    using item_type = _Item;
    
    static _Item const *begin(_Item const *array_) {   return array_;   }
    static _Item const *end(_Item const *array_) {   return array_ + _N;   }
    static _Item const &item(_Item *p_) {   return *p_;   }
    
    // static void insert(type &array_, item_type &&item_);
};


template<typename T>
struct __std_sequence_traits : sequence_traits
{
    using type = T;
    using iterator = typename type::const_iterator;
    using item_type = typename type::value_type;
    
    static iterator begin(type const &seq_) {   return seq_.begin();   }
    static iterator end(type const &seq_) {   return seq_.end();   }
    static item_type const &item(iterator it_) {   return *it_;   }
    
    static void insert(type &seq_, item_type &&item_)
    {   seq_.push_back(std::move(item_));   }
};

template<typename _Item, typename _Alloc>
struct type_traits<std::vector<_Item, _Alloc>> :
    __std_sequence_traits<std::vector<_Item, _Alloc>>
{};

template<typename _Item, typename _Alloc>
struct type_traits<std::list<_Item, _Alloc>> :
    __std_sequence_traits<std::list<_Item, _Alloc>>
{};

template<typename _Item, typename _Alloc>
struct type_traits<std::deque<_Item, _Alloc>> :
    __std_sequence_traits<std::deque<_Item, _Alloc>>
{};


template<typename T>
struct __std_readonly_sequence_traits : sequence_traits
{
    using type = T;
    using iterator = typename type::const_iterator;
    using item_type = typename type::value_type;
    
    static iterator begin(type const &seq_) {   return seq_.begin();   }
    static iterator end(type const &seq_) {   return seq_.end();   }
    static item_type const &item(iterator it_) {   return *it_;   }
    
    // static void insert(type &seq_, item_type &&item_);
};

template<typename _Item, typename _Alloc>
struct type_traits<std::forward_list<_Item, _Alloc>> :
    __std_readonly_sequence_traits<std::forward_list<_Item, _Alloc>>
{};

template<typename _Item, std::size_t _N>
struct type_traits<std::array<_Item, _N>> :
    __std_readonly_sequence_traits<std::array<_Item, _N>>
{};


template<typename T>
struct __std_set_traits : sequence_traits
{
    using type = T;
    using iterator = typename type::const_iterator;
    using item_type = typename type::key_type;
    
    static iterator begin(type const &set_) {   return set_.begin();   }
    static iterator end(type const &set_) {   return set_.end();   }
    static item_type const &item(iterator it_) {   return *it_;   }
    
    static void insert(type &set_, item_type &&item_)
    {   set_.insert(std::move(item_));   }
};

template<typename _K, typename _Comp, typename _Alloc>
struct type_traits<std::set<_K, _Comp, _Alloc>> :
    __std_set_traits<std::set<_K, _Comp, _Alloc>>
{};

template<typename _K, typename _Comp, typename _Alloc>
struct type_traits<std::unordered_set<_K, _Comp, _Alloc>> :
    __std_set_traits<std::unordered_set<_K, _Comp, _Alloc>>
{};


// --- Maps ---

template<typename T>
struct __std_map_traits : map_traits
{
    using type = T;
    using iterator = typename type::const_iterator;
    using key_type = typename type::key_type;
    using value_type = typename type::mapped_type;
    
    static iterator begin(type const &map_) {   return map_.begin();   }
    static iterator end(type const &map_) {   return map_.end();   }
    static string_view key(iterator it_) {   return {it_->first.c_str(), it_->first.size()};   }
    static value_type const &value(iterator it_) {   return it_->second;   }
    
    static void insert(type &map_, string_view key_, value_type &&value_)
    {   map_.insert(std::make_pair(key_type(key_.data(), key_.size()), value_));   }
};

template<typename _Alloc_K, typename _V, typename _Comp, typename _Alloc>
struct type_traits<std::map<
    std::basic_string<char, std::char_traits<char>, _Alloc_K>, _V, _Comp, _Alloc
>> : __std_map_traits<std::map<
    std::basic_string<char, std::char_traits<char>, _Alloc_K>, _V, _Comp, _Alloc
>> {};

template<typename _Alloc_K, typename _V, typename _Comp, typename _Alloc>
struct type_traits<std::unordered_map<
    std::basic_string<char, std::char_traits<char>, _Alloc_K>, _V, _Comp, _Alloc
>> : __std_map_traits<std::unordered_map<
    std::basic_string<char, std::char_traits<char>, _Alloc_K>, _V, _Comp, _Alloc
>> {};


} //- namespace serialize

#endif //- _SERIALIZE__STD_TYPES_TRAITS_HPP

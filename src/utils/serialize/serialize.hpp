#ifndef _SERIALIZE__SERIALIZE_HPP
#define _SERIALIZE__SERIALIZE_HPP

#include <functional>
#include <stdexcept>
#include <type_traits>
#include "../memory/string.hpp"

namespace serialize
{

using memory::string_view;
using memory::u16string_view;
using memory::u32string_view;


struct serializable;

template<typename> struct type_traits;
struct value_traits;
struct sequence_traits;
struct map_traits;

struct value_encoder;
struct sequence_encoder;
struct map_encoder;

struct value_decoder;
struct sequence_decoder;
struct map_decoder;


template<int _L, int... _I>
struct __names : public __names<_L - 1, _L - 1, _I...>
{
    typedef __names<_L - 1, _L - 1, _I...> _Base;
    
    constexpr __names(char const str_[]) : _Base(str_) {}
};

template<int... _I>
struct __names<0, _I...>
{
    char const data[sizeof...(_I)];
    
    static constexpr bool is_identifier(char ch)
    {
        return(('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') ||
               ('0' <= ch && ch <= '9') || ch == '_');
    }
    
    constexpr __names(char const str_[]) :
        data{(is_identifier(str_[_I]) ? str_[_I] : '\0')...}
    {}
};

template<int _L>
constexpr __names<_L> __make_names(char const (&str_)[_L])
{   return __names<_L>(str_);   }


template<typename _Value, typename _Return=void>
using __enable_if_serializable = typename std::enable_if<(
    std::is_base_of<serializable, _Value>::value
), _Return>::type;

template<typename _Value, typename _Return=void>
using __enable_if_value_type = typename std::enable_if<(
    std::conditional<
        (! std::is_base_of<serializable, _Value>::value),
        std::is_base_of<value_traits, type_traits<_Value>>,
        std::false_type
    >::type::value
), _Return>::type;

template<typename _Value, typename _Return=void>
using __enable_if_sequence_type = typename std::enable_if<(
    std::conditional<
        (! std::is_base_of<serializable, _Value>::value),
        std::is_base_of<sequence_traits, type_traits<_Value>>,
        std::false_type
    >::type::value
), _Return>::type;

template<typename _Value, typename _Return=void>
using __enable_if_map_type = typename std::enable_if<(
    std::conditional<
        (! std::is_base_of<serializable, _Value>::value),
        std::is_base_of<map_traits, type_traits<_Value>>,
        std::false_type
    >::type::value
), _Return>::type;

// --- *** ---


struct serializable
{
    void serialize(value_encoder &&encoder_) const {}
    void serialize(map_encoder &&encoder_) const {}
    
    void deserialize(value_decoder &&decoder_) {}
    void deserialize(map_decoder &&decoder_) {}
    
#define define_serialize_fields(BaseClass, ... /*Fields*/) \
public: \
    void serialize(::serialize::map_encoder &&serialize__encoder_) const \
    { \
        BaseClass::serialize(std::move(serialize__encoder_)); \
        static constexpr auto serialize__names__ = ::serialize::__make_names(#__VA_ARGS__); \
        ::serialize::__serialize_fields( \
            std::move(serialize__encoder_), serialize__names__.data, __VA_ARGS__ \
        ); \
    } \
    \
    void serialize(::serialize::value_encoder &&encoder_) const \
    { \
        encoder_.as_map([this](::serialize::map_encoder &&m_encoder){ \
            this->serialize(std::move(m_encoder)); \
        }); \
    }
    
#define define_deserialize_fields(BaseClass, ... /*Fields*/) \
public: \
    void deserialize(::serialize::map_decoder &&serialize__decoder_) \
    { \
        BaseClass::deserialize(std::move(serialize__decoder_)); \
        static constexpr auto serialize__names__ = ::serialize::__make_names(#__VA_ARGS__); \
        ::serialize::__deserialize_fields( \
            std::move(serialize__decoder_), serialize__names__.data, __VA_ARGS__ \
        ); \
    } \
    \
    void deserialize(::serialize::value_decoder &&decoder_) \
    { \
        decoder_.as_map([this](::serialize::map_decoder &&m_decoder){ \
            this->deserialize(std::move(m_decoder)); \
        }); \
    }
    
#define serializable_fields(BaseClass, ... /*Fields*/) \
    define_serialize_fields(BaseClass, __VA_ARGS__) \
    define_deserialize_fields(BaseClass, __VA_ARGS__)
};


template<typename T> struct type_traits
{
    static_assert(std::is_same<T, void>::value,
                  "This type is not supported for serialization");
};

struct value_traits {};
// static void put(value_encoder &, T const &);
// static T get(value_decoder &);

struct sequence_traits {};
// typename item_type;
// static /*iterator*/ begin(T const &);
// static /*iterator*/ end(T const &);
// static item_type const &item(/*iterator*/ const &);
// static void insert(T &, item_type &&);

struct map_traits {};
// typename value_type;
// static /*iterator*/ begin(T const &);
// static /*iterator*/ end(T const &);
// static string_view key(/*iterator*/ const &);
// static value_type const &value(/*iterator*/ const &);
// static void insert(T &, string_view, value_type &&);

// --- *** ---


struct unsupported : std::domain_error
{
    unsupported() : std::domain_error("unsupported encode/decode operation") {}
};


struct value_encoder
{
    virtual void put_bool(bool value_) {   throw unsupported{};   }
    virtual void put_char(char value_) {   throw unsupported{};   }
    virtual void put_int(long long int value_) {   throw unsupported{};   }
    virtual void put_float(long double value_) {   throw unsupported{};   }
    virtual void put_string(string_view value_) {   throw unsupported{};   }
    virtual void put_string16(u16string_view value_) {   throw unsupported{};   }
    virtual void put_string32(u32string_view value_) {   throw unsupported{};   }
    
    virtual void as_sequence(std::function<void(sequence_encoder &&)> step_)
    {   throw unsupported{};   }
    
    virtual void as_map(std::function<void(map_encoder &&)> step_)
    {   throw unsupported{};   }
};

struct sequence_encoder
{
    virtual void put_sub(std::size_t i_, std::function<void(value_encoder &&)> step_)
    {   throw unsupported{};   }
};

struct map_encoder
{
    virtual void put_sub(string_view key_, std::function<void(value_encoder &&)> step_)
    {   throw unsupported{};   }
};


struct value_decoder
{
    virtual bool get_bool() {   throw unsupported{};   }
    virtual char get_char() {   throw unsupported{};   }
    virtual long long int get_int() {   throw unsupported{};   }
    virtual long double get_float() {   throw unsupported{};   }
    virtual string_view get_string() {   throw unsupported{};   }
    virtual u16string_view get_string16() {   throw unsupported{};   }
    virtual u32string_view get_string32() {   throw unsupported{};   }
    
    virtual void as_sequence(std::function<void(sequence_decoder &&)> step_)
    {   throw unsupported{};   }
    
    virtual void as_map(std::function<void(map_decoder &&)> step_)
    {   throw unsupported{};   }
};

struct sequence_decoder
{
    virtual std::size_t size() {   throw unsupported{};   }
    
    virtual void get_sub(std::size_t i_, std::function<void(value_decoder &&)> step_)
    {   throw unsupported{};   }
};

struct map_decoder
{
    virtual string_view next_key() {   throw unsupported{};   }
    
    virtual void get_sub(string_view key_, std::function<void(value_decoder &&)> step_)
    {   throw unsupported{};   }
};

// --- *** ---


template<typename _Value>
inline __enable_if_serializable<_Value>
    serialize(value_encoder &&encoder_, _Value const &value_)
{
    value_.serialize(std::move(encoder_));
}

template<typename _Value>
inline __enable_if_value_type<_Value>
    serialize(value_encoder &&encoder_, _Value const &value_)
{
    type_traits<_Value>::put(encoder_, value_);
}

template<typename _Value>
inline __enable_if_map_type<_Value> serialize(value_encoder &&, _Value const &);

template<typename _Value>
inline __enable_if_sequence_type<_Value>
    serialize(value_encoder &&encoder_, _Value const &value_)
{
    encoder_.as_sequence([&value_](sequence_encoder &&s_encoder)
    {
        std::size_t i = 0;
        auto it = type_traits<_Value>::begin(value_);
        auto it_end = type_traits<_Value>::end(value_);
        for(; it != it_end; ++i, ++it)
        {
            auto item = type_traits<_Value>::item(it);
            s_encoder.put_sub(i, [&item](value_encoder &&sub){
                serialize(std::move(sub), item);
            });
        }
    });
}

template<typename _Value>
inline __enable_if_map_type<_Value>
    serialize(value_encoder &&encoder_, _Value const &value_)
{
    encoder_.as_map([&value_](map_encoder &&m_encoder)
    {
        auto it = type_traits<_Value>::begin(value_);
        auto it_end = type_traits<_Value>::end(value_);
        for(; it != it_end; ++it)
        {
            string_view key = type_traits<_Value>::key(it);
            auto sub_value = type_traits<_Value>::value(it);
            m_encoder.put_sub(key, [&sub_value](value_encoder &&sub){
                serialize(std::move(sub), sub_value);
            });
        }
    });
}

// ---

inline void __serialize_fields(map_encoder &&, char const *) {}

template<typename _Value1, typename... _Values>
inline void __serialize_fields(map_encoder &&encoder_, char const *names_,
                               _Value1 const &value1_, _Values const &... values_)
{
    while(! *names_) ++names_;
    encoder_.put_sub(names_, [&value1_](value_encoder &&sub){
        serialize(std::move(sub), value1_);
    });
    while(*names_) ++names_;
    __serialize_fields(std::move(encoder_), names_, values_...);
}

// --- *** ---


template<typename _Value>
inline __enable_if_serializable<_Value, _Value>
    deserialize(value_decoder &&decoder_)
{
    _Value out; out.deserialize(std::move(decoder_));
    return out;
}

template<typename _Value>
inline __enable_if_value_type<_Value, _Value>
    deserialize(value_decoder &&decoder_)
{
    return type_traits<_Value>::get(decoder_);
}

template<typename _Value>
inline __enable_if_map_type<_Value, _Value> deserialize(value_decoder &&);

template<typename _Value>
inline __enable_if_sequence_type<_Value, _Value>
    deserialize(value_decoder &&decoder_)
{
    using item_type = typename type_traits<_Value>::item_type;
    
    _Value out; decoder_.as_sequence([&out](sequence_decoder &&s_decoder)
    {
        for(std::size_t i = 0, size = s_decoder.size(); i < size; ++i)
        {
            s_decoder.get_sub(i, [&out](value_decoder &&sub){
                type_traits<_Value>::insert(out, deserialize<item_type>(std::move(sub)));
            });
        }
    });
    return out;
}

template<typename _Value>
inline __enable_if_map_type<_Value, _Value>
    deserialize(value_decoder &&decoder_)
{
    using value_type = typename type_traits<_Value>::value_type;
    
    _Value out; decoder_.as_map([&out](map_decoder &&m_decoder)
    {
        for(string_view key = m_decoder.next_key();
            key.size() > 0; key = m_decoder.next_key())
        {
            m_decoder.get_sub(key, [&out, key](value_decoder &&sub){
                type_traits<_Value>::insert(out, key, deserialize<value_type>(std::move(sub)));
            });
        }
    });
    return out;
}

// ---

inline void __deserialize_fields(map_decoder &&, char const *) {}

template<typename _Value1, typename... _Values>
inline void __deserialize_fields(map_decoder &&decoder_, char const *names_,
                                 _Value1 &out_value1_, _Values &... out_values_)
{
    while(! *names_) ++names_;
    decoder_.get_sub(names_, [&out_value1_](value_decoder &&sub){
        out_value1_ = deserialize<_Value1>(std::move(sub));
    });
    while(*names_) ++names_;
    __deserialize_fields(std::move(decoder_), names_, out_values_...);
}


} //- namespace serialize

#include "std_types_traits.hpp"

#endif //- _SERIALIZE__SERIALIZE_HPP

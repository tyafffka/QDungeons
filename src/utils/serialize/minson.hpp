#ifndef _SERIALIZE__MINSON_HPP
#define _SERIALIZE__MINSON_HPP

#include "serialize.hpp"
#include <minson/minson.hpp>

namespace serialize
{


class minson_encoder : public value_encoder
{
    struct __sequence_encoder : sequence_encoder
    {
        minson::array &__array;
        
        explicit __sequence_encoder(minson::array &array_) : __array(array_) {}
        
        void put_sub(std::size_t i_, std::function<void(value_encoder &&)> step_) override
        {
            (void)i_;
            minson::object out = minson::object::null();
            step_(minson_encoder(out));
            __array.put(std::move(out));
        }
    };
    
    struct __map_encoder : map_encoder
    {
        minson::array &__array;
        
        explicit __map_encoder(minson::array &array_) : __array(array_) {}
        
        void put_sub(string_view key_, std::function<void(value_encoder &&)> step_) override
        {
            minson::object out = minson::object::null();
            step_(minson_encoder(out, key_));
            __array.put(std::move(out));
        }
    };
    
    minson::object &__obj;
    std::string __name;
    
public:
    explicit minson_encoder(minson::object &out_) : __obj(out_) {}
    minson_encoder(minson::object &out_, string_view name_) :
        __obj(out_), __name(name_.data(), name_.size())
    {}
    
    void put_bool(bool value_) override
    {   __obj = minson::object::boolean(value_, __name);   }
    
    void put_char(char value_) override
    {   __obj = minson::object::string(std::string(1, value_), __name);   }
    
    void put_int(long long int value_) override
    {   __obj = minson::object::number(value_, __name);   }
    
    void put_float(long double value_) override
    {   __obj = minson::object::floatNumber(value_, __name);   }
    
    void put_string(string_view value_) override
    {
        __obj = minson::object::string(std::string(value_.data(), value_.size()), __name);
    }
    
    void as_sequence(std::function<void(sequence_encoder &&)> step_) override
    {
        minson::array array{__name};
        step_(__sequence_encoder(array));
        __obj = std::move(array);
    }
    
    void as_map(std::function<void(map_encoder &&)> step_) override
    {
        minson::array array{__name};
        step_(__map_encoder(array));
        __obj = std::move(array);
    }
};

// --- *** ---


class minson_decoder : public value_decoder
{
    struct __sequence_decoder : sequence_decoder
    {
        minson::array const &__array;
        minson::iterator __it;
        
        __sequence_decoder(minson::array const &array_) :
            __array(array_), __it(array_.begin())
        {}
        
        std::size_t size() override
        {   return __array.count();   }
        
        void get_sub(std::size_t i_, std::function<void(value_decoder &&)> step_) override
        {
            (void)i_; step_(minson_decoder(*__it)); ++__it;
        }
    };
    
    struct __map_decoder : map_decoder
    {
        minson::array const &__array;
        minson::iterator __it, __it_end;
        std::string __name;
        
        __map_decoder(minson::array const &array_) :
            __array(array_), __it(array_.begin()), __it_end(array_.end())
        {}
        
        string_view next_key() override
        {
            while(__it != __it_end)
            {
                __name = __it.name();
                if(! __name.empty()) return (string_view)__name;
                ++__it;
            }
            return {};
        }
        
        void get_sub(string_view key_, std::function<void(value_decoder &&)> step_) override
        {
            step_(minson_decoder(__array.get(std::string(key_.data(), key_.size()))));
        }
    };
    
    minson::object const &__obj;
    std::string __extracted_str;
    
public:
    explicit minson_decoder(minson::object const &obj_) : __obj(obj_) {}
    
    bool get_bool() override
    {   return (bool)__obj;   }
    
    char get_char() override
    {
        std::string str = (std::string)__obj;
        return (! str.empty())? str[0] : '\0';
    }
    
    long long int get_int() override
    {   return (int64_t)__obj;   }
    
    long double get_float() override
    {   return (double)__obj;   }
    
    string_view get_string() override
    {
        __extracted_str = (std::string)__obj;
        return (string_view)__extracted_str;
    }
    
    void as_sequence(std::function<void(sequence_decoder &&)> step_) override
    {   step_(__sequence_decoder((minson::array)__obj));   }
    
    void as_map(std::function<void(map_decoder &&)> step_) override
    {   step_(__map_decoder((minson::array)__obj));   }
};


} //- namespace serialize

#endif //- _SERIALIZE__MINSON_HPP

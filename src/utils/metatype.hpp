#ifndef __UTILS_METATYPE_HPP
#define __UTILS_METATYPE_HPP

/* Система метатипов, которая предоставляет механизм динамической
 * параметризации классов. Каждый объект системы имеет данные,
 * разделяемые между несколькими объектами, которые,
 * однако, могут существовать в нескольких экземплярах.
 * Один экземпляр таких данных называется метатипом; их структура
 * зависит от класса объекта. Объекты создаются по имени метатипа.
 * 
 * Пример:
 * ```
 * class Object : public BaseMetatypeObject
 * {
 *     metatype_data
 *     {
 *         int a;
 *         float b;
 *         
 *         metatype_parameters(a, b);
 *     };
 *     
 * public:
 *     void use()
 *     {
 *         std::cout << metatype.a << metatype.b;
 *     }
 * };
 * 
 * METATYPE_CLASS(Object);
 * ``` */

#include "serialize/serialize.hpp"
#include "memory/allocator.hpp"
#include "memory/string.hpp"
#include "memory/unordered_map.hpp"
#include <atomic>
#include <type_traits>


struct __base_metatype_objects_factory;


/* Базовый класс для объектов системы.
 * Используемые наследные классы должны иметь конструктор по умолчанию. */
class BaseMetatypeObject
{
    /* Ключевое слово для определения/расширения структуры данных метатипа. */
#define metatype_data \
    private: using __t_base_metatype_data = MetatypeData; \
    public: struct MetatypeData : public __t_base_metatype_data
    
    /* Указание полей для десериализации при создании метатипа. */
#define metatype_parameters(... /*Fields*/) \
    define_deserialize_fields(__t_base_metatype_data, __VA_ARGS__)
    
    /* Ключевое слово для доступа к полям метатипа. */
#define metatype (*static_cast<MetatypeData *>(__metatype_data))
    
    friend class MetatypeManager;
    
public:
    class MetatypeData : public serialize::serializable
    {
        friend class MetatypeManager;
        friend class BaseMetatypeObject;
        
    private:
        std::atomic<long> __refs_count{0};
        __base_metatype_objects_factory *__factory;
        memory::allocator *__alloc;
        char const */*own*/ __type_name;
        
    public:
        ~MetatypeData() noexcept;
        
        /* Возвращает имя метатипа. */
        char const *metatypeName() const noexcept
        {   return __type_name;   }
    };
    
private:
    static thread_local MetatypeData *__incoming_metatype_data;
    
protected:
    MetatypeData *__metatype_data = nullptr;
    
    BaseMetatypeObject() noexcept;
    
public:
    /* Удаляет данный объект. Внимание: после вызова
     * данного метода объект перестанет существовать! */
    void destroy() noexcept;
    
    /* Возвращает имя метатипа. */
    char const *metatypeName() const noexcept
    {   return __metatype_data->__type_name;   }
};


/* Регистрирует класс `ObjectType`, наследный от `BaseMetatypeObject`,
 * для использования в метатипах с именем как указано. */
#define METATYPE_CLASS(ObjectType) \
    __metatype_objects_factory<ObjectType> __metatype_objects_factory__ ## ObjectType{ \
        & __metatype_objects_factory__ ## ObjectType, # ObjectType \
    }

// --- *** ---


struct __base_metatype_objects_factory
{
    using BaseMetatypeData = BaseMetatypeObject::MetatypeData;
    
    __base_metatype_objects_factory *next;
    memory::string_view class_name;
    
    __base_metatype_objects_factory(__base_metatype_objects_factory *using_guard,
                                    char const *class_name_) noexcept;
    
    virtual BaseMetatypeObject *newObject(memory::allocator *alloc_) = 0;
    virtual void deleteObject(BaseMetatypeObject *p_, memory::allocator *alloc_) noexcept = 0;
    
    virtual BaseMetatypeData *newData(serialize::map_decoder &&decoder_, memory::allocator *alloc_) = 0;
    virtual void deleteData(BaseMetatypeData *p_, memory::allocator *alloc_) noexcept = 0;
};

template<class _Object>
struct __metatype_objects_factory : __base_metatype_objects_factory
{
    static_assert(std::is_base_of<BaseMetatypeObject, _Object>::value,
                  "This is not a metatype object class!");
    static_assert(std::is_default_constructible<_Object>::value,
                  "Metatype object class must have a default constructor!");
    
    using _MetatypeData = typename _Object::MetatypeData;
    
    __metatype_objects_factory(__metatype_objects_factory *using_guard,
                               char const *class_name_) noexcept :
        __base_metatype_objects_factory(using_guard, class_name_)
    {}
    
    BaseMetatypeObject *newObject(memory::allocator *alloc_)
    {   return alloc_->make<_Object>();   }
    
    void deleteObject(BaseMetatypeObject *p_, memory::allocator *alloc_) noexcept
    {   alloc_->destroy(static_cast<_Object *>(p_));   }
    
    BaseMetatypeData *newData(serialize::map_decoder &&decoder_, memory::allocator *alloc_)
    {
        memory::allocator::unwinder_dealloc<_MetatypeData> w{
            alloc_, alloc_->make<_MetatypeData>()
        };
        ++w.curr; w.begin->deserialize(std::move(decoder_));
        
        w.curr = nullptr;
        return w.begin;
    }
    
    void deleteData(BaseMetatypeData *p_, memory::allocator *alloc_) noexcept
    {   alloc_->destroy(static_cast<_MetatypeData *>(p_));   }
};

// --- *** ---


/* Диспетчер системы метатипов.
 * `alloc_` -- аллокатор для объектов системы и внутренних структур. */
class MetatypeManager
{
    friend class BaseMetatypeObject;
    
    MetatypeManager(MetatypeManager const &) = delete;
    MetatypeManager(MetatypeManager &&) = delete;
    
private:
    using BaseMetatypeData = BaseMetatypeObject::MetatypeData;
    
#if __cplusplus >= 201703L
    using __str_view_hash = std::hash<std::string_view>;
#else
    struct __str_view_hash
    {
        std::size_t operator ()(memory::string_view str_) const noexcept;
    };
#endif
    
    memory::allocator *__alloc;
    memory::unordered_map<memory::string_view, BaseMetatypeData *,
                          __str_view_hash> __types;
    
    BaseMetatypeObject *__new_object(memory::string_view type_name_) const;
    BaseMetatypeData *__get_data(memory::string_view type_name_) const noexcept;
    
public:
    explicit MetatypeManager(memory::allocator *alloc_=&memory::std_allocator) noexcept;
    ~MetatypeManager() noexcept;
    
    /* Создаёт метатип с непустым именем `type_name_`. `parameters_` должен
     * предоставлять словарь, в котором содержится имя класса под ключом
     * "class" и данные для десериализации полей метатипа этого класса.
     * Если тип с таким именем уже существует или указанный класс не найден,
     * будет выброшено исключение `std::invalid_argument`. */
    void createMetatype(memory::string_view type_name_,
                        serialize::value_decoder &&parameters_);
    
    /* Создаёт и возвращает новый объект по имени метатипа.
     * Возвращает `nullptr`, если такого метатипа не существует.
     * Объект следует удалять с помощью вызова метода `destroy()`. */
    template<class _Object=BaseMetatypeObject>
    _Object *newObject(memory::string_view type_name_) const
    {
        return static_cast<_Object *>(__new_object(type_name_));
    }
    
    /* Возвращает указатель на структуру данных метатипа.
     * Возвращает `nullptr`, если такого метатипа не существует. */
    template<class _Object=BaseMetatypeObject>
    typename _Object::MetatypeData *getData(memory::string_view type_name_) const noexcept
    {
        return static_cast<typename _Object::MetatypeData *>(__get_data(type_name_));
    }
    
    /* Возвращает аллокатор для объектов системы. */
    memory::allocator *getAllocator() const {   return __alloc;   }
};


#endif //- __UTILS_METATYPE_HPP

#include "platform.hpp"
#include <string.h>
#include <stdint.h>

#if defined(__WIN32__) || defined(_WIN32) || defined(__CYGWIN__)
# define is_windows 1
#elif defined(__APPLE__) || defined(__APPLE_CC__)
# define is_apple 1
#elif defined(__linux) || defined(__linux__)
# define is_linux 1
#else
# error "Not supported platform!"
#endif

#if is_windows

# include <windows.h>
# include <wchar.h>

# define PATH_CHAR WCHAR
# define PATH_CHAR_C(ch) L ## ch
# define PATH_SEPARATOR L'\\'
# define PATH_MAX MAX_PATH

#elif is_linux

# include <sys/stat.h>
# include <limits.h>
# include <unistd.h>
# include <dirent.h>
# include <fnmatch.h>

# define PATH_CHAR char
# define PATH_CHAR_C(ch) ch
# define PATH_SEPARATOR '/'

#elif is_apple

# include <mach-o/dyld.h>
# include <sys/stat.h>
# include <sys/syslimits.h>
# include <unistd.h>
# include <dirent.h>
# include <fnmatch.h>
# include <stdlib.h>

# define PATH_CHAR char
# define PATH_CHAR_C(ch) ch
# define PATH_SEPARATOR '/'

#endif

// --- *** ---


#if is_windows

static std::string __to_utf8(PATH_CHAR const *str_) noexcept
{
    std::string out;
    
    for(; *str_; ++str_)
    {
        uint16_t utf_ch = *reinterpret_cast<uint16_t const *>(str_);
        
        if((utf_ch & (uint16_t)0xF800) != 0) // (1111 1xxx xxxx xxxx) 3-byte sequence (16 bit)
        {
            out.push_back((char)( 0xE0 | (0x0F & (utf_ch >> 12)) )); // (1110 xxxx)
            out.push_back((char)( 0x80 | (0x3F & (utf_ch >> 6 )) )); // (10xx xxxx)
            out.push_back((char)( 0x80 | (0x3F &  utf_ch       ) )); // (10xx xxxx)
        }
        else if((utf_ch & (uint16_t)0x0780) != 0) // (0000 0111 1xxx xxxx) 2-byte sequence (11 bit)
        {
            out.push_back((char)( 0xC0 | (0x1F & (utf_ch >> 6)) )); // (110x xxxx)
            out.push_back((char)( 0x80 | (0x3F &  utf_ch      ) )); // (10xx xxxx)
        }
        else /*if((utf_ch & (uint16_t)0x007F) != 0)*/ // (0000 0000 0111 1111) regular char (7 bit)
        {   out.push_back((char)utf_ch);   }
    }
    return out;
}


static std::basic_string<PATH_CHAR> __from_utf8(char const *str_) noexcept
{
    std::basic_string<PATH_CHAR> out;
    uint32_t utf_ch = 0;
    uint8_t multibyte_remain = 0;
    
    for(; *str_; ++str_)
    {
        uint8_t ch = *reinterpret_cast<uint8_t const *>(str_);
        
        if((ch & (uint8_t)0x80) == 0) // (0xxx xxxx) regular char (7 bit)
        {
            multibyte_remain = 0; out.push_back((PATH_CHAR)ch);
        }
        else if((ch & (uint8_t)0x40) == 0) // (10xx xxxx) continuation of multibyte
        {
            if(multibyte_remain > 0) // corrupted otherwise
            {
                utf_ch = (utf_ch << 6) | (ch & (uint8_t)0x3F);
                if(--multibyte_remain == 0) out.push_back((PATH_CHAR)utf_ch);
            }
        }
        else if((ch & (uint8_t)0x20) == 0) // (110x xxxx) start of 2-byte sequence (11 bit)
        {
            multibyte_remain = 1; utf_ch = ch & (uint8_t)0x1F;
        }
        else if((ch & (uint8_t)0x10) == 0) // (1110 xxxx) start of 3-byte sequence (16 bit)
        {
            multibyte_remain = 2; utf_ch = ch & (uint8_t)0x0F;
        }
        else // corrupted
        {   multibyte_remain = 0;   }
    }
    return out;
}

#endif

// --- *** ---


std::string normalizePath(std::string const &path_) noexcept
{
    std::string out; out.reserve(path_.size() + 1);
    
    bool separator_before = false;
    int separators_count = 0;
    for(char ch : path_)
    {
        if(ch == '/' || ch == '\\')
        {
            if(separator_before) continue;
#if is_windows
            out.push_back('\\');
#elif is_linux || is_apple
            out.push_back('/');
#endif
            separator_before = true;
            ++separators_count;
        }
        else
        {
            separator_before = false;
            out.push_back(ch);
        }
    }
    
    if(separator_before && separators_count > 1) out.pop_back();
    return out;
}

// --- *** ---


file_type getFileType(std::string const &path_) noexcept
{
#if is_windows
    
    auto w_path = __from_utf8(path_);
    DWORD attrs = GetFileAttributesW(w_path.c_str());
    
    if(attrs == INVALID_FILE_ATTRIBUTES)
        return FILE_NOT_EXIST;
    else if((attrs & FILE_ATTRIBUTE_DIRECTORY) != 0)
        return FILE_TYPE_DIRECTORY;
    else /*if((attrs & FILE_ATTRIBUTE_NORMAL) != 0)*/
        return FILE_TYPE_REGULAR;
    /*else
        return FILE_TYPE_SPECIAL;*/
    
#elif is_linux || is_apple
    
    struct stat attrs;
    if(stat(path_.c_str(), &attrs) != 0)
        return FILE_NOT_EXIST;
    else if(S_ISDIR(attrs.st_mode))
        return FILE_TYPE_DIRECTORY;
    else if(S_ISREG(attrs.st_mode))
        return FILE_TYPE_REGULAR;
    else
        return FILE_TYPE_SPECIAL;
    
#endif
}

// --- *** ---


static void __find_in_dir_impl(PATH_CHAR const *root_, PATH_CHAR const *mask_,
                               bool for_subdirs_, bool recursive_, size_t infix_offset_,
                               std::vector<std::string> &out_) noexcept
{
    PATH_CHAR subname[PATH_MAX];
    size_t subname_pre_len = 0;
    {
        PATH_CHAR const *p = root_;
        while(*p) subname[subname_pre_len++] = *p++;
    }
    subname[subname_pre_len++] = PATH_SEPARATOR;
    
#if is_windows
    
    if(recursive_)
    {
        wcscpy(subname + subname_pre_len, PATH_CHAR_C("*"));
        
        WIN32_FIND_DATAW data;
        HANDLE found = FindFirstFileW(subname, &data);
        if(found == INVALID_HANDLE_VALUE) return;
        do
        {
            if((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) continue;
            if(wcscmp(data.cFileName, PATH_CHAR_C(".")) == 0) continue;
            if(wcscmp(data.cFileName, PATH_CHAR_C("..")) == 0) continue;
            
            wcscpy(subname + subname_pre_len, data.cFileName);
            __find_in_dir_impl(subname, mask_, for_subdirs_, recursive_,
                               infix_offset_, out_);
        }
        while(FindNextFileW(found, &data));
        FindClose(found);
    }
    
    wcscpy(subname + subname_pre_len, mask_);
    
    WIN32_FIND_DATAW data;
    HANDLE found = FindFirstFileW(subname, &data);
    if(found == INVALID_HANDLE_VALUE) return;
    do
    {
        if(for_subdirs_)
        {
            if((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) continue;
            if(wcscmp(data.cFileName, PATH_CHAR_C(".")) == 0) continue;
            if(wcscmp(data.cFileName, PATH_CHAR_C("..")) == 0) continue;
        }
        else
        {
            if((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) continue;
        }
        
        if(infix_offset_ < subname_pre_len)
        {
            wcscpy(subname + subname_pre_len, data.cFileName);
            out_.push_back(__to_utf8(subname + infix_offset_));
        }
        else
        {   out_.push_back(__to_utf8(data.cFileName));   }
    }
    while(FindNextFileW(found, &data));
    FindClose(found);
    
#elif is_linux || is_apple
    
    DIR *dir = opendir(root_);
    if(dir == nullptr) return;
    
    struct dirent *entry;
    while((entry = readdir(dir)) != nullptr)
    {
        if((entry->d_type == DT_DIR) && recursive_)
        {
            strcpy(subname + subname_pre_len, entry->d_name);
            __find_in_dir_impl(subname, mask_, for_subdirs_, recursive_,
                               infix_offset_, out_);
        }
        
        if((entry->d_type == DT_DIR) != for_subdirs_) continue;
        if(fnmatch(mask_, entry->d_name, FNM_PATHNAME) != 0) continue;
        
        if(infix_offset_ < subname_pre_len)
        {
            subname[subname_pre_len] = PATH_CHAR_C('\0');
            out_.push_back(std::string{subname + infix_offset_} + entry->d_name);
        }
        else
        {   out_.push_back(std::string{entry->d_name});   }
    }
    (void)closedir(dir);
    
#endif
    return;
}


std::vector<std::string> findInDir(std::string const &root_, std::string const &mask_,
                                   bool for_subdirs_, bool recursive_) noexcept
{
    std::vector<std::string> out;
    if(mask_.empty()) return out;
    
#if is_windows
    
    auto w_root = __from_utf8(root_.c_str());
    auto w_mask = __from_utf8(mask_.c_str());
    __find_in_dir_impl(w_root.c_str(), w_mask.c_str(), for_subdirs_, recursive_,
                       w_root.size() + /*separator*/ 1, out);
    
#elif is_linux || is_apple
    
    __find_in_dir_impl(root_.c_str(), mask_.c_str(), for_subdirs_, recursive_,
                       root_.size() + /*separator*/ 1, out);
#endif
    return out;
}

// --- *** ---


static bool __make_dir_impl(PATH_CHAR *dir_, bool recursive_) noexcept
{
    if(recursive_)
    {
        bool first_separator = true;
        for(PATH_CHAR ch, *p = dir_; (ch = *p); ++p)
        {
            if(ch != PATH_SEPARATOR) continue;
            if(first_separator) {   first_separator = false; continue;   }
            
            *p = PATH_CHAR_C('\0');
            bool make_one = __make_dir_impl(dir_, false);
            *p = ch;
            if(! make_one) return false;
        }
    }
    
#if is_windows
    
    DWORD attrs = GetFileAttributesW(const_cast<PATH_CHAR const *>(dir_));
    if(attrs != INVALID_FILE_ATTRIBUTES)
    {   return((attrs & FILE_ATTRIBUTE_DIRECTORY) != 0);   }
    else
    {   return CreateDirectoryW(const_cast<PATH_CHAR const *>(dir_), nullptr);   }
    
#elif is_linux || is_apple
    
    struct stat attrs;
    if(stat(const_cast<PATH_CHAR const *>(dir_), &attrs) == 0)
    {   return S_ISDIR(attrs.st_mode);   }
    else
    {   return(mkdir(const_cast<PATH_CHAR const *>(dir_), 0775) == 0);   }
    
#endif
}


bool makeDir(std::string const &dir_, bool recursive_) noexcept
{
#if is_windows
    
    auto w_dir = __from_utf8(dir_.c_str());
    return __make_dir_impl(const_cast<PATH_CHAR *>(w_dir.c_str()), recursive_);
    
#elif is_linux || is_apple
    
    if(recursive_)
    {
        PATH_CHAR buffer[dir_.size() + 1]; strcpy(buffer, dir_.c_str());
        return __make_dir_impl(buffer, true);
    }
    else
    {
        return __make_dir_impl(const_cast<PATH_CHAR *>(dir_.c_str()), false);
    }
    
#endif
}

// --- *** ---


static bool __remove_dir_impl(PATH_CHAR const *root_, bool force_) noexcept
{
    if(force_) // Delete files and folders recursively
    {
        PATH_CHAR subname[PATH_MAX];
        size_t subname_pre_len = 0;
        {
            PATH_CHAR const *p = root_;
            while(*p) subname[subname_pre_len++] = *p++;
        }
        subname[subname_pre_len++] = PATH_SEPARATOR;
        
#if is_windows
        
        wcscpy(subname + subname_pre_len, PATH_CHAR_C("*"));
        
        WIN32_FIND_DATAW data;
        HANDLE found = FindFirstFileW(subname, &data);
        if(found == INVALID_HANDLE_VALUE) return false;
        do
        {
            wcscpy(subname + subname_pre_len, data.cFileName);
            if((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
            {
                if(wcscmp(data.cFileName, PATH_CHAR_C(".")) == 0) continue;
                if(wcscmp(data.cFileName, PATH_CHAR_C("..")) == 0) continue;
                
                if(! __remove_dir_impl(subname, force_))
                {
                    FindClose(found); return false;
                }
            }
            else
            {
                if(! DeleteFileW(subname))
                {
                    FindClose(found); return false;
                }
            }
        }
        while(FindNextFileW(found, &data));
        FindClose(found);
        
#elif is_linux || is_apple
        
        DIR *dir = opendir(root_);
        if(dir == nullptr) return false;
        
        struct dirent *entry;
        while((entry = readdir(dir)) != nullptr)
        {
            strcpy(subname + subname_pre_len, entry->d_name);
            if(entry->d_type == DT_DIR)
            {
                if(! __remove_dir_impl(subname, force_))
                {
                    (void)closedir(dir); return false;
                }
            }
            else
            {
                if(unlink(subname) != 0)
                {
                    (void)closedir(dir); return false;
                }
            }
        }
        (void)closedir(dir);
        
#endif
    } //- if(force_)
    
#if is_windows
    
    if(RemoveDirectoryW(root_)) return true;
    
    DWORD err = GetLastError();
    return(err == ERROR_FILE_NOT_FOUND || err == ERROR_PATH_NOT_FOUND);
    
#elif is_linux || is_apple
    
    if(rmdir(root_) == 0) return true;
    return(errno == ENOENT);
    
#endif
}


bool removeDir(std::string const &dir_, bool force_) noexcept
{
#if is_windows
    auto w_dir = __from_utf8(dir_.c_str());
    return __remove_dir_impl(w_dir.c_str(), force_);
#elif is_linux || is_apple
    return __remove_dir_impl(dir_.c_str(), force_);
#endif
}

// --- *** ---


bool removeFile(std::string const &file_) noexcept
{
#if is_windows
    
    auto w_file = __from_utf8(file_.c_str());
    if(DeleteFileW(w_file.c_str())) return true;
    
    DWORD err = GetLastError();
    return(err == ERROR_FILE_NOT_FOUND || err == ERROR_PATH_NOT_FOUND);
    
#elif is_linux || is_apple
    
    if(unlink(file_.c_str()) == 0) return true;
    return(errno == ENOENT);
    
#endif
}

// --- *** ---


std::string const &executableOrigin() noexcept
{
    static std::string __saved_res{};
    if(! __saved_res.empty()) return __saved_res;
    
    // Get image path
#if is_windows
    
    PATH_CHAR buffer[PATH_MAX];
    DWORD lenght = GetModuleFileNameW(GetModuleHandleW(nullptr), buffer, PATH_MAX - 1);
    if(lenght == 0) return __saved_res;
    
#elif is_linux
    
    PATH_CHAR buffer[PATH_MAX];
    ssize_t lenght = readlink("/proc/self/exe", buffer, PATH_MAX - 1);
    if(lenght < 0) return __saved_res;
    
#elif is_apple
    
    PATH_CHAR buffer_1[PATH_MAX];
    uint32_t buffer_1_size = PATH_MAX;
    if(_NSGetExecutablePath(buffer_1, &buffer_1_size) == 0) return __saved_res;
    
    PATH_CHAR buffer[PATH_MAX];
    if(realpath(buffer_1, buffer) == nullptr) return __saved_res;
    long lenght = (long)strlen(buffer);
    
#endif
    buffer[lenght] = PATH_CHAR_C('\0');
    
    // Get base directory
    for(PATH_CHAR *p = buffer + lenght; lenght >= 0; --lenght, --p)
    {
        if(*p == PATH_SEPARATOR) {   *p = PATH_CHAR_C('\0'); break;   }
    }
    
#if is_windows
    return (__saved_res = __to_utf8(buffer));
#elif is_linux || is_apple
    return (__saved_res = std::string(buffer));
#endif
}

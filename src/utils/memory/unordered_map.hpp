#ifndef _MEMORY__UNORDERED_MAP_HPP
#define _MEMORY__UNORDERED_MAP_HPP

#include "allocator.hpp"
#include <unordered_map>

namespace memory
{

template<typename K, typename V, typename Hash=std::hash<K>, typename Equ=std::equal_to<K>>
using unordered_map = std::unordered_map<K, V, Hash, Equ, std_alloc_wrapper<std::pair<K const, V>>>;

template<typename K, typename V, typename Hash=std::hash<K>, typename Equ=std::equal_to<K>>
using unordered_multimap = std::unordered_multimap<K, V, Hash, Equ, std_alloc_wrapper<std::pair<K const, V>>>;

}

#endif //- _MEMORY__UNORDERED_MAP_HPP

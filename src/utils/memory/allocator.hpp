#ifndef _MEMORY__ALLOCATOR_HPP
#define _MEMORY__ALLOCATOR_HPP

#include <new>
#include <utility>

namespace memory
{


class allocator
{
public:
    template<typename T>
    struct unwinder
    {
        T *begin, *curr;
        
        unwinder(T *p) noexcept : begin(p), curr(p) {}
        
        ~unwinder() noexcept
        {
            if(curr == nullptr) return;
            while(curr > begin) (--curr)->~T();
        }
    };
    
    template<typename T>
    struct unwinder_dealloc
    {
        allocator *alloc;
        T *begin, *curr;
        
        unwinder_dealloc(allocator *alloc_, T *p) noexcept :
            alloc(alloc_), begin(p), curr(p)
        {}
        
        ~unwinder_dealloc() noexcept
        {
            if(curr == nullptr) return;
            while(curr > begin) (--curr)->~T();
            alloc->deallocate(begin);
        }
    };
    
    typedef std::size_t size_type;
    
    // ---
    
    virtual ~allocator() noexcept;
    virtual void *allocate(size_type raw_size, size_type alignment);
    virtual void deallocate(void *p) noexcept;
    
    // ---
    
    template<typename T>
    T *allocate(size_type n=1)
    {
        if(n > size_type(-1) / sizeof(T)) throw std::bad_alloc();
        return static_cast<T *>(allocate(n * sizeof(T), alignof(T)));
    }
    
    template<typename T, typename... Args>
    T *make(Args &&... args) // same as `new(allocator) T{args...};`
    {
        unwinder_dealloc<T> w{
            this, static_cast<T *>(allocate(sizeof(T), alignof(T)))
        };
        ::new((void *)w.curr) T{std::forward<Args>(args)...};
        
        w.curr = nullptr;
        return w.begin;
    }
    
    template<typename T>
    T *makeArray(size_type n) // same as `new(allocator) T[n];`
    {
        unwinder_dealloc<T> w{this, allocate<T>(n)};
        for(size_type i = 0; i < n; ++i, ++w.curr) ::new((void *)w.curr) T{};
        
        w.curr = nullptr;
        return w.begin;
    }
    
    template<typename T, typename... Inits>
    T *makeArrayInit(Inits &&... inits) // same as `new(allocator) T[n]{inits...};`
    {
        unwinder_dealloc<T> w{this, allocate<T>(sizeof...(Inits))};
        (void)(T *[]){
            (::new((void *)w.curr) T{std::forward<Inits>(inits)}, ++w.curr)...
        };
        
        w.curr = nullptr;
        return w.begin;
    }
    
    template<typename T>
    void destroy(T *p, size_type n=1) noexcept
    {
        if(p == nullptr) return;
        
        T *curr = p + n; while(curr > p) (--curr)->~T();
        deallocate(p);
    }
};


extern allocator std_allocator;


} //- namespace memory


void *operator new(std::size_t raw_size, memory::allocator *alloc);
void *operator new(std::size_t raw_size, memory::allocator &alloc);
void *operator new[](std::size_t raw_size, memory::allocator *alloc);
void *operator new[](std::size_t raw_size, memory::allocator &alloc);
#if __cplusplus >= 201703L
void *operator new(std::size_t raw_size,
                   std::align_val_t alignment, memory::allocator *alloc);
void *operator new(std::size_t raw_size,
                   std::align_val_t alignment, memory::allocator &alloc);
void *operator new[](std::size_t raw_size,
                     std::align_val_t alignment, memory::allocator *alloc);
void *operator new[](std::size_t raw_size,
                     std::align_val_t alignment, memory::allocator &alloc);
#endif

void operator delete(void *p, memory::allocator *alloc) noexcept;
void operator delete(void *p, memory::allocator &alloc) noexcept;
void operator delete[](void *p, memory::allocator *alloc) noexcept;
void operator delete[](void *p, memory::allocator &alloc) noexcept;


namespace memory
{


template<typename T>
class clean_lock
{
private:
    T *__p;
    allocator *__alloc;
    
public:
    explicit clean_lock(T *p, allocator *alloc_=&std_allocator) noexcept :
        __p(p), __alloc(alloc_)
    {}
    clean_lock(clean_lock &&mv) noexcept :
        __p(mv.__p), __alloc(mv.__alloc)
    {   mv.__p = nullptr;   }
    
    ~clean_lock() noexcept
    {   __alloc->destroy(__p);   }
    
    T *release() noexcept
    {   T *p = __p; __p = nullptr; return p;   }
};


template<typename T>
class clean_lock<T[]>
{
private:
    T *__p;
    std::size_t __n;
    allocator *__alloc;
    
public:
    clean_lock(T *p, std::size_t n, allocator *alloc_=&std_allocator) noexcept :
        __p(p), __n(n), __alloc(alloc_)
    {}
    clean_lock(clean_lock &&mv) noexcept :
        __p(mv.__p), __n(mv.__n), __alloc(mv.__alloc)
    {   mv.__p = nullptr;   }
    
    ~clean_lock() noexcept
    {   __alloc->destroy(__p, __n);   }
    
    T *release() noexcept
    {   T *p = __p; __p = nullptr; return p;   }
    
    std::size_t size() noexcept
    {   return __n;   }
};


template<typename T>
class std_alloc_wrapper
{
    template<typename>
    friend class std_alloc_wrapper;
    
public:
    typedef std::size_t    size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T *            pointer;
    typedef T const *      const_pointer;
    typedef T &            reference;
    typedef T const &      const_reference;
    typedef T              value_type;
    
    template<typename U>
    struct rebind
    {   typedef std_alloc_wrapper<U> other;   };
    
private:
    allocator *__alloc;
    
public:
    constexpr std_alloc_wrapper() noexcept : __alloc(&std_allocator) {}
    std_alloc_wrapper(allocator *alloc_) noexcept : __alloc(alloc_) {}
    
    template<typename U>
    std_alloc_wrapper(std_alloc_wrapper<U> const &cp) noexcept : __alloc(cp.__alloc) {}
    
    template<typename U>
    std_alloc_wrapper &operator =(std_alloc_wrapper<U> const &cp) noexcept
    {   __alloc = cp.__alloc;   }
    
    std_alloc_wrapper(std_alloc_wrapper const &cp) noexcept = default;
    std_alloc_wrapper &operator =(std_alloc_wrapper const &) noexcept = default;
    ~std_alloc_wrapper() noexcept = default;
    
    pointer address(reference x) const noexcept
    {   return std::addressof(x);   }
    
    const_pointer address(const_reference x) const noexcept
    {   return std::addressof(x);   }
    
    T *allocate(size_type n) const
    {   return __alloc->allocate<T>(n);   }
    
    void deallocate(T *p, size_type n) const noexcept
    {   __alloc->deallocate(p);   }
    
    size_type max_size() const noexcept
    {   return size_type(-1) / sizeof(value_type);   }
    
    template<typename U, typename... Args>
    void construct(U *p, Args &&... args) const
    {   ::new((void *)p) U{std::forward<Args>(args)...};   }
    
    template<typename U>
    void destroy(U *p) const noexcept
    {   p->~U();   }
    
    template<typename U>
    bool operator ==(std_alloc_wrapper<U> const &r) const noexcept
    {   return __alloc == r.__alloc;   }
    
    template<typename U>
    bool operator !=(std_alloc_wrapper<U> const &r) const noexcept
    {   return __alloc != r.__alloc;   }
    
    allocator *operator ()() const noexcept
    {   return __alloc;   }
};


} //- namespace memory

#endif //- _MEMORY__ALLOCATOR_HPP

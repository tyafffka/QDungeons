#ifndef _MEMORY__TRANSIENT_REF_HPP
#define _MEMORY__TRANSIENT_REF_HPP

#include "allocator.hpp"
#include <memory>
#include <type_traits>

namespace memory
{


template<typename T> class shared_ref;


template<typename T>
class transient_ref
{
    static_assert(! std::is_array<T>::value,
                  "Fixed-size array type is not allowed");
    static_assert(! std::is_reference<T>::value,
                  "Inner reference is not allowed");
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
private:
    allocator *__alloc;
    T *__object;
    
public:
    template<typename... Args>
    static transient_ref<T> make(Args &&... args)
    {
        return transient_ref<T>(
            std_allocator.make<T>(std::forward<Args>(args)...), &std_allocator
        );
    }
    
    template<typename... Args>
    static transient_ref<T> makeAlloc(allocator *alloc_, Args &&... args)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return transient_ref<T>(
            alloc_->make<T>(std::forward<Args>(args)...), alloc_
        );
    }
    
    transient_ref() noexcept : __alloc(nullptr), __object(nullptr) {}
    transient_ref(std::nullptr_t) noexcept : __alloc(nullptr), __object(nullptr) {}
    
    template<typename U>
    transient_ref(U *p, allocator *alloc_=nullptr) noexcept :
        __alloc((p != nullptr)? alloc_ : nullptr), __object(static_cast<T *>(p))
    {}
    
    template<typename U>
    transient_ref(shared_ref<U> const &cp) noexcept :
        __alloc(nullptr), __object(static_cast<T *>(cp.base()))
    {}
    
    template<typename U>
    transient_ref(shared_ref<U[]> const &cp) = delete;
    
    template<typename U>
    transient_ref(transient_ref<U> const &cp) noexcept :
        __alloc(nullptr), __object(static_cast<T *>(cp.__object))
    {}
    
    template<typename U>
    transient_ref(transient_ref<U[]> const &cp) = delete;
    
    template<typename U>
    transient_ref(transient_ref<U> &&mv) noexcept :
        __alloc(mv.__alloc), __object(static_cast<T *>(mv.__object))
    {   mv.__alloc = nullptr;   }
    
    template<typename U>
    transient_ref(transient_ref<U[]> &&mv) = delete;
    
    ~transient_ref() noexcept
    {   if(__alloc != nullptr) __alloc->destroy(__object);   }
    
    transient_ref &operator =(std::nullptr_t) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__object);
        __alloc = nullptr; __object = nullptr;
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(shared_ref<U> const &cp) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__object);
        __alloc = nullptr; __object = static_cast<T *>(cp.base());
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(shared_ref<U[]> const &cp) = delete;
    
    template<typename U>
    transient_ref &operator =(transient_ref<U> const &cp) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__object);
        __alloc = nullptr; __object = static_cast<T *>(cp.__object);
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(transient_ref<U[]> const &cp) = delete;
    
    template<typename U>
    transient_ref &operator =(transient_ref<U> &&mv) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__object);
        __alloc = mv.__alloc; __object = static_cast<T *>(mv.__object);
        mv.__alloc = nullptr;
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(transient_ref<U[]> &&mv) = delete;
    
    T &operator *() const noexcept
    {   return *__object;   }
    
    T *operator ->() const noexcept
    {   return __object;   }
    
    explicit operator bool() const noexcept
    {   return(__object != nullptr);   }
    
    bool operator !() const noexcept
    {   return(__object == nullptr);   }
    
    template<typename U>
    bool operator ==(transient_ref<U> const &r) const noexcept
    {   return(__object == r.__object);   }
    
    template<typename U>
    bool operator ==(transient_ref<U[]> const &r) = delete;
    
    template<typename U>
    bool operator !=(transient_ref<U> const &r) const noexcept
    {   return(__object != r.__object);   }
    
    template<typename U>
    bool operator !=(transient_ref<U[]> const &r) = delete;
    
    bool isOwn() const noexcept
    {   return(__alloc != nullptr);   }
    
    [[nodiscard]] T *release() const noexcept
    {   __alloc = nullptr; return __object;   }
    
    T *base() const noexcept
    {   return __object;   }
};

// --- *** ---


template<typename T>
class transient_ref<T[]>
{
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
private:
    allocator *__alloc;
    std::size_t __size;
    T *__objects;
    
public:
    static transient_ref<T[]> make(std::size_t size_)
    {
        return transient_ref<T[]>(
            std_allocator.makeArray<T>(size_), size_, &std_allocator
        );
    }
    
    static transient_ref<T[]> makeAlloc(allocator *alloc_, std::size_t size_)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return transient_ref<T[]>(alloc_->makeArray<T>(size_), size_, alloc_);
    }
    
    template<typename... Inits>
    static transient_ref<T[]> makeInit(Inits &&... inits_)
    {
        return transient_ref<T[]>(
            std_allocator.makeArrayInit<T>(std::forward<Inits>(inits_)...),
            sizeof...(Inits), &std_allocator
        );
    }
    
    template<typename... Inits>
    static transient_ref<T[]> makeInitAlloc(allocator *alloc_, Inits &&... inits_)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return transient_ref<T[]>(
            alloc_->makeArrayInit<T>(std::forward<Inits>(inits_)...),
            sizeof...(Inits), alloc_
        );
    }
    
    transient_ref() noexcept : __alloc(nullptr), __size(0), __objects(nullptr) {}
    transient_ref(std::nullptr_t) noexcept : __alloc(nullptr), __size(0), __objects(nullptr) {}
    
    template<typename U>
    transient_ref(U *p, std::size_t size_, allocator *alloc_=nullptr) noexcept :
        __alloc((p != nullptr)? alloc_ : nullptr), __size(size_), __objects(static_cast<T *>(p))
    {}
    
    template<typename U, std::size_t N>
    transient_ref(U (&p)[N], allocator *alloc_=nullptr) noexcept :
        __alloc(alloc_), __size(N), __objects(static_cast<T *>(p))
    {}
    
    template<typename U>
    transient_ref(shared_ref<U[]> const &cp) noexcept :
        __alloc(nullptr), __size(cp.size()), __objects(static_cast<T *>(cp.base()))
    {}
    
    template<typename U>
    transient_ref(transient_ref<U[]> const &cp) noexcept :
        __alloc(nullptr), __size(cp.__size), __objects(static_cast<T *>(cp.__objects))
    {}
    
    template<typename U>
    transient_ref(transient_ref<U[]> &&mv) noexcept :
        __alloc(mv.__alloc), __size(mv.__size), __objects(static_cast<T *>(mv.__objects))
    {   mv.__alloc = nullptr;   }
    
    ~transient_ref() noexcept
    {   if(__alloc != nullptr) __alloc->destroy(__objects, __size);   }
    
    transient_ref &operator =(std::nullptr_t) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__objects, __size);
        __alloc = nullptr; __size = 0; __objects = nullptr;
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(shared_ref<U[]> const &cp) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__objects, __size);
        __alloc = nullptr; __size = cp.size(); __objects = static_cast<T *>(cp.base());
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(transient_ref<U[]> const &cp) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__objects, __size);
        __alloc = nullptr; __size = cp.__size; __objects = static_cast<T *>(cp.__objects);
        return *this;
    }
    
    template<typename U>
    transient_ref &operator =(transient_ref<U[]> &&mv) noexcept
    {
        if(__alloc != nullptr) __alloc->destroy(__objects, __size);
        __alloc = mv.__alloc; __size = mv.__size; __objects = static_cast<T *>(mv.__objects);
        mv.__alloc = nullptr;
        return *this;
    }
    
    T &operator [](std::size_t i) const noexcept
    {   return __objects[i];   }
    
    explicit operator bool() const noexcept
    {   return(__objects != nullptr);   }
    
    bool operator !() const noexcept
    {   return(__objects == nullptr);   }
    
    template<typename U>
    bool operator ==(transient_ref<U[]> const &r) const noexcept
    {   return(__objects == r.__objects);   }
    
    template<typename U>
    bool operator !=(transient_ref<U[]> const &r) const noexcept
    {   return(__objects != r.__objects);   }
    
    std::size_t size() const noexcept
    {   return __size;   }
    
    bool isOwn() const noexcept
    {   return(__alloc != nullptr);   }
    
    [[nodiscard]] T *release() const noexcept
    {   __alloc = nullptr; return __objects;   }
    
    T *base() const noexcept
    {   return __objects;   }
};

// --- *** ---


#if __cplusplus >= 201703L
template<typename U, typename T=typename std::pointer_traits<U>::element_type>
transient_ref(U) -> transient_ref<T>;

template<typename U, typename T=typename std::pointer_traits<U>::element_type>
transient_ref(U, allocator *) -> transient_ref<T>;

template<typename T>
transient_ref(T *, std::size_t) -> transient_ref<T[]>;

template<typename T>
transient_ref(T *, std::size_t, allocator *) -> transient_ref<T[]>;

template<typename T, std::size_t N>
transient_ref(T (&)[N]) -> transient_ref<T[]>;

template<typename T, std::size_t N>
transient_ref(T (&)[N], allocator *) -> transient_ref<T[]>;

template<typename T>
transient_ref(shared_ref<T> const &) -> transient_ref<T>;

template<typename T>
transient_ref(transient_ref<T> const &) -> transient_ref<T>;

template<typename T>
transient_ref(transient_ref<T> &&) -> transient_ref<T>;
#endif


template<typename U, typename T=typename std::pointer_traits<U>::element_type>
inline transient_ref<T> trans(U p, allocator *alloc_=nullptr)
{   return transient_ref<T>(p, alloc_);   }

template<typename T>
inline transient_ref<T[]> trans(T *p, std::size_t size_, allocator *alloc_=nullptr)
{   return transient_ref<T[]>(p, size_, alloc_);   }

template<typename T, std::size_t N>
inline transient_ref<T[]> trans(T (&p)[N], allocator *alloc_=nullptr)
{   return transient_ref<T[]>(p, N, alloc_);   }

// ---

template<typename T, typename U>
inline bool operator ==(transient_ref<T> const &l_, U *r_) noexcept
{   return l_.base() == r_;   }

template<typename T, typename U>
inline bool operator ==(T *l_, transient_ref<U> const &r_) noexcept
{   return l_ == r_.base();   }

template<typename T, typename U>
inline bool operator !=(transient_ref<T> const &l_, U *r_) noexcept
{   return l_.base() != r_;   }

template<typename T, typename U>
inline bool operator !=(T *l_, transient_ref<U> const &r_) noexcept
{   return l_ != r_.base();   }

// ---

template<typename T, typename U, typename=typename std::enable_if<
    (std::is_array<T>::value == std::is_array<U>::value)
>::type>
inline bool operator ==(transient_ref<T> const &l_, shared_ref<U> const &r_) noexcept
{   return l_.base() == r_.base();   }


template<typename T, typename U, typename=typename std::enable_if<
    (std::is_array<T>::value == std::is_array<U>::value)
>::type>
inline bool operator ==(shared_ref<T> const &l_, transient_ref<U> const &r_) noexcept
{   return l_.base() == r_.base();   }


template<typename T, typename U, typename=typename std::enable_if<
    (std::is_array<T>::value == std::is_array<U>::value)
>::type>
inline bool operator !=(transient_ref<T> const &l_, shared_ref<U> const &r_) noexcept
{   return l_.base() != r_.base();   }


template<typename T, typename U, typename=typename std::enable_if<
    (std::is_array<T>::value == std::is_array<U>::value)
>::type>
inline bool operator !=(shared_ref<T> const &l_, transient_ref<U> const &r_) noexcept
{   return l_.base() != r_.base();   }


} //- namespace memory

#endif //- _MEMORY__TRANSIENT_REF_HPP

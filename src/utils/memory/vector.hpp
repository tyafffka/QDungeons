#ifndef _MEMORY__VECTOR_HPP
#define _MEMORY__VECTOR_HPP

#include "allocator.hpp"
#include <vector>

namespace memory
{

template<typename V>
using vector = std::vector<V, std_alloc_wrapper<V>>;

}

#endif //- _MEMORY__VECTOR_HPP

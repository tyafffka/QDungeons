#ifndef _MEMORY__SHARED_REF_HPP
#define _MEMORY__SHARED_REF_HPP

#include "allocator.hpp"
#include <type_traits>
#include <atomic>
#include <stddef.h>

namespace memory
{


template<typename T> class weak_ref;


template<typename T>
class __base_shared_ref
{
    template<typename> friend class shared_ref;
    template<typename> friend class weak_ref;
    
private:
    struct __ref_bundle
    {
        std::atomic<unsigned int> refs_count{1};
        std::atomic<unsigned int> weak_refs_count{1};
        allocator *alloc;
        alignas(T) unsigned char payload_place[sizeof(T)];
        
        T *payload() noexcept
        {   return reinterpret_cast<T *>(payload_place);   }
        
        explicit __ref_bundle(allocator *alloc_) noexcept : alloc(alloc_) {}
    };
    
    __ref_bundle *__bundle;
    
    __base_shared_ref() noexcept : __bundle(nullptr) {}
    __base_shared_ref(__ref_bundle *bundle_) noexcept : __bundle(bundle_) {}
};

// ---

template<typename T>
class shared_ref : public __base_shared_ref<T>
{
    static_assert(! std::is_array<T>::value,
                  "Fixed-size array type is not allowed");
    static_assert(! std::is_reference<T>::value,
                  "Inner reference is not allowed");
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
private:
    using _Base = __base_shared_ref<T>;
    using typename _Base::__ref_bundle;
    using _Base::__bundle;
    
    template<typename... Args>
    static __ref_bundle *__make_bundle(allocator *alloc_, Args &&... args)
    {
        allocator::unwinder_dealloc<__ref_bundle> w{
            alloc_, alloc_->make<__ref_bundle>(alloc_)
        };
        ++w.curr;
        ::new((void *)w.begin->payload()) T{std::forward<Args>(args)...};
        
        w.curr = nullptr;
        return w.begin;
    }
    
    shared_ref(__ref_bundle *bundle_) noexcept : _Base(bundle_) {}
    
    void __deref() noexcept
    {
        if(__bundle != nullptr && --(__bundle->refs_count) == 0)
        {
            __bundle->payload()->~T();
            
            if(--(__bundle->weak_refs_count) == 0) __bundle->alloc->destroy(__bundle);
        }
    }
    
public:
    template<typename... Args>
    static shared_ref<T> make(Args &&... args)
    {
        return shared_ref<T>(__make_bundle(
            &std_allocator, std::forward<Args>(args)...
        ));
    }
    
    template<typename... Args>
    static shared_ref<T> makeAlloc(allocator *alloc_, Args &&... args)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return shared_ref<T>(__make_bundle(
            alloc_, std::forward<Args>(args)...
        ));
    }
    
    shared_ref() noexcept {}
    shared_ref(std::nullptr_t) noexcept {}
    
    shared_ref(weak_ref<T> const &cp) noexcept : _Base(cp.__strongify()) {}
    
    shared_ref(shared_ref<T> const &cp) noexcept : _Base(cp.__bundle)
    {   if(__bundle != nullptr) ++(__bundle->refs_count);   }
    
    shared_ref(shared_ref<T> &&mv) noexcept : _Base(mv.__bundle)
    {   mv.__bundle = nullptr;   }
    
    ~shared_ref() noexcept {   __deref();   }
    
    shared_ref &operator =(std::nullptr_t) noexcept
    {
        __deref(); __bundle = nullptr; return *this;
    }
    
    shared_ref &operator =(weak_ref<T> const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref(); __bundle = cp.__strongify();
        return *this;
    }
    
    shared_ref &operator =(shared_ref<T> const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref();
        if((__bundle = cp.__bundle) != nullptr) ++(cp.__bundle->refs_count);
        return *this;
    }
    
    shared_ref &operator =(shared_ref<T> &&mv) noexcept
    {
        if(__bundle == mv.__bundle) return *this;
        __deref(); __bundle = mv.__bundle; mv.__bundle = nullptr;
        return *this;
    }
    
    T &operator *() const noexcept
    {   return *(__bundle->payload());   }
    
    T *operator ->() const noexcept
    {   return __bundle->payload();   }
    
    explicit operator bool() const noexcept
    {   return(__bundle != nullptr);   }
    
    bool operator !() const noexcept
    {   return(__bundle == nullptr);   }
    
    bool operator ==(shared_ref<T> const &r) const noexcept
    {   return(__bundle == r.__bundle);   }
    
    bool operator !=(shared_ref<T> const &r) const noexcept
    {   return(__bundle != r.__bundle);   }
    
    unsigned int useCount() const noexcept
    {   return (__bundle != nullptr)? __bundle->refs_count.load() : 0;   }
    
    T *base() const noexcept
    {   return __bundle->payload();   }
};

// --- *** ---


template<typename T>
class __base_shared_ref<T[]>
{
    template<typename> friend class shared_ref;
    template<typename> friend class weak_ref;
    
private:
    struct __array_ref_bundle
    {
        std::atomic<unsigned int> refs_count{1};
        std::atomic<unsigned int> weak_refs_count{1};
        allocator *alloc;
        std::size_t size;
        alignas(T) unsigned char payload_pad;
        
        __array_ref_bundle(allocator *alloc_, std::size_t size_) noexcept :
            alloc(alloc_), size(size_)
        {}
        
        T *payload() noexcept
        {   return reinterpret_cast<T *>(&payload_pad);   }
    };
    
    __array_ref_bundle *__bundle;
    
    static std::size_t __bundle_size(std::size_t array_size_) noexcept
    {   return offsetof(__array_ref_bundle, payload_pad) + sizeof(T) * array_size_;   }
    
    __base_shared_ref() noexcept : __bundle(nullptr) {}
    __base_shared_ref(__array_ref_bundle *bundle_) noexcept : __bundle(bundle_) {}
};

// ---

template<typename T>
class shared_ref<T[]> : public __base_shared_ref<T[]>
{
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
private:
    using _Base = __base_shared_ref<T[]>;
    using typename _Base::__array_ref_bundle;
    using _Base::__bundle;
    
    static __array_ref_bundle *__make_bundle(allocator *alloc_, std::size_t size_)
    {
        allocator::unwinder_dealloc<__array_ref_bundle> w{
            alloc_, static_cast<__array_ref_bundle *>(alloc_->allocate(
                _Base::__bundle_size(size_), alignof(__array_ref_bundle)
            ))
        };
        ::new((void *)w.curr) __array_ref_bundle(alloc_, size_), ++w.curr;
        
        allocator::unwinder<T> wa{w.begin->payload()};
        for(std::size_t i = 0; i < size_; ++i, ++wa.curr) ::new((void *)wa.curr) T{};
        
        wa.curr = nullptr; w.curr = nullptr;
        return (void)wa, w.begin;
    }
    
    template<typename... Inits>
    static __array_ref_bundle *__make_bundle_init(allocator *alloc_, Inits &&... inits_)
    {
        allocator::unwinder_dealloc<__array_ref_bundle> w{
            alloc_, static_cast<__array_ref_bundle *>(alloc_->allocate(
                _Base::__bundle_size(sizeof...(Inits)), alignof(__array_ref_bundle)
            ))
        };
        ::new((void *)w.curr) __array_ref_bundle(alloc_, sizeof...(Inits)), ++w.curr;
        
        allocator::unwinder<T> wa{w.begin->payload()};
        (void)(T *[]){
            (::new((void *)wa.curr) T{std::forward<Inits>(inits_)}, ++wa.curr)...
        };
        
        wa.curr = nullptr; w.curr = nullptr;
        return (void)wa, w.begin;
    }
    
    shared_ref(__array_ref_bundle *bundle_) noexcept : _Base(bundle_) {}
    
    void __deref() noexcept
    {
        if(__bundle != nullptr && --(__bundle->refs_count) == 0)
        {
            T *begin = __bundle->payload(), *curr = begin + __bundle->size;
            while(curr > begin) (--curr)->~T();
            
            if(--(__bundle->weak_refs_count) == 0) __bundle->alloc->destroy(__bundle);
        }
    }
    
public:
    static shared_ref<T[]> make(std::size_t size_)
    {
        return shared_ref<T[]>(__make_bundle(&std_allocator, size_));
    }
    
    static shared_ref<T[]> makeAlloc(allocator *alloc_, std::size_t size_)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return shared_ref<T[]>(__make_bundle(alloc_, size_));
    }
    
    template<typename... Inits>
    static shared_ref<T[]> makeInit(Inits &&... inits_)
    {
        return shared_ref<T[]>(__make_bundle_init(
            &std_allocator, std::forward<Inits>(inits_)...
        ));
    }
    
    template<typename... Inits>
    static shared_ref<T[]> makeInitAlloc(allocator *alloc_, Inits &&... inits_)
    {
        if(alloc_ == nullptr) throw std::bad_alloc();
        return shared_ref<T[]>(__make_bundle_init(
            alloc_, std::forward<Inits>(inits_)...
        ));
    }
    
    shared_ref() noexcept {}
    shared_ref(std::nullptr_t) noexcept {}
    
    shared_ref(weak_ref<T[]> const &cp) noexcept : _Base(cp.__strongify()) {}
    
    shared_ref(shared_ref<T[]> const &cp) noexcept : _Base(cp.__bundle)
    {   if(__bundle != nullptr) ++(__bundle->refs_count);   }
    
    shared_ref(shared_ref<T[]> &&mv) noexcept : _Base(mv.__bundle)
    {   mv.__bundle = nullptr;   }
    
    ~shared_ref() noexcept {   __deref();   }
    
    shared_ref &operator =(std::nullptr_t) noexcept
    {
        __deref(); __bundle = nullptr; return *this;
    }
    
    shared_ref &operator =(weak_ref<T[]> const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref(); __bundle = cp.__strongify();
        return *this;
    }
    
    shared_ref &operator =(shared_ref<T[]> const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref();
        if((__bundle = cp.__bundle) != nullptr) ++(cp.__bundle->refs_count);
        return *this;
    }
    
    shared_ref &operator =(shared_ref<T[]> &&mv) noexcept
    {
        if(__bundle == mv.__bundle) return *this;
        __deref(); __bundle = mv.__bundle; mv.__bundle = nullptr;
        return *this;
    }
    
    T &operator [](std::size_t i) const noexcept
    {   return __bundle->payload()[i];   }
    
    explicit operator bool() const noexcept
    {   return(__bundle != nullptr);   }
    
    bool operator !() const noexcept
    {   return(__bundle == nullptr);   }
    
    bool operator ==(shared_ref<T[]> const &r) const noexcept
    {   return(__bundle == r.__bundle);   }
    
    bool operator !=(shared_ref<T[]> const &r) const noexcept
    {   return(__bundle != r.__bundle);   }
    
    std::size_t size() const noexcept
    {   return (__bundle != nullptr)? __bundle->size : 0;   }
    
    unsigned int useCount() const noexcept
    {   return (__bundle != nullptr)? __bundle->refs_count.load() : 0;   }
    
    T *base() const noexcept
    {   return __bundle->payload();   }
};

// --- *** ---


#if __cplusplus >= 201703L
template<typename T>
shared_ref(weak_ref<T> const &) -> shared_ref<T>;

template<typename T>
shared_ref(shared_ref<T> const &) -> shared_ref<T>;

template<typename T>
shared_ref(shared_ref<T> &&) -> shared_ref<T>;
#endif


} //- namespace memory

#endif //- _MEMORY__SHARED_REF_HPP

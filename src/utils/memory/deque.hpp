#ifndef _MEMORY__DEQUE_HPP
#define _MEMORY__DEQUE_HPP

#include "allocator.hpp"
#include <deque>

namespace memory
{

template<typename V>
using deque = std::deque<V, std_alloc_wrapper<V>>;

}

#endif //- _MEMORY__DEQUE_HPP

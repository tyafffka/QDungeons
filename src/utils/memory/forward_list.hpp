#ifndef _MEMORY__FORWARD_LIST_HPP
#define _MEMORY__FORWARD_LIST_HPP

#include "allocator.hpp"
#include <forward_list>

namespace memory
{

template<typename V>
using forward_list = std::forward_list<V, std_alloc_wrapper<V>>;

}

#endif //- _MEMORY__FORWARD_LIST_HPP

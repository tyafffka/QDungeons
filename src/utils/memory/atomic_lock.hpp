#ifndef _MEMORY__ATOMIC_LOCK_HPP
#define _MEMORY__ATOMIC_LOCK_HPP

#include <atomic>

namespace memory
{


struct atomic_lock
{
    std::atomic_flag &f;
    
    atomic_lock(std::atomic_flag &f_) noexcept : f(f_)
    {   while(f_.test_and_set(std::memory_order_acquire));   }
    
    ~atomic_lock() noexcept
    {   f.clear(std::memory_order_release);   }
};


} //- namespace memory

#endif //- _MEMORY__ATOMIC_LOCK_HPP

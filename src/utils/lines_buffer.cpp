#include "lines_buffer.hpp"
#include <algorithm>

#define __bit7 (uint8_t)0x80
#define __bit6 (uint8_t)0x40
#define __bit5 (uint8_t)0x20
#define __bit4 (uint8_t)0x10
#define __bit3 (uint8_t)0x08
#define __bit2 (uint8_t)0x04
#define __bit1 (uint8_t)0x02

#define __mask1 (uint8_t)0x01
#define __mask2 (uint8_t)0x03
#define __mask3 (uint8_t)0x07
#define __mask4 (uint8_t)0x0F
#define __mask5 (uint8_t)0x1F
#define __mask6 (uint8_t)0x3F


void utf8_lines_buffer::_decode_utf8(std::string const &in_,
                                     std::function<void(uint32_t utf_ch)> yield_)
{
    uint32_t utf_ch = 0;
    uint8_t multibyte_remain = 0;
    
    for(char const *it = in_.c_str(); *it; ++it)
    {
        uint8_t ch = *reinterpret_cast<uint8_t const *>(it);
        
        if((ch & __bit7) == 0) // (0xxx xxxx) regular char (7 bit)
        {
            multibyte_remain = 0; yield_((uint32_t)ch);
        }
        else if((ch & __bit6) == 0) // (10xx xxxx) continuation of multibyte
        {
            if(multibyte_remain > 0) // corrupted otherwise
            {
                utf_ch = (utf_ch << 6) | (ch & __mask6);
                if(--multibyte_remain == 0) yield_(utf_ch);
            }
        }
        else if((ch & __bit5) == 0) // (110x xxxx) start of 2-byte sequence (11 bit)
        {
            multibyte_remain = 1; utf_ch = ch & __mask5;
        }
        else if((ch & __bit4) == 0) // (1110 xxxx) start of 3-byte sequence (16 bit)
        {
            multibyte_remain = 2; utf_ch = ch & __mask4;
        }
        else if((ch & __bit3) == 0) // (1111 0xxx) start of 4-byte sequence (21 bit)
        {
            multibyte_remain = 3; utf_ch = ch & __mask3;
        }
        else if((ch & __bit2) == 0) // (1111 10xx) start of 5-byte sequence (26 bit)
        {
            multibyte_remain = 4; utf_ch = ch & __mask2;
        }
        else if((ch & __bit1) == 0) // (1111 110x) start of 6-byte sequence (31 bit)
        {
            multibyte_remain = 5; utf_ch = ch & __mask1;
        }
        else // corrupted
        {   multibyte_remain = 0;   }
    }
    return;
}


void utf8_lines_buffer::_do_format(std::string const &format_,
                                   _t_params_pack const &params_, uint8_t style_,
                                   std::function<void(_t_char ch, uint8_t style)> yield_)
{
    bool is_escape = false, is_param = false;
    
    _decode_utf8(format_, [&](uint32_t utf_ch)
    {
        if((0x0080 <= utf_ch && utf_ch < 0x00A0) || 0xD800 <= utf_ch)
            return; // (continue)
        
        if(is_param)
        {
            int arg_i = -1;
            if('0' <= utf_ch && utf_ch <= '9') arg_i = (int)(utf_ch - '0');
            else if('a' <= utf_ch && utf_ch <= 'f') arg_i = (int)(utf_ch - 'a' + 10);
            else if('A' <= utf_ch && utf_ch <= 'F') arg_i = (int)(utf_ch - 'A' + 10);
            
            if(0 <= arg_i && arg_i < params_.size())
            {
                lines_buffer_content const &param = params_[arg_i];
                std::size_t length = std::min(param.chars.size(), param.styles.size());
                
                for(std::size_t i = 0; i < length; ++i)
                {
                    _t_char p_utf_ch = param.chars[i];
                    uint8_t p_style = param.styles[i];
                    
                    if((0x0080 <= p_utf_ch && p_utf_ch < 0x00A0) || 0xD800 <= p_utf_ch)
                        continue;
                    
                    if((p_style & LINES_BUFFER_COLOR_MASK) == LINES_BUFFER_DEFAULT_COLOR)
                        p_style = (p_style & (~LINES_BUFFER_COLOR_MASK)) | (style_ & LINES_BUFFER_COLOR_MASK);
                    if((style_ & LINES_BUFFER_ACCENT) != 0)
                        p_style |= LINES_BUFFER_ACCENT;
                    
                    yield_((_t_char)p_utf_ch, p_style);
                }
            }
            else
            {
                yield_((_t_char)LINES_BUFFER_PARAM_CHAR, LINES_BUFFER_DEFAULT_COLOR);
                yield_((_t_char)utf_ch, LINES_BUFFER_DEFAULT_COLOR);
            }
            is_param = false;
        }
        else if(is_escape)
        {
            if('0' <= utf_ch && utf_ch <= '9')
                style_ = (style_ & (~LINES_BUFFER_COLOR_MASK)) | (uint8_t)(utf_ch - '0');
            else if('a' <= utf_ch && utf_ch <= 'f')
                style_ = (style_ & (~LINES_BUFFER_COLOR_MASK)) | (uint8_t)(utf_ch - 'a' + 10);
            else if('A' <= utf_ch && utf_ch <= 'F')
                style_ = (style_ & (~LINES_BUFFER_COLOR_MASK)) | (uint8_t)(utf_ch - 'A' + 10);
            else if(utf_ch == LINES_BUFFER_ACCENT_CHAR)
                style_ |= LINES_BUFFER_ACCENT;
            else if(utf_ch == LINES_BUFFER_ACCENT_END_CHAR)
                style_ &= ~LINES_BUFFER_ACCENT;
            else if(utf_ch == LINES_BUFFER_PARAM_CHAR)
                is_param = true;
            else if(utf_ch == LINES_BUFFER_ESCAPE_CHAR)
                yield_((_t_char)utf_ch, style_);
            else
                yield_((_t_char)utf_ch, LINES_BUFFER_DEFAULT_COLOR);
            
            is_escape = false;
        }
        else if(utf_ch == LINES_BUFFER_ESCAPE_CHAR)
        {   is_escape = true;   }
        else
        {   yield_((_t_char)utf_ch, style_);   }
    });
    return;
}


void utf8_lines_buffer::_put_format(std::string const &format_, _t_params_pack const &params_)
{
    _t_write_iter line_it = _lines.end();
    uint8_t start_style = LINES_BUFFER_DEFAULT_COLOR;
    
    if(line_it != _lines.begin())
    {
        --line_it;
        start_style = line_it->styles.back();
        if(line_it->chars.back() == '\n') ++line_it; // need new line (go to end)
    }
    
    _do_format(format_, params_, start_style, [&](_t_char ch, uint8_t style)
    {
        if(line_it == _lines.end()) // create new line
        {
            if(_max_lines > 0 && _lines.size() >= _max_lines) _lines.erase(_lines.begin());
            line_it = _lines.insert(_lines.end(), lines_buffer_content{});
        }
        
        line_it->chars.push_back(ch); line_it->styles.push_back(style);
        
        if(_max_width > 0 && line_it->chars.size() >= _max_width) // wrap line by width limit
        {
            line_it->chars.push_back((ch = '\n')); line_it->styles.push_back(style);
            // Tip: placing line-feed just afted wrapping cause empty line at end. And I don't intend to fix it.
        }
        
        if(ch == '\n') ++line_it; // need new line (go to end)
    });
    return;
}


bool utf8_lines_buffer::_get_iterators(_t_read_iter &out_from_, _t_read_iter &out_to_,
                                       int from_, int to_) const
{
    int list_size = (int)_lines.size();
    if(from_ < 0) from_ += list_size;
    if(to_ <= 0) to_ += list_size;
    
    // assertion
    if((from_ < 0 || list_size <= from_) || (to_ <= 0 || list_size < to_) || from_ >= to_)
        return false;
    
    if(from_ < list_size / 2)
    {
        out_from_ = _lines.begin();
        for(int c = from_; c > 0; --c) ++out_from_;
    }
    else
    {
        out_from_ = _lines.end();
        for(int c = from_; c < list_size; ++c) --out_from_;
    }
    
    out_to_ = out_from_;
    for(int c = from_; c < to_; ++c) ++out_to_;
    return true;
}

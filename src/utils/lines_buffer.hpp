#ifndef __UTILS_LINES_BUFFER_HPP
#define __UTILS_LINES_BUFFER_HPP

#include <vector>
#include <list>
#include <string>
#include <sstream>
#include <functional>
#include <stdint.h>

#define LINES_BUFFER_COLOR_MASK    ((uint8_t)0x0F)
#define LINES_BUFFER_DEFAULT_COLOR ((uint8_t)0x0F)
#define LINES_BUFFER_ACCENT        ((uint8_t)0x80)

#define LINES_BUFFER_ESCAPE_CHAR     '&'
#define LINES_BUFFER_ACCENT_CHAR     '{'
#define LINES_BUFFER_ACCENT_END_CHAR '}'
#define LINES_BUFFER_PARAM_CHAR      '_'


struct lines_buffer_content
{
    std::u16string chars{};
    std::vector<uint8_t> styles{};
};


/* Контейнер для многострочного моноширного текста с опциональным ограничением
 * на количество строк и длину строки. При вводе осуществляется декодирование
 * текста из UTF-8, автоматический перенос по длине строки и вытеснение
 * строк, находящихся вначале. Каждый символ контейнера обладает дополнительными
 * свойствами, такими как: номер цвета (от 0 до 15), флаг стилевого выделения.
 * 
 * При вводе класс использует форматирование текста со следующими
 * специальными последовательностями:
 * - `&<n>`, где <n> -- шестнадцатеричная цифра от `0` до `f` (без учёта
 *   регистра) -- изменить цвет дальнейшего текста на цвет с номером <n>;
 * - `&{` -- добавить дальнейшему тексту стилевое выделение;
 * - `&}` -- отменить стилевое выделение дальнейшего текста;
 * - `&_<n>`, где <n> -- шестнадцатеричная цифра от `0` до `f` (без учёта
 *   регистра) -- подставить значение параметра с номером <n>; если такого
 *   параметра нет или <n> -- не шестнадцатеричная цифра, то будет выведено
 *   `_<n>` без выделения, с цветом по умолчанию;
 * - `&&` -- подставить одиночный символ `&`;
 * - если за `&` следует любой другой символ, он будет выведен без выделения,
 *   с цветом по умолчанию.
 * 
 * В конструктор передаются ограничения на количество строк и длину строки.
 * Каждое ограничение будет действовать только при значении больше нуля. */
class utf8_lines_buffer
{
protected /*static*/:
    using _t_char = std::u16string::value_type;
    using _t_params_pack = std::vector<lines_buffer_content>;
    using _t_read_iter = std::list<lines_buffer_content>::const_iterator;
    using _t_write_iter = std::list<lines_buffer_content>::iterator;
    
    static void _decode_utf8(std::string const &in_,
                             std::function<void(uint32_t utf_ch)> yield_);
    
    static void _do_format(std::string const &format_,
                           _t_params_pack const &params_, uint8_t style_,
                           std::function<void(_t_char ch, uint8_t style)> yield_);
    
    template<typename _Param>
    static void _put_param(_Param const &param_, _t_params_pack &v_)
    {
        lines_buffer_content content{};
        std::stringstream in{}; in << param_;
        
        _decode_utf8(in.str(), [&](uint32_t utf_ch){
            content.chars.push_back((_t_char)utf_ch);
        });
        content.styles.assign(content.chars.size(), LINES_BUFFER_DEFAULT_COLOR);
        
        v_.push_back(std::move(content));
    }
    
    static void _put_param(lines_buffer_content &&param_, _t_params_pack &v_)
    {   v_.push_back(std::move(param_));   }
    
protected:
    std::list<lines_buffer_content> _lines;
    int const _max_width, _max_lines;
    
    void _put_format(std::string const &format_, _t_params_pack const &params_);
    
    bool _get_iterators(_t_read_iter &out_from_, _t_read_iter &out_to_,
                        int from_, int to_) const;
public:
    explicit utf8_lines_buffer(int max_width_=0, int max_lines_=0) :
        _lines(), _max_width(max_width_), _max_lines(max_lines_)
    {}
    
    /* Форматирует текст из `format_` по правилам, описанным выше, и вводит
     * его в контейнер. `params_` -- набор параметров для подстановки. */
    template<typename... _Params>
    void putFormat(std::string const &format_, _Params &&... params_)
    {
        _t_params_pack v; v.reserve(sizeof...(_Params));
        (void)(int[]){(_put_param(std::forward<_Params>(params_), v), 0)...};
        
        _put_format(format_, v);
    }
    
    /* Возвращает текущее количество сформированных строк */
    int linesCount() const {   return (int)_lines.size();   }
    
    /* Извлекает из контейнера строки с `from_` (включительно) по `to_`
     * и возвращает их (переносы строк присутствуют).
     * Если `from_` или `to_` отрицательный, то отсчёт ведётся с конца.
     * `to_`, равный нулю, указывает на конец контейнера. */
    lines_buffer_content getLines(int from_, int to_) const
    {
        lines_buffer_content out{};
        
        _t_read_iter itbegin, itend;
        if(_get_iterators(itbegin, itend, from_, to_))
        {
            size_t total_size = 0;
            for(_t_read_iter it = itbegin; it != itend; ++it) total_size += it->chars.size();
            
            out.chars.reserve(total_size); out.styles.reserve(total_size);
            for(_t_read_iter it = itbegin; it != itend; ++it)
            {
                out.chars.append(it->chars);
                out.styles.insert(out.styles.end(), it->styles.begin(), it->styles.end());
            }
        }
        return out;
    }
    
    /* Форматирует текст из `format_` и `params_` по правилам,
     * описанным выше, и возвращает результат. */
    template<typename... _Params>
    static lines_buffer_content format(std::string const &format_, _Params &&... params_)
    {
        _t_params_pack v; v.reserve(sizeof...(_Params));
        (void)(int[]){(_put_param(std::forward<_Params>(params_), v), 0)...};
        
        lines_buffer_content out{};
        _do_format(format_, v, LINES_BUFFER_DEFAULT_COLOR, [&](_t_char ch, uint8_t style)
        {
            out.chars.push_back(ch); out.styles.push_back(style);
        });
        return out;
    }
};


#endif //- __UTILS_LINES_BUFFER_HPP

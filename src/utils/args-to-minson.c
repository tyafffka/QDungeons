#include "args-to-minson.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/* named:
 *   (string) => arg-(string)
 *   (number) => arg-(number)
 *   (float) => arg-(float)
 *   (bool) => flag-(bool)
 *   (*) => arg-(string)
 *   (sub-array) {=>} args-(string)
 * unnamed:
 *   positional args.
**/

typedef char *const *t_args_iter;


/* Return "is value used". */
static bool __put_value(t_minson_iter arg_, char const *value_)
{
    if(minsonIterObject(arg_)->type == C_MINSON_BOOLEAN)
    {
        ((t_minson_boolean *)minsonIterObject(arg_))->value = 1;
        return false;
    }
    
    if(value_ == NULL) return true;
    
    t_minson_object *new__ = NULL;
    switch(minsonIterObject(arg_)->type)
    {
    case C_MINSON_ARRAY:
        minsonArrayAddString(minsonIterGetArray(arg_), NULL, value_);
        return true;
        
    case C_MINSON_NUMBER:
        new__ = minsonCreateNumber(minsonIterName(arg_), atol(value_));
        break;
        
    case C_MINSON_FLOAT_NUMBER:
        new__ = minsonCreateFloatNumber(minsonIterName(arg_), atof(value_));
        break;
        
    default:
        new__ = minsonCreateString(minsonIterName(arg_), value_);
    }
    
    minsonDelete(minsonIterObject(arg_));
    if(new__ != NULL) minsonIterObject(arg_) = new__;
    return true;
}


void args_to_minson(t_minson_array *minson_,
                    t_minson_array *shorts_map_,
                    int argc, t_args_iter argv)
{
    if(minson_ == NULL || shorts_map_ == NULL ||
       argc <= 1 || argv == NULL)
    {   return;   }
    
    t_args_iter argend = argv + argc;
    for(t_args_iter argi = argv + 1; argi < argend; ++argi)
    {
        char const *arg = *argi;
        
        if(arg[0] != '-') // positional
        {
            minsonArrayAddString(minson_, NULL, arg); continue;
        }
        
        // named
        if(arg[1] == '-') // long named
        {
            if(arg[2] == '\0') // end of flags
            {
                for(++argi; argi < argend; ++argi)
                    minsonArrayAddString(minson_, NULL, *argi);
                return;
            }
            
            char *value = strchr(arg + 2, '=');
            if(value != NULL) *value++ = '\0';
            
            t_minson_iter found = minsonArrayGetIter(minson_, arg + 2);
            if(found == NULL) continue;
            
            if(value == NULL && (argi + 1) < argend)
            {
                if(__put_value(found, argi[1])) ++argi;
            }
            else
            {
                __put_value(found, value);
                if(value != NULL) *--value = '=';
            }
            continue;
        }
        else if(arg[1] == '\0') // 'stdin' argument
        {
            t_minson_object *found = minsonArrayGet(minson_, arg);
            if(found != NULL) if(found->type == C_MINSON_BOOLEAN)
            {
                ((t_minson_boolean *)found)->value = 1;
            }
            
            minsonArrayAddString(minson_, NULL, arg);
            continue;
        }
        
        // short named
        for(char const *p_ch = arg + 1; *p_ch; ++p_ch)
        {
            char short_name[2] = {*p_ch, '\0'};
            char const *long_name = minsonArrayGetString(shorts_map_, short_name);
            if(long_name == NULL) continue;
            
            t_minson_iter found = minsonArrayGetIter(minson_, long_name);
            if(found == NULL) continue;
            
            if(p_ch[1] != '\0')
            {
                if(__put_value(found, p_ch + 1)) break;
            }
            else if((argi + 1) < argend)
            {
                if(__put_value(found, argi[1])) ++argi;
            }
            else
            {   __put_value(found, NULL);   }
        } //- for(short flags)
    } //- for(args)
    return;
}

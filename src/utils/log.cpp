#include "log.hpp"
#include <string.h>


TLog defaultLog{};


TLog::TStream::TStream(TLog &base_, char const *tag_, char const *trace_) :
    __log(base_), __tag(tag_), __trace(trace_), __enabled(true)
{
    for(std::string const &tag : __log._excluded_tags)
    {
        if(tag.compare(__tag) == 0)
        {   __enabled = false; return;   }
    }
    
    if(trace_ != nullptr && trace_[0] != '\0' && __log._is_short_trace)
    {
        if((trace_ = strrchr(__trace, '/')) != nullptr)
            __trace = trace_ + 1;
        else if((trace_ = strrchr(__trace, '\\')) != nullptr)
            __trace = trace_ + 1;
    }
    
    time_t t = time(nullptr);
    tm *now = localtime(&t);
    
    snprintf(__timestamp, 22, "%2d:%02d:%02d / %2d.%02d.%04d",
             now->tm_hour, now->tm_min, now->tm_sec,
             now->tm_mday, now->tm_mon + 1, now->tm_year + 1900);
    return;
}


TLog::TStream::~TStream()
{
    if(! __enabled) return;
    
    std::string message = __buffer.str();
    __buffer.clear();
    
    for(BaseLogProvider *p : __log._providers)
        p->write(__tag, __trace, __timestamp, message);
    return;
}


TLog::~TLog() noexcept
{
    for(BaseLogProvider *p : _providers) delete p;
    return;
}


void TLog::writeMessage(char const *tag_, std::string const &message_,
                        char const *trace_)
{
    for(std::string const &tag : _excluded_tags)
    {
        if(tag.compare(tag_) == 0) return;
    }
    
    if(trace_ != nullptr && trace_[0] != '\0' && _is_short_trace)
    {
        char const *trace__;
        if((trace__ = strrchr(trace_, '/')) != nullptr)
            trace_ = trace__ + 1;
        else if((trace__ = strrchr(trace_, '\\')) != nullptr)
            trace_ = trace__ + 1;
    }
    
    time_t t = time(nullptr);
    tm *now = localtime(&t);
    
    char timestamp[22];
    snprintf(timestamp, 22, "%2d:%02d:%02d / %2d.%02d.%04d",
             now->tm_hour, now->tm_min, now->tm_sec,
             now->tm_mday, now->tm_mon + 1, now->tm_year + 1900);
    
    for(BaseLogProvider *p : _providers)
        p->write(tag_, trace_, timestamp, message_);
    return;
}

// --- *** ---

#include <iostream>

#if ! defined(__WIN32__) && ! defined(_WIN32) && ! defined(__CYGWIN__)
# define SYSLOG_NAMES
# include <syslog.h>
#endif


void TStdLogProvider::write(char const *tag_, char const *trace_,
                            char const *timestamp_, std::string const &message_)
{
    std::lock_guard<std::mutex> lock(_write_mutex);
    
    std::clog << "[" << timestamp_ << "]: " << tag_;
    if(trace_ != nullptr) std::clog << " [" << trace_ << "]";
    std::clog << ": " << message_ << std::endl;
    return;
}

// ---


std::string TFileLogProvider::_generate_timed_name(std::string const &prefix_,
                                                   std::string const &suffix_)
{
    std::string res;
    res.reserve(prefix_.size() + 20 + suffix_.size());
    
    time_t t = time(nullptr);
    tm *now = localtime(&t);
    
    char timestamp[21];
    snprintf(timestamp, 21, "%04d_%02d_%02d__%02d_%02d_%02d",
             now->tm_year + 1900, now->tm_mon + 1, now->tm_mday,
             now->tm_hour, now->tm_min, now->tm_sec);
    
    res.append(prefix_);
    res.append(timestamp);
    res.append(suffix_);
    return res;
}

TFileLogProvider::TFileLogProvider(std::string const &filename_) :
    _file(filename_, std::ios_base::out | std::ios_base::app)
{}

TFileLogProvider::TFileLogProvider(std::string const &prefix_,
                                   std::string const &suffix_) :
    _file(_generate_timed_name(prefix_, suffix_),
          std::ios_base::out | std::ios_base::app)
{}

void TFileLogProvider::write(char const *tag_, char const *trace_,
                             char const *timestamp_, std::string const &message_)
{
    std::lock_guard<std::mutex> lock(_write_mutex);
    
    _file << "[" << timestamp_ << "]: " << tag_;
    if(trace_ != nullptr) _file << " [" << trace_ << "]";
    _file << ": " << message_ << std::endl;
    return;
}

// ---


TSysLogProvider::TSysLogProvider(std::string const &priority_,
                                 std::string const &facility_,
                                 std::string const &identifier_)
{
#if ! defined(__WIN32__) && ! defined(_WIN32) && ! defined(__CYGWIN__)
    
    _priority = LOG_NOTICE;
    for(CODE *p = prioritynames; p->c_name != nullptr; ++p)
        if(priority_.compare(p->c_name) == 0)
        {   _priority = p->c_val; break;   }
    
    int facility = -1;
    for(CODE *p = facilitynames; p->c_name != nullptr; ++p)
        if(facility_.compare(p->c_name) == 0)
        {   facility = p->c_val; break;   }
    
    if((_has_opened =(facility != -1)))
    {
        openlog((identifier_.empty() ? nullptr : identifier_.c_str()),
                LOG_PID | LOG_NDELAY | LOG_NOWAIT,
                facility);
    }
#else
    (void)priority_; (void)facility_; (void)identifier_;
#endif
    return;
}

TSysLogProvider::~TSysLogProvider()
{
#if ! defined(__WIN32__) && ! defined(_WIN32) && ! defined(__CYGWIN__)
    if(_has_opened) closelog();
#endif
    return;
}

void TSysLogProvider::write(char const *tag_, char const *trace_,
                            char const *timestamp_, std::string const &message_)
{
    (void)trace_;
#if ! defined(__WIN32__) && ! defined(_WIN32) && ! defined(__CYGWIN__)
    syslog(_priority, "[%s]: %s: %s", timestamp_, tag_, message_.c_str());
#else
    (void)tag_; (void)timestamp_; (void)message_;
#endif
    return;
}

// ---


TFilterLogProvider::~TFilterLogProvider()
{
    delete _original_log; return;
}

void TFilterLogProvider::write(char const *tag_, char const *trace_,
                               char const *timestamp_, std::string const &message_)
{
    if(_original_log == nullptr) return;
    
    for(std::string const &tag : _passed_tags)
    {
        if(strcmp(tag_, tag.c_str()) == 0)
        {
            _original_log->write(tag_, trace_, timestamp_, message_);
            return;
        }
    }
    return;
}

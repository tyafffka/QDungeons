#ifndef __UTILS_ACTIVITIES_HPP
#define __UTILS_ACTIVITIES_HPP

#include "memory/allocator.hpp"
#include "events.hpp"
#include "timers.hpp"
#include <atomic>
#include <utility>
#include <type_traits>


/* Аллокатор, который используется для создания новых активностей. */
extern memory::allocator *activityAllocator;


/* Активность -- базовый компонент приложения, который выполняет
 * какую-либо работу в цикле (события обрабатываются автоматически).
 * При вызове других активностей образуется стек вызовов. */
class BaseActivity : public base_event_handler
{
    template<class _Activity, typename... _Args>
    friend void runActivity(_Args &&...);
    
private:
    struct __t_context
    {
        TTimeCounter time;
        std::atomic<bool> is_cooldown;
        std::atomic<bool> is_finish_all;
        
        __t_context() noexcept;
        ~__t_context() noexcept;
    };
    friend struct __t_context;
    
    static thread_local __t_context *__current_context;
    
    __t_context *__context;
    BaseActivity *__next;
    std::atomic<bool> __is_active;
    std::atomic<bool> __is_finish;
    
    void __add_activity(BaseActivity *activity_) noexcept;
    void __run();
    
protected:
    /* Помещает новую активность на стек. Аргументы передаются
     * в конструктор объекта. Переход будет осуществлён в следующем цикле. */
    template<class _Activity, typename... _Args>
    _Activity *start(_Args &&... args_)
    {
        static_assert(std::is_base_of<BaseActivity, _Activity>::value,
                      "Not an activity class!");
        
        __current_context = __context;
        _Activity *p = new(activityAllocator) _Activity{std::forward<_Args>(args_)...};
        __add_activity(p);
        return p;
    }
    
public:
    BaseActivity();
    virtual ~BaseActivity() noexcept;
    
    /* Вызывается при выходе активности на передний план.
     * Если произошёл возврат назад по стеку, `back_from_` будет указывать
     * на объект активности, из которой был произведён возврат. */
    virtual void onStart(BaseActivity *back_from_) {}
    /* Основной метод, вызываемый в цикле. */
    virtual void onLoop() {}
    /* Вызывается при уходе активности с переднего плана. */
    virtual void onStop() {}
    
    /* Завершает работу активности. Если новой активности на стеке нет,
     * производит возврат назад по стеку. */
    void finish() noexcept;
    /* Завершает работу всех активностей системы и удаляет весь текущий стек.
     * После этого работа системы будет завершена. */
    void finishAll() noexcept;
    /* Возвращает, находится ли активность на переднем плане. */
    bool isActive() noexcept;
    /* Возвращает признак завершения работы активности. */
    bool isFinish() noexcept;
    
    /* Сбрасывает флаг простоя. Если этот флаг останется установленным,
     * то в конце цикла обработки будет вызвана минимальная задержка
     * ради снижения нагрузки простоя. */
    void cancelCooldown() noexcept;
    /* Возвращает счётчик времени, который следует использовать для таймеров.
     * Данный счётчик обновляется в начале цикла обработки. */
    TTimeCounter const &timeCounter() noexcept;
};

// --- *** ---


/* Запускает систему активностей и создаёт одну активность в ней.
 * Аргументы передаются в конструктор объекта.
 * Возвращается после завершения работы всех активностей. */
template<class _Activity, typename... _Args>
inline void runActivity(_Args &&... args_)
{
    static_assert(std::is_base_of<BaseActivity, _Activity>::value,
                  "Not an activity class!");
    
    BaseActivity::__t_context ctx{};
    _Activity activity{std::forward<_Args>(args_)...};
    static_cast<BaseActivity &>(activity).__run();
}


#endif //- __UTILS_ACTIVITIES_HPP

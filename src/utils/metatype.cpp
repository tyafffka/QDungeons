#include "metatype.hpp"
#include <stdexcept>
#include <stdint.h>


thread_local BaseMetatypeObject::MetatypeData *
    BaseMetatypeObject::__incoming_metatype_data;


BaseMetatypeObject::MetatypeData::~MetatypeData() noexcept
{
    if(__type_name != nullptr)
        __alloc->deallocate(const_cast<char *>(__type_name));
}

// --- *** ---


BaseMetatypeObject::BaseMetatypeObject() noexcept
{
    __metatype_data = __incoming_metatype_data;
    ++(__metatype_data->__refs_count);
    return;
}


void BaseMetatypeObject::destroy() noexcept
{
    MetatypeData *data = this->__metatype_data;
    __base_metatype_objects_factory *factory = data->__factory;
    memory::allocator *alloc = data->__alloc;
    
    factory->deleteObject(this, alloc);
    if(--(data->__refs_count) <= 0) factory->deleteData(data, alloc);
    return;
}

// --- *** ---


static __base_metatype_objects_factory *__factory_list = nullptr;


__base_metatype_objects_factory::__base_metatype_objects_factory(
    __base_metatype_objects_factory *using_guard,
    char const *class_name_
) noexcept : next(__factory_list), class_name(class_name_)
{
    (void)using_guard;
    __factory_list = this;
}

// --- *** ---


#if __cplusplus < 201703L
static inline uint32_t __hash_scramble(uint32_t k)
{
    k *= 0xcc9e2d51;
    k = (k << 15) | (k >> 17);
    k *= 0x1b873593;
    return k;
}

std::size_t MetatypeManager::__str_view_hash::operator ()(memory::string_view str_) const noexcept
{
    char const *key = str_.data();
    uint32_t h = 0;
    uint32_t k;
    
    // Read in groups of 4.
    for(std::size_t i = str_.size() >> 2; i; --i)
    {
        // Here is a source of differing results across endiannesses.
        // A swap here has no effects on hash properties though.
        k = *reinterpret_cast<uint32_t const *>(key);
        key += sizeof(uint32_t);
        h ^= __hash_scramble(k);
        h = (h << 13) | (h >> 19);
        h = h * 5 + 0xe6546b64;
    }
    
    // Read the rest.
    k = 0;
    for(size_t i = str_.size() & 3; i; --i)
    {
        k <<= 8; k |= key[i - 1];
    }
    
    // A swap is *not* necessary here because the preceeding loop already
    // places the low bytes in the low places according to whatever endianness
    // we use. Swaps only apply when the memory is copied in a chunk.
    h ^= __hash_scramble(k);
    // Finalize.
    h ^= str_.size();
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}
#endif

// --- *** ---


MetatypeManager::MetatypeManager(memory::allocator *alloc_) noexcept :
    __alloc(alloc_), __types(alloc_)
{}


MetatypeManager::~MetatypeManager() noexcept
{
    for(auto const &pair : __types)
    {
        if(--(pair.second->__refs_count) <= 0)
            pair.second->__factory->deleteData(pair.second, __alloc);
    }
    return;
}


void MetatypeManager::createMetatype(memory::string_view type_name_,
                                     serialize::value_decoder &&parameters_)
{
    if(type_name_.size() == 0) throw std::invalid_argument(
        "MetatypeManager::createMetatype(): type name is empty"
    );
    if(__types.find(type_name_) != __types.end()) throw std::invalid_argument(
        "MetatypeManager::createMetatype(): type already exists"
    );
    
    __base_metatype_objects_factory *factory = nullptr;
    BaseMetatypeData *data;
    
    parameters_.as_map([&](serialize::map_decoder &&map)
    {
        map.get_sub("class", [&](serialize::value_decoder &&sub)
        {
            memory::string_view class_name = sub.get_string();
            for(auto *it = __factory_list; it != nullptr; it = it->next)
            {
                if(it->class_name == class_name) {   factory = it; break;   }
            }
        });
        
        if(factory != nullptr)
            data = factory->newData(std::move(map), __alloc);
    });
    if(factory == nullptr) throw std::invalid_argument(
        "MetatypeManager::createMetatype(): class not found"
    );
    
    data->__factory = factory;
    data->__alloc = __alloc;
    try
    {
        char *name = __alloc->allocate<char>(type_name_.size() + 1);
        data->__type_name = name;
        std::char_traits<char>::copy(name, type_name_.data(), type_name_.size());
    }
    catch(...)
    {   factory->deleteData(data, __alloc); throw;   }
    
    __types.insert(std::make_pair(
        memory::string_view{data->__type_name, type_name_.size()}, data
    ));
    ++(data->__refs_count);
    return;
}


BaseMetatypeObject *MetatypeManager::__new_object(memory::string_view type_name_) const
{
    auto found = __types.find(type_name_);
    if(found == __types.end()) return nullptr;
    
    BaseMetatypeData *data = found->second;
    BaseMetatypeObject::__incoming_metatype_data = data;
    return data->__factory->newObject(__alloc);
}


BaseMetatypeObject::MetatypeData *
    MetatypeManager::__get_data(memory::string_view type_name_) const noexcept
{
    auto found = __types.find(type_name_);
    if(found == __types.end()) return nullptr;
    return found->second;
}

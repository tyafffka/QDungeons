#include "input_action.hpp"
#include <algorithm>


BaseInputAction::__t_slot const *BaseInputAction::__find_slot(t_slot_id slot_id_) const noexcept
{
    auto found = std::lower_bound(__slots.begin(), __slots.end(), slot_id_);
    return (found != __slots.end() && found->id == slot_id_)? found.base() : nullptr;
}

// --- *** ---


void BaseInputAction::addSlot(t_slot_id slot_id_)
{
    __slots.push_back(__t_slot{slot_id_, C_NO_KEY, 0});
    std::inplace_merge(__slots.begin(), __slots.end() - 1, __slots.end());
    return;
}


void BaseInputAction::removeSlot(t_slot_id slot_id_) noexcept
{
    auto found = std::lower_bound(__slots.begin(), __slots.end(), slot_id_);
    if(found != __slots.end() && found->id == slot_id_) __slots.erase(found);
    return;
}


void BaseInputAction::setKey(t_slot_id slot_id_, c_key_type type_, t_key_num key_) noexcept
{
    __t_slot *slot = const_cast<__t_slot *>(__find_slot(slot_id_));
    if(slot != nullptr) {   slot->type = type_; slot->key = key_;   }
    return;
}


void BaseInputAction::resetKey(t_slot_id slot_id_) noexcept
{
    __t_slot *slot = const_cast<__t_slot *>(__find_slot(slot_id_));
    if(slot != nullptr) {   slot->type = C_NO_KEY; slot->key = 0;   }
    return;
}


BaseInputAction::c_key_type BaseInputAction::getKeyType(t_slot_id slot_id_) const noexcept
{
    __t_slot const *slot = __find_slot(slot_id_);
    return (slot != nullptr)? slot->type : C_NO_KEY;
}


BaseInputAction::t_key_num BaseInputAction::getKey(t_slot_id slot_id_) const noexcept
{
    __t_slot const *slot = __find_slot(slot_id_);
    return (slot != nullptr)? slot->key : (t_key_num)0;
}

// --- *** ---


bool BaseInputAction::isInputSilenced(t_slot_id slot_id_) const
{
    t_key_num c = getKeysCount(slot_id_);
    for(t_key_num k = 0; k < c; ++k)
    {
        if(getKeyPushed(slot_id_, k)) return false;
    }
    
    c = getAxesCount(slot_id_);
    for(t_key_num k = 0; k < c; ++k)
    {
        if(getAxisValue(slot_id_, k) != 0) return false;
    }
    return true;
}


bool BaseInputAction::catchKey(t_slot_id slot_id_)
{
    __t_slot *slot = const_cast<__t_slot *>(__find_slot(slot_id_));
    if(slot == nullptr) return true;
    
    t_key_num c = getAxesCount(slot_id_);
    for(t_key_num k = 0; k < c; ++k)
    {
        float value = getAxisValue(slot_id_, k);
        if(value > .0f)
        {
            slot->type = C_POSITIVE_SEMIAXIS; slot->key = k;
            return true;
        }
        else if(value < .0f)
        {
            slot->type = C_NEGATIVE_SEMIAXIS; slot->key = k;
            return true;
        }
    }
    
    c = getKeysCount(slot_id_);
    for(t_key_num k = 0; k < c; ++k)
    {
        if(getKeyPushed(slot_id_, k))
        {
            slot->type = C_KEY; slot->key = k;
            return true;
        }
    }
    return false;
}


void BaseInputAction::update()
{
    float new_value = .0f;
    for(__t_slot const &slot : __slots)
    {
        float v;
        switch(slot.type)
        {
        case C_KEY:
            if(getKeyPushed(slot.id, slot.key))
                if(new_value < 1.0f) new_value = 1.0f;
            break;
        case C_POSITIVE_SEMIAXIS:
            if((v = getAxisValue(slot.id, slot.key)) > .0f)
                if(new_value < v) new_value = v;
            break;
        case C_NEGATIVE_SEMIAXIS:
            if((v = getAxisValue(slot.id, slot.key)) < .0f)
                if(new_value < -v) new_value = -v;
            break;
        default:;
        }
    }
    
    _is_value_changed = (_value == .0f)?(new_value != .0f):(new_value == .0f);
    _value = new_value;
    return;
}

// --- *** ---


void InputConflictsContext::addAction(BaseInputAction const *action_,
                                      std::initializer_list<t_slot_id> slots_)
{
    for(t_slot_id slot_id : slots_)
    {
        BaseInputAction::__t_slot const *slot = action_->__find_slot(slot_id);
        __t_ident ident{slot->type, slot->key};
        
        auto found = __has_repeat.find(ident);
        if(found != __has_repeat.end()) found->second = true;
    }
    
    for(t_slot_id slot_id : slots_)
    {
        BaseInputAction::__t_slot const *slot = action_->__find_slot(slot_id);
        __t_ident ident{slot->type, slot->key};
        
        auto found = __has_repeat.find(ident);
        if(found == __has_repeat.end()) __has_repeat.insert(std::make_pair(ident, false));
    }
    return;
}


bool InputConflictsContext::isConflict(BaseInputAction const *action_,
                                       t_slot_id slot_id_) const noexcept
{
    BaseInputAction::__t_slot const *slot = action_->__find_slot(slot_id_);
    __t_ident ident{slot->type, slot->key};
    
    auto found = __has_repeat.find(ident);
    return(found != __has_repeat.end() && found->second);
}

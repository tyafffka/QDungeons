#ifndef __UTILS_EVENTS_HPP
#define __UTILS_EVENTS_HPP

/* Система событий с отложенным вызовом обработчиков через
 * очередь обработки событий.
 * 
 * Система потокобезопасна, однако при удалении обработчика или события
 * они не должны использоваться из другого потока!
 * При этом они автоматически отвязываются друг от друга. */

#include "memory/allocator.hpp"
#include <tuple>
#include <atomic>
#include <utility>
#include <type_traits>


namespace __event_traits
{


template<typename...> struct is_all_convertible;

template<>
struct is_all_convertible<>
{
    template<typename...>
    struct to : std::true_type {};
    
    template<typename _U, typename... _Us>
    struct to<_U, _Us...> : std::false_type {};
};

template<typename _T, typename... _Ts>
struct is_all_convertible<_T, _Ts...>
{
    template<typename...>
    struct to : std::true_type {};
    
    template<typename _U, typename... _Us>
    struct to<_U, _Us...> : std::conditional<
        std::is_convertible<_T, _U>::value,
        typename is_all_convertible<_Ts...>::template to<_Us...>,
        std::false_type
    >::type {};
};


template<typename...> struct is_all_storable;

template<>
struct is_all_storable<> : std::true_type {};

template<typename _T, typename... _Ts>
struct is_all_storable<_T, _Ts...> : std::conditional<
    ((! std::is_rvalue_reference<_T>::value) && (! std::is_array<_T>::value)),
    is_all_storable<_Ts...>,
    std::false_type
>::type {};


} //- namespace __event_traits


class __base_event;
template<typename> class event;
class base_event_handler;
struct __events_queue;


struct __base_event_args
{
    std::atomic<int> refs_count{1};
    
    virtual ~__base_event_args() noexcept = default;
};

template<typename... _Args>
struct __event_args : __base_event_args, std::tuple<_Args...>
{
    typedef std::tuple<_Args...> _Tuple;
    
    explicit __event_args(_Args &&... args_) : _Tuple(std::forward<_Args>(args_)...) {}
};


struct __base_event_binding
{
    std::atomic<int> refs_count{1};
    std::atomic_flag spinlock{true};
    
    bool linked_by_event, linked_by_object;
    __base_event_binding *prev_by_event, *next_by_event;
    __base_event_binding *prev_by_object, *next_by_object;
    
    __base_event *event;
    base_event_handler *object;
    
    virtual ~__base_event_binding() noexcept = default;
    virtual void invoke(__base_event_args *args_) = 0;
};

template<class _MObject, typename _MPointer, int _MArgC, typename... _Args>
struct __event_binding : __base_event_binding
{
    template<int _N, int... _I_s>
    struct gen : gen<_N - 1, _N - 1, _I_s...> {};
    
    template<int... _I_s>
    struct gen<0, _I_s...>
    {
        static void invoke(_MObject *object_, _MPointer method_,
                           std::tuple<_Args...> &args_)
        {   (object_->*method_)(std::get<_I_s>(args_)...);   }
    };
    
    _MPointer method;
    
    __event_binding(_MObject *object_, _MPointer method_) noexcept
    {   object = object_; method = method_;   }
    
    void invoke(__base_event_args *args_) override
    {
        gen<_MArgC>::invoke(static_cast<_MObject *>(object), method,
                            *static_cast<__event_args<_Args...> *>(args_));
    }
};


class __base_event
{
    template<typename> friend class event;
    friend class base_event_handler;
    
private:
    std::atomic_flag mutable __spinlock;
    __base_event_binding *__first;
    
    __base_event() noexcept;
    
    void __dispatch(__base_event_args *args_) const;
    void __bind(__base_event_binding *binding_) noexcept;
    void __unbind(base_event_handler *object_) noexcept;
    
public:
    ~__base_event() noexcept;
};

// --- *** ---


/* Аллокатор, используемый для внутренних структур системы.
 * Можно изменять только при отсутствии объектов системы. */
extern memory::allocator *eventsAllocator;


/* Объект события, объявляется с сигнатурой функции в качестве
 * параметра шаблона (возвращаемый тип должен быть `void`). */
template<typename... _Args>
class event<void(_Args...)> final : public __base_event
{
    static_assert(__event_traits::is_all_storable<_Args...>::value,
                  "All arguments must be suitable for storage");
public:
    event() noexcept = default;
    
    /* Сохраняет указанные параметры в очереди обработки
     * для всех привязанных обработчиков. */
    void operator ()(_Args... args_) const
    {
        typedef __event_args<_Args...> args_type;
        __dispatch(new(eventsAllocator) args_type{std::forward<_Args>(args_)...});
    }
    
    /* Привязать к событию метод `method_` обработчика `object_`.
     * Параметры метода должны быть совместимы с параметрами события:
     * каждый используемый параметр должен допускать преобразование
     * копированием из соответствующего параметра события. */
    template<class _Object, class _MObject, typename... _MArgs>
    void bind(_Object *object_, void (_MObject::*method_)(_MArgs...))
    {
        static_assert(std::is_base_of<base_event_handler, _MObject>::value,
                      "Not handler method");
        static_assert(__event_traits::is_all_convertible<_Args &...>::template to<_MArgs...>::value,
                      "Incompatible handler method");
        static_assert(std::is_base_of<_MObject, _Object>::value,
                      "Method can't be invoked on this object");
        
        typedef __event_binding<_MObject, decltype(method_),
                                sizeof...(_MArgs), _Args...> binding_type;
        if(object_ != nullptr && method_ != nullptr)
            __bind(new(eventsAllocator) binding_type{object_, method_});
    }
    
    /* Удалить привязку обработчика `object_` к событию. */
    void unbind(base_event_handler *object_) noexcept
    {
        if(object_ != nullptr) __unbind(object_);
    }
};


/* Базовый класс для обработчика событий. Очередь обработки одна на поток.
 * Создание, удаление объекта и цикл обработки должны быть в одном и том же
 * потоке. При обработке вызывается метод наследного класса, привязанный к
 * событию. Сигнатура метода должна быть совместима с сигнатурой события. */
class base_event_handler
{
    friend class __base_event;
    
private:
    std::atomic_flag mutable __spinlock;
    __events_queue *__queue;
    __base_event_binding *__first;
    
protected:
    base_event_handler() noexcept;
    
public:
    ~base_event_handler() noexcept;
    
    /* Применить ранее сохранённые события из очереди обработки.
     * При этом применяются только события, произошедшие до вызова.
     * Предполагается вызывать этот метод в цикле. */
    static void applyEvents();
    /* Очистить очередь обработки. */
    static void clearEventsQueue() noexcept;
};


#endif //- __UTILS_EVENTS_HPP

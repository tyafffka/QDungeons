#include "events.hpp"
#include "memory/deque.hpp"
#include <exception>


template<typename _T>
static inline void incref(_T *p) noexcept
{   ++(p->refs_count);   }

template<typename _T>
static inline void decref(_T *p) noexcept
{   if(--(p->refs_count) <= 0) eventsAllocator->destroy(p);   }


static inline bool try_lock(std::atomic_flag &f_) noexcept
{   return(! f_.test_and_set(std::memory_order_acquire));   }

static inline void lock(std::atomic_flag &f_) noexcept
{   while(f_.test_and_set(std::memory_order_acquire));   }

static inline void unlock(std::atomic_flag &f_) noexcept
{   f_.clear(std::memory_order_release);   }


struct __events_queue
{
    struct t_item
    {
        __base_event_binding *binding;
        __base_event_args *args;
    };
    
    int refs_count = 1;
    std::atomic_flag spinlock{false};
    memory::deque<t_item> items;
    
    __events_queue() : items(eventsAllocator) {}
    
    ~__events_queue() noexcept
    {
        for(auto &item : items)
        {   decref(item.binding); decref(item.args);   }
    }
    
    inline void add(__base_event_binding *binding_, __base_event_args *args_)
    {
        items.push_back(t_item{binding_, args_});
        incref(binding_); incref(args_);
    }
    
    inline void swap(memory::deque<t_item> &r_) noexcept
    {   items.swap(r_);   }
};

// --- *** ---


static thread_local __events_queue *thread_queue = nullptr;

memory::allocator *eventsAllocator = &memory::std_allocator;


__base_event::__base_event() noexcept :
    __spinlock(false), __first(nullptr)
{}


void __base_event::__dispatch(__base_event_args *args_) const // Event thread
{
    std::exception_ptr exc__{nullptr};
    __base_event_binding *it = nullptr, *next;
    
    lock(__spinlock);
    
    next = __first;
    while(next != nullptr)
    {
        lock(next->spinlock);
        unlock((it != nullptr)?(it->spinlock): __spinlock);
        
        it = next; next = it->next_by_event;
        
        lock(it->object->__queue->spinlock);
        try
        {   it->object->__queue->add(it, args_);   }
        catch(...)
        {   if(! (bool)exc__) exc__ = std::current_exception();   }
        
        unlock(it->object->__queue->spinlock);
    }
    
    unlock((it != nullptr)?(it->spinlock): __spinlock);
    decref(args_);
    
    if((bool)exc__) std::rethrow_exception(exc__);
    return;
}


void __base_event::__bind(__base_event_binding *binding_) noexcept // Handler thread
{
    binding_->linked_by_event = binding_->linked_by_object = true;
    binding_->prev_by_event = binding_->prev_by_object = nullptr;
    binding_->event = this;
    
    lock(__spinlock);
    __base_event_binding *next = __first; __first = binding_;
    
    if(next != nullptr)
    {
        lock(next->spinlock);
        next->prev_by_event = binding_;
    }
    unlock(__spinlock);
    
    binding_->next_by_event = next;
    if(next != nullptr) unlock(next->spinlock);
    
    // ---
    base_event_handler *object = binding_->object;
    
    lock(object->__spinlock);
    next = object->__first; object->__first = binding_;
    
    if(next != nullptr)
    {
        lock(next->spinlock);
        next->prev_by_object = binding_;
    }
    unlock(object->__spinlock);
    
    binding_->next_by_object = next;
    if(next != nullptr) unlock(next->spinlock);
    
    // ---
    unlock(binding_->spinlock);
    return;
}


void __base_event::__unbind(base_event_handler *object_) noexcept // Handler thread
{
    bool rest_linked; do
    {
        __base_event_binding *it = nullptr, *next;
        bool found = rest_linked = false;
        
        lock(__spinlock);
        
        next = __first;
        while(next != nullptr)
        {
            lock(next->spinlock);
            unlock((it != nullptr)?(it->spinlock): __spinlock);
            
            it = next; next = it->next_by_event;
            if(it->object == object_)
            {   found = true; break;   }
        }
        
        if(found)
        {
            if(it->linked_by_object)
            {
                __base_event_binding *prev_by_object = it->prev_by_object;
                __base_event_binding *next_by_object = it->next_by_object;
                
                std::atomic_flag *p_prev_f;
                if(prev_by_object != nullptr)
                {   p_prev_f = &(prev_by_object->spinlock);   }
                else
                {   p_prev_f = &(object_->__spinlock);   }
                
                // ---
                if(try_lock(*p_prev_f))
                {
                    if(next_by_object != nullptr)
                    {
                        lock(next_by_object->spinlock);
                        next_by_object->prev_by_object = prev_by_object;
                    }
                    
                    if(prev_by_object != nullptr)
                        prev_by_object->next_by_object = next_by_object;
                    else
                        object_->__first = next_by_object;
                    
                    unlock(*p_prev_f);
                    if(next_by_object != nullptr) unlock(next_by_object->spinlock);
                    
                    it->prev_by_object = it->next_by_object = nullptr;
                    it->linked_by_object = false;
                }
                else
                {   rest_linked = true;   }
            }
            
            if(it->linked_by_event)
            {
                __base_event_binding *prev_by_event = it->prev_by_event;
                __base_event_binding *next_by_event = it->next_by_event;
                
                std::atomic_flag *p_prev_f;
                if(prev_by_event != nullptr)
                {   p_prev_f = &(prev_by_event->spinlock);   }
                else
                {   p_prev_f = &__spinlock;   }
                
                // ---
                if(try_lock(*p_prev_f))
                {
                    if(next_by_event != nullptr)
                    {
                        lock(next_by_event->spinlock);
                        next_by_event->prev_by_event = prev_by_event;
                    }
                    
                    if(prev_by_event != nullptr)
                        prev_by_event->next_by_event = next_by_event;
                    else
                        __first = next_by_event;
                    
                    unlock(*p_prev_f);
                    if(next_by_event != nullptr) unlock(next_by_event->spinlock);
                    
                    it->prev_by_event = it->next_by_event = nullptr;
                    it->linked_by_event = false;
                }
                else
                {   rest_linked = true;   }
            }
        } //- found
        
        unlock((it != nullptr)?(it->spinlock): __spinlock);
        
        if(found && ! rest_linked) decref(it);
    }
    while(rest_linked);
    return;
}


__base_event::~__base_event() noexcept // Event thread
{
    bool rest_linked; do
    {
        __base_event_binding *it = nullptr, *next;
        rest_linked = false;
        
        lock(__spinlock);
        
        next = __first;
        while(next != nullptr)
        {
            lock(next->spinlock);
            unlock((it != nullptr)?(it->spinlock): __spinlock);
            
            it = next; next = it->next_by_event;
            if(! it->linked_by_object) continue;
            
            __base_event_binding *prev_by_object = it->prev_by_object;
            __base_event_binding *next_by_object = it->next_by_object;
            
            base_event_handler *object;
            std::atomic_flag *p_prev_f;
            if(prev_by_object != nullptr)
            {   p_prev_f = &(prev_by_object->spinlock);   }
            else
            {   object = it->object; p_prev_f = &(object->__spinlock);   }
            
            // ---
            if(try_lock(*p_prev_f))
            {
                if(next_by_object != nullptr)
                {
                    lock(next_by_object->spinlock);
                    next_by_object->prev_by_object = prev_by_object;
                }
                
                if(prev_by_object != nullptr)
                    prev_by_object->next_by_object = next_by_object;
                else
                    object->__first = next_by_object;
                
                unlock(*p_prev_f);
                if(next_by_object != nullptr) unlock(next_by_object->spinlock);
                
                it->prev_by_object = it->next_by_object = nullptr;
                it->linked_by_object = false;
            }
            else
            {   rest_linked = true;   }
        }
        
        unlock((it != nullptr)?(it->spinlock): __spinlock);
    }
    while(rest_linked);
    // Теперь вся цепочка принадлежит только текущему потоку
    
    __base_event_binding *it, *next = __first;
    while(next != nullptr)
    {
        it = next; next = it->next_by_event; decref(it);
    }
    return;
}

// --- *** ---


base_event_handler::base_event_handler() noexcept :
    __spinlock(false), __first(nullptr)
{
    if(thread_queue == nullptr)
        thread_queue = new(eventsAllocator) __events_queue{};
    else
        ++(thread_queue->refs_count);
    
    __queue = thread_queue;
}


base_event_handler::~base_event_handler() noexcept // Handler thread
{
    bool rest_linked; do
    {
        __base_event_binding *it = nullptr, *next;
        rest_linked = false;
        
        lock(__spinlock);
        
        next = __first;
        while(next != nullptr)
        {
            lock(next->spinlock);
            unlock((it != nullptr)?(it->spinlock): __spinlock);
            
            it = next; next = it->next_by_object;
            if(! it->linked_by_event) continue;
            
            __base_event_binding *prev_by_event = it->prev_by_event;
            __base_event_binding *next_by_event = it->next_by_event;
            
            __base_event *event;
            std::atomic_flag *p_prev_f;
            if(prev_by_event != nullptr)
            {   p_prev_f = &(prev_by_event->spinlock);   }
            else
            {   event = it->event; p_prev_f = &(event->__spinlock);   }
            
            // ---
            if(try_lock(*p_prev_f))
            {
                if(next_by_event != nullptr)
                {
                    lock(next_by_event->spinlock);
                    next_by_event->prev_by_event = prev_by_event;
                }
                
                if(prev_by_event != nullptr)
                    prev_by_event->next_by_event = next_by_event;
                else
                    event->__first = next_by_event;
                
                unlock(*p_prev_f);
                if(next_by_event != nullptr) unlock(next_by_event->spinlock);
                
                it->prev_by_event = it->next_by_event = nullptr;
                it->linked_by_event = false;
            }
            else
            {   rest_linked = true;   }
        }
        
        unlock((it != nullptr)?(it->spinlock): __spinlock);
    }
    while(rest_linked);
    // Теперь вся цепочка принадлежит только текущему потоку
    
    __base_event_binding *it, *next = __first;
    while(next != nullptr)
    {
        it = next; next = it->next_by_object; decref(it);
    }
    
    if(--(thread_queue->refs_count) <= 0)
    {
        eventsAllocator->destroy(thread_queue); thread_queue = nullptr;
    }
    return;
}


void base_event_handler::applyEvents() // Handler thread
{
    memory::deque<__events_queue::t_item> buffer{};
    std::exception_ptr exc__{nullptr};
    
    lock(thread_queue->spinlock);
    thread_queue->swap(buffer);
    unlock(thread_queue->spinlock);
    
    for(auto &item : buffer)
    {
        lock(item.binding->spinlock);
        bool usable =(item.binding->linked_by_event && item.binding->linked_by_object);
        unlock(item.binding->spinlock);
        
        if(usable) try
        {   item.binding->invoke(item.args);   }
        catch(...)
        {   if(! (bool)exc__) exc__ = std::current_exception();   }
        
        decref(item.binding); decref(item.args);
    }
    
    if((bool)exc__) std::rethrow_exception(exc__);
    return;
}


void base_event_handler::clearEventsQueue() noexcept // Handler thread
{
    memory::deque<__events_queue::t_item> buffer{};
    
    lock(thread_queue->spinlock);
    thread_queue->swap(buffer);
    unlock(thread_queue->spinlock);
    
    for(auto &item : buffer)
    {   decref(item.binding); decref(item.args);   }
    return;
}

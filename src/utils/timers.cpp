#include "timers.hpp"
#include <chrono>
#include <thread>
#include <assert.h>


using namespace std::chrono;

using t_steady_time = steady_clock::duration;
using t_high_time = high_resolution_clock::duration;
using t_time = std::common_type<t_steady_time, t_high_time>::type;

static constexpr intmax_t __nticks = ~(intmax_t)0;


template<t_time::rep _Steady_Res>
struct __step_ticks_impl
{
    static intmax_t __do(intmax_t &prev_steady_ticks_,
                         intmax_t &prev_high_ticks_) noexcept
    {
        t_steady_time steady_ticks = steady_clock::now().time_since_epoch();
        t_high_time high_ticks = high_resolution_clock::now().time_since_epoch();
        
        t_time steady_step = (prev_steady_ticks_ != __nticks)?
            duration_cast<t_time>(steady_ticks - t_steady_time{prev_steady_ticks_}) :
            t_time::zero();
        
        t_time high_step = (prev_high_ticks_ != __nticks)?
            duration_cast<t_time>(high_ticks - t_high_time{prev_high_ticks_}) :
            t_time::zero();
        
        intmax_t step = steady_step.count() +
            (high_step.count() % _Steady_Res + _Steady_Res) % _Steady_Res;
        
        prev_steady_ticks_ = steady_ticks.count();
        prev_high_ticks_ = high_ticks.count();
        return step;
    }
};

template<>
struct __step_ticks_impl<1>
{
    static intmax_t __do(intmax_t &prev_steady_ticks_,
                         intmax_t &prev_high_ticks_) noexcept
    {
        t_steady_time steady_ticks = steady_clock::now().time_since_epoch();
        
        intmax_t step = (prev_steady_ticks_ != __nticks)?
                        (steady_ticks.count() - prev_steady_ticks_): 0;
        
        prev_steady_ticks_ = steady_ticks.count();
        return step;
    }
};

struct __step_ticks : __step_ticks_impl<
    duration_cast<t_time>(t_steady_time{1}).count()
> {};

// ---

template<intmax_t _Num, intmax_t _Den>
struct __to_seconds_impl
{
    static double __do(intmax_t elapsed_ticks_) noexcept
    {   return (double)(_Num * elapsed_ticks_) / (double)_Den;   }
};

template<intmax_t _Den>
struct __to_seconds_impl<1, _Den>
{
    static double __do(intmax_t elapsed_ticks_) noexcept
    {   return (double)elapsed_ticks_ / (double)_Den;   }
};

struct __to_seconds : __to_seconds_impl<
    t_time::period::num, t_time::period::den
> {};

// --- *** ---


void nanodelay()
{
    std::this_thread::sleep_for(std::chrono::nanoseconds{1});
    return;
}


// --- TTimeCounter ---

TTimeCounter::TTimeCounter() noexcept :
    __prev_steady_ticks(__nticks), __prev_high_ticks(__nticks), __point(0)
{}

void TTimeCounter::update() noexcept
{
    __point += __step_ticks::__do(__prev_steady_ticks, __prev_high_ticks);
    return;
}


// --- TTimer ---

TTimer::TTimer() noexcept :
    __counter(nullptr), __last_started(__nticks), __elapsed(.0)
{}

TTimer::TTimer(TTimeCounter const &counter_) noexcept :
    __counter(&counter_), __last_started(__nticks), __elapsed(.0)
{}

void TTimer::setCounter(TTimeCounter const &counter_) noexcept
{
    if(__last_started != __nticks) __elapsed = elapsed();
    __counter = &counter_;
    __last_started = counter_.__point.load();
    return;
}

void TTimer::resetCounter() noexcept
{
    if(__last_started != __nticks) __elapsed = elapsed();
    __counter = nullptr;
    __last_started = steady_clock::now().time_since_epoch().count();
    return;
}

void TTimer::start(t_value begin_from_) noexcept
{
    __last_started = (__counter != nullptr)?
        __counter->__point.load() : steady_clock::now().time_since_epoch().count();
    __elapsed = begin_from_;
    return;
}

void TTimer::stop() noexcept
{
    if(__last_started != __nticks)
    {
        __elapsed = elapsed(); __last_started = __nticks;
    }
    return;
}

void TTimer::resume() noexcept
{
    if(__last_started == __nticks)
        __last_started = (__counter != nullptr)?
            __counter->__point.load() : steady_clock::now().time_since_epoch().count();
    return;
}

void TTimer::rewind_back(t_value value_) noexcept
{
    if(__last_started != __nticks)
    {
        if(__counter != nullptr)
        {
            intmax_t point = __counter->__point.load();
            if(point > __last_started)
                __elapsed += __to_seconds::__do(point - __last_started);
            __last_started = point;
        }
        else
        {
            intmax_t point = steady_clock::now().time_since_epoch().count();
            __elapsed += (double)(t_steady_time::period::num * (point - __last_started)) /
                         (double)t_steady_time::period::den;
            __last_started = point;
        }
    }
    
    __elapsed -= value_;
    return;
}

TTimer::t_value TTimer::elapsed() const noexcept
{
    if(__last_started == __nticks) return __elapsed;
    
    if(__counter != nullptr)
    {
        intmax_t point = __counter->__point.load();
        if(point > __last_started)
            return (t_value)(__elapsed + __to_seconds::__do(point - __last_started));
        return __elapsed;
    }
    else
    {
        intmax_t point = steady_clock::now().time_since_epoch().count();
        return __elapsed + (double)(t_steady_time::period::num * (point - __last_started)) /
                           (double)t_steady_time::period::den;
    }
}

bool TTimer::isStarted() const noexcept
{   return(__last_started != __nticks);   }


// --- TCountdown ---

TTimer::t_value TCountdown::remaining() const noexcept
{
    t_value t = _target_time - elapsed();
    return (t > .0)? t : (t_value).0;
}


// --- TAperiodic ---

void TAperiodic::__step(t_value x) noexcept
{
    t_value diff = x - _output;
    if(diff < .0) diff = -diff;
    if(diff <= _error)
    {
        _output = x; return;
    }
    
    t_value t = elapsed() / _period;
    if(t >= 1.0)
    {
        _output = x; return;
    }
    
    _output += ((_input + x) * 0.5 - _output) * t;
    return;
}

TAperiodic::TAperiodic(t_value period_, t_value equality_error_,
                       t_value start_value_) noexcept :
    TTimer(), _period(period_),
    _input(start_value_), _output(start_value_), _error(equality_error_)
{
#ifndef NDEBUG
    assert(period_ > .0 && equality_error_ > .0);
#endif
    start();
    return;
}

TAperiodic::TAperiodic(TTimeCounter const &counter_, t_value period_,
                       t_value equality_error_, t_value start_value_) noexcept :
    TTimer(counter_), _period(period_),
    _input(start_value_), _output(start_value_), _error(equality_error_)
{
#ifndef NDEBUG
    assert(period_ > .0 && equality_error_ > .0);
#endif
    start();
    return;
}

TTimer::t_value TAperiodic::operator ()() noexcept
{
    __step(_input); start();
    return _output;
}

TTimer::t_value TAperiodic::operator ()(t_value x) noexcept
{
    __step(x); start(); _input = x;
    return _output;
}

TTimer::t_value TAperiodic::operator +=(t_value dx) noexcept
{
    __step(_input + dx); start(); _input += dx;
    return _output;
}

void TAperiodic::reset(t_value start_value_) noexcept
{
    start(); _input = _output = start_value_;
    return;
}

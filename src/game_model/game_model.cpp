#include "game_model.hpp"
#include "libutils/application.hpp"
#include "libutils/MetadataManager.hpp"
#include "utils/memory/atomic_lock.hpp"
#include "utils/log.hpp"
#include "utils/platform.hpp"


static LibUtils::GlobalMetadata<float> __game_model_tick_period{"game_model_tick_period"};

static GameModel *__singleton = nullptr;


GameModel::GameModel(TTimeCounter const &counter_, std::string const &world_path_) :
    __is_active(true), __finalized_spinlock(false), __finalized_queue(),
    __tick_timer(counter_), _world_path(world_path_)
{
    if(__singleton != nullptr) throw std::runtime_error(
        "Fatal! Game model created twice!"
    );
    __singleton = this;
    
    auto &meta = LibUtils::MetadataManager::getSingleton();
    
    level = new LevelMap(10, 10);
    level->generateLevel((uint32_t)time(nullptr));
    
    thePlayer = meta.newObject<PlayerEntity>("qdungeons:man");
    if(thePlayer == nullptr)
    {
        delete level; throw std::bad_alloc();
    }
    
    thePlayer->attach(level->physicsWorld, 4.5f, .0f);
    
    __prev_physics_time = __prev_tick_time = __t_time_point::clock::now();
    __tick_timer.start();
    
    LOG("GAME") << "Game model created.";
    return;
}


GameModel::~GameModel()
{
    __is_active = false; backendFlag.wait();
    // => SYNC_ENDS
    
    if(thePlayer != nullptr) thePlayer->finalize();
    delete level;
    
    for(BaseEntity *entity : __finalized_queue) entity->destroy();
    
    __singleton = nullptr;
    LOG("GAME") << "Game model closed.";
    return;
}

// --- *** ---


void GameModel::update(float frame_time_) SYNC_ENDS
{
    // Clean up finalized entities:
    std::deque<BaseEntity *> finalized{};
    {
        memory::atomic_lock lock{__finalized_spinlock};
        finalized.swap(__finalized_queue);
    }
    
    for(BaseEntity *entity : finalized) entity->destroy();
    
    // Do a frame:
    level->onFrame(frame_time_);
    thePlayer->onFrame(frame_time_);
    
    // Do physics:
    startBackgroundTask("GameModel - Physics", {backendFlag}, [this]()
    {
        if(! __is_active.load()) return;
        
        float elapsed = (backgroundTaskStartTime() - __prev_physics_time).count() *
                        __t_time_point::period::num / (float)__t_time_point::period::den;
        __prev_physics_time = backgroundTaskStartTime();
        
        level->physicsStep(elapsed);
        backgroundTaskCancellationPoint();
        thePlayer->onPhysics();
    });
    
    // Do a tick:
    if(__tick_timer.elapsed() >= *__game_model_tick_period)
    {
        __tick_timer.start();
        startBackgroundTask("GameModel - Tick", {backendFlag}, [this]()
        {
            if(! __is_active.load()) return;
            
            float elapsed = (backgroundTaskStartTime() - __prev_tick_time).count() *
                            __t_time_point::period::num / (float)__t_time_point::period::den;
            __prev_tick_time = backgroundTaskStartTime();
            
            level->onTick(elapsed);
            backgroundTaskCancellationPoint();
            thePlayer->onTick(elapsed);
        });
    }
    return;
}

// --- *** ---


event<void(LevelMap *level_)> onLevelCreated;
event<void(PlayerEntity *player_)> onPlayerAppeared;


GameModel &getGameModel()
{
    if(__singleton == nullptr) throw std::runtime_error(
        "Fatal! Game model not exists now!"
    );
    return *__singleton;
}


std::vector<std::string> getWorldsList()
{
    std::vector<std::string> ret = findInDir(
        LibUtils::savesDir, "?*.world", true /*dirs*/
    );
    for(std::string &name : ret)
        name.erase(name.size() - (sizeof(".world") - 1));
    return ret;
}


std::string getWorldPath(std::string const &world_name_)
{
    std::string ret;
    if(world_name_.empty()) return ret;
    
    ret = normalizePath(LibUtils::savesDir + "/" + world_name_ + ".world");
    if(! makeDir(ret)) return ret;
    return ret;
}

#ifndef _QDUNGEONS_BASE_ENTITY_HPP
#define _QDUNGEONS_BASE_ENTITY_HPP

#include "utils/metatype.hpp"
#include "utils/background_tasks.hpp"
#include <OgrePrerequisites.h>
#include <Box2D/Dynamics/b2Body.h>
#include <string>

/* Метод предназначен для синхронизации фронтенда с бэкендом.
 * Изменять объекты фронтенда можно только в таких методах. */
#define SYNC_ENDS


/* Главный флаг, с помощью которого синхронизируется работа бэкенда. */
extern background_task_flag backendFlag;


/* Игровая сущность -- базовый объект, единица взаимодействия в игровом мире.
 * Базовая сущность не делает ничего, только отображает 3D-модель.
 * Неиспользуемые сущности следует финализировать вызовом метода `finalize`. */
class BaseEntity : public BaseMetatypeObject
{
    metatype_data
    {
        std::string mesh;
        std::string material;
        metatype_parameters(mesh, material);
    };
    
private:
    b2Rot __need_rotation; // TODO: вынести все плюшки в подклассы
    Ogre::Real __need_angle, __curr_angle; // TODO +
    
protected:
    Ogre::Entity *_entity = nullptr;
    
    /* Вызывается для создания визуальной части при первой обработке кадра. */
    virtual void createFrontend() SYNC_ENDS;
    
public:
    Ogre::SceneNode *node = nullptr;
    b2Body *entityBody = nullptr; // TODO +
    b2Vec2 position;
    
    static bool isVisible;
    
    BaseEntity();
    ~BaseEntity() SYNC_ENDS;
    
    /* Финализирует сущность: отсоединяет её от модели игрового мира и
     * удаляет при следующей синхронизации фронтенда с бэкендом.
     * После вызова данного метода сущность не должна больше использоваться. */
    void finalize();
    
    // ---
    
    /* Точка синхронизации фронтенда и бэкенда. Вызывается в каждом кадре.
     * `frame_time_` -- время в секундах, прошедшее с предыдущего кадра. */
    virtual void onFrame(float frame_time_ /*sec*/) SYNC_ENDS;
    /* Вызывается в каждом игровом такте. Частота -- 20 тактов/сек,
     * однако она может быть ограничена сверху частотой кадров.
     * `tick_time_` -- время в секундах, прошедшее с предыдущего такта. */
    virtual void onTick(float tick_time_ /*sec*/) {}
    /* Вызывается после расчёта игровой физики. */
    virtual void onPhysics() {}
    
    /* Вызывается для присоединения сущности к указанному
     * физическому миру по указанным координатам. */
    virtual void attach(b2World &world_, float x_, float y_);
    /* Вызывается для отсоединения сущности
     * от присоединённого ранее физического мира. */
    virtual void detach();
};


#endif // _QDUNGEONS_BASE_ENTITY_HPP

#ifndef _QDUNGEONS_GAME_MODEL_HPP
#define _QDUNGEONS_GAME_MODEL_HPP

#include "level_map.hpp"
#include "entities/player.hpp"
#include "utils/events.hpp"
#include "utils/timers.hpp"
#include <deque>
#include <vector>
#include <string>
#include <atomic>


/* Базовая модель игрового мира.
 * Может существовать только в единственном экземпляре. */
class GameModel
{
    friend class BaseEntity;
    
    /* Найти сущность, лежащую на луче, ближайшую к его началу
     * на расстоянии от `min_distance_` до `max_distance_`.
     * Если таковой сущности не найдено - возвращает `nullptr`. */
    //BaseEntity *rayCast(Ogre::Ray const &ray_,
    //                    float min_distance_, float max_distance_);
private:
    using __t_time_point = decltype(backgroundTaskStartTime());
    
    std::atomic<bool> __is_active;
    std::atomic_flag __finalized_spinlock;
    std::deque<BaseEntity *> __finalized_queue;
    
    __t_time_point __prev_physics_time, __prev_tick_time;
    TTimer __tick_timer;
    
protected:
    std::string const _world_path;
    
public:
    LevelMap *level;
    PlayerEntity *thePlayer;
    
    // ---
    
    GameModel(TTimeCounter const &counter_, std::string const &world_path_);
    ~GameModel();
    
    /* Вызывает обработку кадра у всех подконтрольных сущностей и
     * запускает периодические задачи бэкенда.
     * `frame_time_` -- время в секундах, прошедшее с предыдущего кадра. */
    void update(float frame_time_ /*sec*/) SYNC_ENDS;
    
    /* Возвращает, находится ли модель в рабочем состоянии. */
    bool isActive() noexcept {   return __is_active.load();   }
};

// --- *** ---


extern event<void(LevelMap *level_)> onLevelCreated;
extern event<void(PlayerEntity *player_)> onPlayerAppeared;


/* Возвращает текущую модель.
 * Можно использовать только если модель находится в рабочем состоянии. */
GameModel &getGameModel();

/* Возвращает имена сохранений (подпапки с окончанием `.world`),
 * находящиеся в папке сохранений. */
std::vector<std::string> getWorldsList();
/* Возвращает путь к папке сохранения по его имени.
 * При необходимости создаёт папку. */
std::string getWorldPath(std::string const &world_name_);


#endif // _QDUNGEONS_GAME_MODEL_HPP

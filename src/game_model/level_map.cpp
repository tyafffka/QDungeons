#include "level_map.hpp"
#include "game_model.hpp"
#include "libutils/MetadataManager.hpp"
#include "utils/log.hpp"
#include <Box2D/Collision/Shapes/b2ChainShape.h>
#include <random>


LevelMap::LevelMap(uint16_t size_x_, uint16_t size_y_) :
    physicsWorld(b2Vec2{.0f, .0f}),
    sizeX((size_x_ > MAP_MAX_AXIS_SIZE)? (uint16_t)MAP_MAX_AXIS_SIZE : size_x_),
    sizeY((size_y_ > MAP_MAX_AXIS_SIZE)? (uint16_t)MAP_MAX_AXIS_SIZE : size_y_)
{
    size_t area = (size_t)sizeX * sizeY;
    
    auto &meta = LibUtils::MetadataManager::getSingleton();
    _map = new(meta.getAllocator()) BaseBlock *[area];
    
    for(size_t i = 0; i < area; ++i) _map[i] = nullptr;
    
    b2BodyDef border_def;
    border_def.type = b2_staticBody;
    border_def.position.SetZero();
    border_def.fixedRotation = true;
    
    b2Vec2 points[4] = {
        {sizeX - 0.5f, sizeY - 0.5f},
        {-0.5f, sizeY - 0.5f},
        {-0.5f, -0.5f},
        {sizeX - 0.5f, -0.5f},
    };
    b2ChainShape border_shape;
    border_shape.CreateLoop(points, 4);
    
    border = physicsWorld.CreateBody(&border_def);
    border->CreateFixture(&border_shape, .0f);
    
    LOG("GAME") << "Level map created with area " << sizeX << " * " << sizeY;
    
    onLevelCreated(this);
    return;
}


LevelMap::~LevelMap()
{
    if(_map == nullptr) return;
    
    size_t area = (size_t)sizeX * sizeY;
    for(size_t i = 0; i < area; ++i) _map[i]->finalize();
    
    auto &meta = LibUtils::MetadataManager::getSingleton();
    meta.getAllocator()->destroy(_map, area);
    
    LOG("GAME") << "Level map closed.";
    return;
}

// --- *** ---


void LevelMap::onFrame(float frame_time_) SYNC_ENDS
{
    for(BaseBlock **it = _map, **it_end = it + (size_t)sizeX * sizeY;
        it != it_end; ++it)
    {
        backgroundTaskCancellationPoint();
        if(*it != nullptr) (*it)->onFrame(frame_time_);
    }
    return;
}


void LevelMap::onTick(float tick_time_)
{
    for(BaseBlock **it = _map, **it_end = it + (size_t)sizeX * sizeY;
        it != it_end; ++it)
    {
        backgroundTaskCancellationPoint();
        if(*it != nullptr) (*it)->onTick(tick_time_);
    }
    return;
}


void LevelMap::physicsStep(float step_time_)
{
    physicsWorld.Step(step_time_, 3, 3); // FIXME: how to choice iterations counts?
    
    for(BaseBlock **it = _map, **it_end = it + (size_t)sizeX * sizeY;
        it != it_end; ++it)
    {
        backgroundTaskCancellationPoint();
        if(*it != nullptr) (*it)->onPhysics();
    }
    return;
}

// --- *** ---


BaseBlock *LevelMap::setBlock(uint16_t x_, uint16_t y_, std::string const &type_)
{
    if(x_ >= sizeX || y_ >= sizeY) return nullptr;
    
    size_t i = x_ + (size_t)sizeX * y_;
    if(_map[i] != nullptr) _map[i]->finalize();
    
    auto &meta = LibUtils::MetadataManager::getSingleton();
    BaseBlock *p = _map[i] = meta.newObject<BaseBlock>(type_);
    
    if(p != nullptr)
    {
        p->attach(physicsWorld, (float)x_, (float)y_);
    }
    else
    {
        LOG("WARNING") << "Block '" << type_ << "' at (" << x_
                       << ", " << y_ << ") not created!";
    }
    return p;
}


BaseBlock *LevelMap::getBlock(uint16_t x_, uint16_t y_)
{
    if(x_ >= sizeX || y_ >= sizeY) return nullptr;
    return _map[x_ + (size_t)sizeX * y_];
}


// _FIXME:LATER
static std::string __blocks_types[] = {
    "qdungeons:flat",
    "qdungeons:stone"
};

void LevelMap::generateLevel(uint32_t seed_)
{
    std::default_random_engine random_engine;
    random_engine.seed(seed_);
    
    std::uniform_int_distribution<uint16_t> dist(0, 1);
    
    for(uint16_t x = 0; x < sizeX; ++x)
        for(uint16_t y = 0; y < sizeY; ++y)
        {
            if(x == 0 || y == 0 || x == sizeX - 1 || y == sizeY - 1)
                setBlock(x, y, "qdungeons:flat");
            else
                setBlock(x, y, __blocks_types[dist(random_engine)]);
        }
    return;
}

#ifndef _QDUNGEONS_LEVEL_MAP_HPP
#define _QDUNGEONS_LEVEL_MAP_HPP

#include "blocks/base_blocks.hpp"
#include <Box2D/Dynamics/b2World.h>

#define MAP_MAX_AXIS_SIZE 2000


/* Горизонтальная прямоугольная карта ландшафта, состоящая из
 * {Q}вадратных ячеек -- блоков, размером `size_x_ * size_y_` блоков.
 * Ширина блока -- 1 метр, высота -- 2 метра, целочисленные координаты
 * находятся внизу посередине блока. Максимальный размер карты
 * по каждой из осей -- 2000 блоков. */
class LevelMap
{
protected:
    BaseBlock **_map;
    
public:
    b2World physicsWorld;
    b2Body *border;
    
    uint16_t const sizeX, sizeY;
    
    // ---
    
    LevelMap(uint16_t size_x_, uint16_t size_y_);
    ~LevelMap();
    
    /* Вызывает обработку кадра у каждого блока.
     * `frame_time_` -- время в секундах, прошедшее с предыдущего кадра. */
    void onFrame(float frame_time_ /*sec*/) SYNC_ENDS;
    /* Вызывает обработку игрового такта у каждого блока.
     * `tick_time_` -- время в секундах, прошедшее с предыдущего такта. */
    void onTick(float tick_time_ /*sec*/);
    /* Производит расчёт игровой физики и вызывает обработку физики у каждого блока.
     * `step_time_` -- время в секундах, прошедшее с предыдущего расчёта. */
    void physicsStep(float step_time_ /*sec*/);
    
    /* Размещает на координатах `(x_, y_)` новый блок указанного метатипа
     * и возвращает указатель на него (или `nullptr` в случае неудачи).
     * При этом старый блок на этих координатах удаляется. */
    BaseBlock *setBlock(uint16_t x_, uint16_t y_, std::string const &type_);
    /* Возвращает указатель на блок, расположенный на координатах `(x_, y_)`.
     * Если блока нет -- возвращает `nullptr`. */
    BaseBlock *getBlock(uint16_t x_, uint16_t y_);
    
    /* Генерирует в случайном порядке блоки на карте. */
    void generateLevel(uint32_t seed_);
};


#endif // _QDUNGEONS_LEVEL_MAP_HPP

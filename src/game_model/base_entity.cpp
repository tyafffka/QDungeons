#include "base_entity.hpp"
#include "game_model.hpp"
#include "libutils/application.hpp"
#include "libutils/MetadataManager.hpp"
#include "utils/memory/atomic_lock.hpp"
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <Box2D/Dynamics/b2World.h>


static LibUtils::GlobalMetadata<float> __angle_error{"entity_view_angle_error"};
static LibUtils::GlobalMetadata<float> __angle_aperiod{"entity_view_angle_aperiod"};

background_task_flag backendFlag;

bool BaseEntity::isVisible = false;


void BaseEntity::createFrontend() SYNC_ENDS
{
    _entity = LibUtils::sceneManager->createEntity(metatype.mesh);
    _entity->setMaterialName(metatype.material);
    _entity->setVisible(isVisible);
    
    node = LibUtils::mainNode->createChildSceneNode();
    node->attachObject(_entity);
    return;
}


BaseEntity::BaseEntity()
{
    __need_rotation.SetIdentity();
    __need_angle = __curr_angle = .0f;
    position.SetZero();
    return;
}


BaseEntity::~BaseEntity() SYNC_ENDS
{
    if(_entity) LibUtils::sceneManager->destroyEntity(_entity);
    if(node) LibUtils::sceneManager->destroySceneNode(node);
    return;
}


void BaseEntity::finalize()
{
    detach();
    
    GameModel &model = getGameModel();
    if(model.isActive())
    {
        memory::atomic_lock lock{model.__finalized_spinlock};
        model.__finalized_queue.push_back(this);
    }
    else
    {   BaseMetatypeObject::destroy();   }
    return;
}

// --- *** ---


void BaseEntity::onFrame(float frame_time_ /*sec*/) SYNC_ENDS
{
    if(node == nullptr) createFrontend();
    
    if(entityBody != nullptr)
    {
        b2Transform tr = entityBody->GetTransform();
        Ogre::Vector3 position__ = node->getPosition();
        
        if(position__.x != tr.p.x || position__.y != tr.p.y)
            node->setPosition(tr.p.x, tr.p.y, position__.z);
        
        if(__need_rotation.c != tr.q.c || __need_rotation.s != tr.q.s)
        {
            __need_rotation = tr.q; __need_angle = tr.q.GetAngle();
        }
    }
    else
    {
        Ogre::Vector3 position__ = node->getPosition();
        
        if(position__.x != position.x || position__.y != position.y)
            node->setPosition(position.x, position.y, position__.z);
    }
    
    // Pulling up angle aperiodically
    Ogre::Real angle_diff = __need_angle - __curr_angle;
    
    if(angle_diff > Ogre::Math::PI)
    {
        __curr_angle += Ogre::Math::TWO_PI; angle_diff -= Ogre::Math::TWO_PI;
    }
    else if(angle_diff < -Ogre::Math::PI)
    {
        __curr_angle -= Ogre::Math::TWO_PI; angle_diff += Ogre::Math::TWO_PI;
    }
    
    if(Ogre::Math::Abs(angle_diff) >= *__angle_error)
    {
        float t = frame_time_ * (*__angle_aperiod);
        if(t >= 1.0f) // prevent Shimmy-effect
        {   __curr_angle = __need_angle;   }
        else
        {   __curr_angle += angle_diff * t;   }
        
        Ogre::Real r = __curr_angle * 0.5f;
        node->setOrientation(Ogre::Quaternion{
            Ogre::Math::Cos(r), .0f, .0f, Ogre::Math::Sin(r)
        });
    }
    return;
}


void BaseEntity::attach(b2World &world_, float x_, float y_)
{
    position.Set(x_, y_); return;
}


void BaseEntity::detach()
{
    if(entityBody != nullptr)
    {
        entityBody->GetWorld()->DestroyBody(entityBody);
        entityBody = nullptr;
    }
    return;
}

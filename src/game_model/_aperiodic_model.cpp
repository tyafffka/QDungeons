// --- *** ---

//#define VIEW_POSITION_ERROR    0.001f /*m*/
//#define VIEW_POSITION_APERIOD  9.0f   /*Hz*/
//#define VIEW_ANGLE_ERROR       0.005f /*rad*/
//#define VIEW_ANGLE_APERIOD    17.0f   /*Hz*/


//b2Vec2 __need_position, __velocity;
//Ogre::Real __need_rotation, __curr_rotation;


void construct()
{
    //__need_position.SetZero(); __velocity.SetZero();
    //__need_rotation = __curr_rotation = .0f;
}


void onFrame(float frame_time_ /*sec*/)
{
    //Ogre::Vector3 position = node->getPosition();
    //bool is_position_changed = false;
    //
    //// Apply velocity
    //if(__velocity.x != .0f || __velocity.y != .0f) // Yes, I compare it by equality operator! I'm dangerous!
    //{
    //    float x_step = __velocity.x * frame_time_;
    //    float y_step = __velocity.y * frame_time_;
    //    
    //    __need_position.x += x_step; position.x += x_step;
    //    __need_position.y += y_step; position.y += y_step;
    //    
    //    is_position_changed = true;
    //}
    //
    //// Pulling up position aperiodically
    //float x_diff /*m*/ = __need_position.x - position.x;
    //float y_diff /*m*/ = __need_position.y - position.y;
    //
    //if(Ogre::Math::Abs(x_diff) >= VIEW_POSITION_ERROR ||
    //   Ogre::Math::Abs(y_diff) >= VIEW_POSITION_ERROR)
    //{
    //    float t = frame_time_ * VIEW_POSITION_APERIOD;
    //    if(t >= 1.0f) // prevent Shimmy-effect
    //    {   position.x = __need_position.x; position.y = __need_position.y;   }
    //    else
    //    {   position.x += x_diff * t; position.y += y_diff * t;   }
    //    
    //    is_position_changed = true;
    //}
    //
    //if(is_position_changed) node->setPosition(position);
    //
    //// Pulling up angle aperiodically
    //Ogre::Real rotation_diff = __need_rotation - __curr_rotation;
    //
    //if(Ogre::Math::Abs(rotation_diff) >= VIEW_ANGLE_ERROR)
    //{
    //    float t = frame_time_ * VIEW_ANGLE_APERIOD;
    //    if(t >= 1.0f) // prevent Shimmy-effect
    //    {   __curr_rotation = __need_rotation;   }
    //    else
    //    {   __curr_rotation += rotation_diff * t;   }
    //    
    //    Ogre::Real r = __curr_rotation * 0.5f;
    //    node->setOrientation(Ogre::Quaternion{
    //        Ogre::Math::Cos(r), .0f, .0f, Ogre::Math::Sin(r)
    //    });
    //}
}


void setPosition()
{
    //// Position is changed from standing state -- it's a teleportation!
    //// Comment this and watch what will happen (maybe will needed "begin game" twice).
    //if((__need_position.x != tr_.p.x || __need_position.y != tr_.p.y) &&
    //   __velocity.x == .0f && __velocity.y == .0f)
    //{
    //    Ogre::Vector3 position = node->getPosition();
    //    position.x = tr_.p.x; position.y = tr_.p.y;
    //    node->setPosition(position);
    //}
    //
    //__need_position = tr_.p; __velocity = v_;
    //__need_rotation = tr_.q.GetAngle();
    //
    //Ogre::Real rotation_diff = __need_rotation - __curr_rotation;
    //if(rotation_diff > Ogre::Math::PI) __curr_rotation += Ogre::Math::TWO_PI;
    //if(rotation_diff < -Ogre::Math::PI) __curr_rotation -= Ogre::Math::TWO_PI;
}

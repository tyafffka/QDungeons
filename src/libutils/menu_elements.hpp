#ifndef __LIBUTILS_MENU_ELEMENTS_HPP
#define __LIBUTILS_MENU_ELEMENTS_HPP

/* Базовый набор элементов меню. */

#include "utils/lines_buffer.hpp"
#include "utils/events.hpp"
#include <OgreOverlay.h>
#include <vector>
#include <string>

namespace LibUtils
{


/* Базовый элемент меню. Отображает текстовую строку
 * с выравниванием слева. Не реагирует на действия. */
class BaseMenuElement
{
    friend class BaseMenuActivity;
    
protected:
    Ogre::OverlayContainer *_layout;
    Ogre::OverlayElement *_label;
    bool _is_enabled;
    
    virtual void _set_focus();
    virtual void _loop() {}
    virtual void _kill_focus();
    
    virtual void _select() {}
    virtual void _decrease() {}
    virtual void _increase() {}
    virtual void _mouse_click(float x_) {}
    
public:
    explicit BaseMenuElement(std::string const &text_);
    virtual ~BaseMenuElement();
    
    /* Изменить текстовую строку элемента. */
    void setText(lines_buffer_content &&content_);
    
    template<typename... _Params>
    void setText(std::string const &format_, _Params &&... params_)
    {
        setText(utf8_lines_buffer::format(format_, std::forward<_Params>(params_)...));
    }
    
    /* Делает элемент активным/неактивным. Неактивный элемент не реагирует
     * на ввод и пропускается при перемещении между элементами.
     * Изначально элемент активен. */
    virtual void setEnabled(bool is_enabled_);
    /* Возвращает, активен ли элемент в данный момент. */
    bool isEnabled() noexcept {   return _is_enabled;   }
    
    /* Событие выбора элемента.
     * Действует для элементов "кнопка", "переменная" и "ввод строки". */
    event<void(BaseMenuElement *element_)> onSelected;
    /* Событие изменения значения у элемента. */
    event<void(BaseMenuElement *element_)> onValueChanged;
};


/* Элемент кнопки. Вызывает событие `onSelected` при выборе. */
class MenuButton : public BaseMenuElement
{
protected:
    void _select() override;
    void _mouse_click(float x_) override;
    
public:
    explicit MenuButton(std::string const &text_) : BaseMenuElement(text_) {}
};


/* Элемент переменной. Наследный от кнопки. Отображает дополнительный
 * текст с выравниванием справа, который можно изменять. */
class MenuVariable : public MenuButton
{
protected:
    Ogre::OverlayElement *_value = nullptr;
    
public:
    explicit MenuVariable(std::string const &text_, std::string const &value_="");
    ~MenuVariable();
    
    void setEnabled(bool is_enabled_) override;
    
    /* Изменить дополнительный текст. */
    void setValue(lines_buffer_content &&content_);
    
    template<typename... _Params>
    void setValue(std::string const &format_, _Params &&... params_)
    {
        setValue(utf8_lines_buffer::format(format_, std::forward<_Params>(params_)...));
    }
};


/* Элемент ввода строки. Содержит дополнительный текст,
 * редактируемый пользователем. Реагирует на `Enter`. */
class MenuInput : public BaseMenuElement
{
protected:
    Ogre::OverlayElement *_input = nullptr;
    
    void _set_focus() override;
    void _loop() override;
    void _kill_focus() override;
    
    void _select() override;
    
public:
    explicit MenuInput(std::string const &text_, std::string const &value_="");
    ~MenuInput();
    
    void setEnabled(bool is_enabled_) override;
    
    /* Изменить дополнительный текст. */
    void setValue(std::string const &value_);
    /* Возвращает дополнительный текст. */
    std::string const &getValue();
};


/* Элемент переключателя. Содержит вектор дополнительных значений
 * и позволяет переключаться между ними стрелками влево/вправо. */
class MenuSwitcher : public BaseMenuElement
{
public:
    typedef std::vector<std::string> t_values;
    
    t_values values;
    
protected:
    Ogre::OverlayElement *_dec = nullptr, *_inc = nullptr, *_value = nullptr;
    int _current_item = 0;
    
    void _update();
    
    void _select() override;
    void _decrease() override;
    void _increase() override;
    void _mouse_click(float x_) override;
    
public:
    MenuSwitcher(std::string const &text_, t_values &&values_);
    ~MenuSwitcher();
    
    void setEnabled(bool is_enabled_) override;
    
    /* Выбрать `i_`-ое значение из вектора в качестве текущего. */
    void setCurrentItem(int i_);
    /* Возвращает номер выбранного значения. */
    int getCurrentItem() noexcept {   return _current_item;   }
    /* Возвращает выбранное значение. */
    std::string const &getCurrentValue() noexcept {   return values[_current_item];   }
};


/* Элемент слайдера. Дополнительно содержит числовое значение в заданных
 * пределах, которое изменяется клавишами влево/вправо с заданным шагом
 * или кликом мыши в точное место слайдера. */
class MenuSlider : public BaseMenuElement
{
protected:
    Ogre::OverlayContainer *_filled = nullptr, *_empty = nullptr;
    float _min, _max, _percent = 0.5f, _step;
    
    void _update();
    
    void _decrease() override;
    void _increase() override;
    void _mouse_click(float x_) override;
    
public:
    explicit MenuSlider(std::string const &text_, float key_change_step_=0.1f,
                        float value_min_=.0f, float value_max_=1.0f);
    ~MenuSlider();
    
    void setEnabled(bool is_enabled_) override;
    
    /* Присвоить числовое значение. Значение обрезается по заданным пределам. */
    virtual void setValue(float value_);
    /* Взвращает текущее числовое значение. */
    virtual float getValue() {   return (_max - _min) * _percent + _min;   }
};


} // namespace LibUtils

#endif // __LIBUTILS_MENU_ELEMENTS_HPP

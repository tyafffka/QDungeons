#ifndef __LIBUTILS_DIALOG_HPP
#define __LIBUTILS_DIALOG_HPP

#include "utils/lines_buffer.hpp"
#include <string>

namespace LibUtils
{


/* Базовый класс, отображающий диалоговую панель с текстовым сообщением.
 * Дополнительно может содержать строку вопроса в конце.
 * В один момент времени на экране может отображаться только один диалог.
 * Требуется переопределить метод `update`,
 * в котором должна содержаться логика работы диалога. */
class BaseDialog
{
private:
    lines_buffer_content __message{};
    lines_buffer_content __question{};
    
protected:
    virtual void update() = 0;
    
public:
    BaseDialog() = default;
    
    explicit BaseDialog(lines_buffer_content &&message_) :
        __message(std::move(message_))
    {}
    
    template<typename... _Params>
    explicit BaseDialog(std::string const &format_, _Params &&... params_) :
        BaseDialog(utf8_lines_buffer::format(format_, std::forward<_Params>(params_)...))
    {}
    
    virtual ~BaseDialog() {   hide();   }
    
    /* Изменить текст строки вопроса. */
    void setQuestion(lines_buffer_content &&question_);
    
    template<typename... _Params>
    void setQuestion(std::string const &format_, _Params &&... params_)
    {
        setQuestion(utf8_lines_buffer::format(format_, std::forward<_Params>(params_)...));
    }
    
    /* Выводит на экран данный диалог.
     * Если при этом отображался другой диалог, он пропадёт. */
    void show();
    /* Убирает с экрана данный диалог, если он отображался. */
    void hide();
    /* Возвращает, отображается ли на экране данный диалог. */
    bool isActive();
    
    /* Выполняет обновление логики у активного диалога.
     * Возвращает `true` если такой диалог есть. */
    static bool updateActive();
};


} //- namespace LibUtils

#endif //- __LIBUTILS_DIALOG_HPP

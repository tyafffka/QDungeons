#include "application.hpp"
#include "input.hpp"
#include "TextManager.hpp"
#include "MetadataManager.hpp"
#include "utils/log.hpp"
#include "utils/platform.hpp"
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreOverlaySystem.h>
#include <OgreOggSoundManager.h>
#include <iostream>
#include <algorithm>

#define LOG_ROTATION_MAX_COUNT 9

void _registerTextInputOverlayElement();
void _registerMultiTextOverlayElement();

// --- *** ---

namespace LibUtils
{


class TOgreLogRedirect : public Ogre::Log
{
public:
    TOgreLogRedirect() : Ogre::Log("", false, true)
    {
        Ogre::LogManager::getSingleton().setDefaultLog(this);
        return;
    }
    
    ~TOgreLogRedirect()
    {
        auto *logs = Ogre::LogManager::getSingletonPtr();
        if(logs != nullptr) logs->setDefaultLog(nullptr);
        return;
    }
    
    void logMessage(Ogre::String const &msg_, Ogre::LogMessageLevel level_, bool no_stdout_)
    {
        if(no_stdout_) return;
        
        char const *tag;
        switch(level_)
        {
        case Ogre::LML_TRIVIAL:  return;
        case Ogre::LML_NORMAL:   tag = "OGRE3D_INFO";    break;
        case Ogre::LML_WARNING:  tag = "OGRE3D_WARNING"; break;
        case Ogre::LML_CRITICAL: tag = "OGRE3D_ERROR";   break;
        default: tag = nullptr;
        }
        
        defaultLog.writeMessage(tag, msg_);
        return;
    }
};


static bool __is_closed = false;
static std::string __properties_file{};
static TOgreLogRedirect *__log_redirect = nullptr;


minson::array properties{};
std::string savesDir{};

Ogre::RenderWindow *window = nullptr;
Ogre::SceneManager *sceneManager = nullptr;
Ogre::Camera *mainCamera = nullptr;
Ogre::SceneNode *mainNode = nullptr;


void saveProperties()
{
    if(minson::serializeFile(properties, __properties_file, true))
        LOG("INFO") << "Properties saved.";
    else
        LOG("ERROR") << "Properties not saved!";
    return;
}

// --- *** ---


void BaseApplication::__close()
{
    closeInput();
    
    if(window != nullptr)
    {
        Ogre::WindowEventUtilities::removeWindowEventListener(window, this);
        __is_closed = false;
    }
    
    delete OgreOggSound::OgreOggSoundManager::getSingletonPtr();
    delete TextManager::getSingletonPtr();
    delete MetadataManager::getSingletonPtr();
    delete Ogre::Root::getSingletonPtr();
    
    delete __log_redirect; __log_redirect = nullptr;
    
    mainNode = nullptr;
    mainCamera = nullptr;
    sceneManager = nullptr;
    window = nullptr;
    return;
}


bool BaseApplication::initApplication(std::string const &saves_dir_)
{
    if(! saves_dir_.empty())
        savesDir = normalizePath(saves_dir_);
    else
        savesDir = normalizePath(executableOrigin() + "/../saves");
    
    // Logs:
    
    std::string logs_dir = normalizePath(savesDir + "/logs");
    if(! makeDir(logs_dir, true /*recursive*/))
    {
        std::clog << "FATAL ERROR: directory '" << logs_dir << "' not created!" << std::endl;
        return false;
    }
    
    auto log_files = findInDir(logs_dir, "log_*.txt", false /*files*/);
    if(log_files.size() > LOG_ROTATION_MAX_COUNT) // Log rotation
    {
        size_t to_remove_count = log_files.size() - LOG_ROTATION_MAX_COUNT;
        
        std::sort(log_files.begin(), log_files.end());
        for(size_t i = 0; i < to_remove_count; ++i)
            removeFile(normalizePath(logs_dir + "/" + log_files[i]));
    }
    
#ifdef NDEBUG
    defaultLog.excludeTag("DEBUG");
#else
    defaultLog.setShortTrace(true);
#endif
    defaultLog.addProvider<TStdLogProvider>();
    defaultLog.addProvider<TFileLogProvider>(normalizePath(logs_dir + "/log_"), ".txt");
    
    // Properties:
    
    __properties_file = normalizePath(savesDir + "/properties.minson");
    try
    {
        minson::array loaded = minson::parseFile(__properties_file);
        properties.update(loaded);
    }
    catch(minson::exception &)
    {
        LOG("INFO") << "Properties not loaded. Save defaults...";
        saveProperties();
    }
    
    LOG("INFO") << "Base application initialized.";
    return true;
}


bool BaseApplication::initUI(bool show_config_dialog_, std::string const &window_title_)
{
    // Init OGRE:
    
    Ogre::Root *root = new Ogre::Root{
        normalizePath(executableOrigin() + "/../lib/ogre-plugins.cfg"),
        normalizePath(savesDir + "/ogre.cfg"),
        ""
    };
    __log_redirect = new TOgreLogRedirect{};
    
    if(! root->restoreConfig()) show_config_dialog_ = true;
    
    if(show_config_dialog_) if(! root->showConfigDialog())
    {
        LOG("INFO") << "Video configuring interrupted!";
        delete root;
        return false;
    }
    
    window = root->initialise(true, window_title_);
    
    sceneManager = root->createSceneManager(Ogre::ST_GENERIC);
    sceneManager->addRenderQueueListener(new Ogre::OverlaySystem{});
    
    new TextManager{};
    new MetadataManager{};
    (new OgreOggSound::OgreOggSoundManager{})->setSceneManager(sceneManager);
    
    mainCamera = sceneManager->createCamera("mainCamera");
    mainCamera->setAutoAspectRatio(true);
    
    Ogre::Viewport *viewport = window->addViewport(mainCamera);
    viewport->setBackgroundColour(Ogre::ColourValue{.0f, .0f, .0f});
    viewport->setOverlaysEnabled(true);
    
    mainNode = sceneManager->getRootSceneNode()->createChildSceneNode();
    
    // Sub-initializations:
    
    if(! initInput(window))
    {
        __close(); return false;
    }
    
    root->addMovableObjectFactory(new OgreOggSound::OgreOggSoundFactory{});
    OgreOggSound::OgreOggSoundManager::getSingleton().init();
    
    _registerTextInputOverlayElement();
    _registerMultiTextOverlayElement();
    
    __is_closed = false;
    Ogre::WindowEventUtilities::addWindowEventListener(window, this);
    Ogre::WindowEventUtilities::messagePump();
    root->renderOneFrame();
    
    LOG("INFO") << "User interface initialized.";
    return true;
}


bool BaseApplication::initResources(std::vector<std::string> const &additional_)
{
    auto &resources = Ogre::ResourceGroupManager::getSingleton();
    try
    {
        resources.addResourceLocation(
            normalizePath(executableOrigin() + "/../resources"), "FileSystem",
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            true /*recursive*/
        );
    }
    catch(Ogre::Exception &)
    {
        LOG("ERROR") << "Default resources not initialized!";
        return false;
    }
    
    for(std::string const &path : additional_)
    {
        std::string norm_path = normalizePath(path);
        try
        {
            switch(getFileType(norm_path))
            {
            case FILE_TYPE_DIRECTORY:
                resources.addResourceLocation(
                    norm_path, "FileSystem",
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    true /*recursive*/
                );
                break;
            case FILE_TYPE_REGULAR: // We suppose it's a Zip-file
                resources.addResourceLocation(
                    norm_path, "Zip",
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    true /*recursive*/
                );
                break;
            default:;
            }
        }
        catch(Ogre::Exception &)
        {
            LOG("WARNING") << "Resources '" << norm_path << "' not initialized!";
        }
    }
    
    resources.initialiseAllResourceGroups();
    
    LOG("INFO") << "Resources initialized.";
    return true;
}


BaseApplication::~BaseApplication()
{
    __close();
    LOG("INFO") << "Base application closed.";
    return;
}


void BaseApplication::windowResized(Ogre::RenderWindow *window_)
{
    if(window_ != window || __is_closed) return;
    
    resizeInput(window_);
    
    Ogre::Viewport *vp = mainCamera->getViewport();
    mainCamera->setAspectRatio(
        (Ogre::Real)vp->getActualWidth() / (Ogre::Real)vp->getActualHeight()
    );
    return;
}


void BaseApplication::windowClosed(Ogre::RenderWindow *window_)
{
    if(window_ != window || __is_closed) return;
    
    closeInput(); __is_closed = true;
    
    LOG("INFO") << "Window has been closed.";
    return;
}

// --- *** ---


void BaseUIActivity::onStart(BaseActivity *back_from_)
{
    (void)back_from_;
    if(__is_closed) finishAll();
    return;
}


void BaseUIActivity::onLoop()
{
    Ogre::WindowEventUtilities::messagePump();
    if(__is_closed)
    {
        finishAll(); return;
    }
    
    if(! Ogre::Root::getSingleton().renderOneFrame())
    {
        closeInput(); __is_closed = true; finishAll();
        return;
    }
    
    OgreOggSound::OgreOggSoundManager::getSingleton().update();
    updateInput();
    return;
}


} // namespace LibUtils

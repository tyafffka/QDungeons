#ifndef __LIBUTILS_TEXT_MANAGER_HPP
#define __LIBUTILS_TEXT_MANAGER_HPP

#include <OgreResourceManager.h>
#include <unordered_map>

namespace LibUtils
{


/* Менеджер ресурсов, который позволяет загружать текстовые файлы
 * и представляет их содержимое как строку. Также загружает
 * структурированные файлы языка и предоставляет отдельные строки из них. */
class TextManager : public Ogre::ResourceManager, public Ogre::Singleton<TextManager>
{
protected:
    std::unordered_map<Ogre::String, Ogre::String> _language{};
    
    Ogre::Resource *createImpl(Ogre::String const &name_, Ogre::ResourceHandle handle_,
                               Ogre::String const &group_, bool is_manual_,
                               Ogre::ManualResourceLoader *loader_,
                               Ogre::NameValuePairList const *create_params_);
public:
    TextManager();
    ~TextManager();
    
    // ---
    
    /* Загружает текстовый файл из ресурсов и возвращает строку с содержимым. */
    Ogre::String load(Ogre::String const &file_,
                      Ogre::String const &group_=Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    
    /* Загружает MinSON-массив из файла и обновляет внутренний набор строк из него.
     * После этого строки будут доступны через метод `getString`. */
    void loadLanguage(Ogre::String const &file_,
                      Ogre::String const &group_=Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    /* Возвращает строку под именем `key_` из внутреннего набора. */
    Ogre::String const &getString(Ogre::String const &key_);
    
    // ---
    
    static TextManager &getSingleton();
    static TextManager *getSingletonPtr();
};


} // namespace LibUtils

#endif // __LIBUTILS_TEXT_MANAGER_HPP

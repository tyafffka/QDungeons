#ifndef __LIBUTILS_METADATA_MANAGER_HPP
#define __LIBUTILS_METADATA_MANAGER_HPP

#include "utils/metatype.hpp"
#include "utils/serialize/minson.hpp"
#include <OgreResourceManager.h>

namespace LibUtils
{


class __base_global_metadata
{
    template<typename> friend class GlobalMetadata;
    friend class MetadataManager;
    
private:
    __base_global_metadata *__next;
    char const *__name;
    
    __base_global_metadata(char const *name_) noexcept;
    ~__base_global_metadata() noexcept;
    
    virtual void __get(serialize::map_decoder &&d_) = 0;
};


/* Объект глобальных метаданных, который десериализуется при вызове
 * `MetadataManager::loadGlobals()`, если в загружаемом MinSON-массиве есть
 * данные с ключом `name_`. `name_` должен быть статичной строкой.
 * Все экземпляры должны быть объявлены как глобальные. */
template<typename T>
class GlobalMetadata final : public __base_global_metadata
{
private:
    T __data;
    
    void __get(serialize::map_decoder &&d_) override
    {
        d_.get_sub(__name, [this](serialize::value_decoder &&sub){
            __data = serialize::deserialize<T>(std::move(sub));
        });
    }
    
public:
    explicit GlobalMetadata(char const *name_) noexcept :
        __base_global_metadata(name_)
    {}
    
    T &operator *() noexcept {   return __data;   }
    T *operator ->() noexcept {   return std::addressof(__data);   }
};

// --- *** ---


/* Диспетчер метатипов, который способен загружать из ресурсов MinSON-файлы,
 * содержащие массивы, и создавать на их основе метатипы. */
class MetadataManager : public Ogre::ResourceManager,
                        public Ogre::Singleton<MetadataManager>,
                        public MetatypeManager
{
protected:
    Ogre::Resource *createImpl(Ogre::String const &name_, Ogre::ResourceHandle handle_,
                               Ogre::String const &group_, bool is_manual_,
                               Ogre::ManualResourceLoader *loader_,
                               Ogre::NameValuePairList const *create_params_);
public:
    explicit MetadataManager(memory::allocator *alloc_=&memory::std_allocator);
    ~MetadataManager();
    
    // ---
    
    /* Загружает MinSON-файл из ресурсов и возвращает его содержимое. */
    minson::object load(Ogre::String const &file_,
                        Ogre::String const &group_=Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    
    /* Загружает MinSON-массив из файла, который должен содержать именованные
     * подмассивы, и использует каждый подмассив для создания метатипа. */
    void loadTypes(Ogre::String const &file_,
                   Ogre::String const &group_=Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    
    /* Загружает MinSON-массив из файла и десериализует объекты глобальных
     * метаданных из него. Каждый объект десериализуется только один раз. */
    void loadGlobals(Ogre::String const &file_,
                     Ogre::String const &group_=Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    // ---
    
    static MetadataManager &getSingleton();
    static MetadataManager *getSingletonPtr();
};


} // namespace LibUtils

#endif // __LIBUTILS_METADATA_MANAGER_HPP

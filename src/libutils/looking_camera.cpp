#include "looking_camera.hpp"
#include "input.hpp"
#include <OgreSceneNode.h>

namespace LibUtils
{


static Ogre::Radian __PI_2{Ogre::Math::HALF_PI};


void LookingCameraActivity::_update_orientation()
{
    Ogre::Radian p = (__PI_2 + __pitch) * 0.5f, r = __rotation * 0.5f;
    Ogre::Real cos_p = Ogre::Math::Cos(p);
    Ogre::Real sin_p = Ogre::Math::Sin(p);
    Ogre::Real cos_r = Ogre::Math::Cos(r);
    Ogre::Real sin_r = Ogre::Math::Sin(r);
    
    mainCamera->setPosition(_camera_offset);
    mainCamera->setOrientation(Ogre::Quaternion{
        cos_r * cos_p, cos_r * sin_p, sin_r * sin_p, sin_r * cos_p
    });
    mainCamera->moveRelative(Ogre::Vector3{.0f, .0f, _camera_distance});
    return;
}


LookingCameraActivity::~LookingCameraActivity()
{
    if(__camera_node != nullptr)
        sceneManager->destroySceneNode(__camera_node);
    return;
}

// --- *** ---


void LookingCameraActivity::onStart(BaseActivity *back_from_)
{
    BaseUIActivity::onStart(back_from_);
    if(isFinish()) return;
    
    getMouse().setLockCursor(true);
    
    __camera_node = sceneManager->getRootSceneNode()->createChildSceneNode();
    __camera_node->attachObject(mainCamera);
    __camera_node->setVisible(true);
    
    mainCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
    _update_orientation();
    return;
}


void LookingCameraActivity::onLoop()
{
    if(__target != nullptr)
        __camera_node->setPosition(__target->_getDerivedPosition());
    
    BaseUIActivity::onLoop();
    return;
}


void LookingCameraActivity::onStop()
{
    BaseUIActivity::onStop();
    if(isFinish()) return;
    
    getMouse().setLockCursor(false);
    return;
}

// --- *** ---


void LookingCameraActivity::connectCameraTo(Ogre::SceneNode *target_)
{
    __pitch = .0f; __rotation = .0f;
    
    if((__target = target_) != nullptr)
        __camera_node->setPosition(__target->_getDerivedPosition());
    //else
    //    __camera_node->setPosition(Ogre::Vector3::ZERO);
    return;
}


void LookingCameraActivity::setCameraPitch(Ogre::Radian pitch_)
{
    if(pitch_.valueRadians() > Ogre::Math::HALF_PI)
        __pitch = Ogre::Math::HALF_PI;
    else if(pitch_.valueRadians() < -Ogre::Math::HALF_PI)
        __pitch = -Ogre::Math::HALF_PI;
    else
        __pitch = pitch_;
    
    _update_orientation();
    return;
}


void LookingCameraActivity::setCameraRotation(Ogre::Radian rotation_)
{
    __rotation = rotation_.valueRadians() -
        Ogre::Math::Floor(rotation_.valueRadians() / Ogre::Math::TWO_PI) *
        Ogre::Math::TWO_PI;
    
    _update_orientation();
    return;
}


Ogre::Vector2 LookingCameraActivity::byCameraRotation(Ogre::Vector2 v_)
{
    Ogre::Real cos_r = Ogre::Math::Cos(__rotation);
    Ogre::Real sin_r = Ogre::Math::Sin(__rotation);
    return Ogre::Vector2{v_.x * cos_r - v_.y * sin_r,
                         v_.x * sin_r + v_.y * cos_r};
}


Ogre::Vector3 LookingCameraActivity::byCameraPitchAndRotation(Ogre::Vector3 v_)
{
    Ogre::Real cos_p = Ogre::Math::Cos(__pitch);
    Ogre::Real sin_p = Ogre::Math::Sin(__pitch);
    Ogre::Real y = v_.y * cos_p - v_.z * sin_p;
    Ogre::Real z = v_.y * sin_p + v_.z * cos_p;
    
    Ogre::Real cos_r = Ogre::Math::Cos(__rotation);
    Ogre::Real sin_r = Ogre::Math::Sin(__rotation);
    return Ogre::Vector3{v_.x * cos_r - y * sin_r,
                         v_.x * sin_r + y * cos_r, z};
}


Ogre::Ray LookingCameraActivity::getCameraLook(Ogre::Real x_center_,
                                               Ogre::Real y_center_)
{
    Ogre::Ray ray = mainCamera->getCameraToViewportRay(
        x_center_ + 0.5f, y_center_ + 0.5f
    );
    ray.setOrigin(ray.getPoint(_camera_distance));
    return ray;
}


} // namespace LibUtils

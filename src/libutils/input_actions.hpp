#ifndef __LIBUTILS_INPUT_ACTIONS_HPP
#define __LIBUTILS_INPUT_ACTIONS_HPP

#include "input.hpp"
#include <string>

namespace LibUtils
{


/* Действие ввода. Принимает настройки клавиш из настроек приложения по имени.
 * Конструктор принимает имя действия и категорию (для группировки действий). */
class InputAction : public BaseOISAction
{
private:
    void __set_key(t_slot_id slot_id_, std::string const &key_);
    std::string __get_key(t_slot_id slot_id_) const;
    
protected:
    std::string _name;
    std::string _category;
    
    void _load_keys() override;
    void _dump_keys() const override;
    
public:
    InputAction(char const *name_, char const *category_) noexcept;
    
    std::string const &getName() const noexcept {   return _name;   }
    std::string const &getCategory() const noexcept {   return _category;   }
    
    /* Итератор по всем экземплярам `InputAction` в приложении. */
    struct iterator
    {
        InputAction *__p;
        
        void __to_common(BaseOISAction *p) noexcept
        {
            while(p != nullptr && dynamic_cast<InputAction *>(p) == nullptr) p = _next(p);
            __p = static_cast<InputAction *>(p);
        }
        
        explicit iterator(BaseOISAction *p) noexcept {   __to_common(p);   }
        
        InputAction &operator *() const noexcept {   return *__p;   }
        InputAction *operator ->() const noexcept {   return __p;   }
        
        iterator &operator ++() noexcept
        {   __to_common(_next(__p)); return *this;   }
        
        bool operator ==(iterator const &r) noexcept {   return(__p == r.__p);   }
        bool operator !=(iterator const &r) noexcept {   return(__p != r.__p);   }
    };
    
    static iterator begin() noexcept {   return iterator(_first());   }
    static iterator end() noexcept {   return iterator(nullptr);   }
};


/* Действие ввода со статично назначенными клавишами. В конструкторе указываются
 * тип и номер клавиши для клавиатуры-мыши и тип и номер клавиши для джойстиков. */
class StaticInputAction : public BaseOISAction
{
protected:
    c_key_type _km_key_type;
    t_key_num _km_key;
    c_key_type _joy_key_type;
    t_key_num _joy_key;
    
    void _load_keys() override;
    
public:
    StaticInputAction(c_key_type km_key_type_, t_key_num km_key_,
                      c_key_type joy_key_type_, t_key_num joy_key_) noexcept;
};


} // namespace LibUtils

#endif // __LIBUTILS_INPUT_ACTIONS_HPP

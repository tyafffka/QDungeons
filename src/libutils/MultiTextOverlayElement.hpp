#ifndef __LIBUTILS_MULTI_TEXT_OVERLAY_ELEMENT_HPP
#define __LIBUTILS_MULTI_TEXT_OVERLAY_ELEMENT_HPP

#include "utils/lines_buffer.hpp"
#include <OgreTextAreaOverlayElement.h>
#include <stdint.h>

namespace LibUtils
{


/* Элемент оверлея, отображающий текст с форматированием и стилевым
 * оформлением, таким как цвет и акцент (наклон шрифта). */
class MultiTextOverlayElement : public Ogre::TextAreaOverlayElement
{
protected:
    std::vector<uint8_t> _m_styles{};
    
public:
    explicit MultiTextOverlayElement(Ogre::String const &name_) :
        TextAreaOverlayElement(name_)
    {}
    
    Ogre::String const &getTypeName() const override;
    void updatePositionGeometry() override;
    void updateColours() override;
    
    /* Устанавливает строку как есть без форматирования с цветом по умолчанию. */
    void setCaption(Ogre::DisplayString const &text_) override;
    
    /* Форматирует текст с помощью `utf8_lines_buffer::format()` и
     * использует результат в качестве своего содержимого. */
    template<typename... _Params>
    void setFormat(std::string const &format_, _Params &&... params_)
    {
        setContent(utf8_lines_buffer::format(format_, std::forward<_Params>(params_)...));
    }
    
    /* Устанавливает содержимое из ранее форматированного текста. */
    void setContent(lines_buffer_content &&content_);
};


} // namespace LibUtils

#endif // __LIBUTILS_MULTI_TEXT_OVERLAY_ELEMENT_HPP

#include "menu.hpp"
#include "input_actions.hpp"
#include "dialog.hpp"
#include "values.hpp"
#include "MultiTextOverlayElement.hpp"
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreOggSoundManager.h>


namespace MenuValues
{

LibUtils::GlobalMetadata<t_color> full_black{"full_black"};
LibUtils::GlobalMetadata<t_text_colors>
    title_color{"menu_title_color"},
    subtitle_color{"menu_subtitle_color"},
    text_color{"menu_text_color"},
    disabled_color{"menu_disabled_color"};

int offset_x, offset_y, width = -1, height = -1,
    element_size, text_size, subtitle_text_size,
    width_padding, height_padding, inner_width, scroll_offset_x;

static LibUtils::GlobalMetadata<t_2d_units>
    __width{"menu_width"},
    __height{"menu_height"},
    __element_height{"menu_element_height"},
    __text_size{"menu_text_size"},
    __subtitle_text_size{"menu_subtitle_text_size"},
    __width_padding{"menu_width_padding"};

OgreOggSound::OgreOggISound
    *change_sound = nullptr,
    *select_sound = nullptr,
    *back_sound   = nullptr;


static void __init()
{
    if(width > 0 && height > 0) return;
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    int viewport_width = overlays.getViewportWidth();
    int viewport_height = overlays.getViewportHeight();
    
    if(viewport_width <= 0 || viewport_height <= 0) return;
    
    // Sizes
    
    width =         __width->         toPixels(viewport_width, viewport_height);
    height =        __height->        toPixels(viewport_width, viewport_height);
    element_size =  __element_height->toPixels(viewport_width, viewport_height);
    text_size =     __text_size->     toPixels(viewport_width, viewport_height);
    subtitle_text_size = __subtitle_text_size->toPixels(viewport_width, viewport_height);
    width_padding = __width_padding-> toPixels(viewport_width, viewport_height);
    
    //if(! (width <= viewport_width && height <= viewport_height &&
    //   element_size * 3 <= height && element_size * 4 <= width &&
    //   text_size <= element_size && subtitle_text_size <= element_size))
    //{   return false;   }
    
    offset_x = (viewport_width - width) / 2;
    offset_y = (viewport_height - height) / 2;
    height_padding = (element_size - text_size) / 2;
    inner_width = width - 2 * width_padding;
    scroll_offset_x = offset_x + width + width_padding / 2;
    
    // Sounds
    
    auto &sounds = OgreOggSound::OgreOggSoundManager::getSingleton();
    
    change_sound = sounds.createSound("menu_change", "sounds/menu_change.ogg", false, false, true);
    select_sound = sounds.createSound("menu_select", "sounds/menu_select.ogg", false, false, true);
    back_sound   = sounds.createSound("menu_back",   "sounds/menu_back.ogg",   false, false, true);
    return;
}


void play(OgreOggSound::OgreOggISound *sound_)
{
    if(change_sound == nullptr || select_sound == nullptr || back_sound == nullptr)
        return;
    
    change_sound->stop(); select_sound->stop(); back_sound->stop();
    sound_->play();
    return;
}

} // namespace MenuValues

// --- *** ---

namespace LibUtils
{


void BaseMenuActivity::__update_visibility()
{
    int i = -__scroll;
    for(BaseMenuElement *element : __elements)
    {
        if(0 <= i && i < __visible_elements_count)
        {
            element->_layout->setPosition(
                0, __offset_elements + i * MenuValues::element_size
            );
            element->_layout->show();
        }
        else
        {
            element->_layout->hide();
            element->_layout->setPosition(0, 0);
        }
        ++i;
    }
    
    int elements_count = (int)__elements.size();
    if(__visible_elements_count < elements_count)
    {
        int scroll_height = __visible_elements_count * MenuValues::element_size;
        
        __scroll_bar->setTop(__offset_elements + scroll_height *
                             ((float)__scroll / (float)elements_count));
        __scroll_bar->setHeight(scroll_height * ((float)__visible_elements_count /
                                                 (float)elements_count));
        __scroll_bar->show();
    }
    else
    {   __scroll_bar->hide();   }
    return;
}

// --- *** ---


void BaseMenuActivity::setTitle(std::string const &title_)
{
    if(__title != nullptr) return;
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *title = static_cast<MultiTextOverlayElement *>(
        overlays.createOverlayElement("MultiText", "title")
    );
    title->setMetricsMode(Ogre::GMM_PIXELS);
    title->setPosition(MenuValues::width / 2, MenuValues::offset_y + MenuValues::height_padding); // relative to foreground
    title->setDimensions(MenuValues::width, MenuValues::element_size);
    title->setCharHeight(MenuValues::text_size);
    title->setAlignment(Ogre::TextAreaOverlayElement::Center);
    title->setFontName("Menu/text");
    title->getMaterial(); // КОСТЫЛЬ
    MenuValues::title_color->applyTo(title);
    title->setFormat(title_);
    title->show();
    
    // ---
    
    __offset_elements += MenuValues::element_size;
    __scroll = __visible_elements_count = __max_scroll = -1;
    
    __foreground->addChild(__title = title);
    return;
}


void BaseMenuActivity::setSubtitle(std::string const &subtitle_)
{
    if(__subtitle != nullptr) return;
    
    int lines = 1;
    for(char const *p = subtitle_.c_str(); *p != '\0'; ++p)
    {
        if(*p == '\n') ++lines;
    }
    int size = MenuValues::height_padding * 2 + MenuValues::subtitle_text_size * lines;
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *subtitle = static_cast<MultiTextOverlayElement *>(
        overlays.createOverlayElement("MultiText", "subtitle")
    );
    subtitle->setMetricsMode(Ogre::GMM_PIXELS);
    subtitle->setPosition(MenuValues::width_padding,
                          __offset_elements + MenuValues::height_padding); // relative to foreground
    subtitle->setDimensions(MenuValues::width, size);
    subtitle->setCharHeight(MenuValues::subtitle_text_size);
    subtitle->setAlignment(Ogre::TextAreaOverlayElement::Left);
    subtitle->setFontName("Menu/text");
    subtitle->getMaterial(); // КОСТЫЛЬ
    MenuValues::subtitle_color->applyTo(subtitle);
    subtitle->setFormat(subtitle_);
    subtitle->show();
    
    // ---
    
    __offset_elements += size;
    __scroll = __visible_elements_count = __max_scroll = -1;
    
    __foreground->addChild(__subtitle = subtitle);
    return;
}


BaseMenuElement *BaseMenuActivity::addElement(BaseMenuElement *new_element_)
{
    __elements.push_back(new_element_);
    __scroll = __visible_elements_count = __max_scroll = -1;
    
    __foreground->addChild(new_element_->_layout);
    return new_element_;
}

// --- *** ---


void BaseMenuActivity::menuBack()
{
    finish(); MenuValues::play(MenuValues::back_sound);
    return;
}


BaseMenuActivity::~BaseMenuActivity()
{
    for(BaseMenuElement *element : __elements) delete element;
    
    if(__layout != nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        if(__title != nullptr)
            overlays.destroyOverlayElement(__title);
        if(__subtitle != nullptr)
            overlays.destroyOverlayElement(__subtitle);
        
        overlays.destroyOverlayElement(__scroll_bar);
        overlays.destroyOverlayElement(__foreground);
        overlays.destroyOverlayElement(__background);
        overlays.destroy(__layout);
    }
    return;
}


void BaseMenuActivity::onStart(BaseActivity *back_from_)
{
    BaseUIActivity::onStart(back_from_);
    if(isFinish()) return;
    
    MenuValues::__init();
    getMouse().setLockCursor(false);
    mainCamera->getViewport()->setBackgroundColour(*MenuValues::full_black);
    
    // ---
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    int viewport_width = overlays.getViewportWidth();
    int viewport_height = overlays.getViewportHeight();
    
    __layout = overlays.create("Menu");
    
    __offset_elements = MenuValues::offset_y;
    __current_element = __scroll = -1;
    __visible_elements_count = __max_scroll = -1;
    
    // ---
    
    auto *background = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", "background_panel")
    );
    background->setMetricsMode(Ogre::GMM_PIXELS);
    background->setPosition(0, 0);
    background->setDimensions(viewport_width, viewport_height);
    if(viewport_width * 2 >= viewport_height * 3)
        background->setMaterialName("Menu/background_wide");
    else
        background->setMaterialName("Menu/background");
    background->setUV(0.0f, 0.0f, 1.0f, 1.0f);
    background->show();
    
    // ---
    
    auto *foreground = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", "foreground_panel")
    );
    foreground->setMetricsMode(Ogre::GMM_PIXELS);
    foreground->setPosition(MenuValues::offset_x, 0);
    foreground->setDimensions(MenuValues::width, viewport_height);
    foreground->setMaterialName("Menu/foreground");
    foreground->show();
    
    // ---
    
    auto *scroll_bar = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", "scroll_bar")
    );
    scroll_bar->setMetricsMode(Ogre::GMM_PIXELS);
    scroll_bar->setPosition(MenuValues::scroll_offset_x, MenuValues::offset_y);
    scroll_bar->setDimensions(MenuValues::width_padding, MenuValues::height);
    scroll_bar->setMaterialName("Menu/foreground");
    scroll_bar->hide();
    
    // ---
    
    background->addChild(__scroll_bar = scroll_bar);
    background->addChild(__foreground = foreground);
    __layout->add2D(__background = background);
    __layout->setZOrder(0);
    __layout->show();
    return;
}


static StaticInputAction
    __scroll_up  {INPUT_POS_AXIS(2), INPUT_NO_KEY},
    __scroll_down{INPUT_NEG_AXIS(2), INPUT_NO_KEY},
    __mouse_click{INPUT_MOUSE_KEY(OIS::MB_Left), INPUT_NO_KEY},
    __up    {INPUT_KEYBOARD_KEY(OIS::KC_UP), INPUT_NEG_AXIS(1)},
    __up2   {INPUT_NO_KEY, INPUT_JOYSTICK_DPAD_UP},
    __down  {INPUT_KEYBOARD_KEY(OIS::KC_DOWN), INPUT_POS_AXIS(1)},
    __down2 {INPUT_NO_KEY, INPUT_JOYSTICK_DPAD_DOWN},
    __left  {INPUT_KEYBOARD_KEY(OIS::KC_LEFT), INPUT_NEG_AXIS(0)},
    __left2 {INPUT_NO_KEY, INPUT_JOYSTICK_DPAD_LEFT},
    __right {INPUT_KEYBOARD_KEY(OIS::KC_RIGHT), INPUT_POS_AXIS(0)},
    __right2{INPUT_NO_KEY, INPUT_JOYSTICK_DPAD_RIGHT},
    __select{INPUT_KEYBOARD_KEY(OIS::KC_RETURN), INPUT_JOYSTICK_KEY(0)},
    __back  {INPUT_KEYBOARD_KEY(OIS::KC_ESCAPE), INPUT_JOYSTICK_KEY(1)};

void BaseMenuActivity::onLoop()
{
    BaseUIActivity::onLoop();
    if(isFinish()) return;
    if(BaseDialog::updateActive()) return;
    
    int elements_count = (int)__elements.size();
    if(elements_count == 0)
    {
        if(__back.isJustActivated()) menuBack();
        return;
    }
    
    if(__visible_elements_count < 0)
    {
        __visible_elements_count =
            (MenuValues::offset_y + MenuValues::height - __offset_elements) /
            MenuValues::element_size;
        if(__visible_elements_count < 1) __visible_elements_count = 1;
    }
    
    if(__max_scroll < 0)
    {
        __max_scroll = elements_count - __visible_elements_count;
        if(__max_scroll < 0) __max_scroll = 0;
    }
    
    int point_to_element = __current_element;
    if(point_to_element < 0)
    {
        point_to_element = 0;
        for(;;)
        {
            if(__elements[point_to_element]->isEnabled()) break;
            if(++point_to_element > elements_count - 1)
            {
                if(__back.isJustActivated()) menuBack();
                return;
            }
        }
    }
    
    // Input
    
    int new_scroll = __scroll - (int)__scroll_up.fullValue(__scroll_down);
    if(new_scroll < 0) new_scroll = 0;
    else if(new_scroll > __max_scroll) new_scroll = __max_scroll;
    
    if(isMouseActive())
    {
        int cursor_x = mouseWindowX() - MenuValues::offset_x;
        int cursor_y = mouseWindowY() - __offset_elements;
        int element = cursor_y / MenuValues::element_size;
        int new_point_to_element = new_scroll + element;
        
        if(0 <= cursor_x && cursor_x <= MenuValues::width && cursor_y >= 0 &&
           element < __visible_elements_count && element < elements_count)
        {
            if(__elements[new_point_to_element]->isEnabled())
            {
                point_to_element = new_point_to_element;
                
                if(__mouse_click.isJustActivated())
                    __elements[point_to_element]->_mouse_click(
                        (float)(cursor_x - MenuValues::width_padding) /
                        (float)MenuValues::inner_width
                    );
            }
        }
    }
    
    if(__up.isJustActivated() || __up2.isJustActivated())
    {
        int new_point_to_element = point_to_element;
        for(;;)
        {
            if(--new_point_to_element < 0) new_point_to_element = elements_count - 1;
            if(new_point_to_element == point_to_element) break;
            if(__elements[new_point_to_element]->isEnabled()) break;
        }
        point_to_element = new_point_to_element;
    }
    
    if(__down.isJustActivated() || __down2.isJustActivated())
    {
        int new_point_to_element = point_to_element;
        for(;;)
        {
            if(++new_point_to_element > elements_count - 1) new_point_to_element = 0;
            if(new_point_to_element == point_to_element) break;
            if(__elements[new_point_to_element]->isEnabled()) break;
        }
        point_to_element = new_point_to_element;
    }
    
    if(__left.isJustActivated() || __left2.isJustActivated())
        __elements[point_to_element]->_decrease();
    
    if(__right.isJustActivated() || __right2.isJustActivated()) 
        __elements[point_to_element]->_increase();
    
    if(__select.isJustActivated())
        __elements[point_to_element]->_select();
    
    if(__back.isJustActivated()) menuBack();
    
    // Update elements
    
    if(point_to_element < new_scroll)
        new_scroll = point_to_element;
    else if(point_to_element >= new_scroll + __visible_elements_count)
        new_scroll = point_to_element - __visible_elements_count + 1;
    
    if(__scroll != new_scroll)
    {
        __scroll = new_scroll; __update_visibility();
    }
    
    if(__current_element != point_to_element)
    {
        if(0 <= __current_element && __current_element < elements_count)
        {
            __elements[__current_element]->_kill_focus();
            MenuValues::play(MenuValues::change_sound);
        }
        
        __elements[(__current_element = point_to_element)]->_set_focus();
    }
    
    __elements[point_to_element]->_loop();
    return;
}


void BaseMenuActivity::onStop()
{
    BaseUIActivity::onStop();
    
    for(BaseMenuElement *element : __elements) delete element;
    __elements.clear();
    
    if(__layout != nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        if(__title != nullptr)
        {
            overlays.destroyOverlayElement(__title); __title = nullptr;
        }
        if(__subtitle != nullptr)
        {
            overlays.destroyOverlayElement(__subtitle); __subtitle = nullptr;
        }
        
        overlays.destroyOverlayElement(__scroll_bar); __scroll_bar = nullptr;
        overlays.destroyOverlayElement(__foreground); __foreground = nullptr;
        overlays.destroyOverlayElement(__background); __background = nullptr;
        overlays.destroy(__layout); __layout = nullptr;
    }
    return;
}


} // namespace LibUtils

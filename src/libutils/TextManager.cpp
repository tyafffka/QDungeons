#include "TextManager.hpp"
#include "utils/log.hpp"
#include "utils/memory/on_final.hpp"
#include <minson/minson.hpp>


namespace Ogre
{
template<> LibUtils::TextManager *Singleton<LibUtils::TextManager>::msSingleton = nullptr;
}

using namespace Ogre;

// --- *** ---

namespace LibUtils
{


class _TextResource : public Resource
{
protected:
    String _text{};
    
    void loadImpl() override
    {
        DataStreamPtr stream = ResourceGroupManager::getSingleton().openResource(mName, mGroup, true, this);
        _text = stream->getAsString();
        stream->close();
        return;
    }
    
    void unloadImpl() override
    {
        _text.clear(); return;
    }
    
    size_t calculateSize() const override
    {   return _text.size();   }
    
public:
    _TextResource(ResourceManager *creator_, String const &name_,
                  ResourceHandle handle_, String const &group_,
                  bool is_manual_=false, ManualResourceLoader *loader_=nullptr) :
        Resource(creator_, name_, handle_, group_, is_manual_, loader_)
    {}
    
    inline String &&moveText() {   return std::move(_text);   }
};

// --- *** ---


Resource *TextManager::createImpl(String const &name_, ResourceHandle handle_,
                                  String const &group_, bool is_manual_,
                                  ManualResourceLoader *loader_,
                                  NameValuePairList const *create_params_)
{
    (void)create_params_;
    return new _TextResource{this, name_, handle_, group_, is_manual_, loader_};
}


TextManager::TextManager()
{
    mResourceType = "Text";
    mLoadOrder = 30.0f;
    ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
    return;
}


TextManager::~TextManager()
{
    ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
    return;
}

// --- *** ---


String TextManager::load(String const &file_, String const &group_)
{
    ResourcePtr p = getResourceByName(file_, group_);
    if(p == nullptr)
    {
        p = createResource(file_,
            (group_ == ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME)?
            ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME : group_
        );
    }
    
    p->load();
    return static_cast<_TextResource *>(p.get())->moveText();
}


void TextManager::loadLanguage(String const &file_, String const &group_)
{
    on_final {   unload(file_);   };
    try
    {
        minson::array content = minson::parse(load(file_, group_));
        for(auto it = content.begin(), itend = content.end(); it != itend; ++it)
        {
            if(! it.hasName()) continue;
            
            String name = it.name();
            _language.erase(name);
            _language.insert(std::make_pair(std::move(name), (String)*it));
        }
    }
    catch(minson::exception &)
    {
        LOG("WARNING") << "TextManager::loadLanguage(): language file '"
                       << file_ << "' corrupted!";
        throw;
    }
    return;
}


String const &TextManager::getString(String const &key_)
{
    auto found = _language.find(key_);
    if(found == _language.end())
    {
        LOG("WARNING") << "TextManager::getString(" << key_ << "): string not found!";
        
        return _language.insert(std::make_pair(key_, "<" + key_ + ">"))
               .first->second;
    }
    return found->second;
}

// --- *** ---


TextManager &TextManager::getSingleton()
{
    assert(msSingleton != nullptr); return(*msSingleton);
}


TextManager *TextManager::getSingletonPtr()
{   return msSingleton;   }


} // namespace LibUtils

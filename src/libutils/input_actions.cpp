#include "input_actions.hpp"
#include "application.hpp"
#include "utils/log.hpp"

namespace LibUtils
{


void InputAction::__set_key(t_slot_id slot_id_, std::string const &key_)
{
    if(key_.compare(0, 3, "key") == 0)
    {
        if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
            setKey(slot_id_, INPUT_KEYBOARD_KEY(std::atoi(key_.c_str() + 3)));
        else
            setKey(slot_id_, INPUT_JOYSTICK_KEY(std::atoi(key_.c_str() + 3)));
    }
    else if(key_.compare(0, 5, "mouse") == 0)
        setKey(slot_id_, INPUT_MOUSE_KEY(std::atoi(key_.c_str() + 5)));
    else if(key_.compare(0, 3, "dpad") == 0)
        setKey(slot_id_, INPUT_JOYSTICK_DPAD(0, std::atoi(key_.c_str() + 3)));
    else if(key_.compare(0, 5, "+axis") == 0)
        setKey(slot_id_, INPUT_POS_AXIS(std::atoi(key_.c_str() + 5)));
    else if(key_.compare(0, 5, "-axis") == 0)
        setKey(slot_id_, INPUT_NEG_AXIS(std::atoi(key_.c_str() + 5)));
    else
        resetKey(slot_id_);
    return;
}

std::string InputAction::__get_key(t_slot_id slot_id_) const
{
    t_key_num key = getKey(slot_id_);
    switch(getKeyType(slot_id_))
    {
    case C_KEY:
        if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
        {
            if(key < 256) return "key" + std::to_string(key);
            else return "mouse" + std::to_string(key - 256);
        }
        else
        {
            if(key < 16) return "dpad" + std::to_string(key);
            else return "key" + std::to_string(key - 16);
        }
    case C_POSITIVE_SEMIAXIS:
        return "+axis" + std::to_string(key);
    case C_NEGATIVE_SEMIAXIS:
        return "-axis" + std::to_string(key);
    default: return "";
    }
}


void InputAction::_load_keys()
{
    try
    {
        minson::array input = properties.get("input");
        minson::array km = input.get("KeyboardMouse");
        __set_key(KEYBOARD_MOUSE_SLOT_ID, km.get(_name));
        
        int count = joysticksCount();
        for(int i = 0; i < count; ++i)
        {
            minson::array joy = input.get(_safe_name(i));
            __set_key(joystickSlotID(i), joy.get(_name));
        }
    }
    catch(minson::exception &)
    {
        LOG("ERROR") << "Invalid value type in input properties!";
    }
    return;
}

void InputAction::_dump_keys() const
{
    minson::array input = properties.get("input");
    minson::array km = input.get("KeyboardMouse");
    km.putString(__get_key(KEYBOARD_MOUSE_SLOT_ID), _name);
    
    int count = joysticksCount();
    for(int i = 0; i < count; ++i)
    {
        minson::array joy = input.get(_safe_name(i));
        joy.putString(__get_key(joystickSlotID(i)), _name);
    }
    return;
}


InputAction::InputAction(char const *name_, char const *category_) noexcept :
    BaseOISAction(), _name(name_), _category(category_)
{}

// --- *** ---


void StaticInputAction::_load_keys()
{
    setKey(KEYBOARD_MOUSE_SLOT_ID, _km_key_type, _km_key);
    
    int count = joysticksCount();
    for(int i = 0; i < count; ++i)
        setKey(joystickSlotID(i), _joy_key_type, _joy_key);
    return;
}


StaticInputAction::StaticInputAction(c_key_type km_key_type_, t_key_num km_key_,
                                     c_key_type joy_key_type_, t_key_num joy_key_) noexcept :
    BaseOISAction(),
    _km_key_type(km_key_type_), _km_key(km_key_),
    _joy_key_type(joy_key_type_), _joy_key(joy_key_)
{}


} // namespace LibUtils

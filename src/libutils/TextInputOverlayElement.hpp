#ifndef __LIBUTILS_TEXT_INPUT_OVERLAY_ELEMENT_HPP
#define __LIBUTILS_TEXT_INPUT_OVERLAY_ELEMENT_HPP

#include <OgreTextAreaOverlayElement.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <vector>

namespace LibUtils
{


/* Элемент оверлея, позволяющий вводить текст с клавиатуры.
 * Также обрабатывает нажатия левой кнопки мыши в пределах области текста.
 * Требует обновления ввода и таймеров. */
class TextInputOverlayElement : public Ogre::TextAreaOverlayElement,
                                public OIS::KeyListener, public OIS::MouseListener
{
private:
    std::vector<float> __x_offsets{};
    size_t __cursor = 0;
    size_t __dirty_offset_from = Ogre::DisplayString::npos;
    bool __is_cursor_dirty = true;
    
    void __update_offsets();
    float __get_text_width();
    float __get_left();
    
public:
    bool isCaptionChanged = false;
    
    TextInputOverlayElement(Ogre::String const &name_) : TextAreaOverlayElement(name_) {}
    ~TextInputOverlayElement();
    
    void postRender(Ogre::SceneManager *sm_, Ogre::RenderSystem *rsys_) override;
    
    bool keyPressed(OIS::KeyEvent const &event_) override;
    bool keyReleased(OIS::KeyEvent const &event_) override;
    bool mouseMoved(OIS::MouseEvent const &event_) override;
    bool mousePressed(OIS::MouseEvent const &event_, OIS::MouseButtonID id_) override;
    bool mouseReleased(OIS::MouseEvent const &event_, OIS::MouseButtonID id_) override;
    
    // ---
    
    void setCaption(Ogre::DisplayString const &text_) override;
    Ogre::String const &getTypeName() const override;
    
    void _invalidateCursor();
    /* Присвоить фокус ввода. Устанавливаются обработчики клавиатуры и мыши,
     * опционально устанавливается положение курсора. */
    void setFocus(size_t cursor_=Ogre::DisplayString::npos);
    /* Снять фокус ввода. Снимаются обработчики клавиатуры и мыши. */
    void killFocus();
};


} // namespace LibUtils

#endif // __LIBUTILS_TEXT_INPUT_OVERLAY_ELEMENT_HPP

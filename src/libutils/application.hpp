#ifndef __LIBUTILS_APPLICATION_HPP
#define __LIBUTILS_APPLICATION_HPP

#include "utils/background_tasks.hpp"
#include "utils/activities.hpp"
#include <minson/minson.hpp>
#include <OgreRenderWindow.h>
#include <OgreSceneManager.h>
#include <OgreCamera.h>
#include <OgreWindowEventUtilities.h>
#include <vector>
#include <string>

namespace LibUtils
{


/* MinSON-массив настроек приложения. Следует заполнить
 * значениями по умолчанию до вызова инициализации базового приложения. */
extern minson::array properties;
/* Папка, где хранятся настройки, логи и сами сохранения. */
extern std::string savesDir;

/* Окно приложения, менеджер сцены и камера по умолчанию,
 * предоставляемые OGRE-ом, а также узел для всех объектов основной сцены. */
extern Ogre::RenderWindow *window;
extern Ogre::SceneManager *sceneManager;
extern Ogre::Camera *mainCamera;
extern Ogre::SceneNode *mainNode;


/* Сохранить настройки приложения из массива `properties` в файл. */
void saveProperties();


/* Базовый класс приложения, который создаёт окружение
 * и даёт методы инициализации различных частей приложения. */
class BaseApplication : public Ogre::WindowEventListener
{
private:
    background_tasks_pool __pool{};
    
    void __close();
    
protected:
    /* Инициализирует базовое приложение. Настройки приложения
     * должны быть заполнены значениями по умолчанию.
     * - `saves_dir_` -- папка сохранений, где должны храниться
     *   настройки приложения, логи и сами сохранения. */
    virtual bool initApplication(std::string const &saves_dir_);
    /* Инициализирует OGRE, а также подсистему ввода.
     * - `show_config_dialog_` -- показывать ли диалог настроек OGRE;
     *   если не удаётся загрузить настройки, диалог будет показан
     *   в любом случае.
     * - `window_title_` -- заголовок окна приложения. */
    virtual bool initUI(bool show_config_dialog_, std::string const &window_title_);
    /* Инициализирует систему ресурсов OGRE. `additional_` -- набор путей
     * к папкам и ZIP-архивам, содержащим дополнительные ресурсы. */
    virtual bool initResources(std::vector<std::string> const &additional_);
    
public:
    ~BaseApplication();
    
    void windowResized(Ogre::RenderWindow *window_) override;
    void windowClosed(Ogre::RenderWindow *window_) override;
};


/* Базовый класс активности приложения, который вызывает обработку окна,
 * рендеринг и обновление устройств ввода. */
class BaseUIActivity : public BaseActivity
{
public:
    void onStart(BaseActivity *back_from_) override;
    void onLoop() override;
};


} // namespace LibUtils

#endif // __LIBUTILS_APPLICATION_HPP

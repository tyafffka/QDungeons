#ifndef __LIBUTILS_LOOKING_CAMERA_HPP
#define __LIBUTILS_LOOKING_CAMERA_HPP

#include "application.hpp"

namespace LibUtils
{


/* Класс активности, дающий управление камерой от третьего лица
 * (или от первого лица при `distance_ == 0`). Также камера может следовать
 * за указанной целью с центром поворота, находящимся на высоте `height_`.
 * `distance_` - расстояние от центра поворота до самой камеры. */
class LookingCameraActivity : public BaseUIActivity
{
private:
    Ogre::SceneNode *__target = nullptr;
    Ogre::SceneNode *__camera_node = nullptr;
    Ogre::Radian __pitch{.0f}, __rotation{.0f};
    
protected:
    Ogre::Vector3 _camera_offset;
    Ogre::Real _camera_distance;
    
    void _update_orientation();
    
public:
    LookingCameraActivity(Ogre::Real height_, Ogre::Real distance_) :
        _camera_offset(.0f, .0f, height_), _camera_distance(distance_)
    {}
    ~LookingCameraActivity();
    
    void onStart(BaseActivity *back_from_) override;
    void onLoop() override;
    void onStop() override;
    
    // ---
    
    /* Указать цель, за которой будет следовать камера.
     * При указании `nullptr` камера переместится в начало координат. */
    void connectCameraTo(Ogre::SceneNode *target_);
    
    /* Присвоить высоту взгляда и поворот камеры вокруг вертикальной оси. */
    void setCameraPitch(Ogre::Radian pitch_);
    void setCameraRotation(Ogre::Radian rotation_);
    /* Возвращают высоту взгляда и поворот камеры соответственно. */
    inline Ogre::Radian getCameraPitch() {   return __pitch;   }
    inline Ogre::Radian getCameraRotation() {   return __rotation;   }
    
    /* Смещают высоту взгляда и угол поворота камеры. */
    inline void addCameraPitch(Ogre::Radian pitch_)
    {   setCameraPitch(__pitch + pitch_);   }
    inline void addCameraRotation(Ogre::Radian rotation_)
    {   setCameraRotation(__rotation + rotation_);   }
    
    /* Поворачивает 2D-вектор в горизонтальной плоскости
     * в соответствии с углом поворота камеры. */
    Ogre::Vector2 byCameraRotation(Ogre::Vector2 v_);
    /* Поворачивает 3D-вектор в соответствии с направлением взгляда камеры. */
    Ogre::Vector3 byCameraPitchAndRotation(Ogre::Vector3 v_);
    
    /* Возвращает луч взгляда камеры, считая от центра поворота. `x_center_`,
     * `y_center_` -- отклонение от центра экрана в диапазоне [-0.5, 0.5]. */
    Ogre::Ray getCameraLook(Ogre::Real x_center_, Ogre::Real y_center_);
};


} // namespace LibUtils

#endif // __LIBUTILS_LOOKING_CAMERA_HPP

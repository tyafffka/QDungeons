#include "MultiTextOverlayElement.hpp"
#include "values.hpp"
#include <OgreRoot.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayElementFactory.h>

#define POS_TEX_BINDING 0
#define COLOUR_BINDING  1
#define UNICODE_CR    0x000D
#define UNICODE_LF    0x000A
#define UNICODE_SPACE 0x0020

namespace LibUtils
{


static Ogre::String const __TypeName{"MultiText"};

static GlobalMetadata<t_text_colors_array<15>> __palette{"multi_text_colors"};
static GlobalMetadata<float> __accent_shift{"multi_text_accent_shift"};

// --- *** ---


Ogre::String const &MultiTextOverlayElement::getTypeName() const
{   return __TypeName;   }


void MultiTextOverlayElement::updatePositionGeometry() /* - Apply accent - */
{
    TextAreaOverlayElement::updatePositionGeometry();
    if(mFont == nullptr) return;
    
    float shift = mCharHeight * (*__accent_shift) * mViewportAspectCoef;
    
    Ogre::HardwareVertexBufferSharedPtr vbuf =
        mRenderOp.vertexData->vertexBufferBinding->getBuffer(POS_TEX_BINDING);
    float *pVert = static_cast<float *>(vbuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));
    
    auto ch_it = mCaption.begin();
    for(uint8_t style : _m_styles)
    {
        auto ch = *ch_it; ++ch_it;
        if(ch == UNICODE_CR  || ch == UNICODE_LF || ch == UNICODE_SPACE) continue;
        
        // [each vert is (x, y, z, u, v)] * [6 verts by char]
        if((style & LINES_BUFFER_ACCENT) != 0)
        {
            *pVert += shift; pVert += 5; // Top left
            *pVert -= shift; pVert += 5; // Bottom left
            *pVert += shift; pVert += 5; // Top right
            *pVert += shift; pVert += 5; // Top right (again)
            *pVert -= shift; pVert += 5; // Bottom left (again)
            *pVert -= shift; pVert += 5; // Bottom right
        }
        else
        {   pVert += 30;   }
    }
    
    vbuf->unlock();
    return;
}


void MultiTextOverlayElement::updateColours() /* - Apply colors - */
{
    Ogre::Root &R = Ogre::Root::getSingleton();
    
    Ogre::RGBA c_top, c_bottom;
    R.convertColourValue(mColourTop, &c_top);
    R.convertColourValue(mColourBottom, &c_bottom);
    uint8_t last_color = LINES_BUFFER_DEFAULT_COLOR;
    
    Ogre::HardwareVertexBufferSharedPtr vbuf =
        mRenderOp.vertexData->vertexBufferBinding->getBuffer(COLOUR_BINDING);
    auto *pDest = static_cast<Ogre::RGBA *>(vbuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));
    
    auto ch_it = mCaption.begin();
    for(uint8_t style : _m_styles)
    {
        auto ch = *ch_it; ++ch_it;
        if(ch == UNICODE_CR  || ch == UNICODE_LF || ch == UNICODE_SPACE) continue;
        
        uint8_t color = style & LINES_BUFFER_COLOR_MASK;
        if(color != last_color)
        {
            if((last_color = color) == LINES_BUFFER_DEFAULT_COLOR)
            {
                R.convertColourValue(mColourTop, &c_top);
                R.convertColourValue(mColourBottom, &c_bottom);
            }
            else
            {
                R.convertColourValue(__palette->color[color].top, &c_top);
                R.convertColourValue(__palette->color[color].bottom, &c_bottom);
            }
        }
        
        *pDest++ = c_top; *pDest++ = c_bottom; *pDest++ = c_top;
        *pDest++ = c_top; *pDest++ = c_bottom; *pDest++ = c_bottom;
    }
    
    vbuf->unlock();
    return;
}


void MultiTextOverlayElement::setCaption(Ogre::DisplayString const &text_)
{
    TextAreaOverlayElement::setCaption(text_);
    
    _m_styles.assign(mCaption.size(), LINES_BUFFER_DEFAULT_COLOR);
    mColoursChanged = true;
    return;
}


void MultiTextOverlayElement::setContent(lines_buffer_content &&content_)
{
    mCaption.clear(); mCaption.reserve(content_.chars.size());
    for(auto ch : content_.chars) mCaption.push_back((Ogre::DisplayString::value_type)ch);
    
    _m_styles = std::move(content_.styles);
    mGeomPositionsOutOfDate = mGeomUVsOutOfDate = mColoursChanged = true;
    return;
}

// --- *** ---


class MultiTextOverlayElementFactory : public Ogre::OverlayElementFactory
{
public:
    Ogre::OverlayElement *createOverlayElement(Ogre::String const &instance_name_) override
    {
        return OGRE_NEW MultiTextOverlayElement(instance_name_);
    }
    
    Ogre::String const &getTypeName() const override
    {   return __TypeName;   }
};


} // namespace LibUtils


void _registerMultiTextOverlayElement()
{
    Ogre::OverlayManager::getSingleton().addOverlayElementFactory(
        OGRE_NEW LibUtils::MultiTextOverlayElementFactory{}
    );
    return;
}

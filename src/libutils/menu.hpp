#ifndef __LIBUTILS_MENU_HPP
#define __LIBUTILS_MENU_HPP

#include "menu_elements.hpp"
#include "application.hpp"
#include <OgreOverlay.h>
#include <vector>

namespace LibUtils
{


/* Базовый класс для меню приложения. Меню может состоять из элементов,
 * расположенных вертикально посередине экрана.
 * Реализует обработку ввода следующим образом:
 * 
 * - Вверх/вниз на клавиатуре или крестовине джойстика,
 *   ось Y джойстика - перемещение между элементами.
 * - Влево/вправо на клавиатуре или крестовине джойстика,
 *   ось X джойстика - изменение значения на элементе, если предусмотрено.
 * - `Enter` на клавиатуре или кнопка #1 на джойстике - выбор элемента -
 *   реакция зависит от типа элемента, также см. `BaseMenuElement::onSelected`.
 * - `ESC` на клавиатуре или кнопка #2 на джойстике - действие "назад" -
 *   см. `menuBack()`.
 * 
 * Также возможно перемещение, выбор и изменение значений элементов мышью. */
class BaseMenuActivity : public BaseUIActivity
{
private:
    Ogre::Overlay *__layout = nullptr;
    Ogre::OverlayElement *__title = nullptr, *__subtitle = nullptr;
    Ogre::OverlayContainer *__background = nullptr, *__foreground = nullptr,
                           *__scroll_bar = nullptr;
    std::vector<BaseMenuElement *> __elements{};
    
    int __offset_elements /*px*/;
    int __current_element, __scroll;
    int __visible_elements_count, __max_scroll;
    
    void __update_visibility();
    
protected:
    /* Задать заголовок для меню. Если не задан - не занимает места. */
    void setTitle(std::string const &title_);
    /* Задать подзаголовок для меню. Может быть многострочным.
     * Если не задан - не занимает места. */
    void setSubtitle(std::string const &subtitle_);
    /* Добавить элемент, созданный с помощью оператора `new`.
     * Владение переходит к объекту меню. */
    virtual BaseMenuElement *addElement(BaseMenuElement *new_element_);
    
    // ---
    
    MenuButton *addButton(std::string const &text_)
    {   return (MenuButton *)addElement(new MenuButton{text_});   }
    
    MenuVariable *addVariable(std::string const &text_,
                              std::string const &value_="")
    {   return (MenuVariable *)addElement(new MenuVariable{text_, value_});   }
    
    MenuInput *addInput(std::string const &text_,
                        std::string const &value_="")
    {   return (MenuInput *)addElement(new MenuInput{text_, value_});   }
    
    MenuSwitcher *addSwitcher(std::string const &text_,
                              std::vector<std::string> &&values_)
    {   return (MenuSwitcher *)addElement(new MenuSwitcher{text_, std::move(values_)});   }
    
    MenuSlider *addSlider(std::string const &text_, float key_change_step_=0.1f,
                          float value_min_=.0f, float value_max_=1.0f)
    {   return (MenuSlider *)addElement(new MenuSlider{text_, key_change_step_, value_min_, value_max_});   }
    
    // ---
    
    /* Реакция на действие "назад". По умолчанию - завершение активности. */
    virtual void menuBack();
    
public:
    ~BaseMenuActivity();
    
    void onStart(BaseActivity *back_from_) override;
    void onLoop() override;
    void onStop() override;
};


} // namespace LibUtils

#endif // __LIBUTILS_MENU_HPP

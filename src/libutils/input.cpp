#include "input.hpp"
#include "application.hpp"
#include "MetadataManager.hpp"
#include "utils/log.hpp"
#include <vector>
#include <string>
#include <chrono>
#include <ctype.h>

using namespace std::chrono;

namespace LibUtils
{


static OIS::InputManager *__manager = nullptr;
static OIS::Keyboard *__keyboard = nullptr;
static OIS::Mouse *__mouse = nullptr;
static std::vector<OIS::JoyStick *> __joysticks{};

static BaseOISAction *__actions_list_first = nullptr;
static BaseOISAction *__actions_list_last = nullptr;
static std::vector<std::string> __joysticks_safe_names{};

static steady_clock::time_point __previous_time;
static float __mouse_scale_period = 1.0f;
static float __mouse_scale;

static GlobalMetadata<float> __joystick_dead_zone{"joystick_dead_zone"};

// --- *** ---


bool initInput(Ogre::RenderWindow *window_)
{
    try
    {
        size_t hwnd = 0;
        window_->getCustomAttribute("WINDOW", &hwnd);
        
        __manager = OIS::InputManager::createInputSystem(hwnd);
        __keyboard = static_cast<OIS::Keyboard *>(
            __manager->createInputObject(OIS::OISKeyboard, false)
        );
        __mouse = static_cast<OIS::Mouse *>(
            __manager->createInputObject(OIS::OISMouse, false)
        );
        
        int joysticks_count = __manager->getNumberOfDevices(OIS::OISJoyStick);
        __joysticks.clear();
        __joysticks.reserve((size_t)joysticks_count);
        __joysticks_safe_names.clear();
        __joysticks_safe_names.reserve((size_t)joysticks_count);
        
        for(int i = 0; i < joysticks_count; ++i)
        {
            OIS::JoyStick *joystick = static_cast<OIS::JoyStick *>(
                __manager->createInputObject(OIS::OISJoyStick, false)
            );
            __joysticks.push_back(joystick);
            
            std::string const &vendor = joystick->vendor();
            LOG("INFO") << "Joystick found: " << vendor;
            
            std::string safe_name; safe_name.reserve(vendor.size());
            for(char ch : vendor) safe_name.push_back(isalnum(ch) ? ch : '_');
            
            LOG("DEBUG") << "Joystick safe name: " << safe_name;
            __joysticks_safe_names.push_back(std::move(safe_name));
        }
        
        resizeInput(window_);
        __previous_time = steady_clock::now();
        
        // make sure joysticks vendors in properties
        minson::array input = properties.get("input");
        minson::array default_joystick = input.get("DefaultJoystick");
        
        for(std::string const &safe_name : __joysticks_safe_names)
        {
            minson::array joy{safe_name}; joy.update(default_joystick);
            if(input.has(safe_name)) joy.update(input.get(safe_name));
            input.put(std::move(joy));
        }
        
        BaseOISAction *action = __actions_list_first;
        for(; action != nullptr; action = action->__next) action->_create_slots();
        
        loadInputKeys();
        
        LOG("INFO") << "Input system initialized.";
        return true;
    }
    catch(OIS::Exception const &)
    {
        LOG("ERROR") << "Input system not initialized!";
        closeInput();
        return false;
    }
}


void closeInput()
{
    if(__manager != nullptr)
    {
        if(__keyboard != nullptr) __manager->destroyInputObject(__keyboard);
        if(__mouse != nullptr) __manager->destroyInputObject(__mouse);
        for(OIS::JoyStick *joystick : __joysticks) __manager->destroyInputObject(joystick);
        
        OIS::InputManager::destroyInputSystem(__manager);
        __manager = nullptr;
    }
    
    __keyboard = nullptr;
    __mouse = nullptr;
    __joysticks.clear();
    __joysticks_safe_names.clear();
    return;
}


void resizeInput(Ogre::RenderWindow *window_)
{
    unsigned int win_width, win_height, win_depth;
    int win_left, win_top;
    window_->getMetrics(win_width, win_height, win_depth, win_left, win_top);
    
    OIS::MouseState const &state = __mouse->getMouseState();
    state.width = win_width;
    state.height = win_height;
    
    __mouse_scale_period = (float)steady_clock::duration::period::den /
        ((float)steady_clock::duration::period::num * (float)win_width);
    return;
}

void updateInput()
{
    steady_clock::time_point now = steady_clock::now();
    steady_clock::duration time = now - __previous_time;
    __previous_time = now;
    
    __mouse_scale = (time.count() > 0)?(__mouse_scale_period / time.count()): 1.0f;
    
    if(__keyboard != nullptr) __keyboard->capture();
    if(__mouse != nullptr) __mouse->capture();
    for(OIS::JoyStick *joystick : __joysticks) joystick->capture();
    
    BaseOISAction *action = __actions_list_first;
    for(; action != nullptr; action = action->__next) action->update();
    return;
}


void loadInputKeys()
{
    BaseOISAction *action = __actions_list_first;
    for(; action != nullptr; action = action->__next) action->_load_keys();
    return;
}

void dumpInputKeys()
{
    BaseOISAction *action = __actions_list_first;
    for(; action != nullptr; action = action->__next) action->_dump_keys();
    return;
}

// --- *** ---


BaseOISAction *BaseOISAction::_first() noexcept
{   return __actions_list_first;   }

BaseOISAction *BaseOISAction::_next(BaseOISAction *p_) noexcept
{   return (p_ != nullptr)? p_->__next : nullptr;   }

std::string const &BaseOISAction::_safe_name(int i_) noexcept
{   return __joysticks_safe_names[i_];   }


BaseOISAction::BaseOISAction() noexcept :
    BaseInputAction(), __next(nullptr)
{
    if(__actions_list_first == nullptr) __actions_list_first = this;
    if(__actions_list_last != nullptr) __actions_list_last->__next = this;
    __actions_list_last = this;
    return;
}

BaseOISAction::~BaseOISAction() noexcept
{
    if(__actions_list_first == this) __actions_list_first = nullptr;
    if(__actions_list_last == this) __actions_list_last = nullptr;
    return;
}

void BaseOISAction::_create_slots()
{
    addSlot(KEYBOARD_MOUSE_SLOT_ID);
    
    int count = joysticksCount();
    for(int i = 0; i < count; ++i) addSlot(joystickSlotID(i));
    return;
}


BaseInputAction::t_key_num BaseOISAction::getKeysCount(t_slot_id slot_id_) const
{
    if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
    {   return (t_key_num)(KEYBOARD_KEYS_COUNT + MOUSE_KEYS_COUNT);   }
    else
    {
        return (t_key_num)(
            DPAD_KEYS_COUNT + ((OIS::JoyStick *)slot_id_)->getJoyStickState().mButtons.size()
        );
    }
}

BaseInputAction::t_key_num BaseOISAction::getAxesCount(t_slot_id slot_id_) const
{
    if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
        return (t_key_num)MOUSE_AXES_COUNT;
    else
        return (t_key_num)(((OIS::JoyStick *)slot_id_)->getJoyStickState().mAxes.size());
}


bool BaseOISAction::getKeyPushed(t_slot_id slot_id_, t_key_num key_) const
{
    if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
    {
        if(key_ < KEYBOARD_KEYS_COUNT)
            return __keyboard->isKeyDown((OIS::KeyCode)key_);
        else
            return __mouse->getMouseState().buttonDown((OIS::MouseButtonID)(key_ - MOUSE_KEYS_SHIFT));
    }
    else
    {
        OIS::JoyStickState const &state = ((OIS::JoyStick *)slot_id_)->getJoyStickState();
        if(key_ < DPAD_KEYS_COUNT)
        {
            int pov_i = (key_ & 0x0C) >> 2;
            switch(key_ & 0x03)
            {
            case 0: return((state.mPOV[pov_i].direction & OIS::Pov::North) != 0);
            case 1: return((state.mPOV[pov_i].direction & OIS::Pov::East) != 0);
            case 2: return((state.mPOV[pov_i].direction & OIS::Pov::South) != 0);
            case 3: return((state.mPOV[pov_i].direction & OIS::Pov::West) != 0);
            default: return false;
            }
        }
        else
        {   return state.mButtons[key_ - JOYSTICK_KEYS_SHIFT];   }
    }
}

float BaseOISAction::getAxisValue(t_slot_id slot_id_, t_key_num axis_) const
{
    if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
    {
        OIS::MouseState const &state = __mouse->getMouseState();
        switch(axis_)
        {
        case 0: return (float)state.X.rel * __mouse_scale;
        case 1: return (float)state.Y.rel * __mouse_scale;
        case 2: return (float)state.Z.rel;
        default: return .0f;
        }
    }
    else
    {
        OIS::JoyStickState const &state = ((OIS::JoyStick *)slot_id_)->getJoyStickState();
        float v = (float)(state.mAxes[axis_].abs * 2) / (float)(OIS::JoyStick::MAX_AXIS - OIS::JoyStick::MIN_AXIS);
        return (v <= -*__joystick_dead_zone || *__joystick_dead_zone <= v)? v : .0f;
    }
}

// --- *** ---


int mouseWindowX()
{   return __mouse->getMouseState().X.abs;   }

int mouseWindowY()
{   return __mouse->getMouseState().Y.abs;   }


int joysticksCount()
{   return (int)__joysticks.size();   }

BaseInputAction::t_slot_id joystickSlotID(int i_)
{   return (BaseInputAction::t_slot_id)__joysticks[i_];   }


bool isKeyboardActive()
{
    for(int i = 0; i < KEYBOARD_KEYS_COUNT; ++i)
        if(__keyboard->isKeyDown((OIS::KeyCode)i)) return true;
    return false;
}

bool isMouseActive()
{
    OIS::MouseState const &state = __mouse->getMouseState();
    
    if(state.X.rel != 0) return true;
    if(state.Y.rel != 0) return true;
    if(state.Z.rel != 0) return true;
    
    for(int i = 0; i < MOUSE_KEYS_COUNT; ++i)
        if(state.buttonDown((OIS::MouseButtonID)i)) return true;
    return false;
}

bool isJoystickActive(int i_)
{
    OIS::JoyStickState const &state = __joysticks[i_]->getJoyStickState();
    
    for(OIS::Axis a : state.mAxes)
        if(a.abs != 0) return true;
    
    for(bool b : state.mButtons)
        if(b) return true;
    
    for(int i = 0; i < 4; ++i)
        if(state.mPOV[i].direction != OIS::Pov::Centered) return true;
    return false;
}


OIS::Keyboard &getKeyboard()
{   return *__keyboard;   }

OIS::Mouse &getMouse()
{   return *__mouse;   }

OIS::JoyStick &getJoystick(int i_)
{   return *(__joysticks[i_]);   }


} // namespace LibUtils

#ifndef __LIBUTILS_VALUES_HPP
#define __LIBUTILS_VALUES_HPP

#include "MetadataManager.hpp"
#include <OgreColourValue.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgrePanelOverlayElement.h>
#include <OgreLight.h>
#include <string>


struct t_color : Ogre::ColourValue, serialize::serializable
{
    serializable_fields(serialize::serializable, r, g, b, a);
};


struct t_text_colors : serialize::serializable
{
    t_color top, bottom;
    serializable_fields(serialize::serializable, top, bottom);
    
    void applyTo(Ogre::TextAreaOverlayElement *element_) const
    {
        element_->setColourTop(top); element_->setColourBottom(bottom);
    }
};


template<int _N>
struct t_text_colors_array : serialize::serializable
{
    t_text_colors color[_N];
    
    void serialize(serialize::value_encoder &&encoder_) const
    {
        encoder_.as_sequence([this](serialize::sequence_encoder &&seq)
        {
            for(int i = 0; i < _N; ++i)
                seq.put_sub(i, [&](serialize::value_encoder &&sub){
                    serialize::serialize(std::move(sub), color[i]);
                });
        });
    }
    
    void deserialize(serialize::value_decoder &&decoder_)
    {
        decoder_.as_sequence([this](serialize::sequence_decoder &&seq)
        {
            std::size_t n = seq.size();
            for(int i = 0; i < _N && i < n; ++i)
                seq.get_sub(i, [&](serialize::value_decoder &&sub){
                    color[i] = serialize::deserialize<t_text_colors>(std::move(sub));
                });
        });
    }
};


class t_2d_units : serialize::serializable
{
private:
    union
    {
        int pixel_units;
        float width_units;
        float height_units;
    };
    enum
    {   T_NONE, T_PIXELS, T_WIDTHS, T_HEIGHTS   }
    type;
    
public:
    t_2d_units() : type(T_NONE) {}
    
    t_2d_units(t_2d_units const &) = default;
    t_2d_units(t_2d_units &&) = default;
    t_2d_units &operator =(t_2d_units const &) = default;
    t_2d_units &operator =(t_2d_units &&) = default;
    
    static t_2d_units px(int t_)
    {
        t_2d_units tmp{}; tmp.type = T_PIXELS; tmp.pixel_units = t_;
        return tmp;
    }
    static t_2d_units w(float t_)
    {
        t_2d_units tmp{}; tmp.type = T_WIDTHS; tmp.width_units = t_;
        return tmp;
    }
    static t_2d_units h(float t_)
    {
        t_2d_units tmp{}; tmp.type = T_HEIGHTS; tmp.height_units = t_;
        return tmp;
    }
    
    int toPixels(int origin_width_, int origin_height_) const
    {
        switch(type)
        {
        case T_NONE:    return 0;
        case T_PIXELS:  return pixel_units;
        case T_WIDTHS:  return (int)(origin_width_ * width_units + 0.5f);
        case T_HEIGHTS: return (int)(origin_height_ * height_units + 0.5f);
        }
    }
    
    void serialize(serialize::value_encoder &&encoder_) const
    {
        switch(type)
        {
        case T_NONE:
            encoder_.put_string(""); return;
        case T_PIXELS:
            encoder_.put_string(std::to_string(pixel_units) + "px"); return;
        case T_WIDTHS:
            encoder_.put_string(std::to_string(width_units) + "w"); return;
        case T_HEIGHTS:
            encoder_.put_string(std::to_string(height_units) + "h"); return;
        }
    }
    
    void deserialize(serialize::value_decoder &&decoder_)
    {
        memory::string_view s = decoder_.get_string();
        if(s.size() < 2) {   type = T_NONE; return;   }
        
        try
        {
            char const *ch = s.data() + s.size();
            switch(*(--ch))
            {
            case 'w':
                type = T_WIDTHS; width_units = std::stof(s); return;
            case 'h':
                type = T_HEIGHTS; height_units = std::stof(s); return;
            case 'x': if(s.size() >= 3 && *(--ch) == 'p')
            {
                type = T_PIXELS; pixel_units = std::stoi(s); return;
            }
            default: type = T_NONE; return;
            }
        }
        catch(...) {   type = T_NONE; return;   }
    }
};


struct t_texture_coords : serialize::serializable
{
    Ogre::Real u1, v1, u2, v2;
    serializable_fields(serialize::serializable, u1, v1, u2, v2);
    
    void applyTo(Ogre::PanelOverlayElement *panel_) const
    {
        panel_->setUV(u1, v1, u2, v2);
    }
};


struct t_attenuation : serialize::serializable
{
    Ogre::Real range, c0, c1, c2;
    serializable_fields(serialize::serializable, range, c0, c1, c2);
    
    void applyTo(Ogre::Light *light_) const
    {
        light_->setAttenuation(range, c0, c1, c2);
    }
};


#endif // __LIBUTILS_VALUES_HPP

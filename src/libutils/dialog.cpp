#include "dialog.hpp"
#include "values.hpp"
#include "MultiTextOverlayElement.hpp"
#include <OgreOverlay.h>
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>

namespace LibUtils
{


static GlobalMetadata<t_text_colors>
    __message_color{"dialog_message_color"},
    __question_color{"dialog_question_color"};

static GlobalMetadata<t_2d_units>
    __height_padding{"dialog_height_padding"},
    __question_margin{"dialog_question_margin"},
    __text_size{"dialog_text_size"};

static Ogre::Overlay *__layout = nullptr;
static Ogre::PanelOverlayElement *__background = nullptr;
static MultiTextOverlayElement *__message_elem = nullptr;
static MultiTextOverlayElement *__question_elem = nullptr;

static BaseDialog *__active_dialog = nullptr;


void __update_sizes()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    int viewport_width = overlays.getViewportWidth();
    int viewport_height = overlays.getViewportHeight();
    
    int message_lines = 1;
    for(auto *p = __message_elem->getCaption().c_str(); *p != '\0'; ++p)
    {
        if(*p == '\n') ++message_lines;
    }
    
    int question_lines = __question_elem->getCaption().empty() ? 0 : 1;
    for(auto *p = __question_elem->getCaption().c_str(); *p != '\0'; ++p)
    {
        if(*p == '\n') ++question_lines;
    }
    
    int height_padding =  __height_padding-> toPixels(viewport_width, viewport_height);
    int question_margin = __question_margin->toPixels(viewport_width, viewport_height);
    int text_size =       __text_size->      toPixels(viewport_width, viewport_height);
    if(__question_elem->getCaption().empty()) question_margin = 0;
    
    int message_height = text_size * message_lines;
    int question_height = text_size * question_lines;
    int full_height = height_padding * 2 + message_height + question_margin + question_height;
    
    __background->setPosition(0, (viewport_height - full_height) / 2);
    __background->setDimensions(viewport_width, full_height);
    
    __message_elem->setPosition(viewport_width / 2, height_padding); // relative to background
    __message_elem->setDimensions(viewport_width, message_height);
    __message_elem->setCharHeight(text_size);
    
    __question_elem->setPosition(viewport_width / 2, height_padding + message_height + question_margin); // relative to background
    __question_elem->setDimensions(viewport_width, question_height);
    __question_elem->setCharHeight(text_size);
    return;
}

// --- *** ---


void BaseDialog::setQuestion(lines_buffer_content &&question_)
{
    __question = std::move(question_);
    if(__active_dialog == this)
    {
        __question_elem->setContent(lines_buffer_content{__question}); // copy them
        __update_sizes();
    }
    return;
}

// --- *** ---


void BaseDialog::show()
{
    if(__active_dialog != nullptr) __active_dialog->hide();
    __active_dialog = this;
    
    if(__layout == nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        __layout = overlays.create("Dialog");
        
        __background = static_cast<Ogre::PanelOverlayElement *>(
            overlays.createOverlayElement("Panel", "dialog_background_panel")
        );
        __background->setMetricsMode(Ogre::GMM_PIXELS);
        __background->setMaterialName("Menu/dialog_background");
        __background->show();
        
        __message_elem = static_cast<MultiTextOverlayElement *>(
            overlays.createOverlayElement("MultiText", "dialog_message")
        );
        __message_elem->setMetricsMode(Ogre::GMM_PIXELS);
        __message_elem->setAlignment(Ogre::TextAreaOverlayElement::Center);
        __message_elem->setFontName("Menu/text");
        __message_elem->getMaterial(); // КОСТЫЛЬ
        __message_color->applyTo(__message_elem);
        __message_elem->show();
        
        __question_elem = static_cast<MultiTextOverlayElement *>(
            overlays.createOverlayElement("MultiText", "dialog_question")
        );
        __question_elem->setMetricsMode(Ogre::GMM_PIXELS);
        __question_elem->setAlignment(Ogre::TextAreaOverlayElement::Center);
        __question_elem->setFontName("Menu/text");
        __question_elem->getMaterial(); // КОСТЫЛЬ
        __question_color->applyTo(__question_elem);
        __question_elem->show();
        
        __background->addChild(__message_elem);
        __background->addChild(__question_elem);
        __layout->add2D(__background);
        __layout->setZOrder(640);
        __layout->show();
    }
    
    __message_elem->setContent(lines_buffer_content{__message}); // copy them
    __question_elem->setContent(lines_buffer_content{__question});
    __update_sizes();
    return;
}


void BaseDialog::hide()
{
    if(__active_dialog != this) return;
    __active_dialog = nullptr;
    
    if(__layout != nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        overlays.destroyOverlayElement(__message_elem); __message_elem = nullptr;
        overlays.destroyOverlayElement(__question_elem); __question_elem = nullptr;
        overlays.destroyOverlayElement(__background); __background = nullptr;
        overlays.destroy(__layout); __layout = nullptr;
    }
    return;
}


bool BaseDialog::isActive()
{   return(__active_dialog == this);   }

// --- *** ---


bool BaseDialog::updateActive()
{
    if(__active_dialog == nullptr) return false;
    __active_dialog->update();
    return true;
}


} //- namespace LibUtils

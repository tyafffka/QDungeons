#include "menu_elements.hpp"
#include "menu.hpp"
#include "values.hpp"
#include "MultiTextOverlayElement.hpp"
#include "TextInputOverlayElement.hpp"
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreOggISound.h>


namespace MenuValues
{

extern LibUtils::GlobalMetadata<t_text_colors>
    title_color, subtitle_color, text_color, disabled_color;

extern int offset_x, offset_y, width, height,
    element_size, text_size, subtitle_text_size,
    width_padding, height_padding, inner_width, scroll_offset_x;

extern OgreOggSound::OgreOggISound *select_sound;

void play(OgreOggSound::OgreOggISound *sound_);

} // namespace MenuValues

// --- *** ---

namespace LibUtils
{


static LibUtils::GlobalMetadata<t_texture_coords>
    __highlight_tex{"menu_highlight_tex"},
    __switcher_left_tex{"menu_switcher_left_tex"},
    __switcher_right_tex{"menu_switcher_right_tex"},
    __switcher_disabled_left_tex{"menu_switcher_disabled_left_tex"},
    __switcher_disabled_right_tex{"menu_switcher_disabled_right_tex"},
    __slider_tex{"menu_slider_tex"},
    __slider_bg_tex{"menu_slider_bg_tex"},
    __slider_disabled_tex{"menu_slider_disabled_tex"},
    __slider_disabled_bg_tex{"menu_slider_disabled_bg_tex"};

static int __elements_counter = 0;


void BaseMenuElement::_set_focus()
{
    auto *layout = static_cast<Ogre::PanelOverlayElement *>(_layout);
    layout->setMaterialName("Menu/menu");
    __highlight_tex->applyTo(layout);
    return;
}

void BaseMenuElement::_kill_focus()
{
    _layout->setMaterialName("Menu/transparent");
    return;
}

BaseMenuElement::BaseMenuElement(std::string const &text_) : _is_enabled(true)
{
    __elements_counter = (__elements_counter + 1) & 0xFFFF;
    std::string layout_name = "element" + std::to_string(__elements_counter);
    std::string label_name = "label" + std::to_string(__elements_counter);
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *layout = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", layout_name)
    );
    layout->setMetricsMode(Ogre::GMM_PIXELS);
    layout->setPosition(.0f, .0f);
    layout->setDimensions(MenuValues::width, MenuValues::element_size);
    layout->setMaterialName("Menu/transparent");
    layout->hide();
    
    auto *label = static_cast<MultiTextOverlayElement *>(
        overlays.createOverlayElement("MultiText", label_name)
    );
    label->setMetricsMode(Ogre::GMM_PIXELS);
    label->setPosition(MenuValues::width_padding, MenuValues::height_padding);
    label->setDimensions(MenuValues::width, MenuValues::element_size);
    label->setCharHeight(MenuValues::text_size);
    label->setAlignment(Ogre::TextAreaOverlayElement::Left);
    label->setFontName("Menu/text");
    label->getMaterial(); // КОСТЫЛЬ
    MenuValues::text_color->applyTo(label);
    label->setFormat(text_);
    label->show();
    
    (_layout = layout)->addChild(_label = label);
    return;
}

BaseMenuElement::~BaseMenuElement()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    overlays.destroyOverlayElement(_label);
    overlays.destroyOverlayElement(_layout);
    return;
}

void BaseMenuElement::setText(lines_buffer_content &&content_)
{
    static_cast<MultiTextOverlayElement *>(_label)->setContent(std::move(content_));
    return;
}

void BaseMenuElement::setEnabled(bool is_enabled_)
{
    t_text_colors &text_color =
        (_is_enabled = is_enabled_)? *MenuValues::text_color : *MenuValues::disabled_color;
    text_color.applyTo(static_cast<MultiTextOverlayElement *>(_label));
    return;
}

// --- *** ---


void MenuButton::_select()
{
    if(! _is_enabled) return;
    MenuValues::play(MenuValues::select_sound); onSelected(this);
    return;
}

void MenuButton::_mouse_click(float x_)
{
    if(_is_enabled) _select();
    return;
}

// --- *** ---


MenuVariable::MenuVariable(std::string const &text_, std::string const &value_) :
    MenuButton(text_)
{
    std::string value_name = "value" + std::to_string(__elements_counter);
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *value = static_cast<MultiTextOverlayElement *>(
        overlays.createOverlayElement("MultiText", value_name)
    );
    value->setMetricsMode(Ogre::GMM_PIXELS);
    value->setPosition(MenuValues::width - MenuValues::width_padding,
                       MenuValues::height_padding);
    value->setDimensions(MenuValues::width, MenuValues::element_size);
    value->setCharHeight(MenuValues::text_size);
    value->setAlignment(Ogre::TextAreaOverlayElement::Right);
    value->setFontName("Menu/text");
    value->getMaterial(); // КОСТЫЛЬ
    MenuValues::text_color->applyTo(value);
    value->setFormat(value_);
    value->show();
    
    _layout->addChild(_value = value);
    return;
}

MenuVariable::~MenuVariable()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    overlays.destroyOverlayElement(_value);
    return;
}

void MenuVariable::setEnabled(bool is_enabled_)
{
    BaseMenuElement::setEnabled(is_enabled_);
    
    t_text_colors &text_color =
        is_enabled_ ? *MenuValues::text_color : *MenuValues::disabled_color;
    text_color.applyTo(static_cast<MultiTextOverlayElement *>(_value));
    return;
}

void MenuVariable::setValue(lines_buffer_content &&content_)
{
    static_cast<MultiTextOverlayElement *>(_value)->setContent(std::move(content_));
    return;
}

// --- *** ---


void MenuInput::_set_focus()
{
    BaseMenuElement::_set_focus();
    if(_is_enabled) static_cast<TextInputOverlayElement *>(_input)->setFocus();
    return;
}

void MenuInput::_loop()
{
    if(! _is_enabled) return;
    
    auto *input = static_cast<TextInputOverlayElement *>(_input);
    if(input->isCaptionChanged)
    {
        input->isCaptionChanged = false;
        MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    }
    return;
}

void MenuInput::_kill_focus()
{
    BaseMenuElement::_kill_focus();
    if(_is_enabled) static_cast<TextInputOverlayElement *>(_input)->killFocus();
    return;
}

void MenuInput::_select()
{
    if(! _is_enabled) return;
    MenuValues::play(MenuValues::select_sound); onSelected(this);
    return;
}

MenuInput::MenuInput(std::string const &text_, std::string const &value_) :
    BaseMenuElement(text_)
{
    std::string input_name = "input" + std::to_string(__elements_counter);
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *input = static_cast<TextInputOverlayElement *>(
        overlays.createOverlayElement("TextInput", input_name)
    );
    input->setMetricsMode(Ogre::GMM_PIXELS);
    input->setPosition(MenuValues::width - MenuValues::width_padding,
                       MenuValues::height_padding);
    input->setDimensions(MenuValues::width, MenuValues::element_size);
    input->setCharHeight(MenuValues::text_size);
    input->setAlignment(Ogre::TextAreaOverlayElement::Right);
    input->setFontName("Menu/text");
    input->getMaterial(); // КОСТЫЛЬ
    MenuValues::text_color->applyTo(input);
    input->setCaption(value_);
    input->show();
    
    _layout->addChild(_input = input);
    return;
}

MenuInput::~MenuInput()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    overlays.destroyOverlayElement(_input);
    return;
}

void MenuInput::setEnabled(bool is_enabled_)
{
    BaseMenuElement::setEnabled(is_enabled_);
    
    t_text_colors &text_color =
        is_enabled_ ? *MenuValues::text_color : *MenuValues::disabled_color;
    auto *input = static_cast<TextInputOverlayElement *>(_input);
    
    text_color.applyTo(input);
    if(! is_enabled_) input->killFocus();
    return;
}

void MenuInput::setValue(std::string const &value_)
{   _input->setCaption(value_); return;   }

std::string const &MenuInput::getValue()
{   return _input->getCaption().asUTF8();   }

// --- *** ---


void MenuSwitcher::_update()
{
    if(_current_item < values.size())
        static_cast<MultiTextOverlayElement *>(_value)->setFormat(values[_current_item]);
    else
        _value->setCaption("");
    return;
}

void MenuSwitcher::_select()
{
    if(! _is_enabled || values.size() <= 1) return;
    
    if(++_current_item >= values.size()) _current_item = 0;
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

void MenuSwitcher::_decrease()
{
    if(! _is_enabled || _current_item <= 0) return;
    
    --_current_item;
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

void MenuSwitcher::_increase()
{
    if(! _is_enabled || _current_item >= values.size() - 1) return;
    
    ++_current_item;
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

void MenuSwitcher::_mouse_click(float x_)
{
    if(! _is_enabled || x_ < 0.45f) return;
    (x_ < 0.75f)? _decrease() : _increase();
    return;
}

MenuSwitcher::MenuSwitcher(std::string const &text_, t_values &&values_) :
    BaseMenuElement(text_), values(std::move(values_))
{
    std::string dec_name = "dec" + std::to_string(__elements_counter);
    std::string inc_name = "inc" + std::to_string(__elements_counter);
    std::string value_name = "value" + std::to_string(__elements_counter);
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *dec = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", dec_name)
    );
    dec->setMetricsMode(Ogre::GMM_PIXELS);
    dec->setPosition(MenuValues::width * 0.5f, 0);
    dec->setDimensions(MenuValues::element_size, MenuValues::element_size);
    dec->setMaterialName("Menu/menu");
    __switcher_left_tex->applyTo(dec);
    dec->show();
    
    auto *inc = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", inc_name)
    );
    inc->setMetricsMode(Ogre::GMM_PIXELS);
    inc->setPosition(MenuValues::width - MenuValues::width_padding - MenuValues::element_size, 0);
    inc->setDimensions(MenuValues::element_size, MenuValues::element_size);
    inc->setMaterialName("Menu/menu");
    __switcher_right_tex->applyTo(inc);
    inc->show();
    
    auto *value = static_cast<MultiTextOverlayElement *>(
        overlays.createOverlayElement("MultiText", value_name)
    );
    value->setMetricsMode(Ogre::GMM_PIXELS);
    value->setPosition(MenuValues::width * 0.5f + MenuValues::inner_width * 0.25f,
                       MenuValues::height_padding);
    value->setDimensions(MenuValues::inner_width * 0.5f, MenuValues::element_size);
    value->setCharHeight(MenuValues::text_size);
    value->setAlignment(Ogre::TextAreaOverlayElement::Center);
    value->setFontName("Menu/text");
    value->getMaterial(); // КОСТЫЛЬ
    MenuValues::text_color->applyTo(value);
    value->show();
    
    _layout->addChild(_dec = dec);
    _layout->addChild(_inc = inc);
    _layout->addChild(_value = value);
    _update();
    return;
}

MenuSwitcher::~MenuSwitcher()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    overlays.destroyOverlayElement(_dec);
    overlays.destroyOverlayElement(_inc);
    overlays.destroyOverlayElement(_value);
    return;
}

void MenuSwitcher::setEnabled(bool is_enabled_)
{
    BaseMenuElement::setEnabled(is_enabled_);
    
    if(is_enabled_)
    {
        __switcher_left_tex->applyTo(static_cast<Ogre::PanelOverlayElement *>(_dec));
        __switcher_right_tex->applyTo(static_cast<Ogre::PanelOverlayElement *>(_inc));
        MenuValues::text_color->applyTo(static_cast<MultiTextOverlayElement *>(_value));
    }
    else
    {
        __switcher_disabled_left_tex->applyTo(static_cast<Ogre::PanelOverlayElement *>(_dec));
        __switcher_disabled_right_tex->applyTo(static_cast<Ogre::PanelOverlayElement *>(_inc));
        MenuValues::disabled_color->applyTo(static_cast<MultiTextOverlayElement *>(_value));
    }
    return;
}

void MenuSwitcher::setCurrentItem(int i_)
{
    if(i_ < 0 || values.size() <= i_) return;
    _current_item = i_; _update();
    return;
}

// --- *** ---


void MenuSlider::_update()
{
    float precent_px = MenuValues::inner_width * 0.5f * _percent;
    float rest_px = MenuValues::inner_width * 0.5f * (1.0f - _percent);
    
    t_texture_coords &tex = _is_enabled ? *__slider_tex : *__slider_disabled_tex;
    t_texture_coords &bg = _is_enabled ? *__slider_bg_tex : *__slider_disabled_bg_tex;
    
    _filled->setWidth(precent_px);
    static_cast<Ogre::PanelOverlayElement *>(_filled)->setUV(
        tex.u1, tex.v1,  tex.u1 + _percent * (tex.u2 - tex.u1), tex.v2
    );
    
    _empty->setLeft(MenuValues::width * 0.5f + precent_px);
    _empty->setWidth(rest_px);
    static_cast<Ogre::PanelOverlayElement *>(_empty)->setUV(
        bg.u1 + _percent * (bg.u2 - bg.u1), bg.v1, bg.u2, bg.v2
    );
    return;
}

void MenuSlider::_decrease()
{
    if(! _is_enabled) return;
    
    if((_percent -= _step) < .0f) _percent = .0f;
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

void MenuSlider::_increase()
{
    if(! _is_enabled) return;
    
    if((_percent += _step) > 1.0f) _percent = 1.0f;
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

void MenuSlider::_mouse_click(float x_)
{
    if(! _is_enabled || x_ < 0.45f) return;
    
    _percent = (x_ - 0.5f) * 2.0f;
    if(_percent > 1.0f) _percent = 1.0f;
    else if(_percent < .0f) _percent = .0f;
    
    _update(); MenuValues::play(MenuValues::select_sound); onValueChanged(this);
    return;
}

MenuSlider::MenuSlider(std::string const &text_, float key_change_step_,
                       float value_min_, float value_max_) :
    BaseMenuElement(text_), _min(value_min_), _max(value_max_)
{
    _step = key_change_step_ / (value_max_ - value_min_);
    
    std::string filled_name = "filled" + std::to_string(__elements_counter);
    std::string empty_name = "empty" + std::to_string(__elements_counter);
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    
    auto *filled = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", filled_name)
    );
    filled->setMetricsMode(Ogre::GMM_PIXELS);
    filled->setPosition(MenuValues::width * 0.5f, .0f);
    filled->setDimensions(MenuValues::inner_width * 0.5f, MenuValues::element_size);
    filled->setMaterialName("Menu/menu");
    filled->show();
    
    auto *empty = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", empty_name)
    );
    empty->setMetricsMode(Ogre::GMM_PIXELS);
    empty->setPosition(MenuValues::width * 0.5f, .0f);
    empty->setDimensions(MenuValues::inner_width * 0.5f, MenuValues::element_size);
    empty->setMaterialName("Menu/menu");
    empty->show();
    
    _layout->addChild(_filled = filled);
    _layout->addChild(_empty = empty);
    _update();
    return;
}

MenuSlider::~MenuSlider()
{
    auto &overlays = Ogre::OverlayManager::getSingleton();
    overlays.destroyOverlayElement(_filled);
    overlays.destroyOverlayElement(_empty);
    return;
}

void MenuSlider::setEnabled(bool is_enabled_)
{
    BaseMenuElement::setEnabled(is_enabled_); _update();
    return;
}

void MenuSlider::setValue(float value_)
{
    _percent = (value_ - _min) / (_max - _min);
    if(_percent > 1.0f) _percent = 1.0f;
    else if(_percent < .0f) _percent = .0f;
    
    _update();
    return;
}


} // namespace LibUtils

#include "MetadataManager.hpp"
#include "utils/memory/on_final.hpp"
#include "utils/log.hpp"


namespace Ogre
{
template<> LibUtils::MetadataManager *Singleton<LibUtils::MetadataManager>::msSingleton = nullptr;
}

using namespace Ogre;

// --- *** ---

namespace LibUtils
{


static __base_global_metadata *__globals_list_first = nullptr;


__base_global_metadata::__base_global_metadata(char const *name_) noexcept :
    __next(__globals_list_first), __name(name_)
{
    __globals_list_first = this; return;
}


__base_global_metadata::~__base_global_metadata() noexcept
{
    if(__globals_list_first == this) __globals_list_first = nullptr;
    return;
}

// --- *** ---


class _MetadataResource : public Resource
{
private:
    size_t __source_size = 0;
    
protected:
    minson::object _metadata;
    
    void loadImpl() override
    {
        DataStreamPtr stream = ResourceGroupManager::getSingleton().openResource(mName, mGroup, true, this);
        __source_size = stream->size();
        _metadata = minson::parse(stream->getAsString());
        stream->close();
        return;
    }
    
    void unloadImpl() override
    {
        __source_size = 0; _metadata = minson::object::null();
        return;
    }
    
    size_t calculateSize() const override
    {   return __source_size;   } // не заморачиваемся и просто отдаём размер исходного текста, всё равно они долго не живут.
    
public:
    _MetadataResource(ResourceManager *creator_, String const &name_,
                      ResourceHandle handle_, String const &group_,
                      bool is_manual_=false, ManualResourceLoader *loader_=nullptr) :
        Resource(creator_, name_, handle_, group_, is_manual_, loader_),
        _metadata(minson::object::null())
    {}
    
    minson::object &&moveData() {   return std::move(_metadata);   }
};

// --- *** ---


Resource *MetadataManager::createImpl(String const &name_, ResourceHandle handle_,
                                      String const &group_, bool is_manual_,
                                      ManualResourceLoader *loader_,
                                      NameValuePairList const *create_params_)
{
    return new _MetadataResource{this, name_, handle_, group_, is_manual_, loader_};
}


MetadataManager::MetadataManager(memory::allocator *alloc_) :
    MetatypeManager(alloc_)
{
    mResourceType = "Metadata";
    mLoadOrder = 31.0f;
    ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
    return;
}


MetadataManager::~MetadataManager()
{
    ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
    return;
}

// --- *** ---


minson::object MetadataManager::load(String const &file_, String const &group_)
{
    ResourcePtr p = getResourceByName(file_, group_);
    if(p == nullptr)
    {
        p = createResource(file_,
            (group_ == ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME)?
            ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME : group_
        );
    }
    
    p->load();
    return static_cast<_MetadataResource *>(p.get())->moveData();
}


void MetadataManager::loadTypes(String const &file_, String const &group_)
{
    on_final {   unload(file_);   };
    
    std::string type_name;
    try
    {
        minson::array data = load(file_, group_);
        for(minson::array metadata : data)
        {
            type_name = metadata.name();
            createMetatype(type_name, serialize::minson_decoder(metadata));
        }
    }
    catch(...)
    {
        LOG("WARNIG") << "MetadataManager::loadTypes(): error during load file '"
                      << file_ << "'! current type: " << type_name;
        throw;
    }
    return;
}


void MetadataManager::loadGlobals(String const &file_, String const &group_)
{
    on_final {   unload(file_);   };
    
    char const *meta_name = nullptr;
    try
    {
        minson::array data = load(file_, group_);
        serialize::minson_decoder(data).as_map([&](serialize::map_decoder &&decoder)
        {
            for(__base_global_metadata *it = __globals_list_first, *itpre = nullptr;
                it != nullptr; it = it->__next)
            {
                meta_name = it->__name;
                if(data.has(meta_name))
                {
                    it->__get(std::move(decoder));
                    if(itpre != nullptr) itpre->__next = it->__next;
                    else __globals_list_first = it->__next;
                }
                else
                {   itpre = it;   }
            }
        });
    }
    catch(...)
    {
        LOG("WARNIG") << "MetadataManager::loadGlobals(): error during load file '"
                      << file_ << "'! current meta: " << meta_name;
        throw;
    }
    return;
}

// --- *** ---


MetadataManager &MetadataManager::getSingleton()
{
    assert(msSingleton != nullptr); return(*msSingleton);
}


MetadataManager *MetadataManager::getSingletonPtr()
{   return msSingleton;   }


} // namespace LibUtils

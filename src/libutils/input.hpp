#ifndef __LIBUTILS_INPUT_HPP
#define __LIBUTILS_INPUT_HPP

#include "utils/input_action.hpp"
#include <OgreRenderWindow.h>
#include <OIS.h>

#define INPUT_NO_KEY                 ::BaseInputAction::C_NO_KEY, 0
#define INPUT_KEYBOARD_KEY(ois_code) ::BaseInputAction::C_KEY, (::BaseInputAction::t_key_num)(ois_code)
#define INPUT_MOUSE_KEY(ois_code)    ::BaseInputAction::C_KEY, (::BaseInputAction::t_key_num)(::LibUtils::MOUSE_KEYS_SHIFT + (int)(ois_code))
#define INPUT_JOYSTICK_DPAD(pad_n, input_code) ::BaseInputAction::C_KEY, (::BaseInputAction::t_key_num)((int)(pad_n) * 4 + (int)(input_code))
#define INPUT_JOYSTICK_DPAD_UP       INPUT_JOYSTICK_DPAD(0, ::LibUtils::DPAD_UP)
#define INPUT_JOYSTICK_DPAD_RIGHT    INPUT_JOYSTICK_DPAD(0, ::LibUtils::DPAD_RIGHT)
#define INPUT_JOYSTICK_DPAD_DOWN     INPUT_JOYSTICK_DPAD(0, ::LibUtils::DPAD_DOWN)
#define INPUT_JOYSTICK_DPAD_LEFT     INPUT_JOYSTICK_DPAD(0, ::LibUtils::DPAD_LEFT)
#define INPUT_JOYSTICK_KEY(key)      ::BaseInputAction::C_KEY, (::BaseInputAction::t_key_num)(::LibUtils::JOYSTICK_KEYS_SHIFT + (int)(key))
#define INPUT_POS_AXIS(axis)         ::BaseInputAction::C_POSITIVE_SEMIAXIS, (::BaseInputAction::t_key_num)(axis)
#define INPUT_NEG_AXIS(axis)         ::BaseInputAction::C_NEGATIVE_SEMIAXIS, (::BaseInputAction::t_key_num)(axis)

namespace LibUtils
{


static constexpr int KEYBOARD_KEYS_COUNT = 256;
static constexpr int MOUSE_KEYS_SHIFT = KEYBOARD_KEYS_COUNT;
static constexpr int MOUSE_KEYS_COUNT = 8;
static constexpr int MOUSE_AXES_COUNT = 3;
static constexpr int DPAD_KEYS_COUNT = 16;
static constexpr int JOYSTICK_KEYS_SHIFT = DPAD_KEYS_COUNT;

static constexpr BaseInputAction::t_slot_id KEYBOARD_MOUSE_SLOT_ID = 0;

enum c_dpad_direction : uint8_t
{   DPAD_UP = 0, DPAD_RIGHT = 1, DPAD_DOWN = 2, DPAD_LEFT = 3   };


/* Инициализирует OIS, присоединяя его к указанному окну. */
bool initInput(Ogre::RenderWindow *window_);
/* Деинициализирует OIS. Важно: вызвать _ДО_ исчезновения окна! */
void closeInput();

/* Реагирует на изменения размеров окна (область перемещения курсора мыши). */
void resizeInput(Ogre::RenderWindow *window_);
/* Обновляет состояние устройств и действий. */
void updateInput();

/* Загружает настройки клавиш из настроек приложения. */
void loadInputKeys();
/* Сохраняет настройки клавиш в настройках приложения. */
void dumpInputKeys();


/* Базовый класс для всех типов действий, предоставляемых библиотекой.
 * Связывает интерфейс действий ввода и OIS.
 * Все экземпляры подклассов должны быть объявлены как глобальные.
 * Вызывать методы `addSlot()` и `removeSlot()` извне не рекомендуется! */
class BaseOISAction : public BaseInputAction
{
    friend bool initInput(Ogre::RenderWindow *);
    friend void updateInput();
    
    friend void loadInputKeys();
    friend void dumpInputKeys();
    
private:
    BaseOISAction *__next;
    
protected:
    static BaseOISAction *_first() noexcept;
    static BaseOISAction *_next(BaseOISAction *p_) noexcept;
    static std::string const &_safe_name(int i_) noexcept;
    
    BaseOISAction() noexcept;
    ~BaseOISAction() noexcept;
    
    void _create_slots();
    virtual void _load_keys() {}
    virtual void _dump_keys() const {}
    
public:
    t_key_num getKeysCount(t_slot_id slot_id_) const override;
    t_key_num getAxesCount(t_slot_id slot_id_) const override;
    
    bool getKeyPushed(t_slot_id slot_id_, t_key_num key_) const override;
    float getAxisValue(t_slot_id slot_id_, t_key_num axis_) const override;
};


/* Возвращают позицию курсора мыши в окне. */
int mouseWindowX();
int mouseWindowY();

/* Возвращает количество джойстиков. */
int joysticksCount();
/* Возвращает ID слота, занимаемого `i_`-ым джойстиком. */
BaseInputAction::t_slot_id joystickSlotID(int i_);

/* Проверяет, есть ли какой-либо ввод на клавиатуре. */
bool isKeyboardActive();
/* Проверяет, есть ли какой-либо ввод на мыши. */
bool isMouseActive();
/* Проверяет, есть ли какой-либо ввод на `i_`-ом джойстике. */
bool isJoystickActive(int i_);

/* Возвращает устройство клавиатуры. */
OIS::Keyboard &getKeyboard();
/* Возвращает устройство мыши. */
OIS::Mouse &getMouse();
/* Возвращает устройство `i_`-ого джойстика. */
OIS::JoyStick &getJoystick(int i_);


} // namespace LibUtils

#endif // __LIBUTILS_INPUT_HPP

#include "TextInputOverlayElement.hpp"
#include "input.hpp"
#include "values.hpp"
#include <OgreOverlay.h>
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreOverlayElementFactory.h>
#include <chrono>

using namespace std::chrono;
using namespace Ogre;

namespace LibUtils
{


static Ogre::String const __TypeName{"TextInput"};

static GlobalMetadata<t_texture_coords> __cursor_tex{"text_input_cursor_tex"};

static TextInputOverlayElement *__input_focus = nullptr;
static Overlay *__input_cursor_layout = nullptr;
static PanelOverlayElement *__input_cursor = nullptr;
static steady_clock::duration __blinking_timer;

// --- *** ---


void TextInputOverlayElement::__update_offsets()
{
    if(__dirty_offset_from == DisplayString::npos) return;
    
    if(mFont == nullptr)
    {   __x_offsets.clear(); return;   }
    
    size_t size = mCaption.size();
    __x_offsets.resize(size);
    if(__dirty_offset_from >= size) return;
    
    float x = (__dirty_offset_from > 0)?
              __x_offsets[__dirty_offset_from - 1] : .0f;
    
    for(; __dirty_offset_from < size; ++__dirty_offset_from)
    {
        uint16_t ch = mCaption[__dirty_offset_from];
        if(ch == ' ')
        {   x += mSpaceWidth * mViewportAspectCoef;   }
        else
        {
            x += mCharHeight * mFont->getGlyphAspectRatio(ch) * mViewportAspectCoef;
        }
        __x_offsets[__dirty_offset_from] = x;
    }
    
    __dirty_offset_from = DisplayString::npos;
    return;
}

float TextInputOverlayElement::__get_text_width()
{
    if(__x_offsets.empty()) return .0f;
    return __x_offsets.back();
}

float TextInputOverlayElement::__get_left()
{
    switch(mAlignment)
    {
    case Left: return _getDerivedLeft();
    case Center: return _getDerivedLeft() - __get_text_width() * 0.5f;
    case Right: return _getDerivedLeft() - __get_text_width();
    }
}

// --- *** ---


TextInputOverlayElement::~TextInputOverlayElement()
{
    killFocus(); return;
}


// Используется просто для цикла обновления
void TextInputOverlayElement::postRender(SceneManager *sm_, RenderSystem *rsys_)
{
    TextAreaOverlayElement::postRender(sm_, rsys_);
    if(__input_focus != this) return;
    
    __update_offsets();
    
    steady_clock::duration now = steady_clock::now().time_since_epoch();
    
    if(__is_cursor_dirty)
    {
        float w = mCharHeight * mViewportAspectCoef;
        float x = __get_left();
        if(__cursor > 0) x += __x_offsets[__cursor - 1];
        
        __input_cursor->setPosition(x - w * 0.5f, _getDerivedTop());
        __input_cursor->setDimensions(w, mCharHeight);
        __input_cursor->show();
        
        __blinking_timer = now;
        __is_cursor_dirty = false;
    }
    
    if(now - __blinking_timer > milliseconds{500})
    {
        __blinking_timer = now;
        __input_cursor->isVisible() ? __input_cursor->hide() : __input_cursor->show();
    }
    return;
}


bool TextInputOverlayElement::keyPressed(OIS::KeyEvent const &event_)
{
    switch(event_.key)
    {
    case OIS::KC_LEFT:
        if(__cursor > 0)
        {
            --__cursor; __is_cursor_dirty = true;
        }
        break;
    case OIS::KC_RIGHT:
        if(__cursor < __x_offsets.size())
        {
            ++__cursor; __is_cursor_dirty = true;
        }
        break;
    case OIS::KC_HOME:
        __cursor = 0; __is_cursor_dirty = true;
        break;
    case OIS::KC_END:
        __cursor = __x_offsets.size(); __is_cursor_dirty = true;
        break;
    case OIS::KC_DELETE:
        if(__cursor < __x_offsets.size())
        {
            mCaption.erase(__cursor, 1);
            TextAreaOverlayElement::setCaption(mCaption);
            __dirty_offset_from = __cursor;
            __is_cursor_dirty = isCaptionChanged = true;
        }
        break;
    case OIS::KC_BACK:
        if(__cursor > 0)
        {
            mCaption.erase(--__cursor, 1);
            TextAreaOverlayElement::setCaption(mCaption);
            __dirty_offset_from = __cursor;
            __is_cursor_dirty = isCaptionChanged = true;
        }
        break;
    default:
        if(event_.text != 0)
        {
            mCaption.insert(mCaption.begin() + __cursor, (uint16_t)event_.text);
            TextAreaOverlayElement::setCaption(mCaption);
            __dirty_offset_from = __cursor;
            ++__cursor;
            __is_cursor_dirty = isCaptionChanged = true;
        }
    }
    return true;
}


bool TextInputOverlayElement::keyReleased(OIS::KeyEvent const &event_)
{   return true;   }


bool TextInputOverlayElement::mouseMoved(OIS::MouseEvent const &event_)
{   return true;   }


bool TextInputOverlayElement::mousePressed(OIS::MouseEvent const &event_,
                                           OIS::MouseButtonID id_)
{
    if(id_ != OIS::MB_Left) return true;
    
    float x_click = (float)mouseWindowX() /
                    OverlayManager::getSingleton().getViewportWidth();
    x_click -= __get_left();
    float y_click = (float)mouseWindowY() /
                    OverlayManager::getSingleton().getViewportHeight();
    y_click -= _getDerivedTop();
    
    if(.0f <= x_click && x_click <= __get_text_width() &&
       .0f <= y_click && y_click <= mHeight)
    {
        __cursor = 0;
        
        float prev_offset = .0f;
        while(__cursor < __x_offsets.size())
        {
            if(x_click <= (prev_offset + __x_offsets[__cursor]) * 0.5f)
                break;
            prev_offset = __x_offsets[__cursor++];
        }
        __is_cursor_dirty = true;
    }
    return true;
}


bool TextInputOverlayElement::mouseReleased(OIS::MouseEvent const &event_,
                                            OIS::MouseButtonID id_)
{   return true;   }

// --- *** ---


void TextInputOverlayElement::setCaption(DisplayString const &text_)
{
    TextAreaOverlayElement::setCaption(text_);
    isCaptionChanged = true;
    _invalidateCursor();
    return;
}


String const &TextInputOverlayElement::getTypeName() const
{   return __TypeName;   }


void TextInputOverlayElement::_invalidateCursor()
{
    __dirty_offset_from = 0;
    __cursor = mCaption.size();
    __is_cursor_dirty = true;
    return;
}


void TextInputOverlayElement::setFocus(size_t cursor_)
{
    if(cursor_ != DisplayString::npos && cursor_ <= mCaption.size())
    {
        __cursor = cursor_; __is_cursor_dirty = true;
    }
    
    if(__input_cursor_layout == nullptr)
    {
        OverlayManager &overlays = OverlayManager::getSingleton();
        
        __input_cursor_layout = overlays.create("InputCursorLayout");
        
        __input_cursor = static_cast<PanelOverlayElement *>(
            overlays.createOverlayElement("Panel", "inputCursor")
        );
        __input_cursor->setMetricsMode(GMM_RELATIVE);
        __input_cursor->setMaterialName("Menu/menu");
        __cursor_tex->applyTo(__input_cursor);
        
        __input_cursor_layout->add2D(__input_cursor);
    }
    
    __input_cursor_layout->setZOrder(getZOrder() + 1);
    __input_cursor_layout->show();
    __input_cursor->hide();
    __is_cursor_dirty = true;
    
    OIS::Keyboard &keyboard = getKeyboard();
    keyboard.setBuffered(true);
    keyboard.setTextTranslation(OIS::Keyboard::Unicode);
    keyboard.setEventCallback(this);
    
    OIS::Mouse &mouse = getMouse();
    mouse.setBuffered(true);
    mouse.setEventCallback(this);
    
    __input_focus = this;
    return;
}


void TextInputOverlayElement::killFocus()
{
    if(__input_focus != this) return;
    
    if(__input_cursor_layout != nullptr)
    {
        OverlayManager &overlays = OverlayManager::getSingleton();
        
        overlays.destroyOverlayElement(__input_cursor); __input_cursor = nullptr;
        overlays.destroy(__input_cursor_layout); __input_cursor_layout = nullptr;
    }
    
    OIS::Keyboard &keyboard = getKeyboard();
    keyboard.setBuffered(false);
    keyboard.setEventCallback(nullptr);
    
    OIS::Mouse &mouse = getMouse();
    mouse.setBuffered(false);
    mouse.setEventCallback(nullptr);
    
    __blinking_timer = steady_clock::duration::zero();
    __input_focus = nullptr;
    return;
}

// --- *** ---


class TextInputOverlayElementFactory : public OverlayElementFactory
{
public:
    OverlayElement *createOverlayElement(String const &instance_name_) override
    {
        return OGRE_NEW TextInputOverlayElement(instance_name_);
    }
    
    String const &getTypeName() const override
    {   return __TypeName;   }
};


} // namespace LibUtils


void _registerTextInputOverlayElement()
{
    Ogre::OverlayManager::getSingleton().addOverlayElementFactory(
        OGRE_NEW LibUtils::TextInputOverlayElementFactory{}
    );
    return;
}

#ifndef _QDUNGEONS_BASE_FRONTEND_HPP
#define _QDUNGEONS_BASE_FRONTEND_HPP

#include "game_model/base_entity.hpp"
#include "libutils/looking_camera.hpp"
#include <OgreFrameListener.h>
#include <vector>


class LevelMap;
class PlayerEntity;


/* Базовая активность игрового процесса. Отображает все сущности на карте. */
class BaseFrontendActivity : public LibUtils::LookingCameraActivity,
                             public Ogre::FrameListener
{
private:
    struct __t_level_border
    {
        Ogre::Entity *entity;
        Ogre::SceneNode *node;
    };
    
    std::vector<__t_level_border> __level_border;
    float __frame_time /*sec*/;
    
    void __add_border(std::string const &mesh_,
                      float x_, float y_, Ogre::Real angle_);
    void __on_level_created(LevelMap *level_);
    void __on_player_appeared(PlayerEntity *player_);
    
public:
    BaseFrontendActivity(Ogre::Real camera_height_, Ogre::Real camera_distance_);
    ~BaseFrontendActivity();
    
    bool frameStarted(Ogre::FrameEvent const &event_) override;
    bool frameEnded(Ogre::FrameEvent const &event_) override;
    
    void onStart(BaseActivity *back_from_) override;
    void onLoop() override;
    void onStop() override;
    
    // ---
    
    /* Вызывается для обновления логики и передачи команд управления. */
    virtual void update(float frame_time_ /*sec*/) SYNC_ENDS {}
};


#endif // _QDUNGEONS_BASE_FRONTEND_HPP

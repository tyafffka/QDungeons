#include "base_frontend.hpp"
#include "game_model/game_model.hpp"
#include "libutils/values.hpp"
#include "utils/log.hpp"
#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>

#ifndef NDEBUG
# include <OgreOverlay.h>
# include <OgreOverlayManager.h>
# include <OgrePanelOverlayElement.h>
# include <OgreTextAreaOverlayElement.h>

# define FPS_COUNTER 1

static Ogre::Overlay *__fps_layout = nullptr;
static Ogre::OverlayContainer *__fps_panel = nullptr;
static Ogre::OverlayElement *__fps_text = nullptr;
static float __fps_accum = .0f;
#else
# define FPS_COUNTER 0
#endif


static LibUtils::GlobalMetadata<float> __fps_smoothing{"fps_smoothing_period"};
static LibUtils::GlobalMetadata<t_color>
    __mental{"mental"}, __background{"dim_background"};

// --- *** ---


void BaseFrontendActivity::__add_border(std::string const &mesh_,
                                        float x_, float y_, Ogre::Real angle_)
{
    Ogre::SceneNode *node = LibUtils::mainNode->createChildSceneNode();
    node->setPosition(Ogre::Vector3{x_, y_, .0f});
    if(angle_ != .0f) node->roll(Ogre::Radian{angle_});
    
    Ogre::Entity *entity = LibUtils::sceneManager->createEntity("models/" + mesh_);
    entity->setVisible(isActive());
    
    node->attachObject(entity);
    __level_border.push_back(__t_level_border{entity, node});
    return;
}


void BaseFrontendActivity::__on_level_created(LevelMap *level_)
{
    for(auto &border : __level_border)
    {
        LibUtils::sceneManager->destroyEntity(border.entity);
        LibUtils::sceneManager->destroySceneNode(border.node);
    }
    
    uint16_t size_x = level_->sizeX, size_y = level_->sizeY;
    
    __level_border.clear();
    __level_border.reserve((size_t)4 + (size_x + size_y) * 2);
    
    __add_border("placeholder_corner.mesh", size_x, size_y, .0f);
    __add_border("placeholder_corner.mesh", -1.0f, size_y, Ogre::Math::HALF_PI);
    __add_border("placeholder_corner.mesh", -1.0f, -1.0f, Ogre::Math::PI);
    __add_border("placeholder_corner.mesh", size_x, -1.0f, -Ogre::Math::HALF_PI);
    
    for(uint16_t x = 0; x < size_x; ++x)
    {
        __add_border("placeholder_side.mesh", x, size_y, Ogre::Math::HALF_PI);
        __add_border("placeholder_side.mesh", x, -1.0f, -Ogre::Math::HALF_PI);
    }
    for(uint16_t y = 0; y < size_y; ++y)
    {
        __add_border("placeholder_side.mesh", size_x, y, .0f);
        __add_border("placeholder_side.mesh", -1.0f, y, Ogre::Math::PI);
    }
    return;
}


void BaseFrontendActivity::__on_player_appeared(PlayerEntity *player_)
{
    connectCameraTo(player_->node); return;
}

// --- *** ---


BaseFrontendActivity::BaseFrontendActivity(Ogre::Real camera_height_,
                                           Ogre::Real camera_distance_) :
    LookingCameraActivity(camera_height_, camera_distance_),
    __level_border(), __frame_time(0.02f)
{
    Ogre::Root::getSingleton().addFrameListener(this);
    
    onLevelCreated.bind(this, &BaseFrontendActivity::__on_level_created);
    onPlayerAppeared.bind(this, &BaseFrontendActivity::__on_player_appeared);
    return;
}


BaseFrontendActivity::~BaseFrontendActivity()
{
    Ogre::Root::getSingleton().removeFrameListener(this);
    
    for(auto &border : __level_border)
    {
        LibUtils::sceneManager->destroyEntity(border.entity);
        LibUtils::sceneManager->destroySceneNode(border.node);
    }
    return;
}

// --- *** ---


static void __update_all_animations(Ogre::SceneNode *node_, float frame_time_)
{
    for(auto it = node_->getChildIterator(); it.hasMoreElements(); it.moveNext())
    {
        Ogre::SceneNode *child = dynamic_cast<Ogre::SceneNode *>(*it.current());
        if(child == nullptr) continue;
        
        __update_all_animations(child, frame_time_);
    }
    
    for(auto it = node_->getAttachedObjectIterator(); it.hasMoreElements(); it.moveNext())
    {
        Ogre::Entity *entity = dynamic_cast<Ogre::Entity *>(*it.current());
        if(entity == nullptr) continue;
        
        Ogre::AnimationStateSet *set = entity->getAllAnimationStates();
        if(set == nullptr) continue;
        
        for(auto anim_it = set->getEnabledAnimationStateIterator();
            anim_it.hasMoreElements(); anim_it.moveNext())
        {   (*anim_it.current())->addTime(frame_time_);   }
    }
    return;
}


bool BaseFrontendActivity::frameStarted(Ogre::FrameEvent const &event_)
{
    if(isActive())
        __update_all_animations(LibUtils::mainNode, (float)event_.timeSinceLastFrame);
    return true;
}


bool BaseFrontendActivity::frameEnded(Ogre::FrameEvent const &event_)
{
    __frame_time = (float)event_.timeSinceLastFrame;
    return true;
}

// --- *** ---


void BaseFrontendActivity::onStart(BaseActivity *back_from_)
{
    LookingCameraActivity::onStart(back_from_);
    if(isFinish()) return;
    
    if(! back_from_) // Creating frontend
    {
        Ogre::Root::getSingleton().setFrameSmoothingPeriod(*__fps_smoothing);
        
        LOG("INFO") << "Frontend activity started.";
        
#if FPS_COUNTER
        auto &overlays = Ogre::OverlayManager::getSingleton();
        __fps_layout = overlays.create("FPS");
        
        auto *panel = static_cast<Ogre::PanelOverlayElement *>(
            overlays.createOverlayElement("Panel", "fps_panel")
        );
        panel->setMetricsMode(Ogre::GMM_PIXELS);
        panel->setPosition(0, 0);
        panel->setDimensions(10, 10);
        
        auto *text = static_cast<Ogre::TextAreaOverlayElement *>(
            overlays.createOverlayElement("TextArea", "text")
        );
        text->setMetricsMode(Ogre::GMM_PIXELS);
        text->setPosition(10, 10);
        text->setDimensions(10, 10);
        text->setCharHeight(20);
        text->setAlignment(Ogre::TextAreaOverlayElement::Left);
        text->setFontName("Menu/text");
        text->getMaterial(); // КОСТЫЛЬ
        text->setColour(Ogre::ColourValue::White);
        text->show();
        
        panel->addChild(__fps_text = text);
        __fps_layout->add2D(__fps_panel = panel);
        __fps_layout->setZOrder(0);
        __fps_layout->show();
#endif
    }
    
    // Apply properties
    
    double view_distance = LibUtils::properties.get("view_distance");
    LibUtils::mainCamera->setNearClipDistance(0.01f);
    LibUtils::mainCamera->setFarClipDistance((Ogre::Real)view_distance);
    
    int64_t fog = LibUtils::properties.get("fog");
    switch((int)fog)
    {
    case 0:
        LibUtils::sceneManager->setFog(Ogre::FOG_NONE);
        break;
    case 1:
        LibUtils::sceneManager->setFog(Ogre::FOG_LINEAR, *__background,
                                       .0f, 0.25f * (Ogre::Real)view_distance,
                                       1.05f * (Ogre::Real)view_distance);
        break;
    case 2:
        LibUtils::sceneManager->setFog(Ogre::FOG_EXP, *__background,
                                       1.3f / (Ogre::Real)view_distance);
        break;
    case 3:
        LibUtils::sceneManager->setFog(Ogre::FOG_EXP2, *__background,
                                       1.5f / (Ogre::Real)view_distance);
    default:;
    }
    
    LibUtils::mainCamera->getViewport()->setBackgroundColour(*__background);
    LibUtils::sceneManager->setAmbientLight(*__mental);
    LibUtils::mainNode->setVisible((BaseEntity::isVisible = true));
    return;
}


void BaseFrontendActivity::onLoop()
{
    LookingCameraActivity::onLoop();
    if(isFinish()) return;
    
    {
        background_task_flag::lock sync_lock{backendFlag};
        // => SYNC_ENDS
        
        getGameModel().update(__frame_time);
        update(__frame_time);
    }
    
#if FPS_COUNTER
    if((__fps_accum += __frame_time) >= 0.2f /*sec*/)
    {
        __fps_accum = .0f;
        __fps_text->setCaption(std::to_string((int)(1.0f / __frame_time)));
    }
#endif
    
    cancelCooldown();
    return;
}


void BaseFrontendActivity::onStop()
{
    LookingCameraActivity::onStop();
    if(isFinish()) // Destroy frontend
    {
        LOG("INFO") << "Frontend activity closed.";
        
        for(auto &border : __level_border)
        {
            LibUtils::sceneManager->destroyEntity(border.entity);
            LibUtils::sceneManager->destroySceneNode(border.node);
        }
        __level_border.clear();
        
#if FPS_COUNTER
        auto &overlays = Ogre::OverlayManager::getSingleton();
        overlays.destroyOverlayElement(__fps_text);
        overlays.destroyOverlayElement(__fps_panel);
        overlays.destroy(__fps_layout);
#endif
    }
    
    LibUtils::sceneManager->setFog(Ogre::FOG_NONE);
    LibUtils::mainNode->setVisible((BaseEntity::isVisible = false));
    return;
}

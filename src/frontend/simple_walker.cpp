#include "simple_walker.hpp"
#include "game_model/game_model.hpp"
#include "libutils/values.hpp"
#include "libutils/input_actions.hpp"
#include "utils/log.hpp"
#include <OgreViewport.h>


static LibUtils::GlobalMetadata<float> camera_height{"camera_height"};
static LibUtils::GlobalMetadata<float> camera_distance{"camera_distance"};
static LibUtils::GlobalMetadata<float> camera_pitch_limit{"camera_pitch_limit"};

static LibUtils::InputAction
    input_pause     {"qdungeons:pause",        "main"},
    input_forward   {"qdungeons:move_forward", "main"},
    input_back      {"qdungeons:move_back",    "main"},
    input_left      {"qdungeons:move_left",    "main"},
    input_right     {"qdungeons:move_right",   "main"},
    input_look_up   {"qdungeons:look_up",      "main"},
    input_look_down {"qdungeons:look_down",    "main"},
    input_look_left {"qdungeons:look_left",    "main"},
    input_look_right{"qdungeons:look_right",   "main"};

static float __camera_move_speed;

// --- *** ---


SimpleWalkerFrontend::SimpleWalkerFrontend() :
    BaseFrontendActivity(*camera_height, *camera_distance),
    _model(nullptr), _current_direction(PlayerEntity::NO_DIRECTION)
{}


SimpleWalkerFrontend::~SimpleWalkerFrontend()
{
    if(_model != nullptr) delete _model;
    return;
}

// --- *** ---


void SimpleWalkerFrontend::onStart(BaseActivity *back_from_)
{
    BaseFrontendActivity::onStart(back_from_);
    if(isFinish()) return;
    
    if(! back_from_)
    {
        try
        {   _model = new GameModel(timeCounter(), "." /*FIXME:mock*/);   }
        catch(...)
        {
            LOG("ERROR") << "Game not created!";
            throw;
        }
    }
    
    // Apply properties
    
    __camera_move_speed = (float)(double)LibUtils::properties.get("camera_move_speed");
    return;
}


void SimpleWalkerFrontend::onStop()
{
    BaseFrontendActivity::onStop();
    if(isFinish())
    {
        delete _model; _model = nullptr;
    }
    return;
}


void SimpleWalkerFrontend::update(float frame_time_) SYNC_ENDS
{
    if(input_pause.isJustActivated())
    {
        LOG("INFO") << "Game becoming paused.";
        pause();
        return;
    }
    
    uint8_t direction = PlayerEntity::NO_DIRECTION;
    if(input_forward.isActivated()) direction |= PlayerEntity::FORWARD;
    if(input_back.isActivated())    direction |= PlayerEntity::BACK;
    if(input_left.isActivated())    direction |= PlayerEntity::LEFT;
    if(input_right.isActivated())   direction |= PlayerEntity::RIGHT;
    
    if(direction != _current_direction)
        _model->thePlayer->movePlayer((_current_direction = direction));
    
    if(input_look_up.isActivated() || input_look_down.isActivated())
    {
        float look_y = frame_time_ * __camera_move_speed *
                       input_look_up.fullValue(input_look_down);
        addCameraPitch(Ogre::Radian{look_y});
        
        if(getCameraPitch().valueRadians() > *camera_pitch_limit)
            setCameraPitch(Ogre::Radian{*camera_pitch_limit});
    }
    
    if(input_look_left.isActivated() || input_look_right.isActivated())
    {
        float look_x = frame_time_ * __camera_move_speed *
                       input_look_left.fullValue(input_look_right);
        addCameraRotation(Ogre::Radian{look_x});
        
        _model->thePlayer->rotatePlayer(b2Rot{getCameraRotation().valueRadians()});
    }
    return;
}

#include "joystick_debug.hpp"
#include "libutils/input_actions.hpp"
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreTextAreaOverlayElement.h>
#include <sstream>


JoystickDebugActivity::~JoystickDebugActivity()
{
    if(__layout != nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        overlays.destroyOverlayElement(__text);
        overlays.destroyOverlayElement(__background);
        overlays.destroy(__layout);
    }
    return;
}


void JoystickDebugActivity::onStart(BaseActivity *back_from_)
{
    BaseUIActivity::onStart(back_from_);
    if(isFinish()) return;
    
    auto &overlays = Ogre::OverlayManager::getSingleton();
    int viewport_width = overlays.getViewportWidth();
    int viewport_height = overlays.getViewportHeight();
    
    __layout = overlays.create("JoystickDebug");
    
    // ---
    
    auto *background = static_cast<Ogre::PanelOverlayElement *>(
        overlays.createOverlayElement("Panel", "background")
    );
    background->setMetricsMode(Ogre::GMM_PIXELS);
    background->setPosition(0, 0);
    background->setDimensions(viewport_width, viewport_height);
    background->setUV(0.0f, 0.0f, 1.0f, 1.0f);
    if(viewport_width * 2 >= viewport_height * 3)
        background->setMaterialName("Menu/background_wide");
    else
        background->setMaterialName("Menu/background");
    background->show();
    
    // ---
    
    auto *text = static_cast<Ogre::TextAreaOverlayElement *>(
        overlays.createOverlayElement("TextArea", "text")
    );
    text->setMetricsMode(Ogre::GMM_PIXELS);
    text->setPosition(10, 10);
    text->setDimensions(viewport_width, viewport_height);
    text->setCharHeight(20);
    text->setAlignment(Ogre::TextAreaOverlayElement::Left);
    text->setFontName("Menu/text");
    text->getMaterial(); // КОСТЫЛЬ
    text->setColour(Ogre::ColourValue::White);
    text->show();
    
    if(LibUtils::joysticksCount() <= 0) text->setCaption("No joysticks!");
    
    // ---
    
    background->addChild(__text = text);
    __layout->add2D(__background = background);
    __layout->setZOrder(0);
    __layout->show();
    return;
}


static LibUtils::StaticInputAction
    __back{INPUT_KEYBOARD_KEY(OIS::KC_ESCAPE), INPUT_NO_KEY};

void JoystickDebugActivity::onLoop()
{
    BaseUIActivity::onLoop();
    if(isFinish()) return;
    
    if(__back.isJustActivated()) {   finish(); return;   }
    if(LibUtils::joysticksCount() <= 0) return;
    
    OIS::JoyStickState const &state = LibUtils::getJoystick(0).getJoyStickState();
    BaseInputAction::t_slot_id slot = LibUtils::joystickSlotID(0);
    std::stringstream out;
    
    out << "Joystick vendor: " << LibUtils::getJoystick(0).vendor() << "\n";
    out << "Is silenced: " << (__back.isInputSilenced(slot) ? "yes" : "no") << "\n";
    
    for(int i = 0; i < 4; ++i)
    {
        out << "POV " << i << ":";
        if((state.mPOV[i].direction & OIS::Pov::North) != 0) out << " up";
        if((state.mPOV[i].direction & OIS::Pov::South) != 0) out << " down";
        if((state.mPOV[i].direction & OIS::Pov::East) != 0) out << " right";
        if((state.mPOV[i].direction & OIS::Pov::West) != 0) out << " left";
        if(state.mPOV[i].direction == OIS::Pov::Centered) out << " centered";
        out << "\n";
    }
    
    int axes_count = (int)state.mAxes.size();
    for(int i = 0; i < axes_count; ++i)
    {
        out << "Axis " << i << ": ("
            << __back.getAxisValue(slot, (BaseInputAction::t_key_num)i)
            << ") " << state.mAxes[i].abs << "\n";
    }
    
    int buttons_count = (int)state.mButtons.size();
    for(int i = 0; i < buttons_count; ++i)
    {
        out << "Button " << i << ": " << (state.mButtons[i] ? "pushed" : "released") << "\n";
    }
    
    __text->setCaption(out.str());
    return;
}


void JoystickDebugActivity::onStop()
{
    BaseActivity::onStop();
    
    if(__layout != nullptr)
    {
        auto &overlays = Ogre::OverlayManager::getSingleton();
        
        overlays.destroyOverlayElement(__text); __text = nullptr;
        overlays.destroyOverlayElement(__background); __background = nullptr;
        overlays.destroy(__layout); __layout = nullptr;
    }
    return;
}

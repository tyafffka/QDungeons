#ifndef _QDUNGEONS_MAIN_MENU_HPP
#define _QDUNGEONS_MAIN_MENU_HPP

#include "libutils/menu.hpp"


/* Главное меню:
 * - Начать игру
 * - Настройки
 * - Управление
 * - Выход */
class MainMenu : public LibUtils::BaseMenuActivity
{
protected:
    void menuBack() override {}
    
    void on_start_game();
    void on_properties();
    void on_input_properties();
#ifndef NDEBUG
    void on_joystick_debug();
#endif
    
public:
    void onStart(BaseActivity *back_from_) override;
};


#endif //- _QDUNGEONS_MAIN_MENU_HPP

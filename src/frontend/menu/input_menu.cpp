#include "input_menu.hpp"
#include "libutils/dialog.hpp"
#include "libutils/input_actions.hpp"
#include "libutils/TextManager.hpp"
#include <vector>

using namespace LibUtils;


lines_buffer_content getInputKeyDisplayName(BaseInputAction &action_,
                                            BaseInputAction::t_slot_id slot_id_)
{
    auto &text = TextManager::getSingleton();
    auto type = action_.getKeyType(slot_id_);
    auto key = action_.getKey(slot_id_);
    char axis_char;
    
    if(slot_id_ == KEYBOARD_MOUSE_SLOT_ID)
    {
        switch(type)
        {
        case BaseInputAction::C_KEY:
            if(key < KEYBOARD_KEYS_COUNT)
                return utf8_lines_buffer::format(
                    text.getString("input.keyboard." + std::to_string(key))
                );
            else if(key == MOUSE_KEYS_SHIFT)
                return utf8_lines_buffer::format(
                    text.getString("input.mouse.left_button")
                );
            else if(key == MOUSE_KEYS_SHIFT + 1)
                return utf8_lines_buffer::format(
                    text.getString("input.mouse.right_button")
                );
            else if(key == MOUSE_KEYS_SHIFT + 2)
                return utf8_lines_buffer::format(
                    text.getString("input.mouse.middle_button")
                );
            else
                return utf8_lines_buffer::format(
                    text.getString("input.mouse.button"), key - MOUSE_KEYS_SHIFT + 1
                );
            
        case BaseInputAction::C_POSITIVE_SEMIAXIS: axis_char = '+'; break;
        case BaseInputAction::C_NEGATIVE_SEMIAXIS: axis_char = '-'; break;
        default: return lines_buffer_content{};
        }
        
        switch(key)
        {
        case 0: return utf8_lines_buffer::format(
                text.getString("input.mouse.axis_x"), axis_char
            );
        case 1: return utf8_lines_buffer::format(
                text.getString("input.mouse.axis_y"), axis_char
            );
        case 2: return utf8_lines_buffer::format(
                text.getString("input.mouse.wheel"), axis_char
            );
        default: return lines_buffer_content{};
        }
    }
    else // Joystick slot
    {
        switch(type)
        {
        case BaseInputAction::C_KEY:
            if(key < DPAD_KEYS_COUNT)
            {
                int pov_i = (key & 0x0C) >> 2;
                switch(key & 0x03)
                {
                case 0: return utf8_lines_buffer::format(
                        text.getString("input.joystick.dpad_up"), pov_i + 1
                    );
                case 1: return utf8_lines_buffer::format(
                        text.getString("input.joystick.dpad_right"), pov_i + 1
                    );
                case 2: return utf8_lines_buffer::format(
                        text.getString("input.joystick.dpad_down"), pov_i + 1
                    );
                case 3: return utf8_lines_buffer::format(
                        text.getString("input.joystick.dpad_left"), pov_i + 1
                    );
                default: return lines_buffer_content{};
                }
            }
            else
            {
                return utf8_lines_buffer::format(
                    text.getString("input.joystick.button"), key - JOYSTICK_KEYS_SHIFT + 1
                );
            }
        case BaseInputAction::C_POSITIVE_SEMIAXIS: axis_char = '+'; break;
        case BaseInputAction::C_NEGATIVE_SEMIAXIS: axis_char = '-'; break;
        default: return lines_buffer_content{};
        }
        
        switch(key)
        {
        case 0: return utf8_lines_buffer::format(
                text.getString("input.joystick.axis_x"), axis_char
            );
        case 1: return utf8_lines_buffer::format(
                text.getString("input.joystick.axis_y"), axis_char
            );
        default: return utf8_lines_buffer::format(
                text.getString("input.joystick.axis"), axis_char, key + 1
            );
        }
    }
}

// === *** ===


struct __ControllerButton : MenuButton
{
    int controller;
    
    __ControllerButton(std::string const &text_, int controller_) :
        MenuButton(text_), controller(controller_)
    {}
};

// --- *** ---


void InputMenu::on_controller(BaseMenuElement *elem_)
{
    start<ControllerMenu>(static_cast<__ControllerButton *>(elem_)->controller);
    return;
}


void InputMenu::on_save()
{
    properties.putFloatNumber(_camera->getValue(), "camera_move_speed");
    saveProperties();
    
    BaseMenuActivity::menuBack();
    return;
}


void InputMenu::onStart(BaseActivity *back_from_)
{
    BaseMenuActivity::onStart(back_from_);
    if(isFinish()) return;
    
    auto &text = TextManager::getSingleton();
    
    setTitle(text.getString("input_menu.title"));
    
    {
        double camera_move_speed = properties.get("camera_move_speed");
        _camera = addSlider(text.getString("input_menu.camera_move_speed"),
                            1.0f, 1.0f, 20.0f);
        _camera->setValue((float)camera_move_speed);
    }
    
    // ---
    
    addElement(new __ControllerButton{text.getString("input_menu.keyboard_mouse"), -1})
        ->onSelected.bind(this, &InputMenu::on_controller);
    
    int count = joysticksCount();
    for(int i = 0; i < count; ++i)
    {
        addElement(new __ControllerButton{getJoystick(i).vendor(), i})
            ->onSelected.bind(this, &InputMenu::on_controller);
    }
    
    addButton(text.getString("menu.save"))
        ->onSelected.bind(this, &InputMenu::on_save);
    addButton(text.getString("menu.back"))
        ->onSelected.bind(this, &InputMenu::menuBack);
    return;
}

// === *** ===


struct __ActionVariable : MenuVariable
{
    InputAction *action;
    
    __ActionVariable(std::string const &text_, InputAction *action_) :
        MenuVariable(text_), action(action_)
    {}
};


struct ControllerMenu::__t_category
{
    struct t_action
    {
        InputAction *action;
        __ActionVariable *control;
    };
    
    std::string name;
    std::vector<t_action> actions{};
    
    explicit __t_category(std::string const &name_) : name(name_) {}
};

static std::vector<ControllerMenu::__t_category> __categories;


class ControllerMenu::_KeyDialog : public BaseDialog
{
protected:
    void update() override
    {
        if(! is_ready)
        {
            is_ready = action->isInputSilenced(slot_id);
        }
        else if(action->catchKey(slot_id))
        {
            onKeyAssigned(); hide();
        }
        return;
    }
    
public:
    InputAction *action;
    BaseInputAction::t_slot_id slot_id;
    bool is_ready;
    
    _KeyDialog(std::string const &msg_, BaseInputAction::t_slot_id slot_id_) :
        BaseDialog(msg_), slot_id(slot_id_)
    {}
    
    event<void()> onKeyAssigned;
};

// --- *** ---


void ControllerMenu::_update_category(__t_category &cat_)
{
    InputConflictsContext conflicts{};
    
    for(__t_category::t_action &act : cat_.actions)
        conflicts.addAction(act.action, {_slot_id});
    
    for(__t_category::t_action &act : cat_.actions)
    {
        lines_buffer_content value = getInputKeyDisplayName(*(act.action), _slot_id);
        if(conflicts.isConflict(act.action, _slot_id))
            value = utf8_lines_buffer::format("&9&_0", std::move(value));
        
        act.control->setValue(std::move(value));
    }
    return;
}


void ControllerMenu::on_start_assign(BaseMenuElement *elem_)
{
    auto *var = static_cast<__ActionVariable *>(elem_);
    auto &text = TextManager::getSingleton();
    
    _key_dialog->setQuestion(text.getString("input_menu.key_assign_current"),
                             getInputKeyDisplayName(*(var->action), _slot_id));
    _key_dialog->action = var->action; _key_dialog->is_ready = false;
    _key_dialog->show();
    return;
}


void ControllerMenu::on_key_assigned()
{
    std::string const &cat_name = _key_dialog->action->getCategory();
    
    auto cat_it = __categories.begin();
    auto cat_itend = __categories.end();
    for(; cat_it != cat_itend; ++cat_it)
    {
        if(cat_it->name == cat_name) break;
    }
    
    if(cat_it != cat_itend) _update_category(*cat_it);
    return;
}


void ControllerMenu::on_save()
{
    dumpInputKeys(); saveProperties(); BaseMenuActivity::menuBack();
    return;
}


void ControllerMenu::menuBack()
{
    loadInputKeys(); BaseMenuActivity::menuBack();
    return;
}


ControllerMenu::ControllerMenu(int controller_) :
    _slot_id((controller_ < 0)? KEYBOARD_MOUSE_SLOT_ID : joystickSlotID(controller_))
{
    auto &text = TextManager::getSingleton();
    
    _controller_name = (controller_ < 0)? text.getString("input_menu.keyboard_mouse") :
                                          getJoystick(controller_).vendor();
    
    _key_dialog = new _KeyDialog(text.getString("input_menu.key_assign_dialog"), _slot_id);
    _key_dialog->onKeyAssigned.bind(this, &ControllerMenu::on_key_assigned);
    
    if(! __categories.empty()) return;
    
    auto it = InputAction::begin();
    auto itend = InputAction::end();
    for(; it != itend; ++it)
    {
        auto cat_it = __categories.begin();
        auto cat_itend = __categories.end();
        for(; cat_it != cat_itend; ++cat_it)
        {
            if(cat_it->name == it->getCategory()) break;
        }
        
        if(cat_it == cat_itend)
        {
            cat_it = __categories.insert(
                (it->getCategory() == "main")? __categories.begin() : cat_itend,
                __t_category{it->getCategory()}
            );
        }
        
        cat_it->actions.push_back({&(*it), nullptr});
    }
    return;
}


ControllerMenu::~ControllerMenu()
{
    delete _key_dialog; return;
}


void ControllerMenu::onStart(BaseActivity *back_from_)
{
    BaseMenuActivity::onStart(back_from_);
    if(isFinish()) return;
    
    auto &text = TextManager::getSingleton();
    
    setTitle(_controller_name);
    setSubtitle(text.getString("input_menu.controller_subtitle"));
    
    for(__t_category &cat : __categories)
    {
        addElement(new BaseMenuElement(text.getString("input.slot_category." + cat.name)))
            ->setEnabled(false);
        
        for(__t_category::t_action &act : cat.actions)
        {
            addElement((act.control = new __ActionVariable(
                " " + text.getString("input.slot." + act.action->getName()), act.action
            )))->onSelected.bind(this, &ControllerMenu::on_start_assign);
        }
        
        _update_category(cat);
    }
    
    addButton(text.getString("menu.save"))
        ->onSelected.bind(this, &ControllerMenu::on_save);
    addButton(text.getString("menu.back"))
        ->onSelected.bind(this, &ControllerMenu::menuBack);
    return;
}

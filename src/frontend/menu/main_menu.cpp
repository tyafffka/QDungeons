#include "main_menu.hpp"
#include "properties_menu.hpp"
#include "input_menu.hpp"
#include "../simple_walker.hpp"
#include "libutils/TextManager.hpp"


void MainMenu::on_start_game()
{
    start<SimpleWalkerFrontend>(); return;
}


void MainMenu::on_properties()
{
    start<PropertiesMenu>(); return;
}


void MainMenu::on_input_properties()
{
    start<InputMenu>(); return;
}


#ifndef NDEBUG
# include "joystick_debug.hpp"

void MainMenu::on_joystick_debug()
{
    start<JoystickDebugActivity>(); return;
}
#endif


void MainMenu::onStart(BaseActivity *back_from_)
{
    BaseMenuActivity::onStart(back_from_);
    if(isFinish()) return;
    
    auto &text = LibUtils::TextManager::getSingleton();
    
    setTitle(text.getString("main_menu.title"));
    
    addButton(text.getString("main_menu.start_game"))
        ->onSelected.bind(this, &MainMenu::on_start_game);
    addButton(text.getString("main_menu.properties"))
        ->onSelected.bind(this, &MainMenu::on_properties);
    addButton(text.getString("main_menu.input"))
        ->onSelected.bind(this, &MainMenu::on_input_properties);
#ifndef NDEBUG
    addButton("Joystick debug")
        ->onSelected.bind(this, &MainMenu::on_joystick_debug);
#endif
    addButton(text.getString("main_menu.exit"))
        ->onSelected.bind(this, &MainMenu::finish);
    return;
}

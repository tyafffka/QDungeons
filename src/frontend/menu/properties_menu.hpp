#ifndef _QDUNGEONS_PROPERTIES_MENU_HPP
#define _QDUNGEONS_PROPERTIES_MENU_HPP

#include "libutils/menu.hpp"


/* Настройки:
 * - Размер экрана
 * - Полный экран
 * - Вертикальная синхронизация
 * - Полноэкранное сглаживание
 * - Фильтрация текстур
 * - Дальность прорисовки
 * - Туман
 * - Громкость
 * - Сохранить
 * - Назад */
class PropertiesMenu : public LibUtils::BaseMenuActivity
{
protected:
    LibUtils::MenuSwitcher *_screen_size = nullptr;
    LibUtils::MenuSwitcher *_full_screen = nullptr;
    LibUtils::MenuSwitcher *_vsync = nullptr;
    LibUtils::MenuSwitcher *_antialiasing = nullptr;
    LibUtils::MenuSwitcher *_textures = nullptr;
    LibUtils::MenuSlider *_view_distance = nullptr;
    LibUtils::MenuSwitcher *_fog = nullptr;
    LibUtils::MenuSlider *_sound_volume = nullptr;
    
    void on_volume_changed();
    void on_save();
    
    void menuBack() override;
    
public:
    void onStart(BaseActivity *back_from_) override;
};


#endif // _QDUNGEONS_PROPERTIES_MENU_HPP

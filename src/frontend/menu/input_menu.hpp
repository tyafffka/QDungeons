#ifndef _QDUNGEONS_INPUT_MENU_HPP
#define _QDUNGEONS_INPUT_MENU_HPP

#include "libutils/menu.hpp"
#include "utils/input_action.hpp"
#include "utils/lines_buffer.hpp"
#include <string>


/* Возвращает локализованное название клавиши или полуоси
 * из указанного слота указанного действия. */
lines_buffer_content getInputKeyDisplayName(BaseInputAction &action_,
                                            BaseInputAction::t_slot_id slot_id_);

/* Управление:
 * - Скорость камеры
 * - Клавиатура & Мышь
 * - ... (джойстики)
 * - Сохранить
 * - Назад */
class InputMenu : public LibUtils::BaseMenuActivity
{
protected:
    LibUtils::MenuSlider *_camera = nullptr;
    
    void on_controller(LibUtils::BaseMenuElement *elem_);
    void on_save();
    
public:
    void onStart(BaseActivity *back_from_) override;
};


/* Схема управления определённого контроллера.
 * Принимает в качестве параметра номер джойстика или `-1` для клавомыши.
 * Конфликты клавиш внутри одной категории действий выделяются красным. */
class ControllerMenu : public LibUtils::BaseMenuActivity
{
public:
    struct __t_category;
    
protected:
    class _KeyDialog;
    
    BaseInputAction::t_slot_id _slot_id;
    std::string _controller_name;
    _KeyDialog *_key_dialog;
    
    void _update_category(__t_category &cat_);
    
    void on_start_assign(LibUtils::BaseMenuElement *elem_);
    void on_key_assigned();
    void on_save();
    
    void menuBack() override;
    
public:
    explicit ControllerMenu(int controller_);
    ~ControllerMenu();
    
    void onStart(BaseActivity *back_from_) override;
};


#endif //- _QDUNGEONS_INPUT_MENU_HPP

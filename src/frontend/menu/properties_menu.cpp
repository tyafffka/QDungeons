#include "properties_menu.hpp"
#include "libutils/TextManager.hpp"
#include <OgreRoot.h>
#include <OgreOggSoundManager.h>


void PropertiesMenu::on_volume_changed()
{
    OgreOggSound::OgreOggSoundManager::getSingleton()
        .setMasterVolume(_sound_volume->getValue());
    return;
}


void PropertiesMenu::on_save()
{
    Ogre::ConfigOptionMap &render_options =
        Ogre::Root::getSingleton().getRenderSystem()->getConfigOptions();
    
    render_options.at("Video Mode").currentValue = _screen_size->getCurrentValue();
    render_options.at("Full Screen").currentValue =
        (_full_screen->getCurrentItem() == 1)? "Yes" : "No";
    render_options.at("VSync").currentValue =
        (_vsync->getCurrentItem() == 1)? "Yes" : "No";
    render_options.at("FSAA").currentValue = _antialiasing->getCurrentValue();
    
    LibUtils::properties.putNumber(_textures->getCurrentItem(), "textures");
    LibUtils::properties.putFloatNumber(_view_distance->getValue(), "view_distance");
    LibUtils::properties.putNumber(_fog->getCurrentItem(), "fog");
    LibUtils::properties.putFloatNumber(_sound_volume->getValue(), "sound_volume");
    
    // ---
    
    Ogre::Root::getSingleton().saveConfig();
    LibUtils::saveProperties();
    
    BaseMenuActivity::menuBack();
    return;
}


void PropertiesMenu::menuBack()
{
    double sound_volume = LibUtils::properties.get("sound_volume");
    OgreOggSound::OgreOggSoundManager::getSingleton()
        .setMasterVolume((float)sound_volume);
    
    BaseMenuActivity::menuBack();
    return;
}


void PropertiesMenu::onStart(BaseActivity *back_from_)
{
    BaseMenuActivity::onStart(back_from_);
    if(isFinish()) return;
    
    auto &text = LibUtils::TextManager::getSingleton();
    
    setTitle(text.getString("properties_menu.title"));
    setSubtitle(text.getString("properties_menu.subtitle"));
    
    // ---
    
    Ogre::ConfigOptionMap const &render_options =
        Ogre::Root::getSingleton().getRenderSystem()->getConfigOptions();
    {
        Ogre::ConfigOption const &screen_size = render_options.at("Video Mode");
        std::vector<std::string> v = screen_size.possibleValues;
        ptrdiff_t i = std::find(v.begin(), v.end(), screen_size.currentValue) - v.begin();
        
        _screen_size = addSwitcher(text.getString("properties_menu.screen_size"),
                                   std::move(v));
        _screen_size->setCurrentItem((int)i);
    }
    {
        _full_screen = addSwitcher(text.getString("properties_menu.full_screen"),
                                   {text.getString("menu.no"), text.getString("menu.yes")});
        _full_screen->setCurrentItem(
            (render_options.at("Full Screen").currentValue == "Yes")? 1 : 0
        );
    }
    {
        _vsync = addSwitcher(text.getString("properties_menu.vsync"),
                             {text.getString("menu.no"), text.getString("menu.yes")});
        _vsync->setCurrentItem(
            (render_options.at("VSync").currentValue == "Yes")? 1 : 0
        );
    }
    {
        Ogre::ConfigOption const &antialiasing = render_options.at("FSAA");
        std::vector<std::string> v = antialiasing.possibleValues;
        ptrdiff_t i = std::find(v.begin(), v.end(), antialiasing.currentValue) - v.begin();
        
        _antialiasing = addSwitcher(text.getString("properties_menu.antialiasing"),
                                    std::move(v));
        _antialiasing->setCurrentItem((int)i);
    }
    {
        int64_t textures = LibUtils::properties.get("textures");
        _textures = addSwitcher(text.getString("properties_menu.textures"),
                                {text.getString("menu.none"),
                                 text.getString("properties_menu.bilinear"),
                                 text.getString("properties_menu.trilinear"),
                                 text.getString("properties_menu.anisotropic2"),
                                 text.getString("properties_menu.anisotropic4"),
                                 text.getString("properties_menu.anisotropic8"),
                                 text.getString("properties_menu.anisotropic16")});
        _textures->setCurrentItem((int)textures);
    }
    {
        double view_distance = LibUtils::properties.get("view_distance");
        _view_distance = addSlider(text.getString("properties_menu.view_distance"),
                                   10.0f, 10.0f, 500.0f);
        _view_distance->setValue((float)view_distance);
    }
    {
        int64_t fog = LibUtils::properties.get("fog");
        _fog = addSwitcher(text.getString("properties_menu.fog"),
                           {text.getString("menu.none"), text.getString("menu.bad"),
                            text.getString("menu.middle"), text.getString("menu.nice")});
        _fog->setCurrentItem((int)fog);
    }
    {
        double sound_volume = LibUtils::properties.get("sound_volume");
        _sound_volume = addSlider(text.getString("properties_menu.sound_volume"));
        _sound_volume->setValue((float)sound_volume);
        _sound_volume->onValueChanged.bind(this, &PropertiesMenu::on_volume_changed);
    }
    
    // ---
    
    addButton(text.getString("menu.save"))
        ->onSelected.bind(this, &PropertiesMenu::on_save);
    addButton(text.getString("menu.back"))
        ->onSelected.bind(this, &PropertiesMenu::menuBack);
    return;
}

#ifndef _QDUNGEONS_JOYSTICK_DEBUG_HPP
#define _QDUNGEONS_JOYSTICK_DEBUG_HPP

#include "libutils/application.hpp"
#include <OgreOverlay.h>


/* Берёт первый подключенный джойстик, если есть,
 * и выводит состояние всех кнопок и осей в реальном времени.
 * Возврат назад по клавише `ESC`. */
class JoystickDebugActivity : public LibUtils::BaseUIActivity
{
private:
    Ogre::Overlay *__layout = nullptr;
    Ogre::OverlayContainer *__background = nullptr;
    Ogre::OverlayElement *__text = nullptr;
    
public:
    ~JoystickDebugActivity();
    
    void onStart(BaseActivity *back_from_) override;
    void onLoop() override;
    void onStop() override;
};


#endif // _QDUNGEONS_JOYSTICK_DEBUG_HPP

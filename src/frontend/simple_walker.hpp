#ifndef _QDUNGEONS_SIMPLE_WALKER_HPP
#define _QDUNGEONS_SIMPLE_WALKER_HPP

#include "base_frontend.hpp"


class GameModel;


/* Простая бродилка по карте. Управление движением игрока. */
class SimpleWalkerFrontend : public BaseFrontendActivity
{
protected:
    GameModel *_model;
    uint8_t _current_direction;
    
    /* Реакция на нажатие `qdungeons:pause`. */
    virtual void pause() {   finish();   }
    
public:
    SimpleWalkerFrontend();
    ~SimpleWalkerFrontend();
    
    void onStart(BaseActivity *back_from_) override;
    void onStop() override;
    
    void update(float frame_time_ /*sec*/) SYNC_ENDS override;
};


#endif // _QDUNGEONS_SIMPLE_WALKER_HPP

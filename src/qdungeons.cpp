#include "qdungeons.hpp"
#include "libutils/TextManager.hpp"
#include "libutils/MetadataManager.hpp"
#include "utils/log.hpp"
#include <OgreTextureManager.h>
#include <OgreMaterialManager.h>


bool QDungeonsApplication::initApplication(std::string const &saves_dir_)
{
    LibUtils::properties.update(minson::parse("{"
        "textures (1)"
        "view_distance (150.0)"
        "fog (1)"
        "sound_volume (1.0)"
        "camera_move_speed (5.0)"
        "input {"
            "KeyboardMouse {"
                "qdungeons:pause \"key1\""
                "qdungeons:move_forward \"key17\""
                "qdungeons:move_back \"key31\""
                "qdungeons:move_left \"key30\""
                "qdungeons:move_right \"key32\""
                "qdungeons:look_up \"-axis1\""
                "qdungeons:look_down \"+axis1\""
                "qdungeons:look_left \"-axis0\""
                "qdungeons:look_right \"+axis0\""
                "qdungeons:attack \"mouse0\""
                "qdungeons:inventory \"key15\""
            "}"
            "DefaultJoystick {"
                "qdungeons:pause \"key1\""
                "qdungeons:move_forward \"-axis1\""
                "qdungeons:move_back \"+axis1\""
                "qdungeons:move_left \"-axis0\""
                "qdungeons:move_right \"+axis0\""
                "qdungeons:look_up \"-axis4\""
                "qdungeons:look_down \"+axis4\""
                "qdungeons:look_left \"-axis3\""
                "qdungeons:look_right \"+axis3\""
                "qdungeons:attack \"+axis5\""
                "qdungeons:inventory \"dpad3\""
            "}"
        "}"
    "}"));
    return BaseApplication::initApplication(saves_dir_);
}


bool QDungeonsApplication::initUI(bool show_config_dialog_, std::string const &window_title_)
{
    if(! BaseApplication::initUI(show_config_dialog_, window_title_))
        return false;
    
    auto &mtrl = Ogre::MaterialManager::getSingleton();
    auto &txtr = Ogre::TextureManager::getSingleton();
    
    mtrl.setDefaultTextureFiltering(Ogre::TFO_ANISOTROPIC);
    mtrl.setDefaultAnisotropy(1);
    txtr.setDefaultNumMipmaps(5);
    switch((int)(int64_t)LibUtils::properties.get("textures"))
    {
    case 0:
        mtrl.setDefaultTextureFiltering(Ogre::TFO_NONE);
        txtr.setDefaultNumMipmaps(3);
        break;
    case 1:
        mtrl.setDefaultTextureFiltering(Ogre::TFO_BILINEAR);
        break;
    case 2:
        mtrl.setDefaultTextureFiltering(Ogre::TFO_TRILINEAR);
        break;
    case 3:
        mtrl.setDefaultAnisotropy(2);
        break;
    case 4:
        mtrl.setDefaultAnisotropy(4);
        break;
    case 5:
        mtrl.setDefaultAnisotropy(8);
        break;
    case 6:
        mtrl.setDefaultAnisotropy(16);
        break;
    default:;
    }
    
    LOG("INFO") << "Video properties applied.";
    return true;
}


bool QDungeonsApplication::loadLanguage()
{
    auto &text = LibUtils::TextManager::getSingleton();
    
    try {   text.loadLanguage("text/lang.minson");   }
    catch(...)
    {
        LOG("ERROR") << "QDungeons language file not loaded!";
        return false;
    }
    
    LOG("INFO") << "QDungeons language file loaded.";
    return true;
}


bool QDungeonsApplication::loadMetadata()
{
    auto &meta = LibUtils::MetadataManager::getSingleton();
    
    try {   meta.loadGlobals("metadata/libutils-global.minson");   }
    catch(...)
    {
        LOG("ERROR") << "LibUtils global metadata not loaded!";
        return false;
    }
    
    try {   meta.loadGlobals("metadata/global.minson");   }
    catch(...)
    {
        LOG("ERROR") << "QDungeons global metadata not loaded!";
        return false;
    }
    
    try {   meta.loadTypes("metadata/blocks.minson");   }
    catch(...)
    {
        LOG("ERROR") << "QDungeons blocks metadata not loaded!";
        return false;
    }
    
    try {   meta.loadTypes("metadata/entities.minson");   }
    catch(...)
    {
        LOG("ERROR") << "QDungeons entities metadata not loaded!";
        return false;
    }
    
    LOG("INFO") << "QDungeons metadata loaded.";
    return true;
}

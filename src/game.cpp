#include "qdungeons.hpp"
#include "frontend/menu/main_menu.hpp"
#include "utils/args-to-minson.h"
#ifdef NDEBUG
# include <iostream>
#endif


class Application : public QDungeonsApplication
{
public:
    bool run(std::string const &saves_dir_, bool config_dialog_,
             std::vector<std::string> const &resources_)
    {
        if(! initApplication(saves_dir_)) return false;
        if(! initUI(config_dialog_, "QDungeons v" QDungeons_VERSION)) return false;
        if(! initResources(resources_)) return false;
        if(! loadLanguage()) return false;
        if(! loadMetadata()) return false;
        
        runActivity<MainMenu>();
        return true;
    }
};


int main(int argc, char *const *argv)
{
#ifdef NDEBUG
    try
    {
#endif
    minson::array arguments = minson::parse("{"
        "saves-dir \"\""
        "config-dialog (n)"
        "resources {}"
    "}");
    args_to_minson(arguments, minson::parse("{"
        "S \"saves-dir\""
        "R \"resources\""
    "}"), argc, argv);
    
    std::string saves_dir = arguments.get("saves-dir");
    bool config_dialog = arguments.get("config-dialog");
    minson::array arg_resources = arguments.get("resources");
    
    std::vector<std::string> resources;
    resources.reserve(arg_resources.count());
    for(std::string res : arg_resources) resources.push_back(std::move(res));
    
    // ---
    
    Application app;
    return app.run(saves_dir, config_dialog, resources) ? 0 : 1;
#ifdef NDEBUG
    }
    catch(std::exception const &e)
    {
        std::clog << "An unknown exception occured: " << e.what()
                  << "! Exit." << std::endl;
        return 2;
    }
    catch(...)
    {
        std::clog << "An unknown exception occured! Exit." << std::endl;
        return 2;
    }
#endif
}

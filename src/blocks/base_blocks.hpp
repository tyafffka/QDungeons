#ifndef _QDUNGEONS_BASE_BLOCKS_HPP
#define _QDUNGEONS_BASE_BLOCKS_HPP

#include "game_model/base_entity.hpp"


/* Блок -- занимает собой одну {Q}вадратную ячейку на карте ландшафта.
 * Ширина блока -- 1 метр, высота -- 2 метра, целочисленные координаты
 * находятся внизу посередине блока. */
class BaseBlock : public BaseEntity
{
    metatype_data
    {
        std::string body;
        metatype_parameters(body);
    };
    
public:
    void attach(b2World &world_, float x_, float y_) override;
};


#endif // _QDUNGEONS_BASE_BLOCKS_HPP

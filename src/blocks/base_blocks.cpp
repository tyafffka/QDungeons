#include "base_blocks.hpp"
#include <Box2D/Dynamics/b2World.h>
#include <Box2D/Collision/Shapes/b2PolygonShape.h>

METATYPE_CLASS(BaseBlock);


void BaseBlock::attach(b2World &world_, float x_, float y_)
{
    if(metatype.body == "full_block")
    {
        b2BodyDef block_def;
        block_def.type = b2_staticBody;
        block_def.position.Set(x_, y_);
        block_def.fixedRotation = true;
        
        b2PolygonShape block_shape;
        block_shape.SetAsBox(0.5f, 0.5f);
        
        entityBody = world_.CreateBody(&block_def);
        entityBody->CreateFixture(&block_shape, .0f);
    }
    
    BaseEntity::attach(world_, x_, y_);
    return;
}

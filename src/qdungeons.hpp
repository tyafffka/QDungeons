#ifndef _QDUNGEONS_APPLICATION_HPP
#define _QDUNGEONS_APPLICATION_HPP

#include "libutils/application.hpp"


/* Класс приложения QDungeons с настройками
 * по умолчанию и дополнительными методами. */
class QDungeonsApplication : public LibUtils::BaseApplication
{
protected:
    bool initApplication(std::string const &saves_dir_) override;
    bool initUI(bool show_config_dialog_, std::string const &window_title_) override;
    
    /* Загружает языковые файлы. */
    virtual bool loadLanguage();
    /* Загружает метаданные и создаёт метатипы. */
    virtual bool loadMetadata();
};


#endif // _QDUNGEONS_APPLICATION_HPP
